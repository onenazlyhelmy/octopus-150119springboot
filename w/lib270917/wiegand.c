/*
 * Wiegand API Raspberry Pi
 * By Kyle Mallory
 * 12/01/2013
 * Based on previous code by Daniel Smith (www.pagemac.com) and Ben Kent (www.pidoorman.com)
 * Depends on the wiringPi library by Gordon Henterson: https://projects.drogon.net/raspberry-pi/wiringpi/
 *
 * The Wiegand interface has two data lines, DATA0 and DATA1.  These lines are normall held
 * high at 5V.  When a 0 is sent, DATA0 drops to 0V for a few us.  When a 1 is sent, DATA1 drops
 * to 0V for a few us. There are a few ms between the pulses.
 *   *************
 *   * IMPORTANT *
 *   *************
 *   The Raspberry Pi GPIO pins are 3.3V, NOT 5V. Please take appropriate precautions to bring the
 *   5V Data 0 and Data 1 voltges down. I used a 330 ohm resistor and 3V3 Zenner diode for each
 *   connection. FAILURE TO DO THIS WILL PROBABLY BLOW UP THE RASPBERRY PI!
 * sudo cc -o wiegandirish wiegandirish.c  -lwiringPi -lpthread -lrt
 */
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <time.h>
#include <unistd.h>
#include <memory.h>
#include "wiegand.h"
 #include "wiethread.h"


#define D0_PIN 0
#define D1_PIN 2

#define WIEGANDMAXDATA 100
#define WIEGANDTIMEOUT 3000000

static unsigned char __wiegandData[WIEGANDMAXDATA];    // can capture upto 32 bytes of data -- FIXME: Make this dynamically allocated in init?
static unsigned long __wiegandBitCount;            // number of bits currently captured

static struct timespec __wiegandBitTime;        // timestamp of the last bit received (used for timeouts)
static char CARDbuf_hex[16];

void data0Pulse(void) {
    if (__wiegandBitCount / 8 < WIEGANDMAXDATA) {
        __wiegandData[__wiegandBitCount / 8] <<= 1;
        __wiegandBitCount++;
    }
    clock_gettime(CLOCK_MONOTONIC, &__wiegandBitTime);
}

void data1Pulse(void) {
    if (__wiegandBitCount / 8 < WIEGANDMAXDATA) {
        __wiegandData[__wiegandBitCount / 8] <<= 1;
        __wiegandData[__wiegandBitCount / 8] |= 1;
        __wiegandBitCount++;
    }
    clock_gettime(CLOCK_MONOTONIC, &__wiegandBitTime);
}

int wiegandsetINIT(int d0pin, int d1pin) {
    // Setup wiringPi
   
    wiringPiSetup() ;
    pinMode(d0pin, INPUT);
    pinMode(d1pin, INPUT);

    wiringPiISR(d0pin, INT_EDGE_FALLING, data0Pulse);
    wiringPiISR(d1pin, INT_EDGE_FALLING, data1Pulse);
}

void wiegandReset() {
    memset((void *)__wiegandData, 0, WIEGANDMAXDATA);
    __wiegandBitCount = 0;
}

int wiegandGetPendingBitCount() {
    struct timespec now, delta;
    clock_gettime(CLOCK_MONOTONIC, &now);
    delta.tv_sec = now.tv_sec - __wiegandBitTime.tv_sec;
    delta.tv_nsec = now.tv_nsec - __wiegandBitTime.tv_nsec;

    if ((delta.tv_sec > 1) || (delta.tv_nsec > WIEGANDTIMEOUT))
        return __wiegandBitCount;

    return 0;
}


int wiegandReadData(void* data, int dataMaxLen) {
    if (wiegandGetPendingBitCount() > 0) {
        int bitCount = __wiegandBitCount;
        int byteCount = (__wiegandBitCount / 8) + 1;
        memcpy(data, (void *)__wiegandData, ((byteCount > dataMaxLen) ? dataMaxLen : byteCount));

        wiegandReset();
        return bitCount;
    }
    return 0;
}

void printCharAsBinary(unsigned char ch) {
    int i;
    for (i = 0; i < 8; i++) {
        printf("%d", (ch & 0x80) ? 1 : 0);
        ch <<= 1;
    }
}

char *decimal_to_binary(int n)
{
   int c, d, count;
   char *pointer;
 
   count = 0;
   pointer = (char*)malloc(32+1);
 
   if ( pointer == NULL )
      exit(EXIT_FAILURE);
 
   for ( c = 7 ; c >= 0 ; c-- )
   {
      d = n >> c;
 
      if ( d & 1 )
         *(pointer+count) = 1 + '0';
      else
         *(pointer+count) = 0 + '0';
 
      count++;
   }
   *(pointer+count) = '\0';
 
   return  pointer;
}

//  void main(void) {
//  int i;
//     int pos = 0;
//     int crd;
//     wiegandInit(D0_PIN, D1_PIN);

//     while(1) {
//         int bitLen = wiegandGetPendingBitCount();
//         int bitLens = wiegandGetPendingBitCount();
//         if (bitLen == 0) {
//             usleep(3000);
//         } else {

//          //---
//           //  unsigned long data;
//          //   bitLens = wiegandReadData((void *)&data, 4);
//           //  int shiftRight = 32 - bitLens;  // how many bits to shift
//           //  datas >>= shiftRight; // shift the data and store the result back to itself
//           //  printf("Read code: %lX (%d bits): ", datas, bitLens);
//         //-----
//             char data[100];
//             char datahex[200];
//             char databin[200];

//             char *pointer;
//             bitLen = wiegandReadData((void *)data, 100);
//             int bytes = bitLen / 8 + 1;
//             printf("GET DATA %d bits (%d bytes): ", bitLen, bytes);

//            if(bitLen  == 32 ){
//             printf("-- ORIGINAL HID WIEGAND REGO RM50.00++ -- \n"); 
//             for (i = 0; i < bytes; i++){
//                      printf("%02X", (int)data[i]);
//                 // sprintf(buf_hex, "%02X", (int)data[i]); // puts string into buffer
//                 // pos += sprintf(&CARDbuf_hex[pos], "%02X", (int)data[i]);
//                     sprintf(datahex +i*2,"%02X",data[i]);
//                      // sprintf(buf, "pre_%d _suff", (int)data[i]); // puts string into buffer
               
//             }
//            }
//             printf("\n");
//             if(bitLen  == 34 ){
//              char filter_str[800];
//                 int bytes = bitLen / 8 +1;

//                 int pos=2, lg, c = 0;
//                 lg=32;

//                 printf("-- CHINA WIEGAND REG0 RM9.00-- \n"); 
//                 for (i = 0; i < bytes; i++){
//                     // printf("HEX : %02X", (int)data[i]);
//                           // printf("\n");
//                     pointer = decimal_to_binary(data[i]);
//                     // printf("BIN String %d is: %s\n", i, pointer);
//                      // sprintf(databin,"%s", (data[i] & 0x80) ? 1 : 0);
//                     sprintf(databin +i*8,"%s",pointer);
//                     // printf("\n");
//                     // printf("[count: %d ]",i);/
//                     // printf("\n");
//                     // printf("BIN : %d", (data[i] & 0x80) ? 1 : 0);
//                     // printf("\n");
//                 }
//                 // printf(" : ");
//                 // printf("CARD DATA (databin)--> %s\n", databin);
//                 // printf(" : \n");
//                  while (c < lg) {
//                       filter_str[c] = databin[pos+c-1];
//                       c++;
//                    }
//                    filter_str[c] = '\0';
//                    printf("filter bit: %s\n", filter_str);
//                    printf(" : \n");

//                     char *a = filter_str;
//                     int num = 0;
//                     do {
//                         int b = *a=='1'?1:0;
//                         num = (num<<1)|b;
//                         a++;
//                     } while (*a);
//                     // printf("%X\n", num);
//                     sprintf(datahex,"%X",num);
//                 // for (i = 0; i < bytes; i++){
//                 //    // printCharAsBinary(data[i]);
//                 //     int c;
//                 //     for (c = 0; c < 8; c++) {
//                 //         printf("%d", (data[i] & 0x80) ? 1 : 0);
//                 //         // sprintf(databin,"%s", (data[i] & 0x80) ? 1 : 0);
//                 //         data[i] <<= 1;
//                 //     }

//                 // }
//              //   printf("CARD BIN (databin)--> %s\n", databin);

//                  }
//                i=0;   
//                data_avail(datahex);
//                printf("CARD DATA (datahex)--> %s\n", datahex);
//             }
//             // printf("CARD DATA --> %s\n", CARDbuf_hex);
           
//             // sprintf(crd, "%d", CARDbuf_hex);
//             // return CARDbuf_hex;

//             // if (!strlen(CARDbuf_hex) == 0){
//             //    // printf("buf_hex empty\n"); 
//             //     memset(&CARDbuf_hex, 0, sizeof(CARDbuf_hex));
//             //     pos=0;

//             // }
            
//              // for (i = 0; i < bytes; i++) {
//              //   // printCharAsBinary(data[i]);
//              //        int i;
//              //    for (i = 0; i < 8; i++) {
//              //        printf("%d", (ch & 0x80) ? 1 : 0);
//              //        ch <<= 1;
//              //    }
//              // }
//             // if (strlen(buf_hex) == 0){
//             //    printf("buf_hex empty\n"); 

//             // } else {
//             //     printf("buf_hex NOT EMPTY\n"); 
//             //     memset(&buf_hex, 0, sizeof(buf_hex));
//             //     pos=0;
//             // }

//             // printf(" : ");
//             // for (i = 0; i < bytes; i++)
//             //    printCharAsBinary(data[i]);
//             // printf("\n");
//             // printf("\n");
//             // return crd;
            
//             // memset(&datahex, 0, sizeof(datahex));
//             usleep(3000);

//             // memset(&buf_hex, 0, sizeof(buf_hex));
        
//     }
// }

int wieInit() {
    int i;
    int pos = 0;
    int crd;
    // wiegandInit(D0_PIN, D1_PIN);

    while(1) {
        int bitLen = wiegandGetPendingBitCount();
        int bitLens = wiegandGetPendingBitCount();
        if (bitLen == 0) {
            usleep(3000);
        } else {

         //---
          //  unsigned long data;
         //   bitLens = wiegandReadData((void *)&data, 4);
          //  int shiftRight = 32 - bitLens;  // how many bits to shift
          //  datas >>= shiftRight; // shift the data and store the result back to itself
          //  printf("Read code: %lX (%d bits): ", datas, bitLens);
        //-----
            char data[100];
            char datahex[200];
            char databin[200];

            char *pointer;
            bitLen = wiegandReadData((void *)data, 100);
            int bytes = bitLen / 8 + 1;
            printf("GET DATA %d bits (%d bytes): ", bitLen, bytes);
            printf("\n");
           if(bitLen  == 32 ){
            printf("--------------- ORIGINAL HID WIEGAND REGO RM50.00++ --------------------- \n"); 
            for (i = 0; i < bytes; i++){
                     printf("%02X", (int)data[i]);
                // sprintf(buf_hex, "%02X", (int)data[i]); // puts string into buffer
                // pos += sprintf(&CARDbuf_hex[pos], "%02X", (int)data[i]);
                    sprintf(datahex +i*2,"%02X",data[i]);
                     // sprintf(buf, "pre_%d _suff", (int)data[i]); // puts string into buffer
               
            }
           }
            printf("\n");
            if(bitLen  == 34 ){
             char filter_str[800];
                int bytes = bitLen / 8 +1;

                int pos=2, lg, c = 0;
                lg=32;

                //printf("-------------------- CHINA WIEGAND REG0 RM9.00---------------------------- \n"); 
                for (i = 0; i < bytes; i++){
                     //printf("HEX : %02X", (int)data[i]);
                          // printf("\n");
                    pointer = decimal_to_binary(data[i]);
                    // printf("BIN String %d is: %s\n", i, pointer);
                     // sprintf(databin,"%s", (data[i] & 0x80) ? 1 : 0);
                    sprintf(databin +i*8,"%s",pointer);
                    // printf("\n");
                    // printf("[count: %d ]",i);/
                    // printf("\n");
                    // printf("BIN : %d", (data[i] & 0x80) ? 1 : 0);
                    // printf("\n");
                }
                // printf(" : ");
                // printf("CARD DATA (databin)--> %s\n", databin);
                // printf(" : \n");
                 while (c < lg) {
                      filter_str[c] = databin[pos+c-1];
                      c++;
                   }
                   filter_str[c] = '\0';
                   // printf("filter bit: %s\n", filter_str);
                   // printf(" : \n");

                    char *a = filter_str;
                    int num = 0;
                    do {
                        int b = *a=='1'?1:0;
                        num = (num<<1)|b;
                        a++;
                    } while (*a);
                    // printf("%X\n", num);
                    sprintf(datahex,"%X",num);
                // for (i = 0; i < bytes; i++){
                //    // printCharAsBinary(data[i]);
                //     int c;
                //     for (c = 0; c < 8; c++) {
                //         printf("%d", (data[i] & 0x80) ? 1 : 0);
                //         // sprintf(databin,"%s", (data[i] & 0x80) ? 1 : 0);
                //         data[i] <<= 1;
                //     }

                // }
              // printf("CARD HEX (#####)--> %s\n", datahex);

                 }
               i=0;  
              // printf("lib:CARD HEX (DATAHEX)--> %s\n", datahex); 
               data_avail(datahex);
               printf("-------------------- &&&&&&&&&&&&& ---------------------------- \n"); 
            }
            // printf("CARD DATA --> %s\n", CARDbuf_hex);
            // printf("CARD DATA (datahex)--> %s\n", datahex);
            // sprintf(crd, "%d", CARDbuf_hex);
            // return CARDbuf_hex;

            // if (!strlen(CARDbuf_hex) == 0){
            //    // printf("buf_hex empty\n"); 
            //     memset(&CARDbuf_hex, 0, sizeof(CARDbuf_hex));
            //     pos=0;

            // }
            
             // for (i = 0; i < bytes; i++) {
             //   // printCharAsBinary(data[i]);
             //        int i;
             //    for (i = 0; i < 8; i++) {
             //        printf("%d", (ch & 0x80) ? 1 : 0);
             //        ch <<= 1;
             //    }
             // }
            // if (strlen(buf_hex) == 0){
            //    printf("buf_hex empty\n"); 

            // } else {
            //     printf("buf_hex NOT EMPTY\n"); 
            //     memset(&buf_hex, 0, sizeof(buf_hex));
            //     pos=0;
            // }

            // printf(" : ");
            // for (i = 0; i < bytes; i++)
            //    printCharAsBinary(data[i]);
            // printf("\n");
            // printf("\n");
            // return crd;
            
            // memset(&datahex, 0, sizeof(datahex));
            usleep(3000);

            // memset(&buf_hex, 0, sizeof(buf_hex));
        
    }
}
