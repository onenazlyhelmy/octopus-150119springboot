package drac.bytenotail;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.LoggerFactory;

import java.awt.Toolkit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import pk.mainsys.pkPROCrecv_ENT;
import pk.mainsys.pkconfig;
import pk.mainsys.pktsk_Qrecv_ENT;
import pk.mainsys.pktsk_Qrecv_EXT;
import pk.mainsys.pkvar;
import redis.drac.pkredis_var;


 

 
public class pk_tmrByteNoTail {
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
	Map<String,String> bytnotail = new HashMap<String,String>(); 
	 public static void main(String[] args) {
		 
		 pkredis_var.redisconn_sentinel_init("redis-sentinel://127.0.0.1:26379/0#mymaster");
		 
		try {
			new pk_tmrByteNoTail();
		} catch (Exception e) {
	
			e.printStackTrace();
		} 
	 }
	
    public static org.slf4j.Logger log = LoggerFactory.getLogger(pkPROCrecv_ENT.class);
	public pk_tmrByteNoTail() throws Exception {
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
	
	       
//	       System.out.println(" -- WIE TASK TRXDATA -- " );
		  
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 100); // subsequent rate
	}
 
	 static byte [] recompile_byte(String [] strbyt_tmp) {
		  
			byte data[]= new byte[strbyt_tmp.length];
			int cc=0;
			
			try {
			for(String bytstr : strbyt_tmp) {
				 bytstr = bytstr.trim();
				 data[cc] = (byte) (Long.parseLong(bytstr.trim(),16) & 0xff);//no sign int
				 
//				 shit[cc] = (byte) Long.parseLong(bytstr.trim(),16) ; //with sign int
//				 shit[cc] = (byte) Integer.parseInt(bytstr.trim(),16) ; //with sign int
				 
//				 if(bytstr.equals("ffffffbf"))  {
					
//					 System.out.println("byte ["+bytstr+"] num : ["+ cc+"] dec ["+ shit[cc] +"]");
//					 System.out.println(">>>cov ["+ String.format("%02X",(shit[cc] & 0xff)));
//				 }

//				 if( bytstr.equals("38") ) {
//					 System.out.println("byte ["+bytstr+"] num : ["+ cc+"]"); 
//				 }
		
				 cc++;
			 }
			}catch(Exception c) {
				c.printStackTrace();
			}
			return data;
	}
	
	
	
	class tmrReminder extends TimerTask {
	
        int x=0;
        String tmpstr[] ;
 
		public void run()  {
			
			try{	
				 String rdrtype = pkconfig.jReadConfig("readerpath");
				long llen = pkredis_var.llen_cmd(rdrtype+"_bytenotail");
				if(llen >1) {
					
				
					String bytenotail0 = pkredis_var.lpop_cmd(rdrtype+"_bytenotail");
					String tmp_str0[] = bytenotail0.split("#");
				    String byt_str0=tmp_str0[3].replace("["," ");
				           byt_str0=byt_str0.replace("]"," ");
				    
				    String bytenotail1 = pkredis_var.lpop_cmd(rdrtype+"_bytenotail");
					String tmp_str1[] = bytenotail1.split("#");
				    String byt_str1=tmp_str1[3].replace("["," ");
				           byt_str1=byt_str1.replace("]"," ");
				    
				           
				    pkredis_var.set_cmd(rdrtype+"_byt_compile", bytenotail1+"________"+bytenotail0);       
				    System.out.println("<<<<<   recompile byte recv  >>>> \n");
				    System.out.println((byt_str1+byt_str0));
				    System.out.println("<<<<<   ####   >>>> ");
				    String byt_compile = byt_str1+byt_str0;
				    
				    pkvar.logtxt("recompile late byte :"+byt_compile);
				    
				    String strbyt_tmp [] = byt_compile.split(",");
				    
				    byte [] byt = recompile_byte(strbyt_tmp);
				    
				   
				    
				    if(rdrtype.equals("entry")) {
				    	  pktsk_Qrecv_ENT.fifobyt_RX.add(byt);
				    }
				    
				    if(rdrtype.equals("exit")) {
				    	  pktsk_Qrecv_EXT.fifobyt_RX.add(byt);
				    }
				  
				}
				
				
				
		

				
			}
			catch (Exception e) {
//				try {
//					Thread.sleep(2000);
//					
//				} catch (Exception e1) {
//					
//					e1.printStackTrace();
//				}
				e.printStackTrace();
//				pk_var.logtxt("\n--------------------------\n"+e.toString()+"\n------------------------------\n");
			}
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}