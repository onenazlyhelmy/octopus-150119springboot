package nioSOCKreaderCoherent;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
//import java.nio.ByteBuffer;
//import java.nio.channels.SelectionKey;
import java.util.Timer;
import java.util.TimerTask;

//import drac.io.wie_GPIO_OUT;
//import drac.main.wie_var;
import pk.mainsys.pkvar;

import java.awt.Toolkit;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

 

 
public class niotmr_sendbyt {
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;

    public static int niotmr_sendbyt_timerstop=0;
	public static byte [] byt_sndscancard={0x09,(byte) 0xb5,0x00,0x00,0x00,0x00,0x00,0x00,(byte) 0xbc};
 
	public niotmr_sendbyt() throws Exception {
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));

			      
		
		System.out.println(pkvar.tarikh()+"] [ --- clear buffer in que-serial/drac-soc --- ]");
		nioCoher_ENT.fifobyt_TX.clear(); 
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 300); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			   try {
				   
//				wie_task_rxtx_netty.fifobyt_TX.add(byt_sndscancard);
				   if(nioCoher_ENT.niomfg_connected==true) {
						nioCoher_ENT.fifobyt_TX.add(byt_sndscancard);
						nioCoher_ENT.fifobyt_multplex.add(0);
//						wie_GPIO_OUT.gpiotrigger_pin_MUXs0x(0);
					    Thread.sleep(80);
			        	nioCoher_ENT.fifobyt_TX.add(byt_sndscancard);
						nioCoher_ENT.fifobyt_multplex.add(1);
				   }else {
					   nioCoher_ENT.fifobyt_TX.clear();
					   nioCoher_ENT.fifobyt_multplex.clear();
				   }
           
				   if(niotmr_sendbyt_timerstop>0) {
					   System.out.println(pkvar.tarikh() +"] ====== Stop timer  scan card =====\n");
					   this.cancel();   
				   } 
				   
//				wie_GPIO_OUT.gpiotrigger_pin_MUXs0x(1);
			    
//			    nioclient_mfgreader.fifobyt_TX.add(byt_sndscancard);
//			    nioclient_mfgreader.fifobyt_multplex.add(2);
			    
//			   String antipassbck = jedis_local.get("antipassbck_chck");
//		        if( antipassbck.equals("true")) { 
//			     wie_var.antipassbck_update_REDIS(wie_var.pathType);
//			     
//			     String antipass_period_chck = jedis_local.get("antipass_period_chck");
//			     if( antipass_period_chck.equals("true")) { 
//			    	 wie_var.antipass_period_check(jedis_local);
//			     }
//				
//			     }
			
				
			   }catch(Exception c) {
				   c.printStackTrace();
				   nioCoher_ENT.fifobyt_TX.clear();
				   nioCoher_ENT.fifobyt_multplex.clear();
//				   wie_var.logtxt("\n--------------------------\n"+c.toString()+"\n------------------------------\n");
			   }
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}