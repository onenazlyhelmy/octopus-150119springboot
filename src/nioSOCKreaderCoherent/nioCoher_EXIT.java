package nioSOCKreaderCoherent;



import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import pk.mainsys.MainXXXXX;
import pk.mainsys.pkByteData;
//import drac.io.wie_GPIO_OUT;
//import drac.io.wie_task_rxtx_netty;
//import drac.io.wietmr_recvbyt_health;
//import drac.main.wie_config;
//import drac.main.wie_var;
import pk.mainsys.pkconfig;
import pk.mainsys.pkinit_rxtx;
import pk.mainsys.pktsk_Qrecv_ENT;
import pk.mainsys.pktsk_Qrecv_EXT;
import pk.mainsys.pkvar;

import java.io.*;

import redis.clients.jedis.Jedis;

public class nioCoher_EXIT  extends Thread  {
	
	public static SocketChannel clientChannel ;
	
	public String registr=""; 
	
	public Selector selector ;
	public static int debugs ;
	

	public Deque<String> writeQueue = new ArrayDeque<String>() ; 
	
	public static boolean niomfg_connected = false ;

	public static int seqwrt_t0  ;

    //-------------------------RAW byte
    public static byte[] byte_send ;
    public static byte[] byte_sendRECV ;
	public ByteBuffer readBuf = ByteBuffer.allocate(1024) ;
    //----------------------------------------
	
	
	public static BlockingQueue <byte[]> fifobyt_TX = new ArrayBlockingQueue<byte[]>(3000);
	public static BlockingQueue <Integer> fifobyt_multplex = new ArrayBlockingQueue<Integer>(3000);
	public static Queue<byte[]> fifobyt_RX = new LinkedList<byte[]>();
//	public static LinkedHashMap<Integer,byte[]> linkbyt_RX = new LinkedHashMap<Integer,byte[]>(); 
	public static ConcurrentHashMap<Integer, byte[]> linkbyt_RX = new ConcurrentHashMap<Integer,byte[]>();
	
	SelectionKey key;
	

	public void init() throws IOException {
		
		
		niotmr_sendbyt.niotmr_sendbyt_timerstop=0;
		selector = Selector.open() ;
		
		clientChannel = SocketChannel.open();
	    clientChannel.configureBlocking(false);
	    
	    try {
	    	System.out.println("[x] NIO-[EXIT]CoherentReader start...[x] "+pkconfig.readerip+", port: "+pkconfig.readerport+"\n");
//			jRFID_var.logtxt("[x] Xport start...[x] "+ipstr+"\n");
		} catch (Exception e11) {
			
			e11.printStackTrace();
		}
	    
	    this.start();
	    
		
	}
	
	public void stop_stop()  {
		

	    try {
	    	
	    	niotmr_sendbyt.niotmr_sendbyt_timerstop=1;
	    
	    	 System.out.println("[nio-clientChannel] : "+clientChannel.isOpen());
	    	if(clientChannel.isOpen()) {
	    		clientChannel.close();
	    	}
	      
//	    	System.out.println("[nio-selector] : "+selector.isOpen());
//	    	 if(selector.isOpen()) {
//		    	   selector.close(); 
//		        }
			fifobyt_TX.clear();
			
			this.join();
			
			System.out.println("#### restart nio sock [EXIT] to reader  ##### : "+pkconfig.readerip+", port: "+pkconfig.readerport);
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public void run()  {
		
		int i = 0 ;
		
		 
			//    System.out.println("ipstr:"+ipstr);
			    
		  try {
				clientChannel.connect(new InetSocketAddress(pkconfig.readerip, pkconfig.readerport));
				clientChannel.register(selector, SelectionKey.OP_CONNECT) ;
			} catch ( IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				try {
					pkvar.logtxt(e1.toString()+ " >> IOException: clientChannel.connect :module: nioCoher_EXIT");
				} catch (Exception e11) {
					
					e11.printStackTrace();
				}
			
			}	    
		
		
		try {
	
			
		while(true) {
			//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			
			//writeQueue.add(in.readLine()) ;
			
			selector.select() ;
			
			Iterator<SelectionKey> skeys = selector.selectedKeys().iterator() ;
			
			while (skeys.hasNext()) {
				key = (SelectionKey) skeys.next();
				skeys.remove();

				if (!key.isValid()) {
					continue;
				}

				
				if (key.isConnectable()) {
					finishConnection(key);
				} else if (key.isReadable()) {
					//read(key);
				
				} else if (key.isWritable()) {
					niomfg_connected=true;
					WRITE_Q_XPORT(key);
				}
				Thread.sleep(30);
			}
			
//			++i ;
		
		}
		
		} catch(IOException | InterruptedException e) {
			System.out.println("<<<IOException>>> "+e) ;
			niomfg_connected=false;
			e.printStackTrace();
		
			try {
//				jRFID_var.logtxt(e);
				pkvar.logtxt(" >>> IOException | InterruptedException: Iterator<SelectionKey> :module: MFGNO <<<");
			} catch (Exception e1) {
				
				e1.printStackTrace();
			}
			
			try {
				this.interrupt();
				System.out.println("[x] NIO EXIT client MFGNO restarting... [x]\n ") ;
				pkvar.logtxt("[x] NIO EXIT client MFGNO restarting... [x] ") ;
				fifobyt_TX.clear();
				Thread.sleep(30);
				clientChannel.close();
			
				
//				xportINTERF_CallBack  xportCB0 = new xportINTERF_CallBack();
				fifobyt_TX.clear();
				fifobyt_multplex.clear();
//				this.xportCB=xportCB0 ;
			
				Thread.sleep(500);
				init();
//				  pkbytedata.TIMOUT=0;
//				     pkbytedata.SEQUENCE_PROC=3;
//				pkbytedata. TIMOUT_whole=0;
//				MainXXXXX.nio_mfgreader_EXIT.stop_stop();
//       	nio_mfgreader_EXIT nio_mfgreader_EXIT = new nio_mfgreader_EXIT();
//       	nio_mfgreader_EXIT.init();
//       	Thread.sleep(50);
//				this.join();
				
			} catch (IOException | InterruptedException e2) {
			
				e2.printStackTrace();
			}
			
//			try {
//				jRFID_xport.xportCallBack =xportCB ;
//				init() ;
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
		}
		
	}

	private void finishConnection(SelectionKey key) throws IOException {
		
		clientChannel.finishConnect() ;
		key.interestOps(SelectionKey.OP_WRITE) ;
		//key.interestOps(SelectionKey.OP_READ) ;
	//	System.out.println("finish connect!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n") ;
	
	}
	
	public void WRITE_Q_XPORT(SelectionKey key) throws InterruptedException, IOException  {
		 ByteBuffer b ;
		 int mult_plex;
		 byte_send=null;
//		 jedis.set("op_xport"+jRFID_var.loca, Integer.toString(seqwrt));
		if(!fifobyt_TX.isEmpty()) {		
		 while (! fifobyt_TX.isEmpty()) {
	    
			 
		 
			 byte_send = new byte [fifobyt_TX.peek().length];
			 byte_send = fifobyt_TX.poll();
			 pkvar.logtxt_rx("[SEND] EXIT  :"+Arrays.toString(pkvar.Bytearray_toStringarray(byte_send))+", SEQ : "+pkvar.checkSEQstringSend(byte_send[4]));
//			 System.out.println("-- compile send proc_exit >>> "+Arrays.toString(pkvar.Bytearray_toStringarray(byte_send)));
//			 mult_plex = fifobyt_multplex.poll();
//		 System.out.print(" ------------ SEND FROM Q:"+byte_send[7]+" ----------------\n");
//		     jRFID_var.log_Q(byte_send);
//		  wie_GPIO_OUT.gpiotrigger_pin_MUXs0x(mult_plex);	//--multiplex-- 0->entry, 1->exit
//		  if(byte_send[3]==58) {
//			  System.out.print(" ------"+mult_plex+"---- SEND FROM buzz :"+Arrays.toString(byte_send)+" ----------------\n");  
//		  }
//		  System.out.print(" ------ SEND FROM nio:"+Arrays.toString(byte_send)+" ----------------\n");
          if (byte_send != null) { 
		    		b = ByteBuffer.wrap(byte_send) ;
					while (true) {
						int n = clientChannel.write(b) ;
					
						if (n == 0 || b.remaining() == 0)
							break ;
					}	
			
					  Thread.sleep(70);
                      
					 
		    	}

	         }

			} 
		 
		 RECVbyt_xport(); 
	}

	
	 public void RECVbyt_xport() throws InterruptedException, IOException {
			
		 readBuf.clear() ;
		 int countdatabyte=0;
//		 readBuf = ByteBuffer.allocate(readBuf.array().length) ;
//			readBuf.flip();
			
			while (true) {
	            // readBuffer.clear();
				
	            int numread = clientChannel.read( readBuf );
	            if(numread>0) {
	            	countdatabyte=numread;
	            }
//	            System.out.println("RECEIVE 1 ===> "+Arrays.toString(readBuf.array())) ;
//	            System.out.println("RECEIVE lenght 1 ===> "+numread) ;

	            if (numread <=0) {
//	              readBuf = ByteBuffer.allocate(32) ;
	              break;
	            }
	            
	           
	          }
			
			 if(countdatabyte>0) {
//				   readBuf = ByteBuffer.allocate(countdatabyte) ;
//				   System.out.println("RECEIVE lenght 2 ===> "+countdatabyte) ;
			       byte_sendRECV = new byte[countdatabyte];
//			       arr = readBuf.array();
			       byte_sendRECV = Arrays.copyOfRange(readBuf.array(), 0, countdatabyte);
	              
//			       fifobyt_RX.add(byte_sendRECV);
			       pkvar.logtxt_rx("[RCV] EXIT  :"+Arrays.toString(pkvar.Bytearray_toStringarray(byte_sendRECV))+", SEQ : "+pkvar.checkSEQstring(byte_sendRECV[3]));
			       pktsk_Qrecv_EXT.fifobyt_RX.add(byte_sendRECV); 
			 }
			
//		       linkbyt_RX.put(mult_plex, byte_sendRECV);
//		       readBuf.clear();
			   key.interestOps(SelectionKey.OP_WRITE) ;
	 }
	
	 public void RECVbyt_xport(int mult_plex) throws InterruptedException, IOException {
		
		 readBuf.clear() ;
//		 readBuf = ByteBuffer.allocate(readBuf.array().length) ;
			//readBuf.flip();
			
			while (true) {
	            // readBuffer.clear();
				
	            int numread = clientChannel.read( readBuf );
//	            System.out.println("RECEIVE 1 ===> "+Arrays.toString(readBuf.array())) ;
//	            System.out.println("RECEIVE lenght  ===> "+readBuf.array().length) ;

	            if (numread <=0) {
	              break;
	            }
	            
	           
	          }
			    
		       byte_sendRECV = new byte[readBuf.array().length];
//		       arr = readBuf.array();
		       byte_sendRECV = readBuf.array();
              
//		       fifobyt_RX.add(byte_sendRECV);
		       linkbyt_RX.put(mult_plex, byte_sendRECV);
//		       readBuf.clear();
			   key.interestOps(SelectionKey.OP_WRITE) ;
	 }
	 //#########################################################################################################################################################################################################
	//------------------------------------------------------------------------------------------------------------------------------------------
		 private void processMsg(String data) {
        

         String msgs[] = data.split("\\r\\n");
         for (String msg : msgs) {
             msg = msg.trim();
             if (msg == null || msg.isEmpty()) continue;
             System.out.println("MSG :"+msg);
      }
         
         
	 }


	 

	
}
