package nettyio.rxtx;


import java.util.Arrays;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.logging.LogLevel;
import io.netty.util.CharsetUtil;

/**
 * Handler implementation for the echo server.
 */
@Sharable
public class nettyServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
//        ctx.write(msg);
//        String in = ( (ByteBuf)msg ).toString();
//        System.out.println("SERVER RECV -> " +in);
//        LogLevel.valueOf(in);
    	   ByteBuf inBuffer = (ByteBuf) msg;

           String received = inBuffer.toString(CharsetUtil.UTF_8);
           System.out.println("Server r"
           		+ "aw ::: " + msg);
           System.out.println("Server received: " + received);
//           LogLevel.valueOf(received);
           ctx.write(Unpooled.copiedBuffer("Hello " + received, CharsetUtil.UTF_8));
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
