package nettyio.rxtx;

import java.util.Arrays;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class RxtxClientHandler extends SimpleChannelInboundHandler<String> {
	static byte [] byt_ENABLE={0x02,0x00,0x08,0x58, 0x01,0x17,0x05,0x21,0x16,0x14,0x58, 0x3a,0x03};
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
//        ctx.writeAndFlush("AT\n");
        ctx.writeAndFlush(byt_ENABLE);
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
//        if ("OK".equals(msg)) {
//            System.out.println("Serial port responded to AT");
//        } else {
//            System.out.println("Serial port responded with not-OK: " + msg);
//        }
    	System.out.println("Recv : " + Arrays.toString(msg.getBytes()));
        ctx.close();
    }
    
//    @Override
//    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//        if ("OK".equals(msg)) {
//            System.out.println("Serial port responded to AT");
//        } else {
//            System.out.println("Serial port responded with not-OK: " + msg);
//        }
//        ctx.close();
//    }
}
