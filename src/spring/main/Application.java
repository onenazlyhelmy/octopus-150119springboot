package spring.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void SpringStart() {
    	String[] args=null;
        SpringApplication.run(Application.class, args);
    }
}