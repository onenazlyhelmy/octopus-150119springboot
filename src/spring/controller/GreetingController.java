package spring.controller;


import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;


import pk.mainsys.pkconfig;
import pk.mainsys.pkrasp_io;
import pk.mainsys.pkvar;
import redis.drac.pkredis_var;
//import spring.fcukoff.Greeting;
import spring.fcukoff.greet_cardinfo;
import spring.fcukoff.greet_status;
@SpringBootApplication
@RestController
@Configuration
public class GreetingController extends SpringBootServletInitializer {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
    @Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GreetingController.class);
	}

//    @RequestMapping("/greeting")
//    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
//        return new Greeting(counter.incrementAndGet(),
//                            String.format(template, name));
//    }
    @RequestMapping("/")
    public String index() {
        return "welcome to drac.valley.ransom!";
    }
    
    @RequestMapping("/forcealbon")
    public greet_status forcealbon() {
       pkconfig.AUTHORIZE ="yes";
//       pkrasp_io.gpio_trigAUTH_ALB(1);
    	return new greet_status(1,"forcealbon","forcealb");
//        return "welcome to drac.valley.ransom!";
    }
    
    @RequestMapping("/forcealboff")
    public greet_status forcealboff() {
      	 pkconfig.AUTHORIZE ="no";
    	return new greet_status(0,"forcealboff","forcealb");
//        return "welcome to drac.valley.ransom!";
    }
    
    @RequestMapping("/loop1status")
    public greet_status loop1status() {
//       	pkredis_var.statloop1="0";
    	return new greet_status(1,"loop1status",	pkredis_var.statloop1);
//        return "welcome to drac.valley.ransom!";
    }
    
    @RequestMapping("/loop2status")
    public greet_status loop2status() {
//    	pkredis_var.statloop2="1";
    	return new greet_status(2,"loop2status",pkredis_var.statloop2);
//        return "welcome to drac.valley.ransom!";
    }
    
    @RequestMapping("/cardinfo")
    public String cardinfo() {
//    	pkredis_var.statloop2="1";
//    	return new greet_cardinfo().getCardinfo();
    	 //----------testing---
//		 JsonObject jsonobj = null;
//		 jsonobj = Json.createObjectBuilder().add("testing", Json.createObjectBuilder().add("data","01010101")).build();
//		 StringWriter strwr =  new StringWriter();
//		 JsonWriter jsonwr = Json.createWriter(strwr);
//		 jsonwr.writeObject(jsonobj);
//		 jsonwr.close();
//    	return   new greet_cardinfo(strwr.getBuffer().toString()).getCardinfo();
		 //--end test---

          return   new greet_cardinfo( pkvar.cardinfojson).getCardinfo();
    }
    
//    @RequestMapping("/killapp")
//    public String killapp() {
//    	pkredis_var.statloop2="1";
//    	return new greet_cardinfo().getCardinfo();
    	 //----------testing---
//		 JsonObject jsonobj = null;
//		 jsonobj = Json.createObjectBuilder().add("testing", Json.createObjectBuilder().add("data","01010101")).build();
//		 StringWriter strwr =  new StringWriter();
//		 JsonWriter jsonwr = Json.createWriter(strwr);
//		 jsonwr.writeObject(jsonobj);
//		 jsonwr.close();
//    	return   new greet_cardinfo(strwr.getBuffer().toString()).getCardinfo();
		 //--end test---
//          System.exit(0);
//          return   "killed!";
//    }
    
    @RequestMapping("/killapp")
    public String killapp(@RequestParam(value="name", defaultValue="World") String name) {
    	if(name.equals("fuckoff")) {
    		 System.exit(0);
    	}
//    	  System.exit(0);
         return   "killed!";
    }
    
    @RequestMapping("/restartapp")
    public String restartapp(@RequestParam("name") String name,@RequestParam("cmd1") String cmd1,
    		@RequestParam("cmd2") String cmd2,@RequestParam("cmd3") String cmd3) {
    	String ret = ">>> cmd >>>>\n";
    	 StringBuilder strb = new StringBuilder();
    	 strb.append(ret);
    	
    	if(name.equals("fuckoff")) {
    		
    	
			
			try {
				
					
//					List<String> commands = new ArrayList<String>();
////				    commands.add("/bin/bash");
////				    commands.add("-c");
////				    commands.add("/bin/sh");
////				    commands.add("-i");
//				    commands.add(cmd1); // command 
//			        commands.add(cmd2); // command 
//			        commands.add(cmd3); //command in Mac 
////					Process pr = Runtime.getRuntime().exec("sudo "+cmd1+" "+cmd2+" "+cmd3);
//			    	ProcessBuilder pb = new ProcessBuilder(commands);
//			    	Process process = pb.start();
//					BufferedReader read = new BufferedReader(new InputStreamReader(process.getInputStream()));
////					ProcessBuilder pb = new ProcessBuilder("/bin/sh"," -i", "rfid.sh");
//					System.out.println("check cmd :"+cmd1+" : "+ read.readLine());
//					while((ret = read.readLine()) != null) {
//						strb.append(ret);
////						System.out.println("check cmd :"+cmd1+" : "+ret);
//						strb.append(System.getProperty("line.separator"));
//					 }
//					System.out.println(">>: : "+strb.toString());
				
				 JSch jsch=new JSch();  
				 String pwd="root123";
				 Session session=jsch.getSession("root", "localhost", 22);
				 UserInfo ui=new MyUserInfo(pwd);
//				 new MyUserInfo().passwd = "root123";
				 session.setUserInfo(ui);
				 session.connect();
				 String command=cmd1+" "+cmd2+" "+cmd3;
				      
				 Channel channel=session.openChannel("exec");
			      ((ChannelExec)channel).setCommand(command);

			      channel.setInputStream(null);

			
			      ((ChannelExec)channel).setErrStream(System.err);

			      InputStream in=channel.getInputStream();

			      channel.connect();

			      byte[] tmp=new byte[1024];
			      while(true){
			        while(in.available()>0){
			          int i=in.read(tmp, 0, 1024);
			          if(i<0)break;
			          System.out.print(new String(tmp, 0, i));
			          strb.append(new String(tmp, 0, i));
			        }
			        if(channel.isClosed()){
			          if(in.available()>0) continue; 
			          System.out.println("exit-status: "+channel.getExitStatus());
			          break;
			        }
			        try{Thread.sleep(100);}catch(Exception ee){}
			      }
			      channel.disconnect();
			      session.disconnect();
							
				
				Thread.sleep(500);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
    		 //System.exit(0);
    	}
    	 return   strb.toString();
//    	  System.exit(0);
         
    }
    
    public static class MyUserInfo implements UserInfo, UIKeyboardInteractive{
    	public String passwd;
    	
    	public MyUserInfo(String passwd) {
    		this.passwd = passwd;
    	}
    	
    	public String getPassword(){
        	
        	return passwd; 
        
        }
        public boolean promptYesNo(String str){
       //   Object[] options={ "yes", "no" };
        //  int foo=JOptionPane.showOptionDialog(null, 
         //        str,
          //       "Warning", 
           //      JOptionPane.DEFAULT_OPTION, 
            //     JOptionPane.WARNING_MESSAGE,
             //    null, options, options[0]);
        	int foo = 1;
           return foo==1;
        }
      
        
        JTextField passwordField=(JTextField)new JPasswordField(20);

        public String getPassphrase(){ return null; }
        public boolean promptPassphrase(String message){ return true; }
        public boolean promptPassword(String message){
         // Object[] ob={passwordField}; 
          int result=1;
         //   JOptionPane.showConfirmDialog(null, ob, message,
          //                                JOptionPane.OK_CANCEL_OPTION);
          if(result==1){
           // passwd=passwordField.getText();
//        	 passwd= "root123";
            return true;
         }
          else{ 
            return false; 
          }
        }
        public void showMessage(String message){
          JOptionPane.showMessageDialog(null, message);
        }
        final GridBagConstraints gbc = 
          new GridBagConstraints(0,0,1,1,1,1,
                                 GridBagConstraints.NORTHWEST,
                                 GridBagConstraints.NONE,
                                 new Insets(0,0,0,0),0,0);
        private Container panel;
        public String[] promptKeyboardInteractive(String destination,
                                                  String name,
                                                  String instruction,
                                                  String[] prompt,
                                                  boolean[] echo){
          panel = new JPanel();
          panel.setLayout(new GridBagLayout());

          gbc.weightx = 1.0;
          gbc.gridwidth = GridBagConstraints.REMAINDER;
          gbc.gridx = 0;
          panel.add(new JLabel(instruction), gbc);
          gbc.gridy++;

          gbc.gridwidth = GridBagConstraints.RELATIVE;

          JTextField[] texts=new JTextField[prompt.length];
          for(int i=0; i<prompt.length; i++){
            gbc.fill = GridBagConstraints.NONE;
            gbc.gridx = 0;
            gbc.weightx = 1;
            panel.add(new JLabel(prompt[i]),gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weighty = 1;
            if(echo[i]){
              texts[i]=new JTextField(20);
            }
            else{
              texts[i]=new JPasswordField(20);
            }
            panel.add(texts[i], gbc);
            gbc.gridy++;
          }

          if(JOptionPane.showConfirmDialog(null, panel, 
                                           destination+": "+name,
                                           JOptionPane.OK_CANCEL_OPTION,
                                           JOptionPane.QUESTION_MESSAGE)
             ==JOptionPane.OK_OPTION){
            String[] response=new String[prompt.length];
            for(int i=0; i<prompt.length; i++){
              response[i]=texts[i].getText();
            }
    	return response;
          }
          else{
            return null;  // cancel
          }
        }
      }
}

