package rdr.config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.LoggerFactory;

import pk.mainsys.pkByteData;
import pk.mainsys.pkvar;

public class adminfunc {

	 public static org.slf4j.Logger log = LoggerFactory.getLogger(adminfunc.class);
	 public byte[] ipadd=new byte[30];
	 public byte[] ipgtway=new byte[30];
	 public byte[] ipnetmask=new byte[30];
//	 public byte[] initconfig = {0x02,0x00,0x02,20};
	 public static void info(byte [] by) {
//		    String a[] = { "A", "E", "I" };
//		      String b[] = { "O", "U" };
//		      List list = new ArrayList(Arrays.asList(a));
//		      list.addAll(Arrays.asList(b));
//		      Object[] c = list.toArray();
//		      System.out.println(Arrays.toString(c));
		 int c=0;
//		 for(byte b : by) {
//			 log.info("byte ["+c+"] :"+	 pkvar.IntValToAsciiChar(b)); 
//			 c++;
//		 }
		 log.info("-INFO- :"+	 new String(by)); 
//		 log.info("IP addr : ----------------------------------------");
//		 for(c=227;c<241;c++) {
//			 log.info("byte ["+c+"] :"+	 pkvar.IntValToAsciiChar(by[c])); 
//		 }
		 System.exit(0);
	 }
	 
	 public  void setipadd(String ip,String netmask,String gtwy) {
//		 byte ipaddr [] = ip.getBytes();
		 this.ipadd = new byte[30];
		 this.ipnetmask = new byte[30];
		 this.ipgtway = new byte[30];
//		 System.arraycopy(ip.getBytes(), 0,this.ipadd, ip.length(), ip.length()-5);
//		 this.ipadd = ipadd >> 12;
//		 System.arraycopy(netmask.getBytes(), 0, this.ipnetmask, 0, netmask.length()-5);
//		 System.arraycopy(gtwy.getBytes(),0, this.ipgtway,  0, gtwy.length()-5);
//		 this.ipadd=  ip.getBytes();
//		 this.ipnetmask =  netmask.getBytes();
//		 this.ipgtway =  gtwy.getBytes();
		 int c=0;
		 for(byte b : ip.getBytes()) {
			 this.ipadd[c]=b;
			 c++;
		 }
		 c=0;
		 for(byte bb : netmask.getBytes()) {
			 this.ipnetmask [c]=bb;
			 c++;
		 }
		 c=0;
		 for(byte bbb : gtwy.getBytes()) {
			 this.ipgtway [c]=bbb;
			 c++;
		 }
		 
	 }
	 
	 public static byte[] setipadd(String ip) {
		 byte ipaddr [] = ip.getBytes();
		 return ipaddr;
	 }
	 
	 public  byte [] byte_ALLmerge() {

	      ByteArrayOutputStream baos = new ByteArrayOutputStream();
	      try {
	    	 
	    	
	  
	    	baos.write(0x00);
			baos.write(ipadd);
		    baos.write(ipnetmask);
		    baos.write(ipgtway);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	      byte[] c = baos.toByteArray();
	      byte[] d=  pkByteData.bytSND_SETIPcompiled(c);
		  return d;

	 }
	 public void startconfig() {
		 pktask_rxtx.fifobyt_TX.add(byte_ALLmerge());
	 }
}
