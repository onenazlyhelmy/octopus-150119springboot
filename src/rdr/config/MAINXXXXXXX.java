package rdr.config;


import java.util.Arrays;

import com.pi4j.io.gpio.RaspiPin;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisStringCommands;
import pk.mainsys.pkByteData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.slf4j.LoggerFactory;

import java.sql.*;

import redis.clients.jedis.Jedis;


public class MAINXXXXXXX extends RaspiPin {
	 static Thread thread_init_SUBrabbitMQ ;
	 static Thread thread_init_PUBrabbitMQ ;
	 static String paymode = "OFFLINE";
		private static Log log = LogFactory.getLog(MAINXXXXXXX.class);
	 
	 public static  void start() {
		 try{
		      BasicConfigurator.configure();
		      Logger root = Logger.getRootLogger();
		      root.setLevel(Level.INFO);
		      
		      setupLog4j("");
//			  pkvar.setupLog4j("");
		      
//		      byte [] en_rdr = pkByteData.bytSND_GETIP_compiled();
//		      pktask_rxtx.fifobyt_TX.add(en_rdr);
		      
//		      byte [] en_rdr = pkbytedata.bytSND_reboot();
//		      pktask_rxtx.fifobyt_TX.add(en_rdr);
//		      pkinit_rxtx.connect("/dev/ttyUSB0");
		      adminfunc adminfunc = new adminfunc();
		      adminfunc.setipadd("10.222.104.250", "255.255.255.0", "10.222.104.1");
		      adminfunc.startconfig();
		      

		      
		      pkinit_rxtx.connect("/dev/ttyUSB0");
		      new pktask_rxtx();
	         
		 } catch (Exception c){  	
		        c.printStackTrace();
//			    jedis = new Jedis("localhost");
//		    	pk_var.jedis_local = new Jedis("localhost");		 
		 } 
	 }
	 
	 private static void setupLog4j(String appName) {

	        // InputStream inStreamLog4j = getClass().getResourceAsStream("/log4j.properties");
//			File Filenm = new File(codeSource.getLocation().toURI().getPath());
	        String propFileName = appName + "log4j.config";
//	        System.out.println("log :"+propFileName);
	        File f = new File( propFileName);
	        if (f.exists()) {

	            try {
	            	 InputStream inStreamLog4j = new FileInputStream(f);
//	                InputStream inStreamLog4j = new FileInputStream(f);
	                Properties propertiesLog4j = new Properties();

	                propertiesLog4j.load(inStreamLog4j);
	                PropertyConfigurator.configure(propertiesLog4j);
	            } catch (Exception e) {
	                e.printStackTrace();
	                BasicConfigurator.configure();
	            }
//	            log.info( "DETECT!!! log4j");
	        } else {
//	        	 log.info( "NOT found log4j setup");
	            BasicConfigurator.configure();
	        }

	        // logger.setLevel(Level.TRACE);
	        log.debug("log4j configured");

	    }

	 
	
    public static void main(String[] args) {
    	try {
    		
//    		pkvar.FareDouble_tobyte_ext(120.00);
//    		System.exit(0);
    		
    		byte bytdata[] = {0x02,0x00,0x08,0x10,0x01,0x18,0x10,0x13,0x10,0x52,0x34,0x76,0x03};
        	Byte init_entry[] = {0x02,0x00,0x02,0x11,0x03,(byte)0xe8,0x03};//send with checksum
        	
        	start();
//			pk_task_rxtx.fifobyt_TX.add(byt_EN_RDR);
    	//--------------------------------------------------------------------------------------------
   //################################################ All task task after SUCCESS REDIS connection ############################################
      
//    	System.out.println("Arrays test!! : "+Arrays.toString(byt)+" len:"+byt.length);
    	
    	//----testing-------------
    	
//    	pk_var.FareDouble_tobyte(123.33);
    	
//    	char yourInt = 1;
//    	int here = 53;// in hex 31// in ascii 1
//    	double init_cash = 19.34;
//    	double last_cash = 4.21;
//    	NumberFormat nfmt = new DecimalFormat("00.00");
    	
//    	double tot = init_cash - last_cash;
//    	System.out.println(" total : "+nfmt.format(tot));
//    	String str1 = String.valueOf(yourInt);
//        System.out.println("ASCIIToChar :"+pk_var.Int_inAsciito_AsciiByte(yourInt));
//        System.out.println("Int to ascii :"+pk_var.Int_inAsciito_AsciiByte(here));
//        System.out.println("Int to ascii :"+pk_var.IntValToAsciiChar(here));
    	
//   	 String str = "2018-09-18 23:08:09";
   	 
//   	 byte [] byte_dt = pk_var.DateString_toByte(str);
// 	 byte[] byt = pk_var.byteSENDbuild(byte_dt,(byte) pk_bytedata.byte_CMD_EN_RDR, (byte) pk_bytedata.byte_SEQ_EN_RDR);
//       System.out.println("bytSND_PROC_ENT_compiled : "+Arrays.toString(pk_var.Bytearray_toStringarray(pk_bytedata.bytSND_PROC_ENT_compiled())));
    	
//    	System.out.println("check convert string hex to long/byte:: "+ Long.parseLong("58", 16));
//       byte[] chcsm = {0x02, 0x00, 0x08, 0x10, 0x10, 0x18, 0x09, 0x12, 0x23, 0x08, 0x09};
//       System.out.println("checksum :"+pk_var.getchecksum( chcsm));
    	
//       System.out.println("ascii:"+String.valueOf(pkvar.IntValToAsciiChar(70)));
       //-----------------------------------------------------------------------------------------------------
       	String vers = "1.9.65-###";
//    	jedis.set("appver", vers);
//    	BasicConfigurator.configure();//initi log4j.........................
       	System.out.println("================ Park ver: "+vers+"===========\n");	
    	
//   	
//    int r= -99;
//    
//       if(r<0) {
//    	  int  cr = 0xff & r;
//    	   System.out.print("heheheh "+Integer.toHexString(cr)); 
////    	   System.out.print("hahah :"+Integer.toHexString(r));
//       }else {
////    	   System.out.print("hahah :"+Integer.toHexString(r));
//       }
//    	
//    	System.out.println("================ SID:prod,user:pms,pwd:pms,ip:192.168.168.100 ===========\n");
//    	   Long longObject = new Long("1234567");
//    	    byte b = longObject.byteValue();
//    	    System.out.println("byte:"+b);
// 
//    	    short s = longObject.shortValue();
//    	    System.out.println("short:"+s);
//
//    	    int i = longObject.intValue();
//    	    System.out.println("int:"+i);
//
//    	    float f = longObject.floatValue();
//    	    System.out.println("float"+f);
//
//    	    double d = longObject.doubleValue();
//    	    System.out.println("double:"+d);
//
//    	    long l = longObject.longValue();
//    	    System.out.println("long:"+l);
//        	System.exit(0);
    	
    	
//    		byte byt[] = {0x02,0x00,0x02,0x58,0x00};
////        	System.out.println("check sum: "+Long.toHexString(pk_var.getchecksum(byt)));
//        	
//        	System.out.println("check convert string hex to long/byte:: "+ Long.parseLong("58", 16));
//        
//           	
//        	System.out.println("check decode byte: "+	Long.decode("0x58").byteValue());
//        	
//        	
//    		pkconfig.config_init();
//    		System.out.println("--> Start init config <-- ");
//    		pkinit_rxtx.connect(pkvar.serialport);
//    		new pktask_rxtx();
//    		System.out.println("--> Start init rxtx <-- ");
//    		pkredis_var.redisconn_sentinel_init();
//    		
//    		pkredis_var.del_cmd(pkredis_var.lpopkeysendbyte);
//    		new pktask_redis();
//    		System.out.println("--> init redis cluster <-- ");
//    		String str =pk_redis_var.lpop_cmd("sendbyte");
    		
//    		Long [] recv_b = pk_var.string_byte(str);
    		
//    		System.out.println("--> "+Arrays.toString(recv_b));
    		
    		
		} catch (Exception e) {
		
			e.printStackTrace();
		}
    }
}
