package rdr.config;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.LoggerFactory;

import java.awt.Toolkit;















import redis.clients.jedis.Jedis;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import pk.mainsys.pkPROCrecv_ENT;
import pk.mainsys.pkByteData;
import pk.mainsys.pkvar;
 

 
public class pktask_rxtx {
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
	public static Queue<byte[]> fifobyt_TX = new LinkedList<byte[]>();
	
	static int byteRECVNoTailNoHeader;
	public static byte [] byt_sndscancard={0x09,(byte) 0xb5,0x00,0x00,0x00,0x00,0x00,0x00,(byte) 0xbc};
	public static byte [] byt_sndbuzzOK={0x04,(byte) 0x11,0x4d,0x58};
	public static byte [] byt_sndbuzzNOK={0x04,(byte) 0x11,0x52,0x47};
	
	//--coherent-----
	public static byte [] byt_ENABLE={0x02,0x00,0x08,0x10,0x01,0x18,0x10,0x08,0x12,0x30,0x25,0x3a,0x03};
//	int  timeout = Integer.parseInt( pkvar.timeoutread);
	int  timeout = 100;
	 public static org.slf4j.Logger log = LoggerFactory.getLogger(pktask_rxtx.class);
	public pktask_rxtx() throws Exception {
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		System.out.println("--> Start init pk_task_rxtx <-- ");
		log.info("--> Start init pk_task_rxtx <-- ");
	
//		  byte[] byt_EN_RDR = pkbytedata.bytSND_EN_compiled();
//			
//			pktask_rxtx.fifobyt_TX.add(byt_EN_RDR);
//			pk_task_rxtx.fifobyt_TX.add(byt_EN_RDR);
	
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 50); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
		  int tot_byt =0;
		  Long ts;
			 byte[] byte_send ;
//			 int dat ;
			 byte [] byte_read;
		  
		public void run()  {
			 ByteBuffer b ;

			
			  
		try {
			
			if(!fifobyt_TX.isEmpty()) {		
			
			 while (! fifobyt_TX.isEmpty()) {
		    
				 
			 
				 byte_send = new byte [fifobyt_TX.peek().length];
				 byte_send = fifobyt_TX.poll();
	
	             pkinit_rxtx.write(byte_send);
	             ts = System.currentTimeMillis();
	            
	             try {
					  Thread.sleep(50);
//					  byte_read = new byte [pk_init_rxtx.dat_in.available()]; 
//					  tot_byt = pk_init_rxtx.dat_in.available();
//					  pk_init_rxtx.dat_in.read(byte_read);
				 } catch ( Exception  e) {
				
					e.printStackTrace();
				}
	          
			  		
		         }
			 
			 //--prevent recv byte spill off
//			 while(true) {
//				 try {
//					if(pk_init_rxtx.dat_in.available() > 0) {    
//						  tot_byt = pk_init_rxtx.dat_in.available();
//						  byte_read = new byte [tot_byt]; 
//						  pk_init_rxtx.dat_in.read(byte_read);
////						  System.out.println("Recv :"+Integer.toHexString(dat));
//
//						  pk_prcss_datrecv.RECV_RAWbytefilter(byte_read);
//						  
//					 } 
//					else {
//			
//						 break;
//					 }
//				} catch (IOException e) {
//				
//					e.printStackTrace();
//				 }
//		    	}
			 
		if(pkinit_rxtx.dat_in.available() > 0) {    
			  tot_byt = pkinit_rxtx.dat_in.available();
			  byte_read = new byte [tot_byt]; 
			  pkinit_rxtx.dat_in.read(byte_read);


//			  pkprcss_datrecv.RECV_RAWbytefilter(byte_read);
//			  pktskRECVbyte.fifobyt_RX.add(byte_read);
			  System.err.println(" RECV-1 >> "+Arrays.toString(pkvar.Bytearray_toStringarray(byte_read)));
			  
		       } 
			} 
			
			if(pkinit_rxtx.dat_in.available() > 0) {    
				  tot_byt = pkinit_rxtx.dat_in.available();
				  byte_read = new byte [tot_byt]; 
				  pkinit_rxtx.dat_in.read(byte_read);

//				  pktskRECVbyte.fifobyt_RX.add(byte_read);
//				  pkprcss_datrecv.RECV_RAWbytefilter(byte_read);
				  System.err.println(" RECV-2 >> "+Arrays.toString(pkvar.Bytearray_toStringarray(byte_read)));
				  adminfunc.info(byte_read);
			  
			       } 
	
			if(byteRECVNoTailNoHeader ==1) {
				Thread.sleep(100);
				if(pkinit_rxtx.dat_in.available() > 0) {    
					  tot_byt = pkinit_rxtx.dat_in.available();
					  byte_read = new byte [tot_byt]; 
					  pkinit_rxtx.dat_in.read(byte_read);

//					  pktskRECVbyte.fifobyt_RX.add(byte_read);
					  System.err.println(" latebyte recv >> "+Arrays.toString(pkvar.Bytearray_toStringarray(byte_read)));
//					  pkprcss_datrecv.RECV_RAWbytefilter(byte_read);
				  
				       } 
			    	byteRECVNoTailNoHeader =0;
			       }
			
	
				
			//--backup spill off byte
//			 while(true) {
//				 try {
			     
			       //--init only---
					
					
				
////					else {
////			
////						 break;
////					 }
//				} catch (IOException e) {
//				
//					e.printStackTrace();
//				 }
//		    	}
			 //read---
	
	
//			if(pk_init_rxtx.dat_in.available() != 0) {    
//			  
//			   try {
////					  Thread.sleep(timeout);
//					  byte_read = new byte [pk_init_rxtx.dat_in.available()]; 
//					  tot_byt = pk_init_rxtx.dat_in.available();
//					  pk_init_rxtx.dat_in.read(byte_read);
//					  System.out.println("[read port] :"+(Arrays.toString(byte_read))+" total byte recv :"+tot_byt +", time recv :"+(System.currentTimeMillis() - ts));
//				} catch ( Exception  e) {
//				
//					e.printStackTrace();
//				}
//			}  
//		      
		       //--read--rx
//        	 while(true) {
//				 try {
//					if(wie_rxtx.dat_in.available() != 0) {    
//						 int dat = wie_rxtx.dat_in.readByte();
//						 System.out.println("Recv :"+Integer.toHexString(dat));
//					 } else {
//			
//						 break;
//					 }
//				} catch (IOException e) {
//				
//					e.printStackTrace();
//				 }
//		    	}
        	 //--------
			  } catch (Exception e) {
				 e.printStackTrace();
				 
			 }
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}