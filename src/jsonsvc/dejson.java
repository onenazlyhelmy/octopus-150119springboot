package jsonsvc;



import java.text.SimpleDateFormat;
import java.util.Date;



import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class dejson {

	private static String locationno;
	private static String terminalno;
	private static String jobno;
	private static String trxn;
	private static String trxnjob;
	private static String datebojtime;
	private static String datebojdate;
	private static String operationaldate;
	private static String terminaltype;
	private static String jobtype;
	private static String badgeno;
	private static String opermode;
	private static String tarikhoperationaldate;
	private static String tarikhoperationaltime;
	private static String trxtype;
	private static String exitoperatorid;
	private static String exitlocation;
	private static String exitterminalno;
	private static String exittcclass;
	private static String exitdetectclass;
	private static String exitdate;
	private static String farelocation;
	private static String exittime;
	private static String fareamount;
	private static String tenderamount;
	private static String outstandingamount;
	private static String balancereturn;
	private static String actualamountpaid;
	private static String taxpaid;
	private static String receiptno;
	private static String infoversion;
	private static String infotype;
	private static String infolen;
	private static String terminalid;
	private static String merchantid;
	private static String trxdatetime;
	private static String trxmaskpan;
	private static String trxcardbin;
	private static String trxhashpan;
	private static String trxamount;
	private static String trxinvoice;
	private static String trxapprtype;
	private static String trxid;
	private static String rrn;
	private static String stan;
	private static String issucces;
	private static String respcode;
	private static String cardtype;
	private static String paymenttype;
	private static String trxapprcode;

	private static String mfgno;
	private static String entryoperatorid;
	private static String entrydatetime;
	
	
	private static String Plaza;
	private static String LaneNo;
	private static String DateTime;
	private static String Type;
	private static String plazaid;
	
	
	
//	public static String faresimulate(String response)  {
//		String ret="";
//		try {
////			JSONObject jsonObject = new JSONObject(response.toString());
//	    JsonObject jsonObject = new JsonParser().parse(response.toString()).getAsJsonObject();
//		
//		  if(jsonObject.has("enable_simulate")) {
//			  RestApifunc.enable_simulate= jsonObject.get("enable_simulate").getAsString();
//			  RestApifunc.simulate_hrs= jsonObject.get("simulate_hrs").getAsLong();
//			  RestApifunc.simulate_min= jsonObject.get("simulate_min").getAsLong();
//		  }
//		}catch(Exception v) {
//			v.printStackTrace();
//		}
//		  return  RestApifunc.enable_simulate+"#" + RestApifunc.simulate_hrs+"#"+ RestApifunc.simulate_min;
//		}
		
	public static String getdecodejson_notuse(String response)  {
		String ret="";
		try {
//			JSONObject jsonObject = new JSONObject(response.toString());
	    JsonObject jsonObject = new JsonParser().parse(response.toString()).getAsJsonObject();
		
		  if(jsonObject.has("datainit")) {
			  cardtype =  jsonObject.get("datainit").getAsJsonObject().get("cardtype").getAsString();
			  locationno =  jsonObject.get("datainit").getAsJsonObject().get("locationno").getAsString();
			  terminalno =  jsonObject.get("datainit").getAsJsonObject().get("terminalno").getAsString();
			  jobno =  jsonObject.get("datainit").getAsJsonObject().get("jobno").getAsString();
			  trxn =  jsonObject.get("datainit").getAsJsonObject().get("trxn").getAsString();
			  trxnjob =  jsonObject.get("datainit").getAsJsonObject().get("trxnjob").getAsString();
			  datebojdate =  jsonObject.get("datainit").getAsJsonObject().get("datebojdate").getAsString();
			  datebojtime =  jsonObject.get("datainit").getAsJsonObject().get("datebojtime").getAsString();
			  operationaldate =  jsonObject.get("datainit").getAsJsonObject().get("operationaldate").getAsString();
		  	     
			  jobtype =  jsonObject.get("datainit").getAsJsonObject().get("jobtype").getAsString();
			  terminaltype =  jsonObject.get("datainit").getAsJsonObject().get("terminaltype").getAsString();
			  opermode =  jsonObject.get("datainit").getAsJsonObject().get("opermode").getAsString();
			  badgeno =  jsonObject.get("datainit").getAsJsonObject().get("badgeno").getAsString();
			  tarikhoperationaldate =  jsonObject.get("datainit").getAsJsonObject().get("tarikhoperationaldate").getAsString();
			  tarikhoperationaltime =  jsonObject.get("datainit").getAsJsonObject().get("tarikhoperationaltime").getAsString();
			  trxtype =  jsonObject.get("datainit").getAsJsonObject().get("trxtype").getAsString();
			  paymenttype =  jsonObject.get("datainit").getAsJsonObject().get("paymenttype").getAsString();
			  exitoperatorid =  jsonObject.get("datainit").getAsJsonObject().get("exitoperatorid").getAsString();
		  	     
			  exitlocation =  jsonObject.get("datainit").getAsJsonObject().get("exitlocation").getAsString();
			  exitterminalno =  jsonObject.get("datainit").getAsJsonObject().get("exitterminalno").getAsString();
			  exittcclass =  jsonObject.get("datainit").getAsJsonObject().get("exittcclass").getAsString();
			  exitdetectclass =  jsonObject.get("datainit").getAsJsonObject().get("exitdetectclass").getAsString();
			  exitdate =  jsonObject.get("datainit").getAsJsonObject().get("exitdate").getAsString();
			  exittime =  jsonObject.get("datainit").getAsJsonObject().get("exittime").getAsString();
			  farelocation =  jsonObject.get("datainit").getAsJsonObject().get("farelocation").getAsString();
			  fareamount =  jsonObject.get("datainit").getAsJsonObject().get("fareamount").getAsString();
		  	     
			  tenderamount =  jsonObject.get("datainit").getAsJsonObject().get("tenderamount").getAsString();
			  balancereturn =  jsonObject.get("datainit").getAsJsonObject().get("balancereturn").getAsString();
			  outstandingamount =  jsonObject.get("datainit").getAsJsonObject().get("outstandingamount").getAsString();
			  actualamountpaid =  jsonObject.get("datainit").getAsJsonObject().get("actualamountpaid").getAsString();
			  taxpaid =  jsonObject.get("datainit").getAsJsonObject().get("taxpaid").getAsString();
			  receiptno =  jsonObject.get("datainit").getAsJsonObject().get("receiptno").getAsString();
		
			  
			  if(cardtype.equals("TNG") || cardtype.equals("tng")) {
				     mfgno =  jsonObject.get("datainit").getAsJsonObject().get("mfgno").getAsString();
				     entryoperatorid =  jsonObject.get("datainit").getAsJsonObject().get("entryoperatorid").getAsString();
				     entrydatetime =  jsonObject.get("datainit").getAsJsonObject().get("entrydatetime").getAsString();
				     
				     if(entryoperatorid.length()>2) {
				    	   entryoperatorid = entryoperatorid.substring(0, 2); 
				     }
				  
				     
			     } else {
			    	 infolen =  jsonObject.get("datainit").getAsJsonObject().get("infolen").getAsString();
			   	     infotype =  jsonObject.get("datainit").getAsJsonObject().get("infotype").getAsString();
				     infoversion =  jsonObject.get("datainit").getAsJsonObject().get("infoversion").getAsString();
			     }
			 
		  }
		  
		  if(jsonObject.has("datacard")) {
			  merchantid =  jsonObject.get("datacard").getAsJsonObject().get("merchantid").getAsString();
			  terminalid =  jsonObject.get("datacard").getAsJsonObject().get("terminalid").getAsString();
			  trxdatetime =  jsonObject.get("datacard").getAsJsonObject().get("trxdatetime").getAsString();
			  trxmaskpan =  jsonObject.get("datacard").getAsJsonObject().get("trxmaskpan").getAsString();
			  trxcardbin =  jsonObject.get("datacard").getAsJsonObject().get("trxcardbin").getAsString();
			  trxhashpan =  jsonObject.get("datacard").getAsJsonObject().get("trxhashpan").getAsString();
			  trxamount =  jsonObject.get("datacard").getAsJsonObject().get("trxamount").getAsString();
			  trxinvoice =  jsonObject.get("datacard").getAsJsonObject().get("trxinvoice").getAsString();
			  trxapprtype =  jsonObject.get("datacard").getAsJsonObject().get("trxapprtype").getAsString();
			  
			  trxid =  jsonObject.get("datacard").getAsJsonObject().get("trxid").getAsString();
			  trxapprcode =  jsonObject.get("datacard").getAsJsonObject().get("trxapprcode").getAsString();
			  rrn =  jsonObject.get("datacard").getAsJsonObject().get("rrn").getAsString();
			  stan =  jsonObject.get("datacard").getAsJsonObject().get("stan").getAsString();
			  issucces =  jsonObject.get("datacard").getAsJsonObject().get("issucces").getAsString();
			  respcode =  jsonObject.get("datacard").getAsJsonObject().get("respcode").getAsString();
			  
			  if(cardtype.equals("EMV") || cardtype.equals("emv")) {
			  
			     trxhashpan = trxhashpan.substring(0, 39);
			  }
		  }

		  

	  }catch(Exception c) {
		  c.printStackTrace();
	

	  }

		return ret;
	}
	
	public static String dateformat_dd_MMMM_yyyy_HHmmss(String datestr) {
		SimpleDateFormat sdf_old = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf_new = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String dte ="";
		try {
		Date dateold = sdf_old.parse(datestr);
		dte = sdf_new.format(dateold);
		}catch(Exception c) {
			c.printStackTrace();
		}
		return dte;
	}
	public static String dateformat_yyyyy_MM_dd_HHmmss(String datestr) {
		SimpleDateFormat sdf_old = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf_new = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dte ="";
		try {
		Date dateold = sdf_old.parse(datestr);
		dte = sdf_new.format(dateold);
		}catch(Exception c) {
			c.printStackTrace();
		}
		return dte;
	}
	public static String dateformat_dd_MMMM_yyyy(String datestr) {
		SimpleDateFormat sdf_old = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdf_new = new SimpleDateFormat("dd-MMM-yyyy");
		String dte ="";
		try {
		Date dateold = sdf_old.parse(datestr);
		dte = sdf_new.format(dateold);
		}catch(Exception c) {
			c.printStackTrace();
		}
		return dte;
	}
  }

