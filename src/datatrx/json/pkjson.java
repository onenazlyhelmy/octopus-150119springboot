package datatrx.json;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import pk.mainsys.pkconfig;
import pk.mainsys.pkvar;

//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;


public class pkjson {

	//--realdata--not simulation
	public static String jsonENCODE_EMV(String totalperiodpark,String cardtype,String merchantid,String terminalid,String trxdatetime,String trxmaskpan,String trxcardbin,String trxhashpan,String trxamount,
			String trxinvoice,String trxapprtype,String trxid,String trxapprcode,
			String locationno,String terminalno,String jobno,String trxn,String trxnjob,String datebojdate,String datebojtime,String operationaldate,String jobtype,
			String terminaltype,String opermode,String badgeno,String tarikhoperationaldate,String tarikhoperationaltime,String paymenttype,String trxtype,
			String exitoperatorid,String exitlocation,String exitterminalno,String exittcclass,String exitdetectclass,String exitdate,String exittime,String farelocation,
			String fareamount,String tenderamount,String balancereturn,String outstandingamount,String actualamountpaid,String taxpaid,String receiptno,
			String infotype,String infoversion,String infolen,String rrn,String stan,String issucces,String respcode,String trxdatetime_entry,String entry_id,String exit_id) {
		JsonObject personObject = null;
		 try {
			 personObject = Json.createObjectBuilder()
			  			          .add("datainit", 
			                  Json.createObjectBuilder().add("cardtype", cardtype)
								                        .add("locationno",locationno)
								                        .add("terminalno",terminalno)
								                          .add("jobno",jobno)
								                             .add("trxn",trxn)
								                             .add("trxnjob",trxnjob)
									                          .add("datebojdate",datebojdate)
									                             .add("datebojtime",datebojtime)
									                       
									                             .add("operationaldate",operationaldate)
									                             .add("jobtype",jobtype)
									                             .add("terminaltype",terminaltype)
									                             .add("opermode",opermode)
									                             .add("badgeno",badgeno)
									                             
									   .add("tarikhoperationaldate",tarikhoperationaldate)
								   .add("tarikhoperationaltime",tarikhoperationaltime)
							      .add("paymenttype",paymenttype)
							      .add("trxtype",trxtype)
						   .add("exitoperatorid",exitoperatorid)
							.add("exitlocation",exitlocation)
							
							.add("exitterminalno",exitterminalno)
							   .add("exittcclass",exittcclass)
						      .add("exitdetectclass",exitdetectclass)
					   .add("exitdate",exitdate)
						.add("exittime",exittime)
						
						
						.add("farelocation",farelocation)
						
						.add("sst",pkvar.sst)
						.add("sst_enable",pkvar.sst_enable)
						.add("surcharge",pkvar.surcharge)
						.add("surcharge_enable",pkvar.surcharge_enable)
						
						.add("bank_orifare",pkconfig.BANK_ORIFARE)
						.add("bank_taxfare",pkconfig.BANK_TAXFARE)
						.add("bank_surcharge",pkconfig.BANK_SURCHARGE)
						.add("bank_taxsurcharge",pkconfig.BANK_TAXSURCHARGE)
						
						   .add("fareamount",fareamount)
					      .add("tenderamount",tenderamount)
				   .add("balancereturn",balancereturn)
					.add("outstandingamount",outstandingamount)
					
					

					.add("actualamountpaid",actualamountpaid)
					   .add("taxpaid",taxpaid)
				      .add("receiptno",receiptno)
			   .add("infotype",infotype)
				.add("infoversion",infoversion)
				.add("infolen",infolen)
						
								                        .build()
                                                          )
			          .add("datacard", 
				               Json.createObjectBuilder().add("merchantid", merchantid)
				                                         .add("terminalid",terminalid)
				                                         .add("trxdatetime", trxdatetime)
				                                         .add("trxmaskpan", trxmaskpan)
				                                         .add("trxcardbin", trxcardbin)
				                                          .add("trxhashpan", trxhashpan)
				                                         .add("trxamount", trxamount)
				                                         .add("trxinvoice", trxinvoice)
				                                         
				                                         .add("trxapprtype", trxapprtype)
				                                         .add("trxid", trxid)
				                                         .add("trxapprcode", trxapprcode)
				                                         .add("totalperiodpark", totalperiodpark)
				                                         .add("rrn", rrn)
				                                         .add("stan", stan)
				                                         .add("issucces", issucces)
				                                         .add("respcode", respcode)
	                                              
				                                         .add("trxdatetime_entry", trxdatetime_entry)
				                                         .add("entry_id", entry_id)
				                                         .add("exit_id", exit_id)
				                                         
				                        .build()
				              )
			          .build();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
           
		 StringWriter stringWriter = new StringWriter();
         JsonWriter writer = Json.createWriter(stringWriter);
         
 //        writers = new JsonWriter(new FileWriter("user.json"));
         
          writer.writeObject(personObject);
          writer.close();
//          System.out.println(stringWriter.getBuffer().toString());
          //---SQLite log
          
//           logDBJSON logDBJSON =new logDBJSON();
       
          
     
      	  //----
      	  
          return stringWriter.getBuffer().toString();
          
         // webPostHttp(stringWriter.getBuffer().toString());
	}
	
	public static String jsonENCODE_EMV(String totalperiodpark,String cardtype,String merchantid,String terminalid,String trxdatetime,String trxmaskpan,String trxcardbin,String trxhashpan,String trxamount,
			String trxinvoice,String trxapprtype,String trxid,String trxapprcode,
			String locationno,String terminalno,String jobno,String trxn,String trxnjob,String datebojdate,String datebojtime,String operationaldate,String jobtype,
			String terminaltype,String opermode,String badgeno,String tarikhoperationaldate,String tarikhoperationaltime,String paymenttype,String trxtype,
			String exitoperatorid,String exitlocation,String exitterminalno,String exittcclass,String exitdetectclass,String exitdate,String exittime,String farelocation,
			String fareamount,String tenderamount,String balancereturn,String outstandingamount,String actualamountpaid,String taxpaid,String receiptno,
			String infotype,String infoversion,String infolen,String rrn,String stan,String issucces,String respcode,
			String SIM_ENABLE,String SIM_DATEENTRY,String SIM_DATEEXIT,String SIM_TRXNO,String SIM_AMNT,String SIM_PH) {
		JsonObject personObject = null;
		 try {
			 personObject = Json.createObjectBuilder()
			  			          .add("datainit", 
			                  Json.createObjectBuilder().add("cardtype", cardtype)
								                        .add("locationno",locationno)
								                        .add("terminalno",terminalno)
								                          .add("jobno",jobno)
								                             .add("trxn",trxn)
								                             .add("trxnjob",trxnjob)
									                          .add("datebojdate",datebojdate)
									                             .add("datebojtime",datebojtime)
									                       
									                             .add("operationaldate",operationaldate)
									                             .add("jobtype",jobtype)
									                             .add("terminaltype",terminaltype)
									                             .add("opermode",opermode)
									                             .add("badgeno",badgeno)
									                             
									   .add("tarikhoperationaldate",tarikhoperationaldate)
								   .add("tarikhoperationaltime",tarikhoperationaltime)
							      .add("paymenttype",paymenttype)
							      .add("trxtype",trxtype)
						   .add("exitoperatorid",exitoperatorid)
							.add("exitlocation",exitlocation)
							
							.add("exitterminalno",exitterminalno)
							   .add("exittcclass",exittcclass)
						      .add("exitdetectclass",exitdetectclass)
					   .add("exitdate",exitdate)
						.add("exittime",exittime)
						
						
						.add("farelocation",farelocation)
						
						.add("sst",pkvar.sst)
						.add("sst_enable",pkvar.sst_enable)
						.add("surcharge",pkvar.surcharge)
						.add("surcharge_enable",pkvar.surcharge_enable)
						
						.add("bank_orifare",pkconfig.BANK_ORIFARE)
						.add("bank_taxfare",pkconfig.BANK_TAXFARE)
						.add("bank_surcharge",pkconfig.BANK_SURCHARGE)
						.add("bank_taxsurcharge",pkconfig.BANK_TAXSURCHARGE)
						
						   .add("fareamount",fareamount)
					      .add("tenderamount",tenderamount)
				   .add("balancereturn",balancereturn)
					.add("outstandingamount",outstandingamount)
					
					

					.add("actualamountpaid",actualamountpaid)
					   .add("taxpaid",taxpaid)
				      .add("receiptno",receiptno)
			   .add("infotype",infotype)
				.add("infoversion",infoversion)
				.add("infolen",infolen)
						
								                        .build()
                                                          )
			          .add("datacard", 
				               Json.createObjectBuilder().add("merchantid", merchantid)
				                                         .add("terminalid",terminalid)
				                                         .add("trxdatetime", trxdatetime)
				                                         .add("trxmaskpan", trxmaskpan)
				                                         .add("trxcardbin", trxcardbin)
				                                          .add("trxhashpan", trxhashpan)
				                                         .add("trxamount", trxamount)
				                                         .add("trxinvoice", trxinvoice)
				                                         
				                                         .add("trxapprtype", trxapprtype)
				                                         .add("trxid", trxid)
				                                         .add("trxapprcode", trxapprcode)
				                                         .add("totalperiodpark", totalperiodpark)
				                                         .add("rrn", rrn)
				                                         .add("stan", stan)
				                                         .add("issucces", issucces)
				                                         .add("respcode", respcode)
	                                              
				                        .build()
				              )
			          .add("datasimulate", 
				               Json.createObjectBuilder().add("SIM_ENABLE", SIM_ENABLE)
				                                         .add("SIM_DATEENTRY",SIM_DATEENTRY)
				                                         .add("SIM_DATEEXIT", SIM_DATEEXIT)
				                                         .add("SIM_TRXNO", SIM_TRXNO)
				                                         .add("SIM_AMNT", SIM_AMNT)
				                                         .add("SIM_PH", SIM_PH)
				                                        .build()
				              )
			          .build();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
           
		 StringWriter stringWriter = new StringWriter();
         JsonWriter writer = Json.createWriter(stringWriter);
         
 //        writers = new JsonWriter(new FileWriter("user.json"));
         
          writer.writeObject(personObject);
          writer.close();
//          System.out.println(stringWriter.getBuffer().toString());
          //---SQLite log
          
//           logDBJSON logDBJSON =new logDBJSON();
       
          
     
      	  //----
      	  
          return stringWriter.getBuffer().toString();
          
         // webPostHttp(stringWriter.getBuffer().toString());
	}
	
		
	public static String jsonENCODE_TNG_entry(String totalperiodpark,String cardtype,String merchantid,String terminalid,String trxdatetime,String trxmaskpan,String trxcardbin,String trxhashpan,String trxamount,
			String trxinvoice,String trxapprtype,String trxid,String trxapprcode,
			String locationno,String terminalno,String jobno,String trxn,String trxnjob,String datebojdate,String datebojtime,String operationaldate,String jobtype,
			String terminaltype,String opermode,String badgeno,String tarikhoperationaldate,String tarikhoperationaltime,String paymenttype,String trxtype,
			String exitoperatorid,String exitlocation,String exitterminalno,String exittcclass,String exitdetectclass,String exitdate,String exittime,String farelocation,
			String fareamount,String tenderamount,String balancereturn,String outstandingamount,String actualamountpaid,String taxpaid,String receiptno,
			String entrydatetime,String entryoperatorid,String mfgno,String rrn,String stan,String issucces,String respcode, Map map_tnginfo) {
		String na ="na";
		JsonObject personObject = null;
		 try {
			 personObject = Json.createObjectBuilder()
			  			          .add("datainit", 
			                  Json.createObjectBuilder().add("cardtype", cardtype)
								                        .add("locationno",locationno)
								                        .add("terminalno",terminalno)
								                          .add("jobno",jobno)
								                             .add("trxn",trxn)
								                             .add("trxnjob",trxnjob)
									                          .add("datebojdate",datebojdate)
									                             .add("datebojtime",datebojtime)
									                       
									                             .add("operationaldate",operationaldate)
									                             .add("jobtype",jobtype)
									                             .add("terminaltype",terminaltype)
									                             .add("opermode",opermode)
									                             .add("badgeno",badgeno)
									                             
									   .add("tarikhoperationaldate",tarikhoperationaldate)
								   .add("tarikhoperationaltime",tarikhoperationaltime)
							      .add("paymenttype",paymenttype)
							      .add("trxtype",trxtype)
						   .add("exitoperatorid",exitoperatorid)
							.add("exitlocation",exitlocation)
							
							.add("exitterminalno",exitterminalno)
							   .add("exittcclass",exittcclass)
						      .add("exitdetectclass",exitdetectclass)
					   .add("exitdate",exitdate)
						.add("exittime",exittime)
						
						
						.add("farelocation",farelocation)
						   .add("fareamount",fareamount)
					      .add("tenderamount",tenderamount)
				   .add("balancereturn",balancereturn)
					.add("outstandingamount",outstandingamount)
					
					

					.add("actualamountpaid",actualamountpaid)
					   .add("taxpaid",taxpaid)
				      .add("receiptno",receiptno)
			   .add("entryoperatorid",entryoperatorid)
				.add("entrydatetime",entrydatetime)
				.add("mfgno",mfgno)
//				.add("infolen",infolen)
							
								                        .build())
			  			          
			          .add("datacard", 
				               Json.createObjectBuilder().add("merchantid", merchantid)
				                                         .add("terminalid",terminalid)
				                                         .add("trxdatetime", trxdatetime)
				                                         .add("trxmaskpan", trxmaskpan)
				                                         .add("trxcardbin", trxcardbin)
				                                          .add("trxhashpan", trxhashpan)
				                                         .add("trxamount", trxamount)
				                                         .add("trxinvoice", trxinvoice)
				                                         
				                                         .add("trxapprtype", trxapprtype)
				                                         .add("trxid", trxid)
				                                         .add("trxapprcode", trxapprcode)
				                                         .add("totalperiodpark", totalperiodpark)
				                                         .add("rrn", "na")
				                                         .add("stan", "na")
				                                         .add("issucces", issucces)
				                                         .add("respcode", respcode)
	                                              
				                   .build())
			
			          .add("dataaddinfo", 
				               Json.createObjectBuilder().add("mfgno", na)
				                                         .add("cardtxnno",na)
				                                         .add("txntype", na)
				                                         .add("txndatetime", na)
				                                         .add("drcr",na)
				                                          .add("purseflag", na)
				                                         .add("txnamt",na)
				                                         .add("afterbal", na)
				                                         
				                                         .add("cardisuer", na)
				                                         .add("machinecode",na)
				                                         .add("obu", na)
				                                         .add("entryspid", na)
				                                         .add("signaturever", na)
				                                         .add("signature", na)
				                                         .add("cardno",na)
				                                         .add("corporateaflag", na)
				                                         
				                                         .add("usercat", na) 	
				                                         .add("csctype", na)
				                                         .add("entryloc", na)
				                                         .add("lastrefillmachinecode", na)
				                                         
				                                         
				                                         .add("appsector",na)
				                                         .add("comma", na)
				                                         .add("terminalid", na) 
				                        .build())
			          .build();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
           
		 StringWriter stringWriter = new StringWriter();
         JsonWriter writer = Json.createWriter(stringWriter);
         
 //        writers = new JsonWriter(new FileWriter("user.json"));
         
          writer.writeObject(personObject);
          writer.close();
//          System.out.println(stringWriter.getBuffer().toString());
          //---SQLite log
          
//           logDBJSON logDBJSON =new logDBJSON();
       
          
     
      	  //----
      	  
          return stringWriter.getBuffer().toString();
          
         // webPostHttp(stringWriter.getBuffer().toString());
	}
	
	public static String jsonENCODE_TNG_exit(String totalperiodpark,String cardtype,String merchantid,String terminalid,String trxdatetime,String trxmaskpan,String trxcardbin,String trxhashpan,String trxamount,
			String trxinvoice,String trxapprtype,String trxid,String trxapprcode,
			String locationno,String terminalno,String jobno,String trxn,String trxnjob,String datebojdate,String datebojtime,String operationaldate,String jobtype,
			String terminaltype,String opermode,String badgeno,String tarikhoperationaldate,String tarikhoperationaltime,String paymenttype,String trxtype,
			String exitoperatorid,String exitlocation,String exitterminalno,String exittcclass,String exitdetectclass,String exitdate,String exittime,String farelocation,
			String fareamount,String tenderamount,String balancereturn,String outstandingamount,String actualamountpaid,String taxpaid,String receiptno,
			String entrydatetime,String entryoperatorid,String mfgno,String rrn,String stan,String issucces,String respcode, Map map_tnginfo) {
		JsonObject personObject = null;
		 try {
			 personObject = Json.createObjectBuilder()
			  			          .add("datainit", 
			                  Json.createObjectBuilder().add("cardtype", cardtype)
								                        .add("locationno",locationno)
								                        .add("terminalno",terminalno)
								                          .add("jobno",jobno)
								                             .add("trxn",trxn)
								                             .add("trxnjob",trxnjob)
									                          .add("datebojdate",datebojdate)
									                             .add("datebojtime",datebojtime)
									                       
									                             .add("operationaldate",operationaldate)
									                             .add("jobtype",jobtype)
									                             .add("terminaltype",terminaltype)
									                             .add("opermode",opermode)
									                             .add("badgeno",badgeno)
									                             
									   .add("tarikhoperationaldate",tarikhoperationaldate)
								   .add("tarikhoperationaltime",tarikhoperationaltime)
							      .add("paymenttype",paymenttype)
							      .add("trxtype",trxtype)
						   .add("exitoperatorid",exitoperatorid)
							.add("exitlocation",exitlocation)
							
							.add("exitterminalno",exitterminalno)
							   .add("exittcclass",exittcclass)
						      .add("exitdetectclass",exitdetectclass)
					   .add("exitdate",exitdate)
						.add("exittime",exittime)
						
						
						
						.add("farelocation",farelocation)
						
						.add("sst",pkvar.sst)
						.add("sst_enable",pkvar.sst_enable)
						.add("surcharge",pkvar.surcharge)
						.add("surcharge_enable",pkvar.surcharge_enable)
						
						   .add("fareamount",fareamount)
					      .add("tenderamount",tenderamount)
				   .add("balancereturn",balancereturn)
					.add("outstandingamount",outstandingamount)
					
					

					.add("actualamountpaid",actualamountpaid)
					   .add("taxpaid",taxpaid)
				      .add("receiptno",receiptno)
			   .add("entryoperatorid",entryoperatorid)
				.add("entrydatetime",entrydatetime)
				.add("mfgno",mfgno)
//				.add("infolen",infolen)
							
								                        .build())
			          .add("datacard", 
				               Json.createObjectBuilder().add("merchantid", merchantid)
				                                         .add("terminalid",terminalid)
				                                         .add("trxdatetime", trxdatetime)
				                                         .add("trxmaskpan", trxmaskpan)
				                                         .add("trxcardbin", trxcardbin)
				                                          .add("trxhashpan", trxhashpan)
				                                         .add("trxamount", trxamount)
				                                         .add("trxinvoice", trxinvoice)
				                                         
				                                         .add("trxapprtype", trxapprtype)
				                                         .add("trxid", trxid)
				                                         .add("trxapprcode", trxapprcode)
				                                         .add("totalperiodpark", totalperiodpark)
				                                         .add("rrn", "na")
				                                         .add("stan", stan)
				                                         .add("issucces", issucces)
				                                         .add("respcode", respcode)
	                                              
				                        .build())
			        
			          .add("dataaddinfo", 
				               Json.createObjectBuilder().add("mfgno", map_tnginfo.get("mfgno").toString())
				                                         .add("cardtxnno",map_tnginfo.get("cardtxnno").toString())
				                                         .add("txntype", map_tnginfo.get("txntype").toString())
				                                         .add("txndatetime", map_tnginfo.get("txndatetime").toString())
				                                         .add("drcr", map_tnginfo.get("drcr").toString())
				                                          .add("purseflag", map_tnginfo.get("purseflag").toString())
				                                         .add("txnamt", map_tnginfo.get("txnamt").toString())
				                                         .add("afterbal", map_tnginfo.get("afterbal").toString())
				                                         
				                                         .add("cardisuer", map_tnginfo.get("cardisuer").toString())
				                                         .add("machinecode", map_tnginfo.get("machinecode").toString())
				                                         .add("obu", map_tnginfo.get("obu").toString())
				                                         .add("entryspid", map_tnginfo.get("entryspid").toString())
				                                         .add("signaturever", map_tnginfo.get("signaturever").toString())
				                                         .add("signature", map_tnginfo.get("signature").toString())
				                                         .add("cardno", map_tnginfo.get("cardno").toString())
				                                         .add("corporateaflag", map_tnginfo.get("corporateaflag").toString())
				                                         
				                                         .add("usercat", map_tnginfo.get("usercat").toString())
				                                         .add("csctype", map_tnginfo.get("csctype").toString())
				                                         .add("entryloc", map_tnginfo.get("entryloc").toString())
				                                         .add("lastrefillmachinecode", map_tnginfo.get("lastrefillmachinecode").toString())
				                                         
				                                         
				                                         .add("appsector", map_tnginfo.get("appsector").toString())
				                                         .add("comma", map_tnginfo.get("comma").toString())
				                                         .add("terminalid", map_tnginfo.get("terminalid").toString()) .build())
			          .build();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
           
		 StringWriter stringWriter = new StringWriter();
         JsonWriter writer = Json.createWriter(stringWriter);
         
 //        writers = new JsonWriter(new FileWriter("user.json"));
         
          writer.writeObject(personObject);
          writer.close();
//          System.out.println(stringWriter.getBuffer().toString());
          //---SQLite log
          
//           logDBJSON logDBJSON =new logDBJSON();
       
          
     
      	  //----
      	  
          return stringWriter.getBuffer().toString();
          
         // webPostHttp(stringWriter.getBuffer().toString());
	}
	
static void webPostHttp(String jsondat,String svrpath,String path,String object) throws Exception {
		
		String USER_AGENT = "jps";
//		String url = "http://10.222.102.38:7777/api/posts";

		String url = svrpath.trim()+path.trim();
		
		System.out.println("POST DATA TO >>" + url);
		
		URL obj = new URL(url);
		URLConnection urlop = obj.openConnection();
	//	URL obj = new URL(null, "https://redmine.xxx.cz/time_entries.xml", new sun.net.www.protocol.https.Handler());
		HttpURLConnection con = (HttpURLConnection) urlop;
 
		//add reuqest header
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		//con.setRequestProperty("Content-type", "application/json");
//		con.setRequestProperty("Content-Type", "application/json");
// 
//		String urlParameters = "message=Testing RFID"
//		+"&registrationIDs=APA91bGTmGU1DjsJh0GPObFQFzpux3jm31NFj45VJT0hLABGCCD90qCsLRmkL_444HhuvAX4vLvBgfTNCouFoDuFV9vdLvRGww9E4d_tI-YGPhpBs0Em70Fqrk1IIMUTYlXR5wd2wF5O"
//		+"&apiKey=AIzaSyA1UlYyaS1h07iyeRvpexwR4p8x0Laik0w";
       
//	    DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
//	       
//        wr.write(json.getBytes("UTF-8"));
//
//        wr.flush();
//
//        wr.close();
//		// Send post request
	
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//		wr.writeBytes("testing");
//		wr.writeChars(jsondat.toString());
//		wr.write(jsondat.getBytes("UTF-8"));
		System.out.println("REPRINT POST DATA TO >>" + jsondat);
		wr.writeBytes(jsondat);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
//		System.out.println("\nSending 'POST' request to URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
//		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
//		System.out.println("********************* RETURN POST JSON ******************************************");
//		System.out.println(response.toString());
 
	}

}
