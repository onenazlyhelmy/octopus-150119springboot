package pk.mainsys;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import pk.mainsys.pkconfig;
import pk.mainsys.pkvar;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
public class pkinit_rxtx {
	
	private static OutputStreamWriter out;
	
	public static int delay=1000;
//	private static	InputStream in;
//	private static BufferedReader input;
	static SerialPort serialPort;
	static DataInputStream dat_in; 
	static DataOutputStream dat_out;
	static CommPortIdentifier portIdentifier;
	 static BufferedReader input;
	 static long ts;

	public pkinit_rxtx() {
		super();
	}
	public static void checkport(String comportUsed) {
		 CommPortIdentifier portId = null;
		 SerialPort port;
		try {

		Enumeration portList;
		String defaultPort;

		String osname = System.getProperty("os.name", "").toLowerCase();
		if (osname.startsWith("windows")) {
		// windows
		defaultPort = "COM4";
		} else if (osname.startsWith("linux")) {
		// linux
		defaultPort = "/dev/ttyS0";
		} else if (osname.startsWith("mac")) {
		// mac
		defaultPort = "????";
		} else {
		System.out
		.println("Sorry, your operating system is not supported");

		}

		portList = CommPortIdentifier.getPortIdentifiers();
		while (portList.hasMoreElements()) {
			 portId = (CommPortIdentifier) portList.nextElement();
			 System.out.println("Found port: " + portId.getName());
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (portId.getName().equals(comportUsed)) {
				System.out.println("Found port: " + comportUsed);
				//break;
				}
			}
		}

//		port = (SerialPort) portId.open(comportUsed, 2000);
//		port.setSerialPortParams(19200, SerialPort.DATABITS_8,
//		SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
//		//in = new InputStreamReader(port.getInputStream());
//		//out = new OutputStreamWriter(port.getOutputStream());
//		System.err.println("Port Open:Success");

		} catch (Exception e) {
		System.err.println("Port Open:Failed");
		System.out.println("Port is already used from other application,");
		System.out.println("Close that application and re-run this application");
		}
		}
	
    static void connect (String portName)  {
    	
    	try {
    //	System.out.println("************** PORT SETTING :"+portName+" *************** \n");
         portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        
        if ( portIdentifier.isCurrentlyOwned() ){
            System.out.println("Error: Port is currently in use");
            pkvar.logtxt("Error: Port TNG is currently in use:"+portName);
        } else {
            CommPort commPort = portIdentifier.open(portName,1000);
            
            if ( commPort instanceof SerialPort ) {
                serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(115200,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.FLOWCONTROL_NONE);
                System.out.println("************** CONNECTED TO PORT TNG :"+portName+" *************** \n");
                pkvar.logtxt("CONNECTED TO PORT TNG:"+portName);
              //  System.out.print("Running...please place card to reader...\n");
          //      in = serialPort.getInputStream();
           //     out = new OutputStreamWriter(serialPort.getOutputStream());
                
            	String osname = System.getProperty("os.name", "").toLowerCase(); 
            	if (osname.startsWith("windows")) {
            		
            			                
	               // InputStream dat_in = new DataInputStream(serialPort.getInputStream());
	                BufferedReader input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
	    	        dat_out = new DataOutputStream(serialPort.getOutputStream()); 
//	    	        write(dat_en_usb);
	    	      //  Thread.sleep(1000);
	    	      //  write(dat_byte);
//	                (new Thread(new SerialReader(input))).start();//Thread.sleep(1000);
	              //   Thread.sleep(1000);
//	                new jRASP_tmr();
//	                jRASP_TNG_context. chksendbyte=1;
	                //*****************************************************LINUX *********************************************
            	} else {
            	 	       
                    pkconfig.connectTTY = "yes";
            		
                    dat_in = new DataInputStream(serialPort.getInputStream());
        	        dat_out = new DataOutputStream(serialPort.getOutputStream()); 
//        	        Thread.sleep(500);
//        	        write(pk_task_rxtx.byt_ENABLE);
        	        }
            
            } else {
                System.out.println("Error: Only serial ports are handled by this instance.");
                pkvar.logtxt("Error: Only serial ports handled by this instance use:"+portName);
            }
        }   
        
    	} catch (Exception e) {
    	     pkconfig.connectTTY = "no";
    		e.printStackTrace();
    		
    	}
    }

    public static void write(byte[] s)  {
    	
    	try {
    		ts =System.currentTimeMillis();
			dat_out.write(s);
	    	dat_out.flush();
	    	System.err.println("[SEQ]: "+pkvar.checkSEQstring(s[3])+": [SEND raw byte] :"+Arrays.toString(pkvar.Bytearray_toStringarray(s))+" total byte Send :"+s.length +" ,time: "+(ts));
	    	pkvar.logtxt("[SEND raw byte] :"+Arrays.toString(pkvar.Bytearray_toStringarray(s))+" SEQ : "+pkvar.checkSEQstring(s[3])+" , total byte Send :"+s.length +" ,time: "+(ts));
	    	System.out.println(" ============================================================================== \n");
    	} catch (IOException e) {
			
			e.printStackTrace();
		}
    	//dat_out.w

    	}
 
    


	
	 private static class JVMShutdownHook extends Thread {
		    public void run() {
		    	

        		System.out.println(" <--- exception -- > ");

      
		    }
		  }
}

