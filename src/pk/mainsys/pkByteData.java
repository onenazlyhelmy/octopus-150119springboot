package pk.mainsys;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;

import boj.sialw.pkboj;
import datatrx.json.pkjson;
import farecsc.faredata;
import farecsc.verifycsc;
import jsonsvc.httpsvc;
import jsonsvc.json;
import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;

public class pkByteData {
	//--------------------------
	
	public static int VALIDCARD=99;
	public static String CARDTYPE="";
	public static String MASKPAN="";
	public static String HASHPAN="";
	public static String ENTRY_DATE="";
	public static String ENTRY_TIME="";
	public static String ENTRY_ID="";
	public static String EXIT_ID="";
	public static String EXIT_DATE="";
	public static String EXIT_TIME="";
	
	public static String NO_ENTRY_INFO="false";
	public static double PENALTI_AMNT=0;
	
	//---tng----
	public static String CARDNO="";
	public static String CARDBALANCE="";
	public static String CARDTRXNO="";
//	public static String CARDTRXNO="";
	//--emv
	public static String RRN="0000";
	public static String STAN="0000";
	public static String APPR_CODE="0000";
	
	public static String AMNTCHARGE="";
	public static double AMNTCHARGEdbl=0;
	public static String FARERATE="";
	public static long TOTHOUR=0;
	public static String TRX_DT="";//yyMMddHHMMss
	public static String TRX_ID="";//yyMMddHHMMss
	
	public static String is_okcard="notok";
   static Map<String,String> map_procexit_tnginfo = new HashMap<String,String>();
   public static Map <String,String> map_tnginitinfo = new HashMap<String,String>();
//	public static String APPRCODE="";//yyMMddHHMMss
	
	public static  NumberFormat nfn_card = new DecimalFormat("0000000000");
	public static NumberFormat nfn_blnc = new DecimalFormat("00");
//	private static org.slf4j.Logger log = LoggerFactory.getLogger(pkPROCESSrecv.class.toString());
	   public static Log log = LogFactory.getLog(pkvar.class.getName());
	private static long TOMINUTE;
	static String MFGNO;
	static DecimalFormat blc_fmt =new DecimalFormat("0.00");
	////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	 public static int SEQUENCE_PROC=99;
	 public static int TIMOUT=0;
	 public static int TIMOUT_whole=0;
	//get info
	
	public static byte byte_SEQ_EN_GETIP= 0x20;
	public static byte byte_CMD_EN_GETIP = 0x45;
	
	//setip
	public static byte byte_SEQ_EN_SETIP= 0x30;
	public static byte byte_CMD_EN_SETIP = 0x44;
	
	//rebot
	public static byte byte_SEQ_reboot= 0x1a;
	public static byte byte_CMD_reboot = 0x4F;
	
 	//enable reader ---
	public static byte bytdata_EN_RDR[] = null; 
 	public static byte byte_SEQ_EN_RDR= 0x10;
	public static byte byte_CMD_EN_RDR = 0x01;

	
	//   init entry	----

	public static byte byte_SEQ_INITent_tng= 0x01;
  	public static byte byte_CMD_INITent_tng = 0x03;
	
//   proceed entry	----------

  	public static byte byte_SEQ_PROCent_tng= 0x02;
	public static byte byte_CMD_PROCent_tng = 0x13;
	
  
	
	///--------------------old------
 	//   init exit	----
	   
	public static byte byte_SEQ_INIText_tng= 0x03;
  	public static byte byte_CMD_INIText_tng = 0x04;
	
//   proceed exit	----------

  	public static byte byte_SEQ_PROCext_tng= 0x04;
	public static byte byte_CMD_PROCext_tng = 0x14;
	//--------------------------old
	
  	//   init sale	----
	   
	public static byte byte_SEQ_INITsale_tng= 0x03;
  	public static byte byte_CMD_INITsale_tng = 0x21;
	
//   proceed sale	----------

  	public static byte byte_SEQ_PROCsale_tng= 0x04;
	public static byte byte_CMD_PROCsale_tng = 0x22;
	private static String TRX_DT_ENTRY;
	private static String TXNTYPE;
	private static String TXNDATETIME;
	private static String DRCR;
	private static String PURSEFLAG;
	private static String TXNAMT;
	private static String AFTERBAL;
	private static String CARDISUER;
	private static String MACHINECODE;
	private static String OBU;
	private static String ENTRYSPID;
	private static String SIGNATUREVER;
	private static String SIGNATURE;
	private static String CORPORATEFLAG;
	private static String USERCAT;
	private static String CSCTYPE;
	private static String ENTRYLOC;
	private static String LASTREFILLMACHINECODE;
	private static String APPSECTOR;
	private static String COMMA;
	private static String TERMINALID;
	private static String PLAZAID;
	private static String LANEID;
	private static String ENTRYDATETIME;
	
	public static byte [] bytSND_EN_compiled() {
    	pkByteData.bytdata_EN_RDR = pkvar.DateString_toByte(pkvar.tarikh());
		byte[] bytsnd_EN_RDR = pkvar.byteSENDbuild(pkByteData.bytdata_EN_RDR,(byte) pkByteData.byte_SEQ_EN_RDR, (byte) pkByteData.byte_CMD_EN_RDR);
		return bytsnd_EN_RDR;
	}
	
	public static byte [] bytSND_reboot() {
    	pkByteData.bytdata_EN_RDR = pkvar.DateString_toByte(pkvar.tarikh());
		byte[] bytsnd_EN_RDR = pkvar.byteSENDbuild(pkByteData.bytdata_EN_RDR,(byte) pkByteData.byte_SEQ_reboot, (byte) pkByteData.byte_CMD_reboot);
		return bytsnd_EN_RDR;
	}

	public static byte [] bytSND_SETIPcompiled(byte [] data) {
//		byte [] fare2 = pkvar.FareDouble_tobyte_ext(Fare);
		byte[] bytsnd  = pkvar.byteSENDbuild(data,(byte) pkByteData.byte_SEQ_EN_SETIP, (byte) pkByteData.byte_CMD_EN_SETIP);
		return bytsnd;
	}
	
	public static byte [] bytSND_GETIP_compiled() {
		byte[] bytsnd  = pkvar.byteSENDbuild((byte) pkByteData.byte_SEQ_EN_GETIP, (byte) pkByteData.byte_CMD_EN_GETIP);
		return bytsnd;
	}
	
	public static byte [] bytSND_INIT_ENT_compiled() {
		byte[] bytsnd  = pkvar.byteSENDbuild((byte) pkByteData.byte_SEQ_INITent_tng, (byte) pkByteData.byte_CMD_INITent_tng);
		return bytsnd;
	}
	public static byte [] bytSND_PROC_ENT_compiled() {
		byte[] byt_data_sial = {0x00};
		byte[] bytsnd  = pkvar.byteSENDbuild(byt_data_sial,(byte) pkByteData.byte_SEQ_PROCent_tng, (byte) pkByteData.byte_CMD_PROCent_tng);
		return bytsnd;
	}
	
	public static byte [] bytSND_INIT_EXT_compiled() {
		byte[] bytsnd  = pkvar.byteSENDbuild((byte) pkByteData.byte_SEQ_INIText_tng, (byte) pkByteData.byte_CMD_INIText_tng);
		return bytsnd;
	}
	
	public static byte [] bytSND_PROC_EXT_compiled(double Fare) {
		System.out.println("****************************");
		System.out.println(">> GET FARE float : "+(Fare));
		System.out.println("****************************");
		byte [] fare2 = pkvar.FareDouble_tobyte_ext(Fare);
		byte[] bytsnd  = pkvar.byteSENDbuild(fare2,(byte) pkByteData.byte_SEQ_PROCext_tng, (byte) pkByteData.byte_CMD_PROCext_tng);
		return bytsnd;
	}
	
	public static byte [] bytSND_INIT_SALE_compiled(double Fare) {
		byte [] fare2 = pkvar.FareDouble_tobyte(Fare);
		byte[] bytsnd  = pkvar.byteSENDbuildSale(fare2,(byte) pkByteData.byte_SEQ_INITsale_tng, (byte) pkByteData.byte_CMD_INITsale_tng);
		return bytsnd;
	}
	
	public static byte [] bytSND_PROC_SALE_compiled() {
//		byte[] byt_data_sial = {0x00};
		byte[] bytsnd  = pkvar.byteSENDbuildsale((byte) pkByteData.byte_SEQ_PROCsale_tng, (byte) pkByteData.byte_CMD_PROCsale_tng);
		return bytsnd;
	}
	
	//-----------------recev--
	
	public static String processCARD(byte data, byte[] datacard) {
		String ret = "### EMV ### ";
		
		try {
//			System.out.println("ret exit :"+data);
			if(data!= -112) {
				CARDTYPE = "EMV";
				ret= "EMV";
				pkconfig.paymenttype = "VSA";
				detaisplitlEMV_init_exit(datacard);
			
			 				
				
			}else {
				
				if(datacard[13] == -112) {
					VALIDCARD = 0;
					//[]2, 0, 50, 3, 0, 30, 30, 30, 30, 30, 30, 30, 30, ffffff90, --example
					log.info(" ### [TNG] prevent antipass back ##");
					pkvar.logtxt(" ##[TNG] prevent antipass back ##");
					return "TNG";
				 }else {
					CARDTYPE = "TNG";
					
					       //EXIT--Verify CSC
						   String result_csc ="false";
		        		   System.out.println("check bzrule [API]["+pkvar.verifycscurl+"] CARD TNG [EXIT] request >>> "+pkvar.tarikh());
		        		   detaisplitlTNG_init_exit(datacard);
		        		   result_csc = pkByteData.verifycsc_url(pkvar.verifycscurl,Arrays.toString(pkvar.Bytearray_toStringarray(datacard)),MFGNO,CARDBALANCE,CARDTRXNO,map_tnginitinfo);
								
                        										
		        		   if(result_csc.equals("false")) {
		        				VALIDCARD = 0;
		        				return "TNG";
		        		   }else {
		        			   // detaisplitlTNG_init_exit(datacard);  
		        		   }

					
				 }
			
				
				ret= "TNG";
			}
			
		}catch(Exception babi) {
			babi.printStackTrace();
			
		}
		
		return ret;
	}
	
//	public static void sndByte_proceedsale() {
//		//--set byte to init sale--tweakking
//		pkByteData.byte_SEQ_INIText_tng=0x05;
//		pkByteData.byte_CMD_INIText_tng=0x21;
//		//--set byte to proceed sale ---
//		pkByteData.byte_SEQ_PROCext_tng=0x06;
//		pkByteData.byte_CMD_PROCext_tng=0x22;
//		//----------------------------------
//	}
//	
//	public static void sndByte_proceedexit() {
//		//--set byte to init exit
//		pkByteData.byte_SEQ_INIText_tng=0x03;
//		pkByteData.byte_CMD_INIText_tng=0x04;
//		//--set byte to proceed exit ---
//		pkByteData.byte_SEQ_PROCext_tng=0x04;
//		pkByteData.byte_CMD_PROCext_tng=0x14;
//		//----------------------------------
//	}
	
	public static int check_ENTRYID(byte[] data) {
//		String entryID="0";
		int entID=0;
		try {
//		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
//		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
//		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
			 byte [] entryid = new byte[13];
      	     int c=0;
             for(int s = 5;s<13;s++) {
//          	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//          	  ENTRY_ID += String.format("%02X",data[s]);
           
          	   entryid[c] = data[s];
          	   entID+=entryid[c];
          	  c++;
             }
             ENTRY_ID = new String(entryid);
//             if(ENTRY_ID.getBytes()) {
            	 System.out.println("int ENTRY_ID : "+entID);
            	 System.out.println("ENTRY_ID : "+ENTRY_ID);
//             }else {
//            	 System.out.println("Not EMpty ENTRY_ID : "+ENTRY_ID+", lenght :"+ENTRY_ID.length());
//             }
//             System.exit(0);
//             ENTRY_ID;
//             return entID;
		}catch(Exception v) {
			v.printStackTrace();
//			return entID;
		}
	    return entID;
		}
	
	public static String detaisplitlEMV_init_exit(byte[] data) {
        String ret = "";
        //command type = 03
//		NumberFormat nfm_byte = new DecimalFormat("00");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	 RRN="";
        	 HASHPAN="";
        	 byte [] entryid = new byte[13];
      	     int c=0;
             for(int s = 5;s<13;s++) {
//          	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//          	  ENTRY_ID += String.format("%02X",data[s]);
          	  entryid[c] = data[s];
          	  c++;
             }
               ENTRY_ID = new String(entryid);
               ENTRY_ID = ENTRY_ID.substring(0, 8);

               
               Date dts = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
               ENTRY_DATE = dreformat.format(dts);  
//              for(int y = 17;y<21;y++) {
//           	  String nm = String.format("%02X",data[y]);
//             	  ENTRY_TIME += nfm_byte.format(nm);  
//                }
               Date tms = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
               ENTRY_TIME = tf.format(tms);
               
               //--simulate date ---
//                int YY=24,MM=16,DD=39;
//                int hh=8,mm=8,ss=37;
//               //--end simulate--------
//                Date dt = df.parse(String.format("%02X",YY)+"-"+String.format("%02X",MM)+"-"+String.format("%02X",DD));
//                ENTRY_DATE = dreformat.format(dt);  
//
//                Date tm = tf.parse(String.format("%02X",hh)+":"+String.format("%02X",mm)+":"+String.format("%02X",ss));
//                ENTRY_TIME = tf.format(tm);
             
               byte [] exitid = new byte[13];
         	   c=0;
               for(int s = 19;s<27;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//            	  ENTRY_ID += String.format("%02X",data[s]);
            	   exitid[c] = data[s];
            	  c++;
               }
               EXIT_ID = new String(exitid);
               EXIT_ID = EXIT_ID.substring(0, 8);
         

                Date extdt = df.parse(String.format("%02X",data[27])+String.format("%02X",data[28])+String.format("%02X",data[29]));
                EXIT_DATE = dreformat.format(extdt);  
//               for(int y = 17;y<21;y++) {
//            	  String nm = String.format("%02X",data[y]);
//              	  ENTRY_TIME += nfm_byte.format(nm);  
//                 }
                Date exttm = tf.parse(String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32]));
                EXIT_TIME = tf.format(exttm);
               //B=bank
                AMNTCHARGEdbl = calculateFARE( ENTRY_DATE+ ENTRY_TIME, EXIT_DATE+EXIT_TIME,"B");
                AMNTCHARGE = String.valueOf(AMNTCHARGEdbl);
//                NumberFormat nf = new DecimalFormat("000000.00");
//                AMNTCHARGE =  Double.toString(calculateFARE( ENTRY_DATE+ENTRY_TIME, EXIT_DATE+EXIT_TIME));
                System.out.println("  AMNTCHARGE =  === "+AMNTCHARGE+",,AMNTCHARGEdbl :"+AMNTCHARGEdbl);
//                AMNTCHARGE = nf.format(AMNTCHARGE); 
                FARERATE = String.valueOf(FARErate());
                
//                int dat[] = pkvar.bytetoINTVALUE(data);
                
//                for(int x=0;x<12;x++) {
//                	MASKPAN +=  String.format("%02d",String.format("%02X",dat[x+19]));
//                	
//                }
//                String [] str =  pkvar.Bytearray_toStringarray(dat);
                MASKPAN = String.format("%02X",data[33])+String.format("%02X",data[34] & 0xff)+String.format("%02X",data[35])+"000"+String.format("%02X",data[39])+String.format("%02X",data[40])+"0000";
//                System.out.println("MASKPAN : "+String.format("%02X",data[19] & 0xff )+Integer.toString(data[20]));
//        	     MASKPAN =String.format("%02d",String.format("%02X",data[21]))+String.format("%02d",String.format("%02X",data[22]));
//        	    		 String.format("%02d",String.format("%02X",data[23]))+String.format("%02d",String.format("%02X",data[24]))+String.format("%02d",String.format("%02X",data[25]))+
//        	    		 String.format("%02d",String.format("%02X",data[26]))+String.format("%02d",String.format("%02X",data[27]))+String.format("%02d",String.format("%02X",data[28]));
        	
            
        	 if(pkByteData.NO_ENTRY_INFO.equals("true")) {
        		 for(int x =29;x<69;x++) {
                	 	HASHPAN += String.valueOf(pkvar.IntValToAsciiChar(data[x]));
                	  } 
        	 }else {
        		  for(int x =43;x<83;x++) {
               	 	HASHPAN += String.valueOf(pkvar.IntValToAsciiChar(data[x]));
               	  } 
        	 }
           
       
        	
        }catch(Exception kosial) {
        	kosial.printStackTrace();
        }
        
        
		return ret;
	}
	
	
	public static String detaisplitlEMV_proc_exit(byte[] data) {
        String ret = "";
//		NumberFormat nfm_byte = new DecimalFormat("00");
        //command type = 04
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	 RRN="";
        	 HASHPAN="";
        	 byte [] entryid = new byte[13];
      	     int c=0;
             for(int s = 5;s<13;s++) {
//          	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//          	  ENTRY_ID += String.format("%02X",data[s]);
          	  entryid[c] = data[s];
          	  c++;
             }
               ENTRY_ID = new String(entryid);
               ENTRY_ID = ENTRY_ID.substring(0, 8);
//               System.out.println("--- > collect info ---> "+pkvar.IntValToAsciiChar(data[5])+pkvar.IntValToAsciiChar(data[6])+pkvar.IntValToAsciiChar(data[7])+pkvar.IntValToAsciiChar(data[8])+pkvar.IntValToAsciiChar(data[9]));
//               for(int w = 13;w<16;w++) {
//            	   String nm = String.format("%02X",data[w]);
//             	  ENTRY_DATE += nfm_byte.format(nm);  
//                }
               
//               Date dts = df.parse(String.format("%02X",data[27])+String.format("%02X",data[28])+String.format("%02X",data[29])); //reader live
               Date dts = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15])); //reader opis
               ENTRY_DATE = dreformat.format(dts);  
//              for(int y = 17;y<21;y++) {
//           	  String nm = String.format("%02X",data[y]);
//             	  ENTRY_TIME += nfm_byte.format(nm);  
//                }
               Date tms = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18])); //reader opis
//               Date tms = tf.parse(String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32])); //reader live
               ENTRY_TIME = tf.format(tms);
               
               //--simulate date ---
//                int YY=24,MM=16,DD=39;
//                int hh=8,mm=8,ss=37;
//               //--end simulate--------
//                Date dt = df.parse(String.format("%02X",YY)+"-"+String.format("%02X",MM)+"-"+String.format("%02X",DD));
//                ENTRY_DATE = dreformat.format(dt);  
//
//                Date tm = tf.parse(String.format("%02X",hh)+":"+String.format("%02X",mm)+":"+String.format("%02X",ss));
//                ENTRY_TIME = tf.format(tm);

//            yg asal   MASKPAN = String.format("%02X",data[33])+String.format("%02X",data[34] & 0xff)+String.format("%02X",data[35])+"000"+String.format("%02X",data[39])+String.format("%02X",data[40])+"0000"; 
               MASKPAN = String.format("%02X",data[19])+String.format("%02X",data[20] & 0xff)+String.format("%02X",data[21])+"000"+String.format("%02X",data[25])+String.format("%02X",data[26]& 0xff);
 	
        	 for(int x =27;x<67;x++) {
        	 	HASHPAN += String.valueOf(pkvar.IntValToAsciiChar(data[x]));
        	  }
        	  
        	 //--disable coz justine update frmware--
//        	 for(int s = 83;s<95;s++) {
        	 for(int s = 83;s<92;s++) {
//           	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
           	  RRN += String.format("%02X",data[s]);
//           	System.out.println(ENTRY_ID);
              }
        	
//        	 STAN = String.format("%02X",data[95]) +String.format("%02X",data[96]) +String.format("%02X",data[97]);
//       
//        	 APPR_CODE = String.valueOf(pkvar.IntValToAsciiChar(data[98])) +String.valueOf(pkvar.IntValToAsciiChar(data[99]))  +String.valueOf(pkvar.IntValToAsciiChar(data[100])) +
//        			 String.valueOf(pkvar.IntValToAsciiChar(data[101])) +String.valueOf(pkvar.IntValToAsciiChar(data[102])) +String.valueOf(pkvar.IntValToAsciiChar(data[103])) ;
        	 
        	 //-------------------------------
        	
        }catch(Exception kosial) {
        	kosial.printStackTrace();
        }
        
        
		return ret;
	}
	
	//-----TNG INIT EXIT----------------------------------------------------------------
	public static String detaisplitlTNG_init_exit(byte[] data) {
        String ret = "";
//		NumberFormat nfm_byte = new DecimalFormat("00");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	 byte [] entryid = new byte[13];
      	   int c=0;
             for(int s = 5;s<13;s++) {
//          	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//          	  ENTRY_ID += String.format("%02X",data[s]);
          	  entryid[c] = data[s];
          	  c++;
             }
               ENTRY_ID = new String(entryid);

               
               Date dts = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
               ENTRY_DATE = dreformat.format(dts);  
//              for(int y = 17;y<21;y++) {
//           	  String nm = String.format("%02X",data[y]);
//             	  ENTRY_TIME += nfm_byte.format(nm);  
//                }
               Date tms = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
               ENTRY_TIME = tf.format(tms);
               
               //--simulate date ---
//                int YY=24,MM=16,DD=39;
//                int hh=8,mm=8,ss=37;
//               //--end simulate--------
//                Date dt = df.parse(String.format("%02X",YY)+"-"+String.format("%02X",MM)+"-"+String.format("%02X",DD));
//                ENTRY_DATE = dreformat.format(dt);  
//
//                Date tm = tf.parse(String.format("%02X",hh)+":"+String.format("%02X",mm)+":"+String.format("%02X",ss));
//                ENTRY_TIME = tf.format(tm);
             
               byte [] exitid = new byte[13];
         	   c=0;
               for(int s = 19;s<27;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//            	  ENTRY_ID += String.format("%02X",data[s]);
            	   exitid[c] = data[s];
            	  c++;
               }
               EXIT_ID = new String(exitid);
         

                Date extdt = df.parse(String.format("%02X",data[27])+String.format("%02X",data[28])+String.format("%02X",data[29]));
                EXIT_DATE = dreformat.format(extdt);  
//               for(int y = 17;y<21;y++) {
//            	  String nm = String.format("%02X",data[y]);
//              	  ENTRY_TIME += nfm_byte.format(nm);  
//                 }
                Date exttm = tf.parse(String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32]));
                EXIT_TIME = tf.format(exttm);
             
                //T=tng
//                AMNTCHARGEdbl = calculateFARE( ENTRY_DATE+ ENTRY_TIME, EXIT_DATE+EXIT_TIME,"T");
                
//                NumberFormat nf = new DecimalFormat("000000.00");
//                AMNTCHARGE = String.valueOf(AMNTCHARGEdbl);
//                AMNTCHARGE =  Double.toString(calculateFARE( ENTRY_DATE+ENTRY_TIME, EXIT_DATE+EXIT_TIME,"T"));
//                AMNTCHARGE = nf.format(AMNTCHARGE); 
                FARERATE = String.valueOf(FARErate());
                
//                int dat[] = pkvar.bytetoINTVALUE(data);

//                String crdno = String.format("%02X",data[36])+String.format("%02X",data[37])+String.format("%02X",data[38])+String.format("%02X",data[39])+String.format("%02X",data[40]);
//                String mfgstr = String.format("%02X", data[36])+String.format("%02X",data[37])+String.format("%02X",data[38])+String.format("%02X",data[39])+String.format("%02X",data[40]);
                //                long cdno = Long.parseLong(crdno,16);
//                 CARDNO = nfn_card.format(cdno);
//                String blc= String.format("%02X", data[43])+String.format("%02X",data[44])+String.format("%02X",data[45])+String.format("%02X",data[46])+String.format("%02X",data[47])+"."+String.format("%02X",data[48]);
                MFGNO = new String(data,87,10);
                CARDTRXNO = new String(data,97,6);
                CARDBALANCE = new String(data,131,11);
                CARDBALANCE=CARDBALANCE.replace("+", "");
                ENTRYDATETIME=ENTRY_DATE+ENTRY_TIME;
            	map_tnginitinfo.put("ENTRYDATETIME", ENTRYDATETIME);
                
            	System.out.println("** CARDBALANCE ** "+CARDBALANCE);
            	System.out.println("CARDTRXNO >>>>> "+CARDTRXNO);
            	System.out.println("MFGNO >>>>> "+MFGNO);
                
            	System.out.println("mfgno >>>> "+new String(data,87,10));
				
				System.out.println("cardtxnno >>>> "+new String(data,97,6));
		    				
				System.out.println("txntype >>>>> "+new String(data,103,2));
			    TXNTYPE = new String(data,103,2);
				map_tnginitinfo.put("TXNTYPE", TXNTYPE);
				
				System.out.println("txndatetime >>>>> "+new String(data,105,14));
			    TXNDATETIME = new String(data,105,14);
				map_tnginitinfo.put("TXNDATETIME", TXNDATETIME);
				
				System.out.println("drcr >>>>> "+new String(data,119,1));
			    DRCR = new String(data,119,1);
			    map_tnginitinfo.put("DRCR", DRCR);
				
				System.out.println("purseflag >>>>> "+new String(data,120,1));
				PURSEFLAG = new String(data,120,1);
				map_tnginitinfo.put("PURSEFLAG", PURSEFLAG);
				
				System.out.println("txnamt >>>>> "+new String(data,121,10));
			    TXNAMT = new String(data,121,10);
			    map_tnginitinfo.put("TXNAMT", TXNAMT);
				
				System.out.println("afterbal >>>>> "+new String(data,131,11));
			    AFTERBAL = new String(data,131,11);
			    map_tnginitinfo.put("AFTERBAL", AFTERBAL);
				
				System.out.println("cardisuer >>>>> "+new String(data,142,2));
			    CARDISUER = new String(data,142,2);
			    map_tnginitinfo.put("CARDISUER", CARDISUER);
				
				System.out.println("machinecode >>>>> "+new String(data,144,8));
				MACHINECODE = new String(data,144,8);
				map_tnginitinfo.put("MACHINECODE", MACHINECODE);
				

				System.out.println("obu >>>>> "+new String(data,152,8));
			    OBU = new String(data,152,8);
			    map_tnginitinfo.put("OBU", OBU);
				
				System.out.println("entryspid >>>>> "+new String(data,160,2));
		        ENTRYSPID = new String(data,160,2);
		        map_tnginitinfo.put("ENTRYSPID", ENTRYSPID);
				
				
				System.out.println("signaturever >>>> "+new String(data,162,3));
		        SIGNATUREVER = new String(data,162,3);
		        map_tnginitinfo.put("SIGNATUREVER", SIGNATUREVER);
				
				System.out.println("signature >>>> "+new String(data,165,16));
				SIGNATURE = new String(data,165,16);
				map_tnginitinfo.put("SIGNATURE", SIGNATURE);
			
				
				System.out.println("cardno >>>>> "+new String(data,181,18));
				CARDNO = new String(data,181,18);
				map_tnginitinfo.put("CARDNO", CARDNO);
		
				
				System.out.println("corporateaflag >>>> "+new String(data,199,1));
				CORPORATEFLAG = new String(data,199,1);
				map_tnginitinfo.put("CORPORATEFLAG", CORPORATEFLAG);
			
				
				System.out.println("usercat >>>>> "+new String(data,200,1));
			    USERCAT = new String(data,200,1);
			    map_tnginitinfo.put("USERCAT", USERCAT);
				
				System.out.println("csctype >>>> "+new String(data,201,1));
				CSCTYPE = new String(data,201,1);
				map_tnginitinfo.put("CSCTYPE", CSCTYPE);
				
				System.out.println("entryloc >>>> "+new String(data,202,3));
				ENTRYLOC = new String(data,202,3);
				map_tnginitinfo.put("ENTRYLOC", ENTRYLOC);
				
				
				System.out.println("lastrefillmachinecode >>>> "+new String(data,205,8));
			    LASTREFILLMACHINECODE=new String(data,205,8);
			    map_tnginitinfo.put("LASTREFILLMACHINECODE", LASTREFILLMACHINECODE);
				
				System.out.println("appsector >>>> "+new String(data,213,2));
			    APPSECTOR = new String(data,213,2);
			    map_tnginitinfo.put("APPSECTOR", APPSECTOR);
				
				System.out.println("comma >>>> "+new String(data,215,1));
				COMMA = new String(data,215,1);
				map_tnginitinfo.put("COMMA", COMMA);
				
				System.out.println("terminalid >>>> "+new String(data,216,3));
			    TERMINALID = new String(data,216,3);
			    map_tnginitinfo.put("TERMINALID", TERMINALID);
			    
			    PLAZAID = pkconfig.jReadConfig("PLAZAID");
			    System.out.println("** PLAZAID ** "+PLAZAID);
			    map_tnginitinfo.put("PLAZAID", PLAZAID);
			    
			    LANEID = pkconfig.jReadConfig("LANEID");
			    System.out.println("** LANEID ** "+LANEID);
			    map_tnginitinfo.put("LANEID", LANEID);

       
        	
        }catch(Exception kosial) {
//        	pkbytedata.SEQUENCE_PROC=3;
        	kosial.printStackTrace();
         

         	
        }
        
        
		return ret;
	}
	
	//---TNG INIT ENTRY
	public static String detaisplitlTNG_init_entry(byte[] data) {
        String ret = "";
//		NumberFormat nfm_byte = new DecimalFormat("00");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	 byte [] entryid = new byte[13];
      	   int c=0;
             for(int s = 5;s<13;s++) {
//          	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//          	  ENTRY_ID += String.format("%02X",data[s]);
          	  entryid[c] = data[s];
          	  c++;
             }
               ENTRY_ID = new String(entryid);

               
               Date dts = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
               ENTRY_DATE = dreformat.format(dts);  
//              for(int y = 17;y<21;y++) {
//           	  String nm = String.format("%02X",data[y]);
//             	  ENTRY_TIME += nfm_byte.format(nm);  
//                }
               Date tms = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
               ENTRY_TIME = tf.format(tms);
               
               //--simulate date ---
//                int YY=24,MM=16,DD=39;
//                int hh=8,mm=8,ss=37;
//               //--end simulate--------
//                Date dt = df.parse(String.format("%02X",YY)+"-"+String.format("%02X",MM)+"-"+String.format("%02X",DD));
//                ENTRY_DATE = dreformat.format(dt);  
//
//                Date tm = tf.parse(String.format("%02X",hh)+":"+String.format("%02X",mm)+":"+String.format("%02X",ss));
//                ENTRY_TIME = tf.format(tm);
             
               byte [] exitid = new byte[13];
         	   c=0;
               for(int s = 19;s<27;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//            	  ENTRY_ID += String.format("%02X",data[s]);
            	   exitid[c] = data[s];
            	  c++;
               }
               EXIT_ID = new String(exitid);
         

                Date extdt = df.parse(String.format("%02X",data[27])+String.format("%02X",data[28])+String.format("%02X",data[29]));
                EXIT_DATE = dreformat.format(extdt);  
//               for(int y = 17;y<21;y++) {
//            	  String nm = String.format("%02X",data[y]);
//              	  ENTRY_TIME += nfm_byte.format(nm);  
//                 }
                Date exttm = tf.parse(String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32]));
                EXIT_TIME = tf.format(exttm);
             
                //T=tng
//                AMNTCHARGEdbl = calculateFARE( ENTRY_DATE+ ENTRY_TIME, EXIT_DATE+EXIT_TIME,"T");
                
//                NumberFormat nf = new DecimalFormat("000000.00");
//                AMNTCHARGE = String.valueOf(AMNTCHARGEdbl);
//                AMNTCHARGE =  Double.toString(calculateFARE( ENTRY_DATE+ENTRY_TIME, EXIT_DATE+EXIT_TIME,"T"));
//                AMNTCHARGE = nf.format(AMNTCHARGE); 
                FARERATE = String.valueOf(FARErate());
                
//                int dat[] = pkvar.bytetoINTVALUE(data);

//                String crdno = String.format("%02X",data[36])+String.format("%02X",data[37])+String.format("%02X",data[38])+String.format("%02X",data[39])+String.format("%02X",data[40]);
//                String mfgstr = String.format("%02X", data[36])+String.format("%02X",data[37])+String.format("%02X",data[38])+String.format("%02X",data[39])+String.format("%02X",data[40]);
                //                long cdno = Long.parseLong(crdno,16);
//                 CARDNO = nfn_card.format(cdno);
//                String blc= String.format("%02X", data[43])+String.format("%02X",data[44])+String.format("%02X",data[45])+String.format("%02X",data[46])+String.format("%02X",data[47])+"."+String.format("%02X",data[48]);
                MFGNO = new String(data,73,10);
                CARDTRXNO = new String(data,83,6);
                CARDBALANCE = new String(data,117,11);
                CARDBALANCE=CARDBALANCE.replace("+", "");
                ENTRYDATETIME=ENTRY_DATE+ENTRY_TIME;
            	map_tnginitinfo.put("ENTRYDATETIME", ENTRYDATETIME);
                
            	System.out.println("** CARDBALANCE ** "+CARDBALANCE);
            	System.out.println("CARDTRXNO >>>>> "+CARDTRXNO);
            	System.out.println("MFGNO >>>>> "+MFGNO);
                
            	System.out.println("mfgno >>>> "+new String(data,73,10));
				
				System.out.println("cardtxnno >>>> "+new String(data,83,6));
		
				
				System.out.println("txntype >>>>> "+new String(data,89,2));
				TXNTYPE = new String(data,89,2);
				map_tnginitinfo.put("TXNTYPE", TXNTYPE);
				
				System.out.println("txndatetime >>>>> "+new String(data,91,14));
				TXNDATETIME = new String(data,91,14);
				map_tnginitinfo.put("TXNDATETIME", TXNDATETIME);
				
				System.out.println("drcr >>>>> "+new String(data,105,1));
			    DRCR = new String(data,105,1);
				map_tnginitinfo.put("DRCR", DRCR);
				
				System.out.println("purseflag >>>>> "+new String(data,106,1));
				PURSEFLAG = new String(data,106,1);
				map_tnginitinfo.put("PURSEFLAG", PURSEFLAG);
				
				System.out.println("txnamt >>>>> "+new String(data,107,10));
				TXNAMT = new String(data,107,10);
				map_tnginitinfo.put("TXNAMT", TXNAMT);
				
			
				
				System.out.println("afterbal >>>>> "+new String(data,117,11));
			    AFTERBAL = new String(data,117,11);
			    map_tnginitinfo.put("AFTERBAL", AFTERBAL);
				
				System.out.println("cardisuer >>>>> "+new String(data,128,2));
			    CARDISUER = new String(data,128,2);
			    map_tnginitinfo.put("CARDISUER", CARDISUER);
				
				System.out.println("machinecode >>>>> "+new String(data,130,8));
				MACHINECODE = new String(data,130,8);
				map_tnginitinfo.put("MACHINECODE", MACHINECODE);
				

				System.out.println("obu >>>>> "+new String(data,138,8));
			    OBU = new String(data,138,8);
				map_tnginitinfo.put("OBU", OBU);
				
				System.out.println("entryspid >>>>> "+new String(data,146,2));
		        ENTRYSPID = new String(data,146,2);
		    	map_tnginitinfo.put("ENTRYSPID", ENTRYSPID);
				
				
				System.out.println("signaturever >>>> "+new String(data,148,3));
		        SIGNATUREVER = new String(data,148,3);
		        map_tnginitinfo.put("SIGNATUREVER", SIGNATUREVER);
				
				System.out.println("signature >>>> "+new String(data,151,16));
				SIGNATURE = new String(data,151,16);
		        map_tnginitinfo.put("SIGNATURE", SIGNATURE);
			
				
				System.out.println("cardno >>>>> "+new String(data,167,18));
				CARDNO = new String(data,167,18);
				 map_tnginitinfo.put("CARDNO", CARDNO);
		
				
				System.out.println("corporateaflag >>>> "+new String(data,185,1));
				CORPORATEFLAG = new String(data,185,1);
				 map_tnginitinfo.put("CORPORATEFLAG", CORPORATEFLAG);
			
				
				System.out.println("usercat >>>>> "+new String(data,186,1));
			    USERCAT = new String(data,186,1);
			    map_tnginitinfo.put("USERCAT", USERCAT);
				
				System.out.println("csctype >>>> "+new String(data,187,1));
				CSCTYPE = new String(data,187,1);
				 map_tnginitinfo.put("CSCTYPE", CSCTYPE);
				
				System.out.println("entryloc >>>> "+new String(data,188,3));
				ENTRYLOC = new String(data,188,3);
				 map_tnginitinfo.put("ENTRYLOC", ENTRYLOC);
				
				
				System.out.println("lastrefillmachinecode >>>> "+new String(data,191,8));
			    LASTREFILLMACHINECODE=new String(data,191,8);
				 map_tnginitinfo.put("LASTREFILLMACHINECODE", LASTREFILLMACHINECODE);
				
				System.out.println("appsector >>>> "+new String(data,199,2));
			    APPSECTOR = new String(data,199,2);
			    map_tnginitinfo.put("APPSECTOR", APPSECTOR);
				
				System.out.println("comma >>>> "+new String(data,201,1));
				COMMA = new String(data,201,1);
				map_tnginitinfo.put("COMMA", COMMA);
				
				System.out.println("terminalid >>>> "+new String(data,202,3));
			    TERMINALID = new String(data,202,3);
				map_tnginitinfo.put("TERMINALID", TERMINALID);
			    
			    PLAZAID = pkconfig.jReadConfig("PLAZAID");
			    System.out.println("** PLAZAID ** "+PLAZAID);
				map_tnginitinfo.put("PLAZAID", PLAZAID);
			    LANEID = pkconfig.jReadConfig("LANEID");
			    System.out.println("** LANEID ** "+LANEID);
			    map_tnginitinfo.put("LANEID", LANEID);
			
      
        	
        }catch(Exception kosial) {
//        	pkbytedata.SEQUENCE_PROC=3;
        	kosial.printStackTrace();
         

         	
        }
        
        
		return ret;
	}
	public static String printinfoEMV_init_exit() {
		String ret = "notok";
		try {
		if(pkvar.readerpath.equals("exit")) {
			int boj = pkboj.BOJcount();
			System.out.println(" **************** card type [ "+CARDTYPE+" ] INIT-EXIT*************** ");
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			
			System.out.println("EXIT_ID : "+ EXIT_ID);
			System.out.println("EXIT_DATE : "+ EXIT_DATE);
			System.out.println("EXIT_TIME : "+ EXIT_TIME);
			System.out.println("MASKPAN : "+ MASKPAN);
			System.out.println("HASHPAN : "+ HASHPAN);
			System.out.println("AMNTCHARGE : "+ AMNTCHARGE);
			System.out.println("FARErate : "+ FARERATE);
			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyymmdd
			System.out.println("datebojtime : "+ pkconfig.datebojtime); //HH:mm:ss
			System.out.println("Operationaldate : "+ pkvar.tarikhoperationaldate());//yyyyymmdd
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("jobno : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
//			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			System.out.println("RESP_CODE : "+ pkconfig.RESP_CODE);
			System.out.println(" ******************************* ");
			
			TRX_DT = pkvar.TRX_DTyymmddHHMMss(EXIT_DATE+EXIT_TIME);//yyMMddHHmmss
			TRX_ID = pkvar.TRX_ID();
			
		
//			boj.sial.pktmr_resetboj.BOJcount +=1;

			//--init exit
			String encodejson = pkjson.jsonENCODE_EMV(String.valueOf(TOMINUTE),CARDTYPE,"000000000000000", EXIT_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
				     "NOAPP_CODE", pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
				     String.valueOf(boj),pkconfig.datebojdate, pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , "01",pkconfig.readerpath,
				     "N",pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid, 
				     pkconfig.exitlocation,  pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
				     pkconfig.farelocation,  AMNTCHARGE, AMNTCHARGE,"000000", "000000", AMNTCHARGE, "000000", "A0000A", "D", "9", "123","norrn","nostan","1",pkconfig.RESP_CODE,TRX_DT_ENTRY,ENTRY_ID,EXIT_ID);
			

			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);//log only_if not success exit______________________________________
			pkvar.cardinfojson = encodejson;
			pkvar.logtxtjson(encodejson);
			
			
//			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
			HASHPAN="";
			EXIT_ID="";
			ENTRY_ID="";
			
			
	    	}
		} catch(Exception v) {
			ret = "notok";
			v.printStackTrace();
		}
		return ret;
		
	}
	public static void printinfoEMV_proc_exit() {
		if(pkvar.readerpath.equals("exit")) {
			int boj = pkboj.BOJcount();
			TRX_DT = pkvar.TRX_DTyymmddHHMMss(EXIT_DATE+EXIT_TIME);//yyMMddHHmmss
			TRX_DT_ENTRY = pkvar.TRX_DTyymmddHHMMss(ENTRY_DATE+ENTRY_TIME);//yyMMddHHmmss
			
			TRX_ID = pkvar.TRX_ID();
			
			if(pkByteData.NO_ENTRY_INFO.equals("true")) {
				System.out.println(" ***[ ***penalti ***] card type EMV [ "+CARDTYPE+" ] PROCEED-EXIT *** ");
				EXIT_DATE=ENTRY_DATE;
				EXIT_TIME=ENTRY_TIME;
				AMNTCHARGEdbl=pkByteData.PENALTI_AMNT;
				AMNTCHARGE=String.valueOf(pkByteData.PENALTI_AMNT);
				System.out.println("Penalti AMNTCHARGE-dbl : "+AMNTCHARGEdbl);
				System.out.println("Penalti AMNTCHARGE : "+AMNTCHARGE);
				System.out.println("ENTRY_ID : "+ ENTRY_ID);
				System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
				System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
				
				System.out.println("EXIT_ID : "+ EXIT_ID);
				System.out.println("EXIT_DATE : "+ EXIT_DATE);
				System.out.println("EXIT_TIME : "+ EXIT_TIME);
				System.out.println(" *** ********************************************************************** ");
			}
			
			System.out.println(" *** card type EMV [ "+CARDTYPE+" ] PROCEED-EXIT *** ");
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			
			System.out.println("EXIT_ID : "+ EXIT_ID);
			System.out.println("EXIT_DATE : "+ EXIT_DATE);
			System.out.println("EXIT_TIME : "+ EXIT_TIME);
			System.out.println("MASKPAN : "+ MASKPAN);
			System.out.println("HASHPAN : "+ HASHPAN);
			System.out.println("AMNTCHARGE : "+ AMNTCHARGEdbl);
			System.out.println("FARErate : "+ FARERATE);
			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			
			System.out.println(" ************** banckcardinfo ***************** ");
			System.out.println("TRX_ID : "+ TRX_ID);
			System.out.println("TRX_DT : "+ TRX_DT);
			System.out.println("RRN : "+ RRN);
			System.out.println("STAN : "+ STAN);
			System.out.println("APPR_CODE : "+ APPR_CODE);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyymmdd
			System.out.println("datebojtime : "+ pkconfig.datebojtime); //HH:mm:ss
			System.out.println("Operationaldate : "+ pkvar.tarikhoperationaldate());//yyyyymmdd
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("jobno : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
			System.out.println("surcharge : "+ pkvar.surcharge);
			System.out.println("surcharge_enable : "+ pkvar.surcharge_enable);
//			System.out.println("sst : "+ pkvar.sst);
			System.out.println("sst_enable : "+ pkvar.sst_enable);
			   System.out.println("SST : "+pkvar.sst+" %");
			System.out.println(" ******************************* ");
	           System.out.println("surcharge : "+pkvar.surcharge+" %");
	           System.out.println("AMNTCHARGE : "+AMNTCHARGE);
	           System.out.println("BANK_ORIFARE : "+pkconfig.BANK_ORIFARE);
	           System.out.println("BANK_TAXFARE : "+pkconfig.BANK_TAXFARE);
	           System.out.println("BANK_SURCHARGE : "+pkconfig.BANK_SURCHARGE);
	           System.out.println("BANK_TAXSURCHARGE : "+pkconfig.BANK_TAXSURCHARGE);
//			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			System.out.println(" RESP_CODE : "+pkconfig.RESP_CODE);
			System.out.println(" ******************************* ");
	
			
		
//			boj.sial.pktmr_resetboj.BOJcount +=1;
//			pkconfig.data_topush.add(pkconfig.locationno+"|"+pkconfig.terminalno+"|"+pkconfig.jobno+"|"+(boj.sial.pktmr_resetboj.BOJcount)+"|"+(boj.sial.pktmr_resetboj.BOJcount)
//					+"|"+pkconfig.datebojdate+"|000000|"+pkvar.tarikhoperationaldate()+"|"+pkconfig.jobtype+"|"+pkconfig.readerpath
//					+"|"+pkconfig.opermode+"|"+ pkconfig.badgeno+"|"+pkvar.tarikhoperationaldate()+"|"+pkvar.tarikhoperationaltime()+"|"
//					+ pkconfig.trxtype+"|"+ pkconfig.exitoperatorid+"|"+ pkconfig.exitlocation+"|"+ pkconfig.exitterminalno+"|"+ pkconfig.exittcclass+"|"+ pkconfig.exitdetectclass+"|"
//					+ EXIT_DATE+"|"+EXIT_TIME+"|000|"+AMNTCHARGE+"|0.00|0.00|0.00|0.00|0.00|000000|S|V|000");
			
		
			
//			pkconfig.data_topushdb.add(pkconfig.locationno+"|"+pkconfig.terminalno+"|"+pkconfig.jobno+"|"+(boj.sial.pktmr_resetboj.BOJcount)+"|"+(boj.sial.pktmr_resetboj.BOJcount)
//					+"|"+pkconfig.datebojdate+"|000000|"+pkvar.tarikhoperationaldate()+"|"+pkconfig.jobtype+"|"+pkconfig.readerpath
//					+"|"+pkconfig.opermode+"|"+ pkconfig.badgeno+"|"+pkvar.tarikhoperationaldate()+"|"+pkvar.tarikhoperationaltime()+"|"
//					+ pkconfig.trxtype+"|"+ pkconfig.exitoperatorid
//					+"|"+ pkconfig.exitlocation+"|"+ pkconfig.exitterminalno+"|"+ pkconfig.exittcclass+"|"+ pkconfig.exitdetectclass+"|"
//					+ pkconfig.exitlocation+"|"+ EXIT_DATE+"|"+EXIT_TIME+"|000|"+AMNTCHARGE+"|0.00|0.00|0.00|0.00|0.00|000000|S|V|000");
			String encodejson = "";
			if(pkconfig.SIM_ENABLE.equals("true")) {
			  
			   pkconfig.SIM_AMNT= String.valueOf(AMNTCHARGEdbl);
			   System.out.println(" --- SIMULATION Enable --");
			    encodejson = pkjson.jsonENCODE_EMV(String.valueOf(TOMINUTE),CARDTYPE,"000000000000000",  EXIT_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
						APPR_CODE, pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
					     String.valueOf(boj),pkconfig.datebojdate, pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
					     "N",pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid,  pkconfig.exitlocation,
					     pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
					     pkconfig.farelocation,  String.valueOf(AMNTCHARGEdbl), String.valueOf(AMNTCHARGEdbl),"000000", "000000", String.valueOf(AMNTCHARGEdbl),
					     "000000", "U_KCUF", "D", "9", "123",RRN,STAN,"1",pkconfig.RESP_CODE,pkconfig.SIM_ENABLE,pkconfig.SIM_DATEENTRY,pkconfig.SIM_DATEEXIT,
					     pkconfig.SIM_TRXNO,pkconfig.SIM_AMNT, pkconfig.SIM_PH);
			    pkconfig.SIM_ENABLE="false";
			   
			 }else {
				 encodejson = pkjson.jsonENCODE_EMV(String.valueOf(TOMINUTE),CARDTYPE,"000000000000000",  EXIT_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
							APPR_CODE, pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
						     String.valueOf(boj),pkconfig.datebojdate, pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
						     "N",pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid,  pkconfig.exitlocation,
						     pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
						     pkconfig.farelocation,  String.valueOf(AMNTCHARGEdbl), String.valueOf(AMNTCHARGEdbl),"000000", "000000", String.valueOf(AMNTCHARGEdbl),
						     "000000", "U_KCUF", "D", "9", "123",RRN,STAN,"1",pkconfig.RESP_CODE,TRX_DT_ENTRY,ENTRY_ID,EXIT_ID);
			 }
			
			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);
			pkvar.cardinfojson = encodejson;
			pkvar.logtxtjson(encodejson);
			
			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
			HASHPAN="";
			EXIT_ID="";
			ENTRY_ID="";
		
		}
		
	}

	
	
	public static String detaisplitlTNG_proc_exit(byte[] data) {
        String ret = "";
//		NumberFormat nfm_byte = new DecimalFormat("00");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	 RRN="";
        	 HASHPAN="";
               for(int s = 5;s<13;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
            	  ENTRY_ID += String.format("%02X",data[s]);
//            	System.out.println(ENTRY_ID);
               }
//               System.out.println("--- > collect info ---> "+pkvar.IntValToAsciiChar(data[5])+pkvar.IntValToAsciiChar(data[6])+pkvar.IntValToAsciiChar(data[7])+pkvar.IntValToAsciiChar(data[8])+pkvar.IntValToAsciiChar(data[9]));
//               for(int w = 13;w<16;w++) {
//            	   String nm = String.format("%02X",data[w]);
//             	  ENTRY_DATE += nfm_byte.format(nm);  
//                }
               
               Date dts = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
               ENTRY_DATE = dreformat.format(dts);  
//              for(int y = 17;y<21;y++) {
//           	  String nm = String.format("%02X",data[y]);
//             	  ENTRY_TIME += nfm_byte.format(nm);  
//                }
               Date tms = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
               ENTRY_TIME = tf.format(tms);
               
               //--simulate date ---
//                int YY=24,MM=16,DD=39;
//                int hh=8,mm=8,ss=37;
               //--end simulate--------
//                Date dt = df.parse(String.format("%02X",YY)+String.format("%02X",MM)+String.format("%02X",DD));
//                ENTRY_DATE = dreformat.format(dt);  
//
//                Date tm = tf.parse(String.format("%02X",hh)+String.format("%02X",mm)+String.format("%02X",ss));
//                ENTRY_TIME = tf.format(tm);
             
            
                 
                
                for(int s = 19;s<27;s++) {
//              	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
              	  EXIT_ID += String.format("%02X",data[s]);
//              	System.out.println(ENTRY_ID);
                 }

                Date extdt = df.parse(String.format("%02X",data[27])+String.format("%02X",data[28])+String.format("%02X",data[29]));
                EXIT_DATE = dreformat.format(extdt);  

                Date exttm = tf.parse(String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32]));
                EXIT_TIME = tf.format(exttm);
             
                //T=tng
                AMNTCHARGEdbl = calculateFARE( ENTRY_DATE+ ENTRY_TIME, EXIT_DATE+EXIT_TIME,"T");
                AMNTCHARGE = String.valueOf(AMNTCHARGEdbl);
             
            
//                AMNTCHARGE = nf.format(AMNTCHARGE); 
                FARERATE = String.valueOf(FARErate());
                
        
//                MASKPAN = String.format("%02X",data[33])+String.format("%02X",data[34] & 0xff)+String.format("%02X",data[35])+"000"+String.format("%02X",data[39])+String.format("%02X",data[40])+"00";
         
//               String crdno = String.format("%02X",data[36])+String.format("%02X",data[37])+String.format("%02X",data[38])+String.format("%02X",data[39])+String.format("%02X",data[40]);
//               long cdno = Long.parseLong(crdno,16);
//                CARDNO = nfn_card.format(cdno);
               String mfgstr = String.format("%02X", data[36])+String.format("%02X",data[37])+String.format("%02X",(data[38] & 0xff))+String.format("%02X",data[39])+String.format("%02X",data[40]);
               MFGNO = mfgstr;
//                CARDBALANCE = nfn_blnc.format( String.format("%02X",data[43]))+nfn_blnc.format( String.format("%02X",data[44]))+nfn_blnc.format( String.format("%02X",data[45]))+nfn_blnc.format( String.format("%02X",data[46]))
//                +nfn_blnc.format( String.format("%02X",data[47]))+nfn_blnc.format( String.format("%02X",data[48]));
                
                String blc= String.format("%02X", data[43])+String.format("%02X",data[44])+String.format("%02X",data[45])+String.format("%02X",data[46])+String.format("%02X",data[47])+"."+String.format("%02X",data[48]);
//             	Float crdblc=Float.parseFloat(blc);
			    CARDBALANCE = blc;
			    
			    //---------------------- detail info -----
				System.out.println("mfgno > "+new String(data,108,10));
				map_procexit_tnginfo.put("mfgno", new String(data,108,10));
				System.out.println("cardtxnno > "+new String(data,118,6));
				map_procexit_tnginfo.put("cardtxnno",new String(data,118,6));
				
				System.out.println("txntype > "+new String(data,124,2));
				map_procexit_tnginfo.put("txntype",new String(data,124,2));
				
				System.out.println("txndatetime > "+new String(data,126,14));
				map_procexit_tnginfo.put("txndatetime",new String(data,126,14));
				
				System.out.println("drcr > "+new String(data,140,1));
				map_procexit_tnginfo.put("drcr",new String(data,140,1));
				
				System.out.println("purseflag > "+new String(data,141,1));
				map_procexit_tnginfo.put("purseflag",new String(data,141,1));
				
				System.out.println("txnamt > "+new String(data,142,10));
				map_procexit_tnginfo.put("txnamt",new String(data,142,10));
				
				System.out.println("afterbal > "+new String(data,152,11));
				map_procexit_tnginfo.put("afterbal",new String(data,152,11));
				
				System.out.println("cardisuer > "+new String(data,163,2));
				map_procexit_tnginfo.put("cardisuer",new String(data,163,2));
				
				System.out.println("machinecode > "+new String(data,165,8));
				map_procexit_tnginfo.put("machinecode",new String(data,165,8));
				
				System.out.println("obu > "+new String(data,173,8));
				map_procexit_tnginfo.put("obu",new String(data,173,8));
				
				System.out.println("entryspid > "+new String(data,181,2));
				map_procexit_tnginfo.put("entryspid",new String(data,181,2));
				
				
				System.out.println("signaturever > "+new String(data,183,3));
				map_procexit_tnginfo.put("signaturever",new String(data,183,3));
				
				System.out.println("signature > "+new String(data,186,16));
				map_procexit_tnginfo.put("signature",new String(data,186,16));
				
				System.out.println("cardno > "+new String(data,202,18));
				map_procexit_tnginfo.put("cardno",new String(data,202,18));
				
				System.out.println("corporateaflag > "+new String(data,220,1));
				map_procexit_tnginfo.put("corporateaflag",new String(data,220,1));
				
				System.out.println("usercat > "+new String(data,221,1));
				map_procexit_tnginfo.put("usercat",new String(data,221,1));
				
				System.out.println("csctype > "+new String(data,222,1));
				map_procexit_tnginfo.put("csctype",new String(data,222,1));
				
				System.out.println("entryloc > "+new String(data,223,3));
				map_procexit_tnginfo.put("entryloc",new String(data,223,3));
				
				
				System.out.println("lastrefillmachinecode > "+new String(data,226,8));
				map_procexit_tnginfo.put("lastrefillmachinecode",new String(data,226,8));
				
				System.out.println("appsector > "+new String(data,234,2));
				map_procexit_tnginfo.put("appsector",new String(data,234,2));
				
				System.out.println("comma > "+new String(data,236,1));
				map_procexit_tnginfo.put("comma",new String(data,236,1));
				
				System.out.println("terminalid > "+new String(data,237,3));
				map_procexit_tnginfo.put("terminalid",new String(data,237,3));
			    //--------------------------------------------
        	
        }catch(Exception kosial) {
        	
        	kosial.printStackTrace();
    
        	return "";
        }
        
        
		return ret;
	}
	
	public static String printinfoTNG_proc_exit() {
		String ret = "notok";
		int boj = pkboj.BOJcount();
		try {
		if(pkvar.readerpath.equals("exit")) {
			AMNTCHARGE = String.format("%.2f",((Float.parseFloat(AMNTCHARGE)/100)));
			System.out.println("\n***********TNG ***** card type [ "+CARDTYPE+" ] PROC-EXIT*************** ");
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("MFGNO : "+ MFGNO);
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			System.out.println("CARDBALANCE : "+ Float.parseFloat(CARDBALANCE));
			
			System.out.println("EXIT_ID : "+ EXIT_ID);
			System.out.println("EXIT_DATE : "+ EXIT_DATE);
			System.out.println("EXIT_TIME : "+ EXIT_TIME);
//			System.out.println("MASKPAN : "+ MASKPAN);
//			System.out.println("HASHPAN : "+ HASHPAN);
			System.out.println("AMNTCHARGE : "+ AMNTCHARGE);
			System.out.println("FARErate : "+ FARERATE);
			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyymmdd
			System.out.println("Operationaldate : "+ pkvar.tarikhoperationaldate());//yyyyymmdd
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("jobno : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
			System.out.println("paymenttype : "+ pkconfig.paymenttype);
			System.out.println("RESP_CODE : "+ pkconfig.RESP_CODE);
			System.out.println(" ******************************* ");
			
			TRX_DT = pkvar.TRX_DTyymmddHHMMss(EXIT_DATE+EXIT_TIME);//yyMMddHHmmss
			TRX_ID = pkvar.TRX_ID();
			
			pkconfig.paymenttype=CARDTYPE;
//			boj.sial.pktmr_resetboj.BOJcount +=1;
		
			
			String encodejson = pkjson.jsonENCODE_TNG_exit(String.valueOf(TOMINUTE),CARDTYPE,"000000000000000",  EXIT_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
					APPR_CODE, pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
				     String.valueOf(boj),pkconfig.datebojdate, pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
				     "N",pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid,  pkconfig.exitlocation,
				     pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
				     pkconfig.farelocation,  AMNTCHARGE, AMNTCHARGE,CARDBALANCE, "000000", AMNTCHARGE, "000000", "U_KCUF", ENTRY_DATE+ENTRY_TIME, ENTRY_ID, MFGNO,RRN,STAN,"1",pkconfig.RESP_CODE,map_procexit_tnginfo);
			

			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);
			pkvar.logtxtjson(encodejson);
			pkvar.cardinfojson = encodejson;
//			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
//			HASHPAN="";
			EXIT_ID="";
			ENTRY_ID="";
			
			
	    	}
		} catch(Exception v) {
			ret = "notok";
			v.printStackTrace();
		}
		return ret;
		
	}
	
	

	
	public static double calculateFARE(String dtfrom,String dtend,String flag) {
		
		//flag B=bank,T=tng
		
		String publicholiday="0";
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		double amnt =0;
		

		try {
			if(pkredis_var.get_cmd("simulate").equals("1")) {
				
			   
				String simulatedate = pkredis_var.get_cmd("simulatedate");
				String tmpdate[] = simulatedate.split("#");
				pkredis_var.set_cmd("simulate","0");
				pkconfig.SIM_ENABLE="true";
				if(tmpdate.length>1) {
					System.out.println(" #### Simulate Date EnableD ########## ");
					dtfrom = tmpdate[0];
					dtend = tmpdate[1];
					publicholiday = tmpdate[2];
					System.out.println(" ## Exact Reader Date From : "+(ENTRY_DATE+ENTRY_TIME));
					System.out.println(" ## Exact Reader Date Exit : "+( EXIT_DATE+EXIT_TIME));
					System.out.println(" ## simulate date Entry : "+dtfrom);
					System.out.println(" ## simulate date Exit : "+dtend);
					
					pkconfig.SIM_DATEENTRY=tmpdate[0];
					pkconfig.SIM_DATEEXIT= tmpdate[1];
					pkconfig.SIM_PH= tmpdate[2];
					pkconfig.SIM_TRXNO=pkvar.simtrxno_dd();
					
										
					if (publicholiday.equals("1")) {
						System.out.println(" #### Public Holiday Enable !!!");
					}
					
					if(pkvar.fareurl_enable.equals("1") && pkconfig.SIM_ENABLE.equals("true")) {  
						amnt = (calculateFARE_url(dtfrom,dtend,pkvar.fareurl,flag))/100;
						if(amnt>0) {
							if(flag.equals("B")) {
						      amnt = SST_surcharge(Integer.parseInt(pkvar.sst_enable),Integer.parseInt(pkvar.surcharge_enable),amnt);
							}
						}else {
						  amnt=0;
						}
					}
					
					System.out.println("### End Simulate #####");
				}else {
					System.out.println("simulate date format problem");
				}
				
			}
			
			}catch(Exception c) {
				c.printStackTrace();
			}
		
	
		
		if(pkvar.fareurl_enable.equals("1") && pkconfig.SIM_ENABLE.equals("false") && pkconfig.REQ_url.equals("false") ) { 
			
			//-------------URL api --------------
			pkconfig.REQ_url = "true";			
			amnt = (calculateFARE_url(dtfrom,dtend,pkvar.fareurl,flag))/100;
			if(amnt>0) {
				if(flag.equals("B")) {
				   amnt = SST_surcharge(Integer.parseInt(pkvar.sst_enable),Integer.parseInt(pkvar.surcharge_enable),amnt);
				 }
			}else {
			  amnt=0;
			}
			
					
		 } else if (pkvar.fareurl_enable.equals("0") && pkconfig.SIM_ENABLE.equals("false")){
	
			
			
			try {
				Date dt_from = df.parse(dtfrom);
				Date dt_end = df.parse(dtend);
				long diff = dt_end.getTime() - dt_from.getTime();
				long diffHours = diff / (60 * 60 * 1000) % 24;
				TOTHOUR = diffHours;
				long diffMinutes = diff / (60 * 1000) % 60;
				TOMINUTE = diffMinutes;
			 
				
				int grace = Integer.parseInt(pkvar.graceperiod);
	
		    	Calendar c1 = Calendar.getInstance();
				if ((c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)  || (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) || publicholiday.equals("1")) { 
				    System.out.println("--------------- WEEKEND PRICE------------------------");
				      
				    
				    if(diffMinutes < grace && diffHours < 1) {
						System.out.println("-- dapat grace period -- :" +TOMINUTE);
						amnt = 0;
					}else {
						amnt = Double.parseDouble(pkvar.flathourvalue);
					}
				    
				} else {
					
				      System.out.println("---WEEKDAY---------");
	
					  String Entrytime = new SimpleDateFormat("HHmm").format(dt_from);
					  
					  Date dtfrom_EntryInput = new SimpleDateFormat("HHmm").parse(Entrytime);
					  
					  Date dt_comp_dtst= new SimpleDateFormat("HHmm").parse(pkvar.flateratestarthour);
					  Date dt_comp_dten= new SimpleDateFormat("HHmm").parse(pkvar.flaterateendhour);
					  
					  if(dtfrom_EntryInput.after(dt_comp_dtst)==false && dtfrom_EntryInput.before(dt_comp_dten)==false) {//after hours
						    System.out.println("### NOT FLAT RATE ###");
							
							diff = dt_end.getTime() - dt_from.getTime();
							diffHours = diff / (60 * 60 * 1000) % 24;
							TOTHOUR = diffHours;
							diffMinutes = diff / (60 * 1000) % 60;
							TOMINUTE = diffMinutes;
						
							if(diffMinutes < grace && diffHours < 1) {
								System.out.println("## dapat GRACE period ## minute:" +TOMINUTE);
								amnt = 0;
							}else {
								long hr = diffHours;
								if( diffMinutes > (grace+1) && (diffHours < 1) ) {
	//								System.out.println("here" +amnt);
									amnt = (hr * FARErate());
								}else if(diffMinutes > 0 && (diffHours > 0) ) {
									amnt = ((hr+1) * FARErate());
								}else if(diffMinutes == 0 && (diffHours > 0) ) {
									amnt = (hr * FARErate());
								}
								if(flag.equals("B")) {
								    amnt = SST_surcharge(Integer.parseInt(pkvar.sst_enable),Integer.parseInt(pkvar.surcharge_enable),amnt);
								}
							}
							
					  } else {
						    System.out.println("### ----GET FLAT RATE-----###");
						    if(diffMinutes < grace && diffHours < 1) {
								System.out.println("-- dapat grace period -- :" +TOMINUTE);
								amnt = 0;
							}else {
								amnt = Double.parseDouble(pkvar.flathourvalue);
							}
					  }
					
				}
	
				
		
				
			
	
				
			} catch (ParseException e) {
			
				e.printStackTrace();
			}
		
			System.out.println("##### Amount :" +amnt+" #######");
			System.out.println(" ## Date Entry : "+dtfrom);
			System.out.println(" ## Date Exit : "+dtend);
			System.out.println("--> hours :" +TOTHOUR);
			System.out.println("--> minutes :" +TOMINUTE);
			System.out.println("##### #### #############" );
//		amnt = 100.00 ;//means == rm1.00 (reader misleading)
		 }
		return amnt*100;
	
	
}
	public static double calculateFARE_url(String dtfrom,String dtend,String url,String flag) {
		double ret=0;
		try {
	
		String jsonData = getdatajson_fare(dtfrom,dtend,flag);
		pkvar.logtxt(jsonData);
		System.out.println("-#### get fare request --> "+url );
		httpsvc.HttpPost_PS =  httpsvc.buildatahttp(pkvar.fareurl);
		String retdat = httpsvc.executeHttpRequest(jsonData, httpsvc.HttpPost_PS);
//		System.out.println("recv fare :"+retdat);
		faredata faredata  = new Gson().fromJson(retdat, faredata.class);
//		JsonElement jsonobj = gson.fromJson(retdat,JsonElement.class);
		pkvar.logtxt("--recv fare :"+retdat);
		
//		System.out.println("--API fare--> "+faredata.getParkingFare());
		ret = Double.parseDouble(faredata.getParkingFare());
		}catch(Exception v) {
			v.printStackTrace();
		}
		return ret;
		
	}
	

	
	public static String verifycsc_url(String url,String data,String mfgno,String purse,String trxno,Map<String,String> map_tnginfo) {
		String ret="false";
		try {
			Date dateNow = new Date();
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMddHHmmss");
		    
	    String date_to_string = format_dt.format(dateNow);
		String jsonData = json.buildjson_csc(data, date_to_string,mfgno,purse,trxno,map_tnginfo);
		pkvar.logtxt(jsonData);
		System.out.println("-#### -connecting to API[csc] first --> "+url );
		httpsvc.HttpPost_PS =  httpsvc.buildatahttp(url);
		String retdat = httpsvc.executeHttpRequest(jsonData, httpsvc.HttpPost_PS);
		System.out.println("-- return verify CSC fare : "+retdat);
		verifycsc verifycsc  = new Gson().fromJson(retdat, verifycsc.class);
//		JsonElement jsonobj = gson.fromJson(retdat,JsonElement.class);
		pkvar.logtxt("--verify csc -- :"+verifycsc.getStatus());
		ret = verifycsc.getStatus();
//		System.out.println("--API fare--> "+faredata.getParkingFare());
//		ret = Double.parseDouble(faredata.getParkingFare());
		}catch(Exception v) {
			v.printStackTrace();
		}
		return ret;
		
	}
	
	public static double calculateFARE_url_test(String data) {
		double ret=0;
        ret = Double.valueOf(data);
		return ret;
		
	}
	
	public static void rest_settle(String status,String data,String url) {
//		String data="00";
//		String status = "ok";
	 try {
		String jsonData = json.json_settle(status, data, pkvar.tarikh());
		pkvar.logtxt(jsonData);
		System.out.println("-#### -connecting to API[settlement] first --> "+url );
		httpsvc.HttpPost_PS =  httpsvc.buildatahttp(url);
		String retdat = httpsvc.executeHttpRequest(jsonData, httpsvc.HttpPost_PS);
	 }catch(Exception v) {
		 v.printStackTrace();
	 }
	}
	
	static String getdatajson_fare(String dtfrom, String dtend,String flag) {
		String ret="{\"data\":\"fail\"}";
		
		try {
		Date dtin = new SimpleDateFormat("yyyyMMddHHmmss").parse(dtfrom); 
		Date dtout = new SimpleDateFormat("yyyyMMddHHmmss").parse(dtend); 
		 String TimeIn = new SimpleDateFormat("HHmmss").format(dtin);
		 String TimeOut = new SimpleDateFormat("HHmmss").format(dtout);
		 String DateIn = new SimpleDateFormat("yyyyMMdd").format(dtin);
		 String DateOut = new SimpleDateFormat("yyyyMMdd").format(dtout);
	//901|1001|K01|123456|20191120090000|20191120092059|0
		String TransactionNo = TimeOut;
		String Flag  =flag;
		ret= json.buildjson_fare(pkconfig.plzno, pkconfig.terminalno, pkconfig.lane, TransactionNo, DateIn, TimeIn, DateOut, TimeOut, Flag);
		}catch(Exception v) {
			v.printStackTrace();
		  
		}
		return ret;
	}



	public static double FARErate() {
		double fare=0;
		 try {
		    fare = Double.parseDouble(pkconfig.jReadConfig("fareperhour"));
		 }catch(Exception kimak) {
			 kimak.getStackTrace();
		 }
		return fare;
	}
	
	public static double SST_surcharge(double sst,double surcharge,double amnt) {
		 double totalamnt =0;
		 try {
//		    fare = Double.parseDouble(pkconfig.jReadConfig("fareperhour"));
			 
			 if(amnt < 1) {
				 
				 return totalamnt;
			 }
			 
			 double sst_fare = Double.parseDouble(pkvar.sst);
			 double surcharge_fare = Double.parseDouble(pkvar.surcharge);
			 pkconfig.BANK_ORIFARE =  String.valueOf(amnt);
	    	 pkconfig.BANK_TAXFARE = pkvar.sst;
	    	 pkconfig.BANK_SURCHARGE = "0";
	    	 pkconfig.BANK_TAXSURCHARGE = "0";
	    	 
	        if(amnt < 1) {
		       	 pkconfig.BANK_ORIFARE =  String.valueOf(amnt);
		    	 pkconfig.BANK_TAXFARE = pkvar.sst;
		    	 pkconfig.BANK_SURCHARGE = "0";
		    	 pkconfig.BANK_TAXSURCHARGE = "0";
				 return totalamnt;
			 }
	    	 
		    if(sst>0 &&  surcharge>0) {
		    	 System.out.println("sst n surcharcharge ");
		    	 pkconfig.BANK_ORIFARE = String.valueOf(amnt);
		    	 pkconfig.BANK_TAXFARE = pkvar.sst;
		    	 System.out.println("### SST  ### :"+pkvar.sst+" %" );
		    	 pkconfig.BANK_SURCHARGE = String.valueOf((surcharge_fare/100 * amnt ));
		    	 pkconfig.BANK_TAXSURCHARGE = String.valueOf((sst_fare/100 * (surcharge_fare/100 * amnt )  ));
		    	 System.out.println("## BANK_TAXSURCHARGE ### : "+ pkconfig.BANK_TAXSURCHARGE);
		    	 totalamnt = (sst_fare/100 * (surcharge_fare/100 * amnt ) ) + (surcharge_fare/100 * amnt ) + amnt  ;
//		    	 System.out.println("###b4 roundup totalamnt ### : "+totalamnt);
		    	 totalamnt = Math.round((totalamnt*100));
		    	 totalamnt = totalamnt/100;
		    	 System.out.println("### totalamnt ### : "+totalamnt);
		    }
		    
           if(sst<1  && surcharge>0) {
        	   System.out.println("surcharcharge");
        	   totalamnt = (surcharge/100 * amnt ) + amnt  ;
		    }
           
           if(sst>0  && surcharge<1) {
        	   System.out.println("sst ");
        	   totalamnt = (sst_fare/100 * amnt ) + amnt  ;
		    }
           
           if(sst<1  && surcharge<1) {
        	   System.out.println("sst n surcharcharge disable");
        	   totalamnt =  amnt  ;
		    }
		 }catch(Exception kimak) {
			 kimak.getStackTrace();
		 }
		return totalamnt;
	}
	
    
	
	public static String bizrulcontext(byte dataresp) {
		String ret ="okcard";
		
		try {
			switch (dataresp) {
			case  0x10 :
//				System.out.println("# status card : Entry_not_allowed");
				ret = "# Failed – Card already registered. Entry not allowed #";
			break;
			case  0x11 :
//				System.out.println("# status card : Exit_not_allowed");
				ret = "# Failed – Card is not registered. Exit not allowed #";
				ret = "no_entry_info";
			break;
			case  0x12 :
//				System.out.println("# status card : Insufficent_fund");
				ret = "# Failed – Transaction Not Allowed. Underflow (Insufficient Fund) #";
			break;
			case  0x13 :
//				System.out.println("# status card : Exceed_limit");
				ret = "# Failed – Transaction Not Allowed. Overflow (Exceeded Limit) #";
			break;
			case  0x14 :
//				System.out.println("# status card : Card not found");
				ret = "Failed – Card Not Found";
			break;
			case  0x15 :
//				System.out.println("# status card : Card_not_valid");
				ret = "Failed – Card Not Accepted/Invalid Card";
			break;
			case  0x16 :
//				System.out.println("Card_blacklist");
				ret = "Failed – Card Blacklisted";
			break;
			case  0x21 :
//				System.out.println("# status card : declined_by_bank");
				ret = "Failed – Transaction Not Completed. Declined by Bank";
			break;
			case  0x22 :
//				System.out.println("# status card : timeout_bank");
				ret = "Failed – Transaction Not Completed. Connection Timeout (Bank)";
			break;
			case  0x23 :
//				System.out.println("# status card : timeout_internal");
				ret = "Failed – Transaction Not Completed. Connection Timeout (Internal)";
			break;
			case  0x31 :
//				System.out.println("# status card : condition_not_satisfied");
				ret = "Failed – Condition Not Satisfied";
			break;
			case  0x32 :
//				System.out.println("# status card : invalid_input_data");
				ret = "Failed – Invalid Input Data/Invalid Input Length";
			break;
			case  (byte) 0xEC :
//				System.out.println("# status card : seq_num_not_match");
				ret = "Failed –Recovery SEQ number not matched";
			break;
			case  (byte) 0xED :
//				System.out.println("# status card : seq_num_not_match");
				ret = "Failed – Recovery SEQ number not matched";
			break;
			}
		}
		catch(Exception v) {
			v.printStackTrace();
		}
//		ret="okcard";
		return ret;
	}
	public static void proccessmsg (byte[] msg) {
		String[] hexstr = new String[ msg.length] ;
		NumberFormat cardfmt = new DecimalFormat("0000000000");
		StringBuilder recv_hex = new StringBuilder();

		
		if(msg[3]== 1) {
//			System.out.println("DETECT card!");
//		    System.out.println("DETECT card!:"+(Arrays.toString(msg)));
		    
		    for(int x=0;x< msg.length;x++) {
				
		    	hexstr[x] = String.format("%02X", msg[x]);
		    	recv_hex.append("_"+String.format("%02X", msg[x]));
		      
			}
		   // System.out.println("DETECT card!:"+(recv_hex));
//		    System.out.println("hex -4-:"+(	hexstr[4]));
		    long shit = Long.parseLong(hexstr[7]+hexstr[6]+hexstr[5]+hexstr[4], 16);
			String asshole = Long.toString(shit);
			System.out.println("--------------- DETECT card!: : --> "+cardfmt.format(shit));
			 if(pkvar.pinLOOP1_state.isLow() ) { //detect loop 1
				if(  pkvar.ent_AUTH < 1) {
				  processcheckauth(cardfmt.format(shit));
				}
			 }
		    
		} else {
//			System.out.println("no card");
//			System.out.println("no card:"+(Arrays.toString(msg)));
		}
	}
	
	public static void processcheckauth(String mfgno) {
		
		 try {
			    Jedis jedis = pkvar.jedis_local_pool.getResource();	
			     pkvar.card_ser = mfgno;
			  if (pkvar.pinLOOP2_state.isHigh()) { //--make sure is clear/not detect any object!
				   
				  int res = pkSQLITE.checkname( pkvar.card_ser,jedis);
//				  System.out.println("HERE :result check name : >>> "+res);
				if(res>0 ) {
					
					 System.out.println("["+pkSQLITE.wieloc+"]["+pkvar.tarikh()+"] **** AUTHORIZE--!! ************ "+ pkvar.card_ser);
					 pkvar.ent_AUTH = 1;
              
                     pktsk_Qsnd_EXT.fifobyt_TX.add(pktsk_Qsnd_EXT.byt_sndbuzzOK);//send buzz Auth
                     if(pkvar.log_en<1) {
//                    	 int ret = pkvar.logdb_rfid(pkvar.tarikh(),pkvar.card_ser,"tid<null>",pkSQLITE.usersid,pkSQLITE.usersname,pkSQLITE.userdept,pkSQLITE.wieloc,pkSQLITE.users_REJ_ACC,pkSQLITE.wieloc,pkSQLITE.users_plno, pkSQLITE.usersorg);
                    	 pkvar.log_en=1;
                     }
                    
					 //tarikh+"|"+tagid+"|"+tid+"|"+userid+"|"+name+"|"+dept+"|"+laneid+"|"+status1+"|"+path1+"|"+users_plno+"|"+raw_org;
				 }else {
					 System.out.println("["+pkSQLITE.wieloc+"]["+pkvar.tarikh()+"]-- NOT AUTHORIZE--!!  "+ pkvar.card_ser);
					 if(pkvar.log_en<1) {
//				      int ret = pkvar.logdb_rfid(pkvar.tarikh(),pkvar.card_ser,"tid<null>",pkSQLITE.usersid,pkSQLITE.usersname,pkSQLITE.userdept,pkSQLITE.wieloc,pkSQLITE.users_REJ_ACC,pkSQLITE.wieloc,pkSQLITE.users_plno, pkSQLITE.usersorg);
				      pkvar.log_en=1;
					 }
				     pktsk_Qsnd_EXT.fifobyt_TX.add(pktsk_Qsnd_EXT.byt_sndbuzzNOK);//send buzz not auth
				     pkvar.ent_AUTH = 0;
				 }
			   }
			  //jedis.close();
			} catch (IOException e) {
			
				e.printStackTrace();
			}
	
	}
	
	
}
