package pk.mainsys;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import javax.swing.text.NumberFormatter;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import boj.sialw.pkboj;
import datatrx.json.pkjson;

import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;
import spring.fcukoff.greet_cardinfo;


public class pkPROCrecv_ENT {
//	static Jedis jedis;
	
	public static int VALIDCARD=99;
	
	public static String CARDTYPE="";
	public static String MASKPAN="";
	public static String HASHPAN="";
	public static String ENTRY_DATE="";
	public static String ENTRY_TIME="";
	public static String ENTRY_ID="";
	public static String EXIT_ID="";
	public static String EXIT_DATE="";
	public static String EXIT_TIME="";
	
	public static String AMNTCHARGE="";
	public static double AMNTCHARGEdbl=0;
	public static String FARERATE="";
	public static long TOTHOUR=0;
	public static String TRX_DT="";//yyMMddHHMMss
	public static String TRX_ID="";//yyMMddHHMMss
	
	public static String RRN="";
	public static String STAN="";
	public static String APPR_CODE="xdelg";
	
	static Map<String,String> map_initentry_tnginfo = new HashMap<String,String>();
	static Map<String,String> map_procentry_tnginfo = new HashMap<String,String>();
	public static String is_okcard="notok";

    public static Queue<String> data_topush = new LinkedList<String>();
//	private static org.slf4j.Logger log = LoggerFactory.getLogger(pkPROCESSrecv.class.toString());
    
    public static org.slf4j.Logger log = LoggerFactory.getLogger(pkPROCrecv_ENT.class);

	private static char[] TOMINUTE;

	private static String MFGNO;
    
	private static String CARDBALANCE;
	static DecimalFormat blc_fmt =new DecimalFormat("0.00");
	public static String RECV_RAWbytefilter (byte[] data) {
		      pkByteData.TIMOUT=0;
		      pkByteData.TIMOUT_whole=0;
		      String ret="xok";
		try {
			 if(data.length < 3) {return "xok";}
//			  System.err.println("[RECV][ENTRY] raw  :"+Arrays.toString(pkvar.Bytearray_toStringarray(data))+" SEQ : "+pkvar.checkSEQstring(data[3])+", time recv :"+(System.currentTimeMillis() - pkinit_rxtx.ts));
//			  pkvar.logtxt("[RECV][ENTRY][SEQ: "+pkvar.checkSEQstring(data[3])+"] raw  :"+Arrays.toString(pkvar.Bytearray_toStringarray(data))+" SEQ : "+pkvar.checkSEQstring(data[3])+",time recv :"+(System.currentTimeMillis() - pkinit_rxtx.ts));
//			  System.out.println("FIRST raw byte is clean !,data[0] "+data[0] +" ,data[data.length-1: "+data[data.length-1]);
			
			if(data[0]==2 && data[data.length-1]==3) {
                 if(pkconfig.bytenotail==1)
			     {
//                	 pkredis_var.lpush_cmd("bytenotail", pkvar.tarikh()+"#"+pkbytedata.SEQUENCE_PROC+"#"+Arrays.toString(pkvar.Bytearray_toStringarray(data)));
                	 pkconfig.bytno_tail.add(Arrays.toString(data)) ;
                	
                	 pkconfig.bytenotail=0;
                	 log.info("late byte : "+Arrays.deepToString(pkconfig.bytno_tail.toArray()));
                 }
//				System.out.println(" ---  raw byte is clean !,data[0] "+data[0] +" ,data.lenght : "+data[data.length-1]+" ---proceed next");
				  pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG  =data[3];
				  String res ="";
			   if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG!=16) {
				   is_okcard = bizrulcontext(data[4]);
//				   return;
				}
			   
			   
			   if(pkvar.readerpath.equals("entry")) { //####### normal #########################  ENTRY ############ #########################
				   
			   
				if(!is_okcard.equals("okcard")) {
					if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG!=16) {
//					    pktskPOLL.SEQUENCE_PROC=1;
						VALIDCARD = 99;
						System.out.println( ">>> CHECK CARD ["+is_okcard+"], cardtype: "+CARDTYPE+" >>> CANNOT process card ! <<< RESP BYTE: "+String.format("%02X",data[4]));
//						System.out.println("RESP_CODE : "+	pkconfig.RESP_CODE);
						pkconfig.RESP_CODE = String.format("%02X",data[4]);
						
					   if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG==2 && CARDTYPE.equals("EMV")) {
						   printinfoEMV_init_entry();	
						}else if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG==2 && CARDTYPE.equals("TNG")) {
					
						   printinfoTNG_init_entry();
						  
						}


					 }

					return "ok";
				}else {
					if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG!=16) {
						if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG==1 ) {
							CARDTYPE = processCARD(data[19],data);
							VALIDCARD = 1;
							pkByteData.TIMOUT=0;
							System.out.println(" >>>> CHECK CARD, cardtype: "+CARDTYPE+"["+String.format("%02X",data[20])+"] >>> VALID ! <<< RESP BYTE:"+String.format("%02X",data[4]));
							pkconfig.RESP_CODE = String.format("%02X",data[4]);
						} else if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG==2 && VALIDCARD ==1) {
							
							 is_okcard = bizrulcontext(data[4]);
							 
							 if(CARDTYPE.equals("EMV")) {
								 
								 detaisplitEMV_proc_entry(data);
								 printinfoEMV_proc_entry();
							 }
							 
							 if(CARDTYPE.equals("TNG")) {
								
								 detaisplitTNG_proc_entry(data);
								 printinfoTNG_proc_entry();
							 }
							
							 System.err.println(" #####  END-TRAX CHECK ################## "+is_okcard+" >>> VALID ! <<< RESP BYTE:"+String.format("%02X",data[4]));
						}
						
					 } else {
						 System.out.println(" scatter byte receive !");
					 }
				
					 return "ok";
				}
//				 return "ok";
			 } 
			   

				
			} else {
				 System.out.println(" >>>> [entry] RECV BYTE <> NO HEADER AND TAIL <> SEQ:["+ pkByteData.SEQUENCE_PROC+"] ["+Arrays.toString(pkvar.Bytearray_toStringarray(data))+"] <<< \n");
				 pkredis_var.lpush_cmd("entry_bytenotail","entry#"+ pkvar.tarikh()+"#"+pkByteData.SEQUENCE_PROC+"#"+Arrays.toString(pkvar.Bytearray_toStringarray(data)));
				 pkconfig.bytenotail=1;

				 return "xok";
			}
			
		}catch(Exception b) {
			b.printStackTrace();
		}
		
	   return "ok";
		
	}
	
	
	


	public static String processCARD(byte data, byte[] datacard) {
		String ret = "### EMV ### ";
		
		try {
//			byte b = (byte) (data  & 0xff);
//			System.out.println("data : "+data);
			if(data!= -112) {
				ret= "EMV";
//				pkconfig.paymenttype = "VSA";
				detaisplitEMV_init_entry(datacard);
				pkconfig.paymenttype = "VSA";
//				 printinfoEMV_init_entry();
				
			}else {
				detaisplitTNG_init_entry(datacard);
				pkconfig.paymenttype = "TNG";
				ret= "TNG";
				
			}
			
		}catch(Exception babi) {
			babi.printStackTrace();
			
		}
		
		return ret;
	}
	
	public static String detaisplitEMV_init_entry(byte[] data) {
        String ret = "";
//		NumberFormat nfm_byte = new DecimalFormat("00");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	 HASHPAN="";
       	     RRN="";
        	   byte [] entryid = new byte[13];
        	   int c=0;
               for(int s = 5;s<13;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//            	  ENTRY_ID += String.format("%02X",data[s]);
            	  entryid[c] = data[s];
            	  c++;
               }
                 ENTRY_ID = new String(entryid);


                Date dt = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
//                System.out.println("date entry---> "+String.format("%X",data[13])+String.format("%X",data[14])+String.format("%X",data[15]));
                ENTRY_DATE = dreformat.format(dt);  

                Date tm = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
//                System.out.println("time entry---> "+String.format("%X",data[16])+String.format("%X",data[17])+String.format("%X",data[18]));
                ENTRY_TIME = tf.format(tm);
                

                MASKPAN = String.format("%02X",data[19])+String.format("%02X",data[20] & 0xff)+String.format("%02X",data[21])+"000"+String.format("%02X",data[25])+String.format("%02X",data[26])+"00";

        	 for(int x =29;x<69;x++) {
        	 	HASHPAN += String.valueOf(pkvar.IntValToAsciiChar(data[x]));
        	  }
       
        	
        }catch(Exception kosial) {
        	kosial.printStackTrace();
        }
        
        
		return ret;
	}
	
	
	public static String detaisplitEMV_proc_entry(byte[] data) {
        String ret = "";
//		NumberFormat nfm_byte = new DecimalFormat("00");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	  HASHPAN="";
        	  RRN="";
        	   byte [] entryid = new byte[13];
        	   int c=0;
               for(int s = 5;s<13;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//            	  ENTRY_ID += String.format("%02X",data[s]);
            	  entryid[c] = data[s];
            	  c++;
               }
                 ENTRY_ID = new String(entryid);


                Date dt = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
//                System.out.println("date entry---> "+String.format("%X",data[13])+String.format("%X",data[14])+String.format("%X",data[15]));
                ENTRY_DATE = dreformat.format(dt);  

                Date tm = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
//                System.out.println("time entry---> "+String.format("%X",data[16])+String.format("%X",data[17])+String.format("%X",data[18]));
                ENTRY_TIME = tf.format(tm);
                

                MASKPAN = String.format("%02X",data[19])+String.format("%02X",data[20] & 0xff)+String.format("%02X",data[21])+"000"+String.format("%02X",data[25])+String.format("%02X",data[26])+"00";

        	 for(int x =29;x<69;x++) {
        	 	HASHPAN += String.valueOf(pkvar.IntValToAsciiChar(data[x]));
        	  }
       
           	 for(int s = 69;s<81;s++) {
//          	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
          	  RRN += String.format("%02X",data[s]);
//          	System.out.println(ENTRY_ID);
             }
       	 
       	 STAN = String.format("%02X",data[81]) +String.format("%02X",data[82]) +String.format("%02X",data[83]);
      
       	 APPR_CODE = String.valueOf(pkvar.IntValToAsciiChar(data[84])) +String.valueOf(pkvar.IntValToAsciiChar(data[85]))  +String.valueOf(pkvar.IntValToAsciiChar(data[86])) +
       			 String.valueOf(pkvar.IntValToAsciiChar(data[87])) +String.valueOf(pkvar.IntValToAsciiChar(data[88])) +String.valueOf(pkvar.IntValToAsciiChar(data[89])) ;
       	
        }catch(Exception kosial) {
        	kosial.printStackTrace();
        }
        
        
		return ret;
	}
	
	public static void printinfoEMV_init_entry() {
		if(pkvar.readerpath.equals("entry")) {
			int boj = pkboj.BOJcount();
			TRX_DT =  ENTRY_DATE+ENTRY_TIME;//yyMMddHHmmss
			TRX_ID = pkvar.TRX_ID();
			
			System.out.println(" ***********EMV INT ENTRY ******************** ");
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			
			System.out.println("TRX_ID : "+ TRX_ID);
//			System.out.println("EXIT_DATE : "+ EXIT_DATE);
//			System.out.println("EXIT_TIME : "+ EXIT_TIME);
			System.out.println("MASKPAN : "+ MASKPAN);
			System.out.println("HASHPAN : "+ HASHPAN);
			System.out.println("AMNTCHARGE : "+ AMNTCHARGE);
			System.out.println("FARErate : "+ FARERATE);
			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyyMMdd
			System.out.println("datebojtime : "+ pkconfig.datebojtime); //HH:mm:ss
			System.out.println("pkconfig.readerpath, : "+ pkconfig.readerpath);
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("trxtype : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
			System.out.println("RESP_CODE : "+	pkconfig.RESP_CODE);
//			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			System.out.println(" ******************************* ");
			
			
			
		
//			boj.sial.pktmr_resetboj.BOJcount +=1;
     
			String encodejson = pkjson.jsonENCODE_EMV("entry_nototal_period",CARDTYPE,"000000000000000", pkconfig.terminalno, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
				     "000000"  , pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
				     String.valueOf(boj),pkconfig.datebojdate, pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
				     "N",pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,
				     pkconfig.exitoperatorid,  pkconfig.exitlocation,  pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
				     pkconfig.farelocation,  AMNTCHARGE, AMNTCHARGE,"000000", "000000", AMNTCHARGE, "000000", "KCUF_U", "D", "9", "123","norrn","nostan","1",pkconfig.RESP_CODE,TRX_DT,ENTRY_ID,ENTRY_ID);
			

			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);//log only_if not success entry
//			new greet_cardinfo(encodejson);
			pkvar.cardinfojson = encodejson;
			pkvar.logtxtjson(encodejson);
			
			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
			HASHPAN="";
        	RRN="";
			EXIT_ID="";
			ENTRY_ID="";
		}
		
	}
	
	
	public static void printinfoEMV_proc_entry() {
		if(pkvar.readerpath.equals("entry")) {
			int boj = pkboj.BOJcount();
			TRX_DT =  ENTRY_DATE+ENTRY_TIME;//yyMMddHHmmss
			TRX_ID = pkvar.TRX_ID();
			
			System.out.println(" ***********EMV PROC ENTRY ******************** ");
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			
			System.out.println("TRX_ID : "+ TRX_ID);
			System.out.println("TRX_DT : "+ TRX_DT);
			System.out.println("EXIT_DATE : "+ EXIT_DATE);
			System.out.println("EXIT_TIME : "+ EXIT_TIME);
			System.out.println("MASKPAN : "+ MASKPAN);
			System.out.println("HASHPAN : "+ HASHPAN);
			System.out.println("AMNTCHARGE : "+ AMNTCHARGE);
			System.out.println("FARErate : "+ FARERATE);
			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			
			System.out.println("STAN : "+ STAN);
			System.out.println("RRN : "+ RRN);
			System.out.println("APPR_CODE : "+ APPR_CODE);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyyMMdd
			System.out.println("datebojtime : "+ pkconfig.datebojtime); //HHmmss
			System.out.println("Operationaldate : "+ pkvar.tarikhoperationaldate());//yyyyymmdd
			System.out.println("pkconfig.readerpath, : "+ pkconfig.readerpath);
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("trxtype : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
//			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			System.out.println("RESP_CODE : "+	pkconfig.RESP_CODE);
			System.out.println(" ******************************* ");
			
			
			
		
//			boj.sial.pktmr_resetboj.BOJcount +=1;
     
			String encodejson = pkjson.jsonENCODE_EMV("0",CARDTYPE,"000000000000000", EXIT_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
					APPR_CODE, pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
				     String.valueOf(boj),pkconfig.datebojdate,  pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
				     "N",pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid,  pkconfig.exitlocation,  pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
				     pkconfig.farelocation,  AMNTCHARGE, AMNTCHARGE,"000000", "000000", AMNTCHARGE, "000000", "KCUF_U", "D", "9", "123",RRN,STAN,"1",pkconfig.RESP_CODE,TRX_DT,ENTRY_ID,ENTRY_ID);
			

			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);//log only_if not success entry
			pkvar.cardinfojson = encodejson;
			pkvar.logtxtjson(encodejson);
			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
			HASHPAN="";
        	RRN="";
			EXIT_ID="";
			ENTRY_ID="";
		}
		
	}
	//------------------------------ TNG ----------------------------------------------------------------
    public static String detaisplitTNG_init_entry(byte[] data) {
		String ret = "";
		
	     
//			NumberFormat nfm_byte = new DecimalFormat("00");
		
//		String add_data ="02849338390629990520151008144258DP00000005000000039500504142434400000000500011111111111111111601464000587000153NNRR514142434410,R50";
				
			SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
			SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
	        try {
	        	   byte [] entryid = new byte[13];
	        	   int c=0;
	               for(int s = 5;s<13;s++) {
//	            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//	            	  ENTRY_ID += String.format("%02X",data[s]);
	            	  entryid[c] = data[s];
	            	  c++;
	               }
	                ENTRY_ID = new String(entryid);


	                Date dt = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
	    
	                ENTRY_DATE = dreformat.format(dt);  

	                Date tm = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
	       
	                ENTRY_TIME = tf.format(tm);
	                //data[19] == not used 
	                String mfgstr = String.format("%02X", data[22])+String.format("%02X",data[23])+String.format("%02X",data[24])+String.format("%02X",data[25])+String.format("%02X",data[26]);
//	        		long shit = Long.parseLong(mfgstr, 16);
//	    			String mfgno = Long.toString(shit);
	    			MFGNO =mfgstr;
	    			String blc = String.format("%02X",data[29])+String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32])+String.format("%02X",data[33])+"."+String.format("%02X",data[34]);
//	    			System.out.println(String.format("%02X", ddcc));
	    			Float crdblc=Float.parseFloat(blc);
	    			CARDBALANCE = blc;
	    			
	        }catch (Exception c){
	        	c.printStackTrace();
	        }     
		
		return ret;
	}
    
    public static String detaisplitTNG_proc_entry(byte[] data) {
    	String ret = "";
		
	     
		NumberFormat nfm_byte = new DecimalFormat("0000000000");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dreformat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HHmmss");
        try {
        	   byte [] entryid = new byte[13];
        	   int c=0;
               for(int s = 5;s<13;s++) {
//            	  ENTRY_ID +=  String.valueOf(pkvar.IntValToAsciiChar(data[s]));  
//            	  ENTRY_ID += String.format("%02X",data[s]);
            	  entryid[c] = data[s];
            	  c++;
               }
                ENTRY_ID = new String(entryid);


                Date dt = df.parse(String.format("%02X",data[13])+String.format("%02X",data[14])+String.format("%02X",data[15]));
                ENTRY_DATE = dreformat.format(dt);  

                Date tm = tf.parse(String.format("%02X",data[16])+String.format("%02X",data[17])+String.format("%02X",data[18]));
                ENTRY_TIME = tf.format(tm);
                //data[19] == not used 
                String mfgstr = String.format("%02X", data[22])+String.format("%02X",data[23])+String.format("%02X",data[24])+String.format("%02X",data[25])+String.format("%02X",data[26]);
//        		long shit = Long.parseLong(mfgstr, 16);
//    			String mfgno = Long.toString(shit);
    			MFGNO =mfgstr;
    			String blc = String.format("%02X",data[29])+String.format("%02X",data[30])+String.format("%02X",data[31])+String.format("%02X",data[32])+String.format("%02X",data[33])+"."+String.format("%02X",data[34]);
//    			System.out.println(String.format("%02X", ddcc));
    			Float crdblc=Float.parseFloat(blc);
    			CARDBALANCE = blc;
    			
    			
        }catch (Exception c){
        	c.printStackTrace();
        }     
	
	return ret;
	}
    
    
    public static void printinfoTNG_init_entry() {
		if(pkvar.readerpath.equals("entry")) {
			int boj = pkboj.BOJcount();
			TRX_DT =  ENTRY_DATE+ENTRY_TIME;//yyMMddHHmmss
			TRX_ID = pkvar.TRX_ID();
			
			System.out.println(" ***********TNG INIT ENTRY ******************** ");
			System.out.println("MFGNO : "+ MFGNO);
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			
//			System.out.println("TRX_ID : "+ TRX_ID);
//			System.out.println("TRX_DT : "+ TRX_DT);
//			System.out.println("EXIT_DATE : "+ EXIT_DATE);
//			System.out.println("EXIT_TIME : "+ EXIT_TIME);
//			System.out.println("MASKPAN : "+ MASKPAN);
//			System.out.println("HASHPAN : "+ HASHPAN);
//			System.out.println("AMNTCHARGE : "+ AMNTCHARGE);
//			System.out.println("FARErate : "+ FARERATE);
//			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			System.out.println("BALANCE : "+ Float.parseFloat(CARDBALANCE));
			
//			System.out.println("STAN : "+ STAN);
//			System.out.println("RRN : "+ RRN);
//			System.out.println("APPR_CODE : "+ APPR_CODE);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyyMMdd
			System.out.println("datebojtime : "+ pkconfig.datebojtime); //HHmmss
			System.out.println("Operationaldate : "+ pkvar.tarikhoperationaldate());//yyyyymmdd
			System.out.println("pkconfig.readerpath, : "+ pkconfig.readerpath);
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("trxtype : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
//			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			System.out.println("RESP_CODE : "+	pkconfig.RESP_CODE);
			System.out.println(" ******************************* ");
			
			
			
		   
//			boj.sial.pktmr_resetboj.BOJcount +=1;
          
		     
			String encodejson = pkjson.jsonENCODE_TNG_entry("0",CARDTYPE,"000000000000000", ENTRY_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
					APPR_CODE, pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
				     String.valueOf(boj),pkconfig.datebojdate,  pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
				     pkconfig.opermode,pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid,  pkconfig.exitlocation,  pkconfig.exitterminalno,
				     pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
				     pkconfig.farelocation,  AMNTCHARGE, AMNTCHARGE,CARDBALANCE, "000000", AMNTCHARGE, "000000", "KCUF_U", "D", "09", MFGNO,RRN,STAN,"1",pkconfig.RESP_CODE,map_initentry_tnginfo);
			

			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);//log only_if not success entry
			pkvar.cardinfojson = encodejson;
			pkvar.logtxtjson(encodejson);
			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
			RRN="";
			HASHPAN="";
			EXIT_ID="";
			ENTRY_ID="";
		}
		
	}
    
    public static void printinfoTNG_proc_entry() {
		if(pkvar.readerpath.equals("entry")) {
			int boj = pkboj.BOJcount();
			TRX_DT =  ENTRY_DATE+ENTRY_TIME;//yyMMddHHmmss
			TRX_ID = pkvar.TRX_ID();
			
			System.out.println(" *********** TNG PROC ENTRY ******************** ");
			System.out.println("CARDTYPE : "+ CARDTYPE);
			System.out.println("MMFGNO : "+ MFGNO);
			System.out.println("CARDBALANCE : "+ Float.parseFloat(CARDBALANCE));
			System.out.println("ENTRY_ID : "+ ENTRY_ID);
			System.out.println("ENTRY_DATE : "+ ENTRY_DATE);
			System.out.println("ENTRY_TIME : "+ ENTRY_TIME);
			
//			System.out.println("TRX_ID : "+ TRX_ID);
//			System.out.println("TRX_DT : "+ TRX_DT);
//			System.out.println("EXIT_DATE : "+ EXIT_DATE);
//			System.out.println("EXIT_TIME : "+ EXIT_TIME);
//			System.out.println("MASKPAN : "+ MASKPAN);
//			System.out.println("HASHPAN : "+ HASHPAN);
//			System.out.println("AMNTCHARGE : "+ AMNTCHARGE);
//			System.out.println("FARErate : "+ FARERATE);
//			System.out.println("TOTAL HOUR : "+ TOTHOUR);
			
//			System.out.println("STAN : "+ STAN);
//			System.out.println("RRN : "+ RRN);
//			System.out.println("APPR_CODE : "+ APPR_CODE);
			
			System.out.println(" ************** ## ***************** ");
			System.out.println("BOJcount/trxn : "+ (boj));
			System.out.println("CommonJOB /BOJcount/trxn : "+ (boj));
			System.out.println("datebojdate : "+ pkconfig.datebojdate); //yyyyyMMdd
			System.out.println("datebojtime : "+ pkconfig.datebojtime); //HHmmss
			System.out.println("Operationaldate : "+ pkvar.tarikhoperationaldate());//yyyyymmdd
			System.out.println("pkconfig.readerpath, : "+ pkconfig.readerpath);
			
			
			System.out.println(" **************  init ***************** ");
			System.out.println("locationno : "+ pkconfig.locationno);
			System.out.println("terminalno : "+ pkconfig.terminalno);
			System.out.println("jobno : "+ pkconfig.jobno);
			System.out.println("jobtype : "+ pkconfig.jobtype);
			System.out.println("terminalpath : "+ pkconfig.readerpath);//terminaltype
			System.out.println("opermode : "+ pkconfig.opermode);
			System.out.println("badgeno : "+ pkconfig.badgeno);
			System.out.println("trxtype : "+ pkconfig.trxtype);
			
			
			System.out.println("exitoperatorid : "+ pkconfig.exitoperatorid);
			System.out.println("exitlocation : "+ pkconfig.exitlocation);
			System.out.println("exitterminalno : "+ pkconfig.exitterminalno);
			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			
			System.out.println("farelocation : "+ pkconfig.farelocation);
//			System.out.println("exittcclass : "+ pkconfig.exittcclass);
			System.out.println("RESP_CODE : "+	pkconfig.RESP_CODE);
			System.out.println(" ******************************* ");
			
			
			
		
//			boj.sial.pktmr_resetboj.BOJcount +=1;
     
			String encodejson = pkjson.jsonENCODE_TNG_entry("0",CARDTYPE,"000000000000000", ENTRY_ID, TRX_DT, MASKPAN,"000000", HASHPAN, AMNTCHARGE, "000000", "01",TRX_ID, 
					APPR_CODE, pkconfig.locationno, pkconfig.terminalno, String.valueOf(pkconfig.jobno), String.valueOf(boj),
				     String.valueOf(boj),pkconfig.datebojdate,  pkconfig.datebojtime , pkvar.tarikhoperationaldate()  , pkconfig.jobtype,pkconfig.readerpath,
				     pkconfig.opermode,pkconfig.badgeno,pkvar.tarikhoperationaldate(),pkvar.tarikhoperationaltime(),pkconfig.paymenttype,pkconfig.trxtype,  pkconfig.exitoperatorid,  pkconfig.exitlocation,  pkconfig.exitterminalno,  pkconfig.exittcclass,  pkconfig.exitdetectclass,  pkvar.tarikhoperationaldate(),  pkvar.tarikhoperationaltime(), 
				     pkconfig.farelocation,  AMNTCHARGE, AMNTCHARGE,CARDBALANCE, "000000", AMNTCHARGE, "000000", "KCUF_U", "D", "09", MFGNO,RRN,STAN,"1",pkconfig.RESP_CODE,map_procentry_tnginfo);
			

			pkconfig.data_topush.add(encodejson);
			pkconfig.data_topushdb.add(encodejson);//log only_if not success entry
			pkvar.cardinfojson = encodejson;
			pkvar.logtxtjson(encodejson);
			System.out.println("TOTAL DATA in list :"+	pkconfig.data_topush.size());
			RRN="";
			HASHPAN="";
			EXIT_ID="";
			ENTRY_ID="";
		}
		
	}
	
	public static String bizrulcontext(byte dataresp) {
		String ret ="okcard";
		
		try {
			switch (dataresp) {
			case  0x10 :
//				System.out.println("# status card : Entry_not_allowed");
				ret = "# Failed – Card already registered. Entry not allowed #";
			break;
			case  0x11 :
//				System.out.println("# status card : Exit_not_allowed");
				ret = "# Failed – Card is not registered. Exit not allowed #";
			break;
			case  0x12 :
//				System.out.println("# status card : Insufficent_fund");
				ret = "# Failed – Transaction Not Allowed. Underflow (Insufficient Fund) #";
			break;
			case  0x13 :
//				System.out.println("# status card : Exceed_limit");
				ret = "# Failed – Transaction Not Allowed. Overflow (Exceeded Limit) #";
			break;
			case  0x14 :
//				System.out.println("# status card : Card not found");
				ret = "Failed – Card Not Found";
			break;
			case  0x15 :
//				System.out.println("# status card : Card_not_valid");
				ret = "Failed – Card Not Accepted/Invalid Card";
			break;
			case  0x16 :
//				System.out.println("Card_blacklist");
				ret = "Failed – Card Blacklisted";
			break;
			case  0x21 :
//				System.out.println("# status card : declined_by_bank");
				ret = "Failed – Transaction Not Completed. Declined by Bank";
			break;
			case  0x22 :
//				System.out.println("# status card : timeout_bank");
				ret = "Failed – Transaction Not Completed. Connection Timeout (Bank)";
			break;
			case  0x23 :
//				System.out.println("# status card : timeout_internal");
				ret = "Failed – Transaction Not Completed. Connection Timeout (Internal)";
			break;
			case  0x31 :
//				System.out.println("# status card : condition_not_satisfied");
				ret = "Failed – Condition Not Satisfied";
			break;
			case  0x32 :
//				System.out.println("# status card : invalid_input_data");
				ret = "Failed – Invalid Input Data/Invalid Input Length";
			break;
			case  (byte) 0xEC :
//				System.out.println("# status card : seq_num_not_match");
				ret = "Failed –Recovery SEQ number not matched";
			break;
			case  (byte) 0xED :
//				System.out.println("# status card : seq_num_not_match");
				ret = "Failed – Recovery SEQ number not matched";
			break;
			}
		}
		catch(Exception v) {
			v.printStackTrace();
		}
//		ret="okcard";
		return ret;
	}
	public static void proccessmsg (byte[] msg) {
		String[] hexstr = new String[ msg.length] ;
		NumberFormat cardfmt = new DecimalFormat("0000000000");
		StringBuilder recv_hex = new StringBuilder();

		
		if(msg[3]== 1) {
//			System.out.println("DETECT card!");
//		    System.out.println("DETECT card!:"+(Arrays.toString(msg)));
		    
		    for(int x=0;x< msg.length;x++) {
				
		    	hexstr[x] = String.format("%02X", msg[x]);
		    	recv_hex.append("_"+String.format("%02X", msg[x]));
		      
			}
		   // System.out.println("DETECT card!:"+(recv_hex));
//		    System.out.println("hex -4-:"+(	hexstr[4]));
		    long shit = Long.parseLong(hexstr[7]+hexstr[6]+hexstr[5]+hexstr[4], 16);
			String asshole = Long.toString(shit);
			System.out.println("--------------- DETECT card!: : --> "+cardfmt.format(shit));
			 if(pkvar.pinLOOP1_state.isLow() ) { //detect loop 1
				if(  pkvar.ent_AUTH < 1) {
				  processcheckauth(cardfmt.format(shit));
				}
			 }
		    
		} else {
//			System.out.println("no card");
//			System.out.println("no card:"+(Arrays.toString(msg)));
		}
	}
	
	public static void processcheckauth(String mfgno) {
		
		 try {
			    Jedis jedis = pkvar.jedis_local_pool.getResource();	
			     pkvar.card_ser = mfgno;
			  if (pkvar.pinLOOP2_state.isHigh()) { //--make sure is clear/not detect any object!
				   
				  int res = pkSQLITE.checkname( pkvar.card_ser,jedis);
//				  System.out.println("HERE :result check name : >>> "+res);
				if(res>0 ) {
					
					 System.out.println("["+pkSQLITE.wieloc+"]["+pkvar.tarikh()+"] **** AUTHORIZE--!! ************ "+ pkvar.card_ser);
					 pkvar.ent_AUTH = 1;
              
                     pktsk_Qsnd_EXT.fifobyt_TX.add(pktsk_Qsnd_EXT.byt_sndbuzzOK);//send buzz Auth
                     if(pkvar.log_en<1) {
                    	 int ret = pkvar.logdb_rfid(pkvar.tarikh(),pkvar.card_ser,"tid<null>",pkSQLITE.usersid,pkSQLITE.usersname,pkSQLITE.userdept,pkSQLITE.wieloc,pkSQLITE.users_REJ_ACC,pkSQLITE.wieloc,pkSQLITE.users_plno, pkSQLITE.usersorg);
                    	 pkvar.log_en=1;
                     }
                    
					 //tarikh+"|"+tagid+"|"+tid+"|"+userid+"|"+name+"|"+dept+"|"+laneid+"|"+status1+"|"+path1+"|"+users_plno+"|"+raw_org;
				 }else {
					 System.out.println("["+pkSQLITE.wieloc+"]["+pkvar.tarikh()+"]-- NOT AUTHORIZE--!!  "+ pkvar.card_ser);
					 if(pkvar.log_en<1) {
				      int ret = pkvar.logdb_rfid(pkvar.tarikh(),pkvar.card_ser,"tid<null>",pkSQLITE.usersid,pkSQLITE.usersname,pkSQLITE.userdept,pkSQLITE.wieloc,pkSQLITE.users_REJ_ACC,pkSQLITE.wieloc,pkSQLITE.users_plno, pkSQLITE.usersorg);
				      pkvar.log_en=1;
					 }
				     pktsk_Qsnd_EXT.fifobyt_TX.add(pktsk_Qsnd_EXT.byt_sndbuzzNOK);//send buzz not auth
				     pkvar.ent_AUTH = 0;
				 }
			   }
			  //jedis.close();
			} catch (IOException e) {
			
				e.printStackTrace();
			}
	
	}

}
