package pk.mainsys;

import java.util.ArrayList;

public class TNGdata {
	String sector;
	String block;
	ArrayList<String> databyt;
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public ArrayList<String> getDatabyt() {
		return databyt;
	}
	public void setDatabyt(ArrayList<String> databyt) {
		this.databyt = databyt;
	}

}
