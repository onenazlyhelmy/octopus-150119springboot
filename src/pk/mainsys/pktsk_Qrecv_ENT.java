package pk.mainsys;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Toolkit;















import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
 

 
public class pktsk_Qrecv_ENT {
	Toolkit toolkit;
	Timer timer;


	public static Queue<byte[]> fifobyt_RX = new LinkedList<byte[]>();
	public static int byte_RECV_SEQ_PROCESS_FLAG = 99;
//	public static int SEQ_INIT_ENT_FLAG = 1; 
//	public static int SEQ_PROC_ENT_FLAG = 2; 
//	public static int SEQ_INIT_EXT_FLAG = 3; 
//	public static int SEQ_PROC_EXT_FLAG = 4; 
//	public static int SEQ_INIT_SALE_FLAG = 5; 
//	public static int SEQ_PROC_SALE_FLAG = 6; 	

	
	//--coherent-----
	private static  org.slf4j.Logger log = LoggerFactory.getLogger(pktsk_Qrecv_ENT.class.toString());
	int  timeout = Integer.parseInt( pkvar.timeoutread);
 
	public pktsk_Qrecv_ENT() throws Exception {

		System.out.println("--> start pk_task_byteRECV <-- ");
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 70); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {

		  int tot_byt =0;
		
		public void run()  {
			 ByteBuffer b ;
			 byte[] byte_RECV ;
			 int dat ;
//			 byte [] byte_read = null;
			
			
		try {
			
			if(!fifobyt_RX.isEmpty()) {		
				
				 while (! fifobyt_RX.isEmpty()) {
			    
					 
				 
					 byte_RECV = new byte [fifobyt_RX.peek().length];
					 byte_RECV = fifobyt_RX.poll();
					 //---FILTER THE RECV
					 
					 System.err.println("[RECV]Q-ENTRY [SEQ: "+pkvar.checkSEQstring(byte_RECV[3])+"]   :"+Arrays.toString(pkvar.Bytearray_toStringarray(byte_RECV))+" SEQ : "+pkvar.checkSEQstring(byte_RECV[3])+" #");
					 pkvar.logtxt("[RECV][ENTRY][SEQ: "+pkvar.checkSEQstring(byte_RECV[3])+"] raw  :"+Arrays.toString(pkvar.Bytearray_toStringarray(byte_RECV))+" SEQ : "+pkvar.checkSEQstring(byte_RECV[3])+",time recv :"+(System.currentTimeMillis() - pkinit_rxtx.ts));
					 
					 pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG  =byte_RECV[3];
					 String tmp_byte_RX [] = pkvar.Bytearray_toStringarray(byte_RECV);
					 pkvar.logtxt("PROCESS RECV Byte >>> "+Arrays.toString(tmp_byte_RX)+", RECV_SEQ_PROCESS_FLAG :"+byte_RECV_SEQ_PROCESS_FLAG); 
					 ///head == 2, tail == 3
					if(byte_RECV[0]==2 && byte_RECV[byte_RECV.length-1]==3 && byte_RECV.length > 6 ) {
						
					
						//fast --respon---gpio--
					
						if(pkByteData.byte_SEQ_PROCent_tng==byte_RECV_SEQ_PROCESS_FLAG && pkconfig.AUTHORIZE.equals("no")) { 
							
							  String cardstatus = pkByteData.bizrulcontext(byte_RECV[4]); 
							  
						  //--gpio--fast respon
							  if(cardstatus.equals("okcard")) {
								  
								  if(pkByteData.CARDTYPE.equals("TNG")) {
									  
										pkByteData.VALIDCARD =1;
										System.err.println(" cardstatus  "+cardstatus);
										System.err.println("### [TNG] ALB ENTRY UP #### ");
										pkconfig.AUTHORIZE="yes";
										pkredis_var.set_cmd("forcealb","1");
								  } else {
									  
										 pkByteData.VALIDCARD =1;
										 System.err.println(" cardstatus  "+cardstatus);
										System.err.println("### [EMV] ALB ENTRY UP #### ");
										  pkconfig.AUTHORIZE="yes";
										pkredis_var.set_cmd("forcealb","1");
								  }
							
							  }
							
							
						
						}
						
						  System.err.println("byte_RECV_SEQ_PROCESS_FLAG: "+byte_RECV_SEQ_PROCESS_FLAG);
						  System.err.println("byte_SEQ_PROCent_tng: "+pkByteData.byte_SEQ_PROCent_tng);
						  System.err.println("SEQUENCE_PROC: "+pkByteData.SEQUENCE_PROC);
						  System.err.println("VALIDCARD "+pkPROCrecv_ENT.VALIDCARD+",pkconfig.AUTHORIZE:"+pkconfig.AUTHORIZE);;
						
						//--------------------
						
						 //hex 16 --- integer --enable reader process cmd--
						 if(pktsk_Qrecv_ENT.byte_RECV_SEQ_PROCESS_FLAG!=16 ) {
					       pkPROCrecv_ENT.RECV_RAWbytefilter(byte_RECV);
						 }
					 
					 }
					 		             
					 
					          if(pkByteData.byte_SEQ_EN_RDR==byte_RECV_SEQ_PROCESS_FLAG) {
				                     	
				                     	
				                     	  if(pkvar.readerpath.equals("entry")) {
				                     		    byte [] byts = pkByteData.bytSND_INIT_ENT_compiled();
						                      	 pktsk_Qsnd_ENT.fifobyt_TX.add(byts);
						                      	 System.out.println("-- SUCCESS {{ ENTRY }} ENABLE READER  >>> "+pkvar.tarikh());
						                         pkByteData.SEQUENCE_PROC=1;
						                      
				                     	  }

			                     }
		                       
					     
					          
					          if(pkvar.readerpath.equals("entry")) {// ############ ENTRY ####################
					        	  
					          
					        	 	 String simulateloop1 = pkredis_var.get_cmd("simulateloop1");
										
										if((pkconfig.simulateloop1.equals("1") || 	pkconfig.loop1 ||  simulateloop1.equals("1")) && pkconfig.AUTHORIZE.equals("no")   ) {
											if(byte_RECV[4]==20) {
												//if receve byte 0x14 = cannot detect anycard
											     byte [] byts = pkByteData.bytSND_INIT_ENT_compiled();
						                      	 pktsk_Qsnd_ENT.fifobyt_TX.add(byts);
												
											}
									 	}
					        	  
					        	  
                                if(pkByteData.byte_SEQ_INITent_tng==byte_RECV_SEQ_PROCESS_FLAG && pkPROCrecv_ENT.VALIDCARD ==1 && pkByteData.SEQUENCE_PROC==1) {
//                                	 System.out.println("success! process Init Entry ,now proceed to: [byte_CMD_INITent_tng] >>> ");
                                
//                             	   String ccc = "[2, 1, ffffff94, 1, 0, 45, 31, 30, 30, 30, 31, 30, 31, 20, 2, 12, 10, 9, 46, ffffff90, 0, 0, 24, 57, 61, ffffff84, 79, 0, 0, 0, 0, 0, 3, 65, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ffffffbf, 38, 0, ffffff84, 32, 34, 35, 37, 36, 31, 38, 34, 37, 39, 30, 36, 32, 39, 39, 33, 30, 33, 32, 30, 32, 30, 30, 32, 31, 32, 31, 30, 30, 39, 34, 36, 44, 50, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 2b, 30, 30, 30, 30, 30, 33, 36, 35, 36, 34, 35, 30, 34, 31, 34, 32, 34, 33, 34, 34, 30, 30, 30, 30, 30, 30, 30, 30, 53, 41, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 36, 30, 31, 34, 36, 34, 30, 30, 30, 35, 38, 37, 30, 30, 30, 31, 35, 33, 4e, 4e, 52, 30, 31, 30, 34, 31, 34, 32, 34, 33, 34, 34, 31, 30, 2c, 58, 30, 31, ffffffbf, 55, 0, ffffffc6, 0, 1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f, 0, 2, 0, 0, 3, 17, 7e, 40, 0, 3, 7f, ffffffb0, ffffffef, 3, 0, 23, 36, ffffffba, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 1, 35, 30, 0, 0, 52, 30, 36, ffffffba, 0, 0, 41, 42, 43, 44, 14, 6a, 7, 1, fffffff6, 11, ffffffe1, 1, ffffffd4, ffffff8e, 0, 0, 1, 34, 59, ffffffe5, ffffffbc, 47, 0, 1, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, ffffffe1, 59, ffffffe5, ffffffbc, 47, 0, 1, 1, d, 0, 50, 0, 33, 41, 42, 43, 44, 0, 0, 0, 0, 0, 0, 0, 0, 0, d, 1, fffffff9, ffffff9c, 35, 30, 32, 31, fffffff0, 49, 2, 0, 3, ffffffb6, ffffff9c, 5b, ffffffa8, ffffffed, e, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 4, 2f, 48, 7c, ffffff92, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 1, 11, fffffff6, 0, 0, ffffffd4, ffffff8e, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ffffffdb, 3]";
        						  //request microservice
                                	
                                	//TNG ENTRY verify CSC
                                	String result="false";
                                	if(pkPROCrecv_ENT.CARDTYPE.equals("TNG")) {
                                	    System.out.println("check bzrule [API]["+pkvar.verifycscurl+"] CARD TNG [ENTRY] request >>> "+pkvar.tarikh());
                                		pkByteData.detaisplitlTNG_init_entry(byte_RECV);
                                		result = pkByteData.verifycsc_url(pkvar.verifycscurl,Arrays.toString(pkvar.Bytearray_toStringarray(byte_RECV)),pkByteData.MFGNO,pkByteData.CARDBALANCE,pkByteData.CARDTRXNO,pkByteData.map_tnginitinfo);
                                	}else {
                                		result="true";
                                	}
                                		
        						   if(result.equals("true")) {
        							 pktsk_Qsnd_ENT.fifobyt_TX.clear();
                                   	 System.out.println("-- RECV SUCCESS INIT {{ENTRY}} >>> "+pkvar.tarikh());
                                   	 byte [] byts = pkByteData.bytSND_PROC_ENT_compiled();
   				                     pktsk_Qsnd_ENT.fifobyt_TX.add(byts);
   				                     pkByteData.SEQUENCE_PROC=2;
   				                     pkByteData.TIMOUT=0;	 
        						   }else {
        							    pkPROCrecv_ENT.VALIDCARD = 1;
        								pkByteData.TIMOUT=0;
        						   }
        						   
                                	
                                
				                 
                                }
                                if(pkByteData.byte_SEQ_PROCent_tng==byte_RECV_SEQ_PROCESS_FLAG && pkPROCrecv_ENT.VALIDCARD ==1  && pkconfig.AUTHORIZE.equals("yes")) {
                                	 System.out.println("-- RECV SUCCESS PROCEED ENTRY >>> "+pkvar.tarikh());
                                	 pkByteData.SEQUENCE_PROC=99;
			                      	 pkconfig.AUTHORIZE="yes";
			                      	 System.out.println(" <<<<<<<<<<<<<<<<<<<<<<<<<<<<< END ENTRY MODE STOP PROCESS >>>>>>>>>>>>>>>>>>>>>>> "+pkvar.tarikh());
			                      	 pkByteData.TIMOUT=0;
			                      	 pktsk_Qsnd_ENT.fifobyt_TX.clear();
			                      	 fifobyt_RX.clear();
			                         Thread.sleep(3000);
                                }
                             
				            }
					          
		             
		             }


			         }

			
				

			  } catch (Exception e) {
				 e.printStackTrace();
				 
			 }
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}