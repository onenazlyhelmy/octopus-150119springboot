package pk.mainsys;

import java.util.Timer;
import java.util.TimerTask;
import java.awt.Toolkit;



 
public class pktask_timeout {
	public static final TimerTask tmrReminder = null;
	Toolkit toolkit;
	Timer timer;
  
	static	int TIMECOUNT = 0;

	 static int TIMEOUTCOUNT_EN_READER = 0,FLAG_EN_READER=99;
	 static int TIMEOUTCOUNT_INIT_ENT = 0,FLAG_INIT_ENT=99;
	 static int TIMEOUTCOUNT_PROC_ENT = 0,FLAG_PROC_ENT=99;
	 
	 static int TIMEOUTCOUNT_INIT_EXT = 0,FLAG_INIT_EXT=99;
	 static int TIMEOUTCOUNT_PROC_EXT = 0,FLAG_PROC_EXT=99; 
	 static int TIMEOUTCOUNT_INIT_SALE = 0,FLAG_INIT_SALE=99;
	 static int TIMEOUTCOUNT_PROC_SALE = 0,FLAG_PROC_SALE=99;
	public pktask_timeout() throws Exception {
		System.out.println("--> start pktask_timeout  <-- ");
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
	   
	       
//	       System.out.println(" -- WIE TASK TRXDATA -- " );
		  
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1000); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			
			try{	
			
//			    amqp_attr.rabbit_pushlog();
//				byte[] byt = pkbytedata.bytSND_EN_compiled();
//				pktask_rxtx.fifobyt_TX.add(byt);
//				this.cancel();
				if(FLAG_EN_READER == 1) {
					TIMEOUTCOUNT_EN_READER +=1;
				}else {
					TIMEOUTCOUNT_EN_READER=0;
				}
                 if(FLAG_INIT_ENT == 1) {
                	 TIMEOUTCOUNT_INIT_ENT+=1;
				}else {
					TIMEOUTCOUNT_INIT_ENT=0;
				}
                 
                 if(FLAG_PROC_ENT == 1) {
                	 TIMEOUTCOUNT_PROC_ENT+=1;
 				}else {
 					TIMEOUTCOUNT_PROC_ENT=0;
				}
                 
                 if(FLAG_INIT_EXT == 1) {
                	 TIMEOUTCOUNT_INIT_EXT+=1;
  				}else {
  					TIMEOUTCOUNT_INIT_EXT=0;
				}
                 if(FLAG_PROC_EXT == 1) {
                	 TIMEOUTCOUNT_PROC_EXT+=1;
   				}else {
   					TIMEOUTCOUNT_PROC_EXT=0;
				}
                 if(FLAG_INIT_SALE == 1) {
                	 TIMEOUTCOUNT_INIT_SALE+=1; 
    				}else {
    					TIMEOUTCOUNT_INIT_SALE=0;
    				}
                 
                 if(FLAG_PROC_SALE == 1) {
 					TIMEOUTCOUNT_PROC_SALE +=1;
 				}else {
 					TIMEOUTCOUNT_PROC_SALE=0;
				}
			}
			catch (Exception e) {

				e.printStackTrace();
//				pk_var.logtxt("\n--------------------------\n"+e.toString()+"\n------------------------------\n");
			}
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}