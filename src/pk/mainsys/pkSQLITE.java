package pk.mainsys;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import redis.clients.jedis.Jedis;

public class pkSQLITE
{
	public  static String usersid ;
	public  static String usersname;
	public  static String usersmfg ;
	public  static String userstagid ;
	public  static String usersepc;
	
	public  static String users_REJ_ACC ;
	public static String userdept;
	public static String users_plno;
	public static String userstatus;
	public static String usersorg;
	
	public static String mfg_hex;
	public static String wieloc="";
	public static String usr_loc;
	public static int loc_type;
	
	//--------sqlite3-----------
	private static ResultSet rs;
	private static ResultSet rs0;
//	static Jedis jedis;
	static Connection con = null;
    static Statement stmt = null;
	public static String rawdataid="";
	public static String rawdatalog="";
	public static String rawdatadate="";
    
    
    static Connection conlog = null;
    static Statement stmtlog = null;
	//-------------------------
    private static Log log = LogFactory.getLog(pktskDATApush.class);
	public static void SQLite_init(){
		String SQLdbnm="";
	 
	 
	    

	    CodeSource codeSource = pkMainTEST.class.getProtectionDomain().getCodeSource();
	    File jarFile;
		try {
//			Properties config = new Properties();
//	    	config.setProperty("open_mode", "1"); 
			
			jarFile = new File(codeSource.getLocation().toURI().getPath());
			SQLdbnm = jarFile.getName()+".db";
		    System.out.print("<< [SQLITE] <load driver>: >> :"+SQLdbnm+"\n");
		    Class.forName("org.sqlite.JDBC");
		    con =  DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
		    con.setReadOnly(false);
	 	      if(con.isClosed()) {
	 	    	  
	 	    	  System.out.println("DATABASE SQLite CLOSE!!\n");
//	 	    	  System.exit(0);
	 	      }
//	 	     con.close();
		     
		} catch ( Exception e) {
			
			e.printStackTrace();
		}
	       	

	 	  
	}
	
	public static void SQLite_init_log(){
		String SQLdbnm="";
	 
	 
	    

	    CodeSource codeSource = pkMainTEST.class.getProtectionDomain().getCodeSource();
	    File jarFile;
		try {
//			Properties config = new Properties();
//	    	config.setProperty("open_mode", "1"); 
			
			if(pkconfig.readerpath.equals("exit")) {
				jarFile = new File(codeSource.getLocation().toURI().getPath());
				SQLdbnm = jarFile.getName()+".exit.log.db";
			    System.out.print("<< [SQLITE-exit] <load driver>: >> :"+SQLdbnm+"\n");
			    Class.forName("org.sqlite.JDBC");
			    conlog=  DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
			    conlog.setReadOnly(false);
		 	      if(conlog.isClosed()) {
		 	    	  
		 	    	  System.out.println("DATABASE SQLite CLOSE!!\n");
//		 	    	  System.exit(0);
		 	      }
			} 
			
			if(pkconfig.readerpath.equals("entry")) { 
				jarFile = new File(codeSource.getLocation().toURI().getPath());
				SQLdbnm = jarFile.getName()+".entry.log.db";
			    System.out.print("<< [SQLITE-entry] <load driver>: >> :"+SQLdbnm+"\n");
			    Class.forName("org.sqlite.JDBC");
			    conlog=  DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
			    conlog.setReadOnly(false);
		 	      if(conlog.isClosed()) {
		 	    	  
		 	    	  System.out.println("DATABASE SQLite CLOSE!!\n");
//		 	    	  System.exit(0);
		 	      }
			}
	
//	 	     con.close();
		     
		} catch ( Exception e) {
			
			e.printStackTrace();
		}
	       	

	 	  
	}
	
	  public static void dbinsertlog(String rawdatalog,String rawdatadate )
	  {
		
			   
		    try {
//		    	Class.forName("org.sqlite.JDBC");
//		 	    con = DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
		        con.setAutoCommit(false);
		    //  System.out.println("Opened database successfully");

		       stmt = con.createStatement();

//		      String sql = "INSERT INTO raw_log (rawdate,rawuserid,rawname,rawtagid,rawpath,rawstatus) " +
//	                   "VALUES ('"+rawdate+"', "+rawuserid+", '"+rawname+"', '"+rawtagid+"', '"+rawpath+"', '"+rawstatus+"' );"; 
		      
		      String sql2 = "INSERT INTO rawdata (rawdatalog,rawdatadate) " +
	                   "VALUES ('"+rawdatalog+"', '"+rawdatadate+"' );"; 
		      
		      stmt.executeUpdate(sql2);


		      stmt.close();
		      con.commit();
//		      con.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//		      System.exit(0);
		    }
		   // System.out.println("Records created successfully");
	  }
	  
	  public static void dbinsertloglog(String rawdatalog,String rawdatadate )
	  {
		
			   
		    try {
//		    	Class.forName("org.sqlite.JDBC");
//		 	    con = DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
		        conlog.setAutoCommit(false);
		    //  System.out.println("Opened database successfully");

		       stmtlog = conlog.createStatement();

//		      String sql = "INSERT INTO raw_log (rawdate,rawuserid,rawname,rawtagid,rawpath,rawstatus) " +
//	                   "VALUES ('"+rawdate+"', "+rawuserid+", '"+rawname+"', '"+rawtagid+"', '"+rawpath+"', '"+rawstatus+"' );"; 
		      
		      String sql2 = "INSERT INTO rawdata (rawdatalog,rawdatadate) " +
	                   "VALUES ('"+rawdatalog+"', '"+rawdatadate+"' );"; 
		      
		      stmtlog.executeUpdate(sql2);


		      stmtlog.close();
		      conlog.commit();
//		      con.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//		      System.exit(0);
		    }
		   // System.out.println("Records created successfully");
	  }
	  public static void DBdeletelog(String ids)
	  {
		


		      try {
			   
		 	       con.setAutoCommit(false);
		 	   
		 	     
		 	       stmt = con.createStatement();
		   
		 	        String sql="DELETE FROM rawdata WHERE rawdataid ="+ids;
			    
	                stmt.executeUpdate(sql);
			   
			     
			         con.commit();
			         stmt.close();
//			         con.close();

		} catch ( SQLException e) {
			
			e.printStackTrace();
		}
	  }
	  
	  public static String DBselectlog_todel()
	  {
		
       String retid ="";

		      try {
			   
		 	       con.setAutoCommit(false);
		 	   
		 	     
		 	       stmt = con.createStatement();
		   
//		 	        String sql="DELETE FROM rawdata WHERE  OR rawdataid ='"+ids+"'";
//			    
//	                stmt.executeUpdate(sql);
	                
	                rs = stmt.executeQuery("SELECT * FROM rawdata ORDER BY rawdataid ASC");
	  		          while (rs.next()) {
	  		        	  
	  		        	  
	  		        
	  		        	  //--------------------------------------------------------------------------
	  		        	rawdataid = String.valueOf(rs.getInt("rawdataid"));
	  		        	
	  		        	rawdatalog = rs.getString("rawdatalog");
	  		        	rawdatadate  = rs.getString("rawdatadate");
	  		        	DBdeletelog(rawdataid);
//	  		        	userdept = rs.getString("users_department");
//	  		        	userstagid = rs.getString("users_tid");
//	  		        	usersepc = rs.getString("users_tagid");
//	  		        	users_plno = rs.getString("users_plno");
//	  		        	userstatus = rs.getString("users_status");
//	  		        	usersorg = rs.getString("users_org");
//	  		        	usr_loc = rs.getString("users_acc_loc");
	  		        
	  		        	  
	  		          }
	  		             stmt.close();
	  			      
			   
			     
			         con.commit();
			         stmt.close();


		} catch ( SQLException e) {
			
			e.printStackTrace();
		}
		      return retid;
	  }
	  
	  public static void dbdelete()
	  {
	    Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:/root/database.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "DELETE from COMPANY where ID=8;";
	      stmt.executeUpdate(sql);
	      c.commit();

	      ResultSet rs = stmt.executeQuery( "SELECT * FROM COMPANY;" );
	      while ( rs.next() ) {
	         int id = rs.getInt("id");
	         String  name = rs.getString("name");
	         int age  = rs.getInt("age");
	         String  address = rs.getString("address");
	         float salary = rs.getFloat("salary");
	         System.out.println( "ID = " + id );
	         System.out.println( "NAME = " + name );
	         System.out.println( "AGE = " + age );
	         System.out.println( "ADDRESS = " + address );
	         System.out.println( "SALARY = " + salary );
	         System.out.println();
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	  }
	  
	  

	  public static int checkname(String tagid, Jedis jedis) throws IOException { //REDIS CHECK NAME//----------------------------------
		   

		    int ret=0;
			String SQLdbnm="";
		    Connection cM = null;
		    Statement stmt = null;
		
		    
		    
		    
//		    Jedis jedis = new Jedis("localhost");
		    String chck_usr = "0000000000";
		    
		     usersid = "null";
		      usersname = "null";
		      usersmfg  = "null";
		     
		      userstagid = "null";
		      usersepc = "null";
		      users_plno = "null";
		      userdept = "null";
		      usersorg = "null";
		      userstatus =  "null";
		      usr_loc = "null";
		    
//		    System.out.println("--sqlite check name ---");
		    CodeSource codeSource = pkMainTEST.class.getProtectionDomain().getCodeSource();
		    File jarFile;
			try {
				
				jarFile = new File(codeSource.getLocation().toURI().getPath());
				SQLdbnm = jarFile.getName()+".db";
			   
			
			     
			} catch (URISyntaxException e1) {
				
				e1.printStackTrace();
			}
		    
		  		  
				    	
			    	//----------------------------------
			    	
			    	   try{
//			 		      chck_usr =  wie_var.jedis_local.hget("mfgno_rfidno", tagid.toLowerCase().trim()); 
			 		     chck_usr =  jedis.hget("mfgno_rfidno", tagid.toLowerCase().trim()); 
	 
			 		      
			 		      String[] tmp_user_data = chck_usr.split("\\|");
//			 		      System.out.println("--> "+tmp_user_data.length3333333333);
			 		      
			 		      usersid = tmp_user_data[0];
			 		      usersname = tmp_user_data[1];
			 		      usersmfg  = tagid.trim();
			 		     
			 		      userstagid = tagid.trim();
			 		      usersepc = tagid.trim();
			 		      users_plno = tmp_user_data[2];
			 		      userdept = tmp_user_data[3];
			 		      usersorg = tmp_user_data[4];
			 		      userstatus =  tmp_user_data[5];
			 		      usr_loc = tmp_user_data[6];
			 			     String allowall =  jedis.get("allowall");
			 			    allowall="0";
			        	     if(allowall.equals("0")) {
			        	    	 
			        	    	 //example redis data : "97|Mohd Halmi Mohd Hassan|WVY 2714|Revenue Assurance|PLUS|OK|1,1"  // "695|Test RFID #1||Research and Innovation|TERAS|OK|TERAS|1,1"

			        	  
					         if(  userstatus.equals("OK") ||   userstatus.equals("ok")) {
					        	 users_REJ_ACC = "ACCEPT";
					           System.out.println( "** USERNAME ** : " +   usersname + "** STATUS ** :" +  userstatus+ "** PLNO ** :" +  users_plno+" ## STAT :"+users_REJ_ACC);	 
					           ret  =1;
//					             users_REJ_ACC = "ACCEPT";
					             pkvar.logtxt("** CARDNO ** : " +   userstagid + "** STATUS ** :" +  userstatus+ "** PLNO ** :" +  users_plno+" :"+users_REJ_ACC);
					         } else {
					        	  users_REJ_ACC = "BLACKLISTED";
					           System.out.println( "** USERNAME ** : " +   usersname + "** STATUS ** :" +  userstatus+ "** PLNO ** :" +  users_plno+" ## STAT :"+users_REJ_ACC);	
					           pkvar.logtxt("** CARDNO ** : " +   userstagid + "** STATUS ** :" +  userstatus+ "** PLNO ** :" +  users_plno+" :"+users_REJ_ACC);
					           
					           ret = 0;
					         }
					         
					         String[] tmp_user_acc_loc  = usr_loc.split(",");
					         
					         System.out.println("get user type acc[config file] = "+tmp_user_acc_loc[  loc_type]+" --- DB type acc:["+usr_loc+"] ");
					         
					         if(tmp_user_acc_loc[  loc_type].equals("1")) {
					        	 System.out.println(" ** User ALLOW for this premise! **");
					        	  pkvar.logtxt("** CARDNO ** : " +   userstagid + " ** User ALLOW for this premise! ** ");
					        	 ret  =1; 
					         }else {
					        	 System.out.println(" ** User NOT ALLOW for this premise! **");
					        	  pkvar.logtxt("** CARDNO ** : " +   userstagid + " **  User NOT ALLOW for this premise! ** ");
					        	 ret  =0; 
					         }
					         
					         //--antipassback---------------------------------------------------------------
					       String antipassbck = jedis.get("antipassbck_chck");
					        if( antipassbck.equals("true")) { 
//					         int antipassbck_chk =  pk_var.antipass_check_REDIS( tagid.trim(),pk_var.pathType,jedis);
					        	 int antipassbck_chk=0;
					         if(antipassbck_chk > 0) {
					           System.out.println(" **** GOT USER  [AUTHORIZE]  --> Check mfgno_rfidno :"+tagid.trim()+": --> "+chck_usr);
					           pkvar.logtxt("GOT USER  [AUTHORIZE]  --> Check mfgno_rfidno :"+tagid.trim()+": --> "+chck_usr);
					           ret  =1; 
					         } else {
					           
					         
					           System.out.println("GOT USER [AUTHORIZE BUT FAIL ON ANTIPASSBACK]--> Check mfgno_rfidno :"+tagid.trim()+": --> "+chck_usr);
					           pkvar.logtxt("GOT USER [AUTHORIZE BUT FAIL ON ANTIPASSBACK]--> Check mfgno_rfidno :"+tagid.trim()+": --> "+chck_usr); 
					           ret  =0; 
					         }
					        }
					        //-------------------end antipass back---
					        
					     
					        
			        	  } else {
			        		   System.out.println(" **RFID overite ALL ACCESS **");
			        		    pkvar.logtxt("**RFID overite ALL ACCESS ** ");
			        		   ret  =1; //overite all access--  
			        	  }
			 		     		 		     
			 	       } catch(NullPointerException e)
			 	        {   
			 	    	     users_REJ_ACC = "REJECT";
			 	    	     System.out.println("[error-check] : [USER NOT EXIST] [NOT AUTHORIZED!] :"+tagid.trim());
			 	    	     pkvar.logtxt("[error-check] : [USER NOT EXIST] [NOT AUTHORIZED!] :"+tagid.trim());
			 	    	     
			 	    	        usersname = "null";
				 		        usersmfg  = "null";
				 		     
				 		        userstagid = tagid.trim();
				 		        usersepc = tagid.trim();
				 		        users_plno ="null";
				 		        userdept ="null";
				 		        usersorg = "null";
				 		        userstatus = "null";
				 		        usr_loc ="null";
			 	    	        ret  =0; 
			 	    	        
			 	    	       //------ antipass whitelist check if return fail---check for whitelist pool--
			 	    	        try {
						      	int result;
						      	if(ret < 1){
						        String antipass_whitelist_en = jedis.get("antipass_whitelist_en");
						        if( antipass_whitelist_en.equals("true")) { 
						      
//						        	result=pk_var.antipass_chck_whitelist(tagid.trim(),jedis);
						        	result=0;
//						        	System.out.println("RESULT:::::::::::"+result);
						        	if(result>0) {
						        		ret=1;
//						        	 System.out.println("CHECK USER [AUTHORIZE ON WHITELIST-POOL]--> Check mfgno_rfidno :"+tagid.trim()+": --> ");
								     pkvar.logtxt("CHECK USER [AUTHORIZE ON  WHITELIST-POOL]--> Check mfgno_rfidno :"+tagid.trim()+": --> "); 
						        	
						        	}else {
						        		ret=0;
//						        	   System.out.println("CHECK USER [STILL FAIL ON WHITELIST-POOL]--> Check mfgno_rfidno :"+tagid.trim()+": --> ");
									   pkvar.logtxt("CHECK USER [STILL FAIL ON WHITELIST-POOL]--> Check mfgno_rfidno :"+tagid.trim()+": --> "); 
						        	}
						         }
						      	}
			 	               } catch(Exception v) {
						      		v.printStackTrace();
						      		pkvar.logtxt(v.toString());
						      	}
						        //------ end antipass whitelist check---
			 	         
			 	        }
			    	//----------------------------------------
			    	
			    	
//			    System.out.print("<< [REDIS-muahhss] <checkname>: >> EPC >>>> :"+tagid+" <<<< \n");
                jedis.close();
				return ret;
	   }
	  
	    
	    //------------------------------------SQLite init ---------------------
	    
	
	  //====================================================================================================================================
	
	///------------------------RELOAD USer to Redis------
    
    public void redis_loaduser() {
    	
  	  String SQLdbnm="";
  	  Connection cM = null;
  	  Statement stmt = null;
  	  String usr_loc;
  	  Jedis jedis = new Jedis("localhost");
  	  jedis.del("mfgno_rfidno") ; 
  	  
  	    CodeSource codeSource = pkMainTEST.class.getProtectionDomain().getCodeSource();
  	    File jarFile;
  		try {
  			
  			jarFile = new File(codeSource.getLocation().toURI().getPath());
  			SQLdbnm = jarFile.getName()+".db";
  		   
  		
  		     
  		} catch (URISyntaxException e1) {
  			
  			e1.printStackTrace();
  		}
  		Properties config = new Properties();
      	config.setProperty("open_mode", "1"); 

   	      try {
  			      Class.forName("org.sqlite.JDBC");
  			      cM = DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm,config);
  		 	     
  		 	      if(cM.isClosed()) {
  		 	    	  
  		 	    	  System.out.println("DATABASE SQLite CLOSE!!\n");
//  		 	    	  System.exit(0);        
  		 	      }
  		 	     cM.setAutoCommit(false);
  		 	   
  		 	     
  		 	       stmt = cM.createStatement();
  		   
  		          rs = stmt.executeQuery("SELECT * FROM users ORDER BY idusertag DESC");
  		          while (rs.next()) {
  		        	  
  		        	  
  		        	usersid = "0";
  		        	usersname = "0";
  		        	usersmfg  = "0";
  		        	userdept = "0";
  		        	userstagid = "0";
  		            usersepc = "0";
  		        	users_plno = "0";
  		        	userstatus = "0";
  		        	usersorg = "0";
  		        	  //--------------------------------------------------------------------------
  		        	usersid = String.valueOf(rs.getInt("idusertag"));
  		        	usersname = rs.getString("users_names");
  		        	usersmfg  = rs.getString("employee_id");
  		        	userdept = rs.getString("users_department");
  		        	userstagid = rs.getString("users_tid");
  		        	usersepc = rs.getString("users_tagid");
  		        	users_plno = rs.getString("users_plno");
  		        	userstatus = rs.getString("users_status");
  		        	usersorg = rs.getString("users_org");
  		        	  usr_loc = rs.getString("users_acc_loc");
  		        	  if(!usersmfg.isEmpty()) {
  		        	      jedis.hset("mfgno_rfidno", usersmfg.toLowerCase() ,usersid+"|"+ usersname+"|"+users_plno+ "|" +userdept+"|"+ usersorg+"|"+userstatus +"|" + usr_loc) ;
//  		        	      System.out.println("MFGNO ["+  usersmfg+"] : "+  usersid+"||"+   usersname+"||"+  users_plno+ "||" +  userdept+"||"+   usersorg+"||"+  userstatus +"||" + usr_loc+"\n");
  		        	  }  
  		        	  if(!usersepc.isEmpty()) {
  		        		  jedis.hset("mfgno_rfidno", usersepc.toLowerCase() ,usersid+"|"+ usersname+"|"+users_plno+ "|" +userdept+"|"+ usersorg+"|"+userstatus +"|" + usr_loc) ;
//  		        		   System.out.println("RFID ["+  usersepc+"]:  "+  usersid+"||"+   usersname+"||"+  users_plno+ "||" +  userdept+"||"+   usersorg+"||"+  userstatus +"||" + usr_loc+"\n");
  		        	  }
  		        	  
  		          }
  		             stmt.close();
  			         cM.close();
  			        
  		} catch (ClassNotFoundException | SQLException e) {
  			
  			e.printStackTrace();
  		}
   	   
  	  
    }
	
	//----------------register user SQLite/Redis---------------------------
	
    @SuppressWarnings("resource")
    public static void redis_reguser(String datauser) {
    		
    	  int deleteuseronly=0;
    	  String SQLdbnm="";
    	  Connection cM = null;
    	  Statement stmt = null;
    	  String usr_loc;
    	   String sql="";
    	   String idusertag= null,users_names= null,users_tagid= null,users_tid = "not yet",employee_id= "null",users_department= null,tarikh_reg= null,
    	   users_status= null,users_plno= null,users_org= null,users_remark= "-",users_acc_loc= null,users_acc_lvl= "operator",users_UPDATE = null;
    	  Jedis jedis = new Jedis("localhost");
//    	  jedis.del("mfgno_rfidno") ; 
    	  
    	  
    	  //------------------split data-----------------
    	  try {
    	  String[] tmp_datauser = datauser.split("\\|");
    	  
//    	  System.out.println("length to register :"+tmp_datauser.length);
    	  
//    	  System.out.println(Arrays.toString(tmp_datauser));
    	  
    	  if(tmp_datauser.length<5) { //delete register user
    		  users_tagid = tmp_datauser[0]; //main use as card num,tagid,tid in redis
    		  idusertag = tmp_datauser[1];
    		  users_names=tmp_datauser[2];
    		 String cmdstr =tmp_datauser[3];
    		  if(cmdstr.equals("DELETE")) {
//    			  DBsqlite_deleteuser(users_tagid,idusertag);//delete reg user sqlite
    			  System.out.println("["+pkvar.tarikh()+"] [CMD]delete user :"+users_tagid +",name :"+users_names);
    			  jedis.hdel("mfgno_rfidno",users_tagid.toLowerCase());
    			  deleteuseronly=1;
    		  }
    		  
    	  } else {
    		  
    		  users_tagid = tmp_datauser[0]; //main use as card num,tagid,tid in redis
    		  idusertag = tmp_datauser[1];
    		  users_names=tmp_datauser[2];
    		  users_plno=tmp_datauser[3];
    		  users_department=tmp_datauser[4];
    		  users_org=tmp_datauser[5];
    		  tarikh_reg=pkvar.tarikh();
    		  users_status=tmp_datauser[6];
    		  users_acc_loc=tmp_datauser[7];
    		  users_UPDATE=tmp_datauser[8];
    		  System.out.println("["+pkvar.tarikh()+"][ CMD:"+ users_UPDATE+" ] -> :"+Arrays.toString(tmp_datauser));
    	  }
    	  
    	  
    	
    	  
    	  //---standby data--
    	  if(tmp_datauser.length>9) {
    		  

    		  employee_id=tmp_datauser[9];
    		  
    		  users_remark=tmp_datauser[10];
    		  users_acc_lvl=tmp_datauser[11];
    	   }
    	  } catch ( ArrayIndexOutOfBoundsException e ) {
    		  e.printStackTrace();
    		  
    		Thread	subthread = new Thread() {
    		        public void run() {
//    		   		 amqp_attr.init_SUBrabbitMQ(); //--------------------------init rabbit subcriber...
    		        }
    		    };
    		 subthread.start();
    	  }
    	
    	  //----------------
    	  
//    	  if(!employee_id.isEmpty()) {
//    	      jedis.hset("mfgno_rfidno", employee_id.toLowerCase() ,users_tid+"|"+ users_names+"|"+users_plno+ "|" +users_department+"|"+ users_org+"|"+users_status +"|" + users_acc_loc) ;
////    	      System.out.println("MFGNO ["+jRFID_var.usersmfg+"] : "+jRFID_var.usersid+"||"+ jRFID_var.usersname+"||"+jRFID_var.users_plno+ "||" +jRFID_var.userdept+"||"+ jRFID_var.usersorg+"||"+jRFID_var.userstatus +"||" + usr_loc+"\n");
//    	  }  
    	  
    	  // in redis card number resgister as tagid
    	
    	 
    	 if(deleteuseronly < 1) { //register user only------------------------------
    		  if(!users_tagid.isEmpty()) {
    			  jedis.hset("mfgno_rfidno", users_tagid.toLowerCase() ,idusertag+"|"+ users_names+"|"+users_plno+ "|" +users_department+"|"+ users_org+"|"+users_status +"|" + users_acc_loc) ;
//    			   System.out.println("RFID ["+jRFID_var.usersepc+"]:  "+jRFID_var.usersid+"||"+ jRFID_var.usersname+"||"+jRFID_var.users_plno+ "||" +jRFID_var.userdept+"||"+ jRFID_var.usersorg+"||"+jRFID_var.userstatus +"||" + usr_loc+"\n");
    		  }
    	    CodeSource codeSource = pkMainTEST.class.getProtectionDomain().getCodeSource();
    	    File jarFile;
    		try {
    			
    			jarFile = new File(codeSource.getLocation().toURI().getPath());
    			SQLdbnm = jarFile.getName()+".db";
    		   
    		
    		     
    		} catch (URISyntaxException e1) {
    			
    			e1.printStackTrace();
    		}
//    		Properties config = new Properties();
//        	config.setProperty("open_mode", "1"); 

     	      try {
    			      Class.forName("org.sqlite.JDBC");
    			      cM = DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
//    		 	     
    		 	      if(cM.isClosed()) {
    		 	    	  
    		 	    	  System.out.println("DATABASE SQLite CLOSE!!\n");
//    		 	    	  System.exit(0);        
    		 	      }
    		 	       cM.setAutoCommit(false);
    		 	   
    		 	     
    		 	       stmt = cM.createStatement();
    		     
    		 	       if(users_UPDATE.equals("UPDATE")) {
    		 	    	   

    		 	    	   
    		 	    	  // old tagid # new tagid
    		 	    	   //'3420631356#1122334455|124|Ramzee|WXD4321|CEO|Skater|OK|1,1|UPDATE'
    		 	    	  String new_tagid ="";
    	 	    		  String old_tagid ="";
    	 	    		  String [] userstagid = users_tagid.split("#");
    	 	    		  if(!users_tagid.isEmpty()) {
    		 	    		  
    			 	    		
    			 	    		 old_tagid = userstagid[0];
    			 	    		 new_tagid = userstagid[1];
    			 	    		  
    			 	    		  jedis.hdel("mfgno_rfidno",old_tagid.toLowerCase());
    			 				  jedis.hset("mfgno_rfidno", new_tagid.toLowerCase() ,idusertag+"|"+ users_names+"|"+users_plno+ "|" +users_department+"|"+ users_org+"|"+users_status +"|" + users_acc_loc) ;
//    			 				   System.out.println("RFID ["+jRFID_var.usersepc+"]:  "+jRFID_var.usersid+"||"+ jRFID_var.usersname+"||"+jRFID_var.users_plno+ "||" +jRFID_var.userdept+"||"+ jRFID_var.usersorg+"||"+jRFID_var.userstatus +"||" + usr_loc+"\n");
    			 			  }
    		 	    	   
    		 	     sql="UPDATE users SET idusertag='"+idusertag+"',users_names='"+users_names+"',users_tagid='"+new_tagid+"',users_tid='"+users_tid+"'"
    		 	     		+ ",employee_id='"+employee_id+"',users_department='"+users_department+"',tarikh_reg='"+tarikh_reg+"',"
    					       		+ "users_status='"+users_status+"',users_plno='"+users_plno+"',users_org='"+users_org+"',users_remark='"+users_remark+"',"
    					       				+ "users_acc_loc='"+users_acc_loc+"',users_acc_lvl='"+users_acc_lvl+"' WHERE users_tagid='"+old_tagid+"'";
    		 	    	   
    		 	       } else {
    		 	
    			       sql="INSERT INTO users (idusertag,users_names,users_tagid,users_tid,employee_id,users_department,tarikh_reg,"
    			       		+ "users_status,users_plno,users_org,users_remark,users_acc_loc,users_acc_lvl)" +
    				      		" VALUES('"+idusertag+"','"+users_names+"','"+users_tagid+"','"+users_tid+"','"+employee_id+"','"+users_department+"',"
    				      				+ "'"+tarikh_reg+"','"+users_status+"','"+users_plno+"','"+users_org+"','"+users_remark+"',"
    				      						+ "'"+users_acc_loc+"','"+users_acc_lvl+"')";
                      
    		 	       }
//    			       System.out.println("SQL: reg: "+sql);
    			      stmt.executeUpdate(sql);
    			   
    			     
    			         cM.commit();
    			         stmt.close();
    			         cM.close();
//    			         cM.setReadOnly(true);
    		} catch (ClassNotFoundException | SQLException e) {
    			
    			e.printStackTrace();
    		}
    	 }    
     	     jedis.close();
     	   
    	  
      }
	  

  public static void dbselectall()
  { 
	  
	    Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:/root/databases.db");
	      if(c.isClosed()) {
	    	  
	    	  System.out.println("DATABASE CLOSE!!");
	    	  System.exit(0);
	      }
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM COMPANY;" );
	      while ( rs.next() ) {
	         int id = rs.getInt("id");
	         String  name = rs.getString("name");
	         int age  = rs.getInt("age");
	         String  address = rs.getString("address");
	         float salary = rs.getFloat("salary");
	         System.out.println( "ID = " + id );
	         System.out.println( "NAME = " + name );
	         System.out.println( "AGE = " + age );
	         System.out.println( "ADDRESS = " + address );
	         System.out.println( "SALARY = " + salary );
	         System.out.println();
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");

    
  }
  
  public static void dbupdate()
  {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:/root/database.db");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1;";
      stmt.executeUpdate(sql);
      c.commit();

      ResultSet rs = stmt.executeQuery( "SELECT * FROM COMPANY;" );
      while ( rs.next() ) {
         int id = rs.getInt("id");
         String  name = rs.getString("name");
         int age  = rs.getInt("age");
         String  address = rs.getString("address");
         float salary = rs.getFloat("salary");
         System.out.println( "ID = " + id );
         System.out.println( "NAME = " + name );
         System.out.println( "AGE = " + age );
         System.out.println( "ADDRESS = " + address );
         System.out.println( "SALARY = " + salary );
         System.out.println();
      }
      rs.close();
      stmt.close();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Operation done successfully");
  }
  
  public static String getDBnm() {
	   //---app name--- as SETTING FILE
	  String jar_nm = "bin";
//       CodeSource codeSource = wie_Main.class.getProtectionDomain().getCodeSource();
//       File jarFile;
//	try {
//		
//		jarFile = new File(codeSource.getLocation().toURI().getPath());
//		jar_nm = jarFile.getName();
//	   //   System.out.print("<< CONFIG NAME: >> :"+jarDir+"\n");
//	     
//	} catch (URISyntaxException e1) {
//		
//		e1.printStackTrace();
//	}  
	  try{
	  jar_nm = pkconfig.jReadConfig("dbname");
	  } catch (Exception e) {
		  e.printStackTrace();
	  }
     return jar_nm;
  }
  public static void dbinsert()
  {
	  Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:/root/database.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "INSERT INTO COMPANY (NAME,AGE,ADDRESS,SALARY) " +
	                   "VALUES ('xxx', 123, 'Calxxx', 12345.67 );"; 
	      stmt.executeUpdate(sql);

	  //    sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
	        //    "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );"; 
	    //  stmt.executeUpdate(sql);

	      //sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
	       //     "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );"; 
	      //stmt.executeUpdate(sql);

	    //  sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
	     //       "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );"; 
	    //  stmt.executeUpdate(sql);

	      stmt.close();
	      c.commit();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Records created successfully");
  }
  
  

  public static int SQLitecheckname(String tagids)
  {
	int ret=0;
	String SQLdbnm="";
    Connection c = null;
    Statement stmt = null;
    
    SQLdbnm = getDBnm();
    System.out.print("<< DBPATH: >> :"+SQLdbnm+"\n");
    
    try {
    	 
 	      Class.forName("org.sqlite.JDBC");
 	      c = DriverManager.getConnection("jdbc:sqlite:"+SQLdbnm);
 	      if(c.isClosed()) {
 	    	  
 	    	  System.out.println("DATABASE SQLite CLOSE!!");
// 	    	  System.exit(0);
 	      }
 	      c.setAutoCommit(false);
 	     // System.out.println("Opened database successfully :"+tg);
 	     
 	      stmt = c.createStatement();


      ResultSet rs0 = stmt.executeQuery( "SELECT * FROM users WHERE usersmfg='"+tagids+"'" );
      while ( rs0.next() ) {
    	  usersid = String.valueOf(rs0.getInt("usersid"));
    	  usersname = rs0.getString("usersname");
    	  usersmfg  = rs0.getString("usersmfg");
    	  userstagid = rs0.getString("userstagid");
    	  usersepc = rs0.getString("usersepc");
    	  System.out.println("NAME : "+ usersname);

         ret = 1;
      }
      if(ret == 1){
      String sql = "INSERT INTO raw_log (rawdate,rawuserid,rawname,rawtagid,rawpath,rawstatus) " +
              "VALUES ('"+pkvar.tarikh()+"', "+usersid+", '"+usersname+"', '"+ usersmfg+"', '"+wieloc+"', 'ok' );"; 
      stmt.executeUpdate(sql);
//      rs.close();
      c.commit();
      } else {
    	  String sql = "INSERT INTO raw_log (rawdate,rawuserid,rawname,rawtagid,rawpath,rawstatus) " +
                  "VALUES ('"+pkvar.tarikh()+"', 'not register', 'not register', '"+ tagids+"', '"+wieloc+"', 'reject' );"; 
          stmt.executeUpdate(sql);
//          rs.close();
          c.commit(); 
      }
      rs0.close();
      stmt.close();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    return ret;
//    System.out.println("Operation done successfully");
  }

  
}