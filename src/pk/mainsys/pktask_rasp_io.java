	package pk.mainsys;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Toolkit;






import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import redis.drac.pkredis_var;

 
public class pktask_rasp_io {
	Toolkit toolkit;
	Timer timer;
	public static Logger log = LoggerFactory.getLogger(pktask_rasp_io.class);
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
	static Pin pinLOOP1 = null ;
	static Pin pinLOOP2  = null  ;
  
	 final GpioController gpio = GpioFactory.getInstance();

     GpioPinDigitalInput lop1 = null;
     GpioPinDigitalInput lop2 = null;
	
 
	public pktask_rasp_io() throws Exception {
		log.info("Start > pktask_rasp_io <");
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
		
		  pinLOOP1 = (Pin) pkvar.getRaspPin(Integer.valueOf(pkvar.gpioAddr_loop1));
		  pinLOOP2 = (Pin) pkvar.getRaspPin(Integer.valueOf(pkvar.gpioAddr_loop2));
		
		   lop1 = gpio.provisionDigitalInputPin( pinLOOP1, PinPullResistance.PULL_DOWN);
	       lop2 = gpio.provisionDigitalInputPin( pinLOOP2, PinPullResistance.PULL_DOWN);
	       lop1.setShutdownOptions(true);
	       lop2.setShutdownOptions(true);
	       
	       
		  
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 50); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			
				try {
				//System.out.println("timer run " );
//				wtdgtmr_sendreq_ext +=1;
//				System.out.println("STATE loop 1: "+lop1.getState());
//				System.out.println("STATE loop 2: "+lop2.getState());
			    pkvar.pinLOOP1_state = lop1.getState();
			    pkvar.pinLOOP2_state = lop2.getState();

			  
				//---lopp1 detect----	
			    
			  	 if( pkconfig.AUTHORIZE.equals("yes")) {
//			  		System.out.println("Authorize!!!!!!!!!! ");
	        		 pkvar.ent_BUZZ =1;
	        		 pkrasp_io.gpio_trigAUTH_ALB(1);

	        	 } 
			    
//				pkconfig.loop1 = pkvar.pinLOOP1_state.isLow();
		        if(pkvar.pinLOOP1_state.isLow() ) { //---------detect loop 1
		        	pkredis_var.statloop1="1";
		        	pkconfig.loop1 = true;
//		       			System.out.println("detect loop 1: ");
	//	        	log.info(pkvar.ANSI_BLUE_BACKGROUND+" -- loop1 status -- "+pkvar.pinLOOP1_state.isLow());
		        	 if( pkconfig.AUTHORIZE.equals("yes")) {
		        	
		        		 pkvar.ent_BUZZ =1;
		        		 pkrasp_io.gpio_trigAUTH_ALB(1);
	
		        	 } 
	
				} 
		        
		        else {
		          	pkconfig.loop1 = false;
		        	pkredis_var.statloop1="0";
	//	          	Thread.sleep(2000);
		        	pkconfig.AUTHORIZE="no";
		           	pkrasp_io.gpio_trigAUTH_ALB(0);
	            	//wie_var.ent_AUTH =1;
	//            	System.out.println("off alb...\n " );
				}
		        
		        if(pkvar.pinLOOP2_state.isLow()) { 
		        	pkredis_var.statloop2="1";
		        }else {
		        	pkredis_var.statloop2="0";
		        }
			//--------------end-------
			
		        
		        //---backdoor--
//		        String forcealb = pkredis_var.get_cmd("forcealb"); 
//		        
//		        if(forcealb.equals("1")) {
//		        	 pkrasp_io.gpio_trigAUTH_ALB(1);
//		        	 pkredis_var.set_cmd("forcealb","0");
//		        }
			//--without loop1 detect------
			
//		        	 if( wie_var.ent_AUTH > 0) {
//		        		
//		        		 wie_var.ent_BUZZ =1;
//		        		 wie_io.gpio_trigAUTH_ALB(1);
//		        		
//		        		  	Thread.sleep(2000);
//		                	wie_io.gpio_trigAUTH_ALB(0);
//		                	wie_var.ent_AUTH =1;
//		                	System.out.println("off alb...\n " );
//		        	    } 
		        	
		        	//-- end without loop1 detect------	
			
//            if(wie_var.pinLOOP2_state.isLow()) { // detect loop 2
//            	wie_var.ent_AUTH =0;
//            	wie_io.gpio_trigAUTH_ALB(0);
//        	  System.out.println("<Loop 2 > Detect :wie_var.pinLOOP2_state.isHigh()"+wie_var.pinLOOP2_state.isHigh());
//            	System.out.println("Loop 2 detect lahanat\n " );
//			}

				
				} 
				catch (Exception e) {
					try {
						Thread.sleep(100);
						//new wie_taskIO();
					} catch (Exception e1) {
						
						e1.printStackTrace();
					}
					e.printStackTrace();
				}
			
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}