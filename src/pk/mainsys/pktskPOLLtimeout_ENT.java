package pk.mainsys;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import nioSOCKreaderCoherent.nioCoher_ENT;
import nioSOCKreaderCoherent.nioCoher_EXIT;
import redis.drac.pkredis_var;

import java.awt.Toolkit;



 
public class pktskPOLLtimeout_ENT {
	public static final TimerTask tmrReminder = null;
	Toolkit toolkit;
	Timer timer;
	int timeout=10;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
	nioCoher_ENT nio_mfgreader_ENTxx;
	int timeoutautorecon;
//	    static int TIMOUT=0;
		private static Log log = LogFactory.getLog(pktskPOLLtimeout_ENT.class);
	public pktskPOLLtimeout_ENT() throws Exception {
		log.info("start --> "+pktskPOLLtimeout_ENT.class);
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
	   
	       
//	       System.out.println(" -- WIE TASK TRXDATA -- " );
		nio_mfgreader_ENTxx = new nioCoher_ENT();
		timeout = Integer.valueOf(pkvar.timeout);
		timeoutautorecon = Integer.valueOf(pkvar.timeoutautorecon);
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 1000); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			
			try{	
			

			   String simulateloop1 = pkredis_var.get_cmd("simulateloop1");
		
//			   System.out.println("----- count time out ----: "+TIMOUT);
				if((pkconfig.simulateloop1.equals("1") || 	pkconfig.loop1 ||  simulateloop1.equals("1"))  && pkconfig.AUTHORIZE.equals("no")    ) {
					
					pkByteData.TIMOUT++;
					pkByteData.TIMOUT_whole++;
				  
					if(pkByteData.TIMOUT>timeout) {
					   System.err.println("--RESET ENTRY not receive any bytes--- "+pkByteData.TIMOUT);
					     pkByteData. TIMOUT=0;
					     pkByteData.SEQUENCE_PROC=1;

//                      	nio_mfgreader_ENTxx.stop_stop();
					    byte [] byts = pkByteData.bytSND_INIT_ENT_compiled();
                 	    pktsk_Qsnd_ENT.fifobyt_TX.add(byts);

						
				   }
					
				}else {
					pkByteData.TIMOUT=40;
				}
				//###############################################3timeout######################################
				if(pkByteData.TIMOUT_whole>timeoutautorecon) {

					pkredis_var.set_cmd("readerbanktimeout", "true");
					pkvar.logtxt("#### readerbanktimeout-entry $$$");
                 	System.err.println("--TIMEOUT--TIMOUT_whole---] %%%%% (ENTRY)--- "+pkByteData.TIMOUT_whole);
				}

			}
			catch (Exception e) {

				e.printStackTrace();
//				pk_var.logtxt("\n--------------------------\n"+e.toString()+"\n------------------------------\n");
			}
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}