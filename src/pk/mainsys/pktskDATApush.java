package pk.mainsys;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Toolkit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import redis.drac.pkredis_var;
 

 
public class pktskDATApush {
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
	private static Log log = LogFactory.getLog(pktskDATApush.class);
 
	public pktskDATApush() throws Exception {

		log.info("start :>>>>>>>>>>>>    pktmrDATApush :");
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 500); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
		
 
		public void run()  {
			
		
			try{	
			
//			       amqp_attr.rabbit_pushlog();
//				byte[] byt = pkbytedata.bytSND_EN_compiled();
//				pktask_rxtxOFFLINE.fifobyt_TX.add(byt);
//				this.cancel();
				if(pkconfig.readerpath.equals("exit")) {
					
				
						  if(!pkconfig.data_topush.isEmpty()) {		
						
						    while (! pkconfig.data_topush.isEmpty()) {
						    	 String dat = pkconfig.data_topush.poll();
//					           amqp_attr.amqp_pub(amqp_attr.amqp_q ,dat);
						    	 System.out.println("rawdata push : "+dat);
						    	 pkredis_var.lpush_cmd("rawdata", dat);
						    	 pkSQLITE.dbinsertlog(dat, pkvar.tarikh());
					   
						    }
						  }
//						  pkSQLITE.DBselectlog_todel();
								
				//------------------------------log only-------------------
				if(!pkconfig.data_topushdb.isEmpty()) {
					
					
						  while (! pkconfig.data_topushdb.isEmpty()) {
							  String dat = pkconfig.data_topushdb.poll();
//						        pkSQLITE.dbinsertlog(dat, pkvar.tarikh());
						        pkSQLITE.dbinsertloglog(dat, pkvar.tarikh());
						       // pkredis_var.lpush_cmd("rawdata", dat);
						  }
					
					  
			    }
				
			 }
			
			//---------------for entry -------------------------------------
			if(pkconfig.readerpath.equals("entry")) { 
				
				  if(!pkconfig.data_topush.isEmpty()) {		
						
					    while (! pkconfig.data_topush.isEmpty()) {
					    	 String dat = pkconfig.data_topush.poll();
//				             amqp_attr.amqp_pub(amqp_attr.amqp_q ,dat);
					    	 System.out.println("rawdata push : "+dat);
					    	  pkredis_var.lpush_cmd("rawdata", dat);
					    	  pkSQLITE.dbinsertlog(dat, pkvar.tarikh());
				   
					    }
				  }
				
				if(!pkconfig.data_topushdb.isEmpty()) {
										
					  while (! pkconfig.data_topushdb.isEmpty()) {
						  String dat = pkconfig.data_topushdb.poll();
//					        pkSQLITE.dbinsertlog(dat, pkvar.tarikh());
					        pkSQLITE.dbinsertloglog(dat, pkvar.tarikh());
					      
					  }
				
				  
		        }
		
					  
			
				
			  }
			}
			catch (Exception e) {

				e.printStackTrace();
//				pk_var.logtxt("\n--------------------------\n"+e.toString()+"\n------------------------------\n");
			}
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}