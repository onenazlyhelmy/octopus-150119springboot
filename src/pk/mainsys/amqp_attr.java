package pk.mainsys;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import redis.clients.jedis.Jedis;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;


public class amqp_attr {	//-------RABbit mq---

    public static  ConnectionFactory factory;
    public static Connection connection;
    public static  Channel channel;
    //----------------
    
	public  static Thread p_thrd_pub;
	public  static Thread p_thrd_sub;
	
	
	//amqp
	static String activeorno="notactive";
	  public static String amqp_ip="localhost";
			  public static int	amqp_port=5672;
			  public static String  amqp_user="root";
					  public static String	amqp_pwd="root123";
					  public static int  amqp_timeout=300;
							  public static String 	amqp_vhost="/";
									  public static String  amqp_type="sub";
									  public static String	  amqp_q="taik";
									  
									  //redis
									  public static String  redis_ip="localhost";
									  public static String	  redis_port="6379";
										
													  public static String redis_type="hash";
															  public static String redis_key="datalog";
															public static String redis_push_en;
															public static String redis_push_val;
															public static int redis_poll_interval;
															public static int redis_push_interval;
															public static String redis_poll_en;
															public static int amqp_tmr;
															public static String amqp_q1;
															public static String rabbit_en;
	
	   public static String init_amqp_pub(String rabbit_ipsvr,int rabbit_port ,String rabbit_qname,String rabbit_user,String rabbit_pwd,String rabbit_vhost,Long thrdID)  {
//	    	 InetAddress addr;
//	    	    try {
//					addr = InetAddress.getLocalHost();
//					 hostnm = addr.getHostName();
//				} catch (UnknownHostException e) {
//					
//					e.printStackTrace();
//				}
		   amqp_attr.activeorno="notactive";
	    	        System.out.println(" ***************************************************** \n");
	    		    System.out.println("Init ["+thrdID+"]-  ["+tarikh()+"] [PUB] Rabbitmq : "+ rabbit_ipsvr+":"+rabbit_port+", Qname: "+rabbit_qname+"\n" );
	    		    System.out.println(" ***************************************************** \n");
	    		    ConnectionFactory factory = new ConnectionFactory();
	    		    factory.setHost(rabbit_ipsvr);
	    		    factory.setVirtualHost(rabbit_vhost);
	    		    factory.setAutomaticRecoveryEnabled(true);
	    		    factory.setConnectionTimeout(200);
	    		    factory.setRequestedHeartbeat(360);
	    		    factory.setNetworkRecoveryInterval(1000);
	    		    factory.setUsername(rabbit_user);
	    		    factory.setPassword(rabbit_pwd);
	    		    factory.setPort(rabbit_port);
	    		 
	    		
	    		   
					try {
						 connection = factory.newConnection();
						 channel = connection.createChannel();
						 channel.queueDeclare(rabbit_qname, true, false, false, null);
						 amqp_attr.activeorno = "active";
						 System.err.println("CONECTED Rabbitmq server !!!!");
					} catch (Exception e) {
						amqp_attr.activeorno = "notactive";
						
						e.printStackTrace();
						
//							jRFID_var.logtxt(e.toString()); //log
//							jRFID_var.logtxt(e.getMessage()); //log
							
					}
					    		   
					return  amqp_attr.activeorno;
	    	   
	    }
		  public static void amqp_pub(String qname ,String msg){
				try {
					channel.basicPublish("",  qname, null, msg.getBytes("UTF-8"));
				} catch (UnsupportedEncodingException e) {
				
					e.printStackTrace();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
		  }
		   public static String init_amqp_sub(String rabbit_ipsvr,int rabbit_port ,String rabbit_qname,String rabbit_user,String rabbit_pwd,String rabbit_vhost,Long thrdID)  {
//		   	 InetAddress addr;
//		   	    try {
//						addr = InetAddress.getLocalHost();
//						 hostnm = addr.getHostName();
//					} catch (UnknownHostException e) {
//						
//						e.printStackTrace();
//					}
		   	    
			   amqp_attr.activeorno="notactive";
		   		    ConnectionFactory factory = new ConnectionFactory();
		   		    factory.setHost(rabbit_ipsvr);
		   		    factory.setAutomaticRecoveryEnabled(true);
		   		    factory.setVirtualHost(rabbit_vhost);
		   		    factory.setConnectionTimeout(200);
		   		    factory.setRequestedHeartbeat(360);
		   		    factory.setNetworkRecoveryInterval(1000);
		   		    factory.setUsername(rabbit_user);
		   		    factory.setPassword(rabbit_pwd);
		   		    factory.setPort(rabbit_port);
		   		 
		   		  Connection connection;
		   		  Channel channel = null;
		   		   
		   		   
		   					try {
		   					  System.out.println(" ***************************************************** \n");
		   					  System.out.println("Init ["+thrdID+"]- ["+tarikh()+"] [SUB] Rabbitmq :"+ rabbit_ipsvr+" Qname: "+rabbit_qname +"\n");
		   				      System.out.println(" ***************************************************** \n");
		   					 connection = factory.newConnection();
		   					 channel = connection.createChannel();
		   					 channel.queueDeclare(rabbit_qname, true, false, false, null);
		   					 amqp_attr.activeorno = "active";
		   				     System.out.println("\n --- [SUB] Rabbitmq :"+ rabbit_ipsvr+"  ["+ amqp_attr.activeorno+"]-- \n"); 
		   					} catch (Exception e) {
		   					 amqp_attr.activeorno = "notactive";
					  	        System.out.println("\n ---["+e.toString()+"]-- \n");
					  	        System.out.println("\n ---trace ["+Arrays.toString(e.getStackTrace())+"] -- \n");
					  	        System.out.println("\n ---class ["+e.getClass()+"]-- \n");
					  	        System.out.println("\n ---[re-connect [SUB] Rabbitmq :"+ rabbit_ipsvr+" ]-- \n");
//		   						e.printStackTrace();
		   					}


						    try {
					
						  	    Consumer consumer = new DefaultConsumer(channel) {
						  	      @Override
						  	      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
						  	          throws IOException {
						  	        String message = new String(body, "UTF-8");
						  	        System.out.println(" [x] Received #-- " + message + " --#");
//						  	        logtxtPARAM("receive param : "+message);
//						  	        logtxtSCRIPT("receive param : "+message);
						  	        if(!message.equals("")) {
						  	        	System.out.println(tarikh()+ ": [x] valid message ?? --#");
//						  	        	 jRFID_SQLite.redis_reguser(message);
						  	        	
						  	        	
						  	        } else {
						  	        	System.out.println(tarikh()+": [x] No message ?? --#");
//						  	        	 logtxtPARAM("receive param : No message: "+message);
						  	        }
						  	       
						  	       
						  	      }
						  	    };
								channel.basicConsume(rabbit_qname, true, consumer);
							} catch (Exception e) {
								
//								e.printStackTrace();
							}
						    
						    return  amqp_attr.activeorno;
		   	   
		   }
		 public static String tarikh(){
		    	
		   	 Date dateNow = new Date();
				    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
				    SimpleDateFormat format_dt= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				    
				  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
				    String date_to_string = format_dt.format(dateNow);
				 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
		   	
		   	return date_to_string;
		   	
		   }
		 
		 public  static String logtxt(String str)
		 {
			   Date dateNow = new Date();
			   
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
			    
			    String date_to_string = format_dt.format(dateNow);
		    		String data = str;
		    
		    		try {
		    			 	    		
		    		BufferedWriter  out = new BufferedWriter(new FileWriter
		    	            ("log//"+date_to_string+"_ga.txt",true));
		    	            out.write(tarikh()+"::"+data+"\n");
		    	            out.flush();
		    	            out.close();
		    		}catch(Exception v)  {
		    			v.printStackTrace();
		    		}
		    	            return "ok";
		     	//	BufferedWriter  out = new BufferedWriter(new FileWriter
		         //    (file,true));
		        //     out.write(data+"..system run..\n");
		         //    out.close();
		 
			      //  System.out.println("Done");
		 
		    	        
		    } 
		 public static String consumetask(String msg) {
			 String ret = "notok";
			 
			 return ret;
		 }
}
