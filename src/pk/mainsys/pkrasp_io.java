package pk.mainsys;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;


public class pkrasp_io {
	static GpioController gpio = GpioFactory.getInstance();
//	static GpioPinDigitalInput myButton=null;
	static  GpioPinDigitalOutput pinENT_ALBio;
	static  GpioPinDigitalOutput pinENT_BUZZio;
	static  GpioPinDigitalOutput pinENT_AUTHio;
	static  GpioPinDigitalOutput pinEXT_ALBio;
	static  GpioPinDigitalOutput pinEXT_BUZZio;
	static  GpioPinDigitalOutput pinEXT_AUTHio;
	
	public pkrasp_io(Pin ent_alb,Pin ent_buzz,Pin ent_auth,Pin ext_alb,Pin ext_buzz,Pin ext_auth) {
//		 GpioPinDigitalOutput 	pin_AUTH = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_24, "LED24", PinState.HIGH);
//		  pin_UNAUTH = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, "LED25", PinState.LOW);
		ent_alb =(Pin) ent_alb;
		ent_buzz =(Pin) ent_buzz;
		ent_auth =(Pin) ent_auth;
		ext_buzz =(Pin) ext_buzz;
		ext_alb =(Pin) ext_alb;
		ext_auth =(Pin) ext_auth;
		pinENT_ALBio=gpio.provisionDigitalOutputPin(ent_alb, "ent_alb", PinState.LOW); 
		pinENT_BUZZio= gpio.provisionDigitalOutputPin(ent_buzz, "ent_buzz", PinState.LOW);
		pinENT_AUTHio= gpio.provisionDigitalOutputPin(ent_auth, "ent_auth", PinState.LOW);
		pinEXT_ALBio=gpio.provisionDigitalOutputPin(ext_alb, "ext_alb", PinState.LOW); 
		pinEXT_BUZZio= gpio.provisionDigitalOutputPin(ext_buzz, "ext_buzz", PinState.LOW);
		pinEXT_AUTHio= gpio.provisionDigitalOutputPin(ext_auth, "ext_auth", PinState.LOW);
		
	}
    public static void gpio_trigAUTH_ALB(int bool) {

    	
  	   
  	  try {
   
  		  if(bool > 0) {
  			pinENT_ALBio.high();
  		  } else{
  			pinENT_ALBio.low();
  		  }
  
		Thread.sleep(10);
		
		} catch (InterruptedException e) {
			System.out.println(" stuck :gpio_trigAUTH_ALB");
			e.printStackTrace();
		}
//        gpio.shutdown();
  }
    
    public static void gpio_trigAUTH_buzz(int bool) {

    	
   	   
    	  try {
     
    		  if(bool > 0) {
    			  pinENT_BUZZio.high();
    		  } else{
    			  pinENT_BUZZio.low();
    		  }
    
  		Thread.sleep(5);
  		
  		} catch (InterruptedException e) {
  			System.out.println(" stuck :gpio_trigAUTH_buzz");
  			e.printStackTrace();
  		}
//          gpio.shutdown();
    }

    

 
    public static void evt_lop0() {
//    	  final GpioController gpio = GpioFactory.getInstance();
//
//	        
////	        final GpioPinDigitalInput lop0 = gpio.provisionDigitalInputPin(RaspiPin.pin_lop0, PinPullResistance.PULL_DOWN);
//	        
//	        
//	        final GpioPinDigitalInput lop0 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_27, PinPullResistance.PULL_DOWN);
//
//	       
//	        lop0.addListener(new GpioPinListenerDigital() {
//	            @Override
//	            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
//	                // display pin state on console
//	                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
//	            }
//	            
//	        });
//	        
////	        System.out.println(" test gpio--> GPIO_04.");
//	        
//	        // keep program running until user aborts (CTRL-C)
//	        while(true) {
//	            try {
//					Thread.sleep(50);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	        }
    }
}