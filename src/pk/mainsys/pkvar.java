package pk.mainsys;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.security.CodeSource;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.sql.*;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import boj.sialw.pkboj;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



public class pkvar {
	public static String cardinfojson="window sux!";

public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

	
	public static  String gpioAddr_loop1;
	public static  String gpioAddr_loop2; 
	public static  String gpioAddr_ent_alb;
	public static  String gpioAddr_ext_alb; 
	
	public static  String gpioAddr_ent_buzz; 
	public static  String gpioAddr_ext_buzz; 
	
	public static  String gpioAddr_ent_auth; 
	public static  String gpioAddr_ext_auth; 
	
	public static String card_ser;
	
	public static int ent_AUTH=0;
	public static int ext_AUTH=0;
	
	public static int ent_BUZZ=0;
	
//  	public static   int gpio_wie0=0;
//  	public static	int gpio_wie1=1;
//  	public static	int gpio_wie2=0;
//  	public static	int gpio_wie3=1;
  	
	public static   int gpio_loop1=0;
  	public static	int gpio_loop2=1;

  	//---log--interlock--
  	public static int log_en=0;
  	//--------------
 
	//PI--pin out-----------------------
	static Pin pinENT_buzz = null ;
	static Pin pinENT_alb  = null  ;
	static Pin pinENT_auth  = null  ;
    
	static Pin pinEXT_buzz = null ;
	static Pin pinEXT_alb  = null  ;
	static Pin pinEXT_auth  = null  ;
	
	//-pin IN
//	static Pin pinLOOP1 = null ;
//	static Pin pinLOOP2  = null  ;
	
	static PinState pinLOOP1_state;
	static PinState pinLOOP2_state;
	
	static boolean loop1,loop2;
	
	public static String serialport;
	
	//-----------------------------
	public static JedisPool jedis_local_pool;
	public static Jedis jedis_local;
  	public static String redisIPexit ="localhost";
  	public static String redisIPentry ="localhost";
	public static String pathType ="entry";
	public static String antipassbck = "false";
	public static String antipass_period = "2"; //in minute motherfucker..
	public static String[] antipass_whitelist_arr = new String[10]; //--exception for antipass
	public static String antipass_whitelist_redis = "0000|0000";
	public static String antipass_period_chck="false";
	//---------------
	public static String timeoutread;
	public static Object readerpath;
	public static int tmr_wdcount;


//	public static pktask_SEQPROC pktask_SEQPROC;
    public static Log log = LogFactory.getLog(pkvar.class.getName());

	public static String sst;

	public static String surcharge;

	public static String sst_enable;

	public static String surcharge_enable;

	public static String graceperiod;

	public static String timeoutautorecon;

	public static String timeout;

	public static String flateratestarthour;

	public static String flathourvalue;

	public static String springboot_enable;

	public static String flaterateendhour;

	public static String fareurl_enable;

	public static String fareurl;

	public static String verifycscurl;

	public static String requestsettleurl;
    
    public static String israsp() {
    	String os = "";
    	//os.arch = arm = raspberry 
    	//os.name = linux
    	try {
    	   os = System.getProperty("os.arch").toLowerCase();
    		
    	}catch(Exception c) {
    		c.printStackTrace();
    	}
    	return os;
    }
    
	static void setupLog4j(String appName) {

//         InputStream inStreamLog4j = getClass().getResourceAsStream("/log4j.properties");

        String propFileName = "log4j.properties";
        File f = new File(propFileName);
        if (f.exists()) {

            try {
                InputStream inStreamLog4j1 = new FileInputStream(f);
                Properties propertiesLog4j = new Properties();

                propertiesLog4j.load(inStreamLog4j1);
                PropertyConfigurator.configure(propertiesLog4j);
            } catch (Exception e) {
                e.printStackTrace();
                BasicConfigurator.configure();
            }
        } else {
            BasicConfigurator.configure();
        }
        Logger.getRootLogger().setLevel(Level.INFO);
        // logger.setLevel(Level.TRACE);
//        logger.debug("log4j configured");

    }
	static public Pin getRaspPin(Integer getpin) {
		switch(getpin.intValue()){
		case 0:
			return RaspiPin.GPIO_00;
		case 1:
			return RaspiPin.GPIO_01;
		case 2:
			return RaspiPin.GPIO_02;
		case 3:
			return RaspiPin.GPIO_03;
		case 4:
			return RaspiPin.GPIO_04;
		case 5:
			return RaspiPin.GPIO_05;
		case 6:
			return RaspiPin.GPIO_06;
		case 7:
			return RaspiPin.GPIO_07;
		case 8:
			return RaspiPin.GPIO_08;
		case 9:
			return RaspiPin.GPIO_09;
		case 10:
			return RaspiPin.GPIO_10;
		case 11:
			return RaspiPin.GPIO_11;
		case 12:
			return RaspiPin.GPIO_12;
		case 13:
			return RaspiPin.GPIO_13;
		case 14:
			return RaspiPin.GPIO_14;
		case 15:
			return RaspiPin.GPIO_15;
		case 16:
			return RaspiPin.GPIO_16;
		case 17:
			return RaspiPin.GPIO_17;
		case 18:
			return RaspiPin.GPIO_18;
		case 19:
			return RaspiPin.GPIO_19;
		case 20:
			return RaspiPin.GPIO_20;
		case 21:
			return RaspiPin.GPIO_21;
		case 22:
			return RaspiPin.GPIO_22;
		case 23:
			return RaspiPin.GPIO_23;
		case 24:
			return RaspiPin.GPIO_24;
			
		case 25:
			return RaspiPin.GPIO_25;
		case 26:
			return RaspiPin.GPIO_26;
		case 27:
			return RaspiPin.GPIO_27;
		case 28:
			return RaspiPin.GPIO_28;
		case 29:
			return RaspiPin.GPIO_29;
		case 30:
			return RaspiPin.GPIO_30;
		default:
			throw new IllegalArgumentException("illigal argurment numbering :).");
		}
	}
	
	
	public static int [] bytetoINTVALUE(byte [] data) {
		int x=0;
		int ddd[]=new int[data.length];
		try {
		for(int dd : data) {
			ddd[x] = Integer.valueOf(data[x]);
			
			 
			
			x++;
		}
		  if(ddd[20]<0) {
           	  ddd[20]= ddd[20] & 0xff;
//           	  System.out.println("babi : "+  ddd[20]);
            }
		}catch(Exception v) {
			v.printStackTrace();
		}
		
		return ddd;
	}
	 public static int  logdb_rfid(String tarikh,String tagid,String tid,String userid,
			 String name,String dept,String laneid,String status1,String path1,String users_plno,String raw_org) {
		     int ret =1;
		     Jedis jedis = new Jedis("localhost");
				  
			 System.out.println("--redis log tid-- : "+tagid);
			
		     String msg = tarikh+"|"+tagid+"|"+tid+"|"+userid+"|"+name+"|"+dept+"|"+laneid+"|"+status1+"|"+path1+"|"+users_plno+"|"+raw_org;
		     
		     jedis.lpush("mfgrfidlog",msg) ;
		    		 
		     jedis.close();
		     return ret;
		    
       }
	 
	 public static String checkSEQstring(int data) {
		 String ret = "entah";
		 try {
		 switch(data) {
		 case 10:
			 ret = "Enable_Reader";
			 break;
		 case 1:
			 ret = "initialize_entry";
			 break;
		 case 2:
			 ret = "proceed_entry";
			 break;
		 case 3:
			 ret = "INITIAL_EXIT";
			 break;
		 case 4:
			 ret = "PROCEED_EXIT";
			 break;
		 case 5:
			 ret = "INITIAL_SALE";
			 break;
		 case 6:
			 ret = "PROCEED_SALE";
			 break;
		  }
		 }catch(Exception c) {
			 c.printStackTrace();
		 }
		 return ret;
	 }
	 
	 public static String checkSEQstringSend(int data) {
		 String ret = "entah";
		 try {
		 switch(data) {
		 case 10:
			 ret = "Enable_Reader";
			 break;
		 case 3:
			 ret = "initialize_entry";
			 break;
		 case 19:
			 ret = "proceed_entry";
			 break;
		 case 4:
			 ret = "INITIAL_EXIT";
			 break;
		 case 20:
			 ret = "PROCEED_EXIT";
			 break;
		 case 33:
			 // 0x21
			 ret = "INITIAL_SALE";
			 break;
		 case 34:
			// 0x21
			 ret = "PROCEED_SALE";
			 break;
		  }
		 }catch(Exception c) {
			 c.printStackTrace();
		 }
		 return ret;
	 }
	 
	 @SuppressWarnings("null")
	public  static byte [] String_byteArrayfull(String str) 
	 {
		 
		 String [] tmp_byt = str.split(",");
		 byte[] ret  =new byte[tmp_byt.length];
//		 System.out.println("full recv : "+str);
		 try { 
		 if(str.length() > 0) {
			 for(int i =0;i<tmp_byt.length;i++) {
//				 System.out.println("recv : "+Long.decode(tmp_byt[i]));
//				  ret[i]  = Long.decode(tmp_byt[i]).byteValue();
				 ret[i]  = Byte.parseByte(tmp_byt[i], 16);
//				= b;
//				 ret[i] =  (byte) Long.parseUnsignedLong(b, 16);
			 }
			
		 }
	   }catch(Exception v) {
		   v.printStackTrace();
//		   if(str.length() > 0) {
//				 for(int i =0;i<tmp_byt.length;i++) {
////					 ret[i] = Byte.valueOf(tmp_byt[i]);
//				 }
//				
//			 }
	   }
		
		 
		 return ret;
	 }
	 
	 public static byte[] DateString_toByte(String dt)  {
//		 String str1 = "2018-09-12 23:08:09";
//		 dt = str1;
		 byte [] byte_dt = new byte[6];
		 Integer [] int_dt = new Integer[6];
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat dt_ss= new SimpleDateFormat("ss");
	 	 SimpleDateFormat dt_min= new SimpleDateFormat("mm");
	 	 SimpleDateFormat dt_HH= new SimpleDateFormat("HH");
	 	 SimpleDateFormat dt_yyyy= new SimpleDateFormat("yy");
	 	 SimpleDateFormat dt_MM= new SimpleDateFormat("MM");
	 	 SimpleDateFormat dt_dd= new SimpleDateFormat("dd");
	
		 try {
			 Date dates = sdf.parse(dt);
			 String str_ss = dt_ss.format(dates);
			 String str_min = dt_min.format(dates);
			 String str_HH = dt_HH.format(dates);
			 String str_yyyy = dt_yyyy.format(dates);
			 String str_MM = dt_MM.format(dates);
			 String str_dd = dt_dd.format(dates);
			 
			 //--these need to re-convert
			 int_dt[5] =  Integer.valueOf(str_ss);
			 int_dt[4] = Integer.valueOf(str_min);
			 int_dt[3] =Integer.valueOf(str_HH);
			 int_dt[2] = Integer.valueOf(str_dd);
			 int_dt[1] =Integer.valueOf(str_MM);
			 int_dt[0] =Integer.valueOf(str_yyyy);
			 
			 //--take 'string' value as exactly parse to 'Byte'..example in String= 18,Byte=18(but in Integer value is 24)
			 byte_dt[5] =  Byte.valueOf(str_ss,16);
			 byte_dt[4] = Byte.valueOf(str_min,16);
			 byte_dt[3] =Byte.valueOf(str_HH,16);
			 byte_dt[2] =Byte.valueOf(str_dd,16);
			 byte_dt[1] =Byte.valueOf(str_MM,16);
			 byte_dt[0] = Byte.valueOf(str_yyyy,16);
			
			 
		 	
//		 	    System.out.println("date :"+  str_dd);
		 }catch(Exception c) {
			 c.printStackTrace();
		 }
		
		 
	
		 return byte_dt;
		 
	 }
	 
	 public static Date Datebyte_toString(byte [] datebyte) {
		 Date dates = null;
		 try {
		
		 SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		 String str_Y = Integer.toHexString(datebyte[0]);
		 String str_M = Integer.toHexString(datebyte[1]);
		 String str_D = Integer.toHexString(datebyte[2]);
		 String str_H = Integer.toHexString(datebyte[3]);
		 String str_m = Integer.toHexString(datebyte[4]);
		 String str_s = Integer.toHexString(datebyte[5]);
		 
		 dates = sdf.parse(str_Y+"-"+str_M+"-"+str_D+" "+str_H+":"+str_m+":"+str_s);
		 
		 }catch(Exception c) 
		  {
			 c.printStackTrace();
		  }
		 
		 
		 
		 return dates;
	 }
	 
	 public static double Farebyte_todouble(byte [] datebyte) {
		 double money=0;
		 try {
			 String str_ratusribu = Integer.toHexString(datebyte[0]);
			 String str_puluhribu = Integer.toHexString(datebyte[1]);
			 String str_ribu = Integer.toHexString(datebyte[2]);
			 String str_ratus = Integer.toHexString(datebyte[3]);
			 String str_puluh = Integer.toHexString(datebyte[4]);
			 String str_sen = Integer.toHexString(datebyte[5]);
			 
			 money = Double.parseDouble(str_ratusribu+str_puluhribu+str_ribu+str_ratus+str_puluh);
			 
		 }catch (Exception v)
		 {v.printStackTrace();}
		 return money;
	 }
	 
	 public static byte[] FareDouble_tobyte_ext(double fare) {
		 byte[] money=new byte[8];
		 
//		 double hh = 34234.19;
		  
		 try {
//			 fare = hh;
//			 String str_ratusribu = Integer.toHexString(datebyte[0]);
//			 String str_puluhribu = Integer.toHexString(datebyte[1]);
//			 String str_ribu = Integer.toHexString(datebyte[2]);
//			 String str_ratus = Integer.toHexString(datebyte[3]);
//			 String str_puluh = Integer.toHexString(datebyte[4]);
//			 String str_sen = Integer.toHexString(datebyte[5]);
//			 
//			 money = Double.parseDouble(str_ratusribu+str_puluhribu+str_ribu+str_ratus+str_puluh);
			 NumberFormat nfm = new DecimalFormat("000000000000.00");
	
 			 
			 String duit = String.valueOf(nfm.format(fare));
			 String [] tmp_duit = duit.split("\\.");
			 String rm = tmp_duit[0];
			 money[7] =  (byte) Integer.parseInt(tmp_duit[1],16);
			 money[6] =  (byte) Integer.parseInt(rm.substring(rm.length()-2, rm.length()),16);
			 money[5] =  (byte) Integer.parseInt(rm.substring(rm.length()-4, rm.length()-2),16);
			 money[4] =  (byte) Integer.parseInt(rm.substring(rm.length()-6, rm.length()-4),16);
			 money[3] =  (byte) Integer.parseInt(rm.substring(rm.length()-8, rm.length()-6),16);
			 money[2] =  (byte) Integer.parseInt(rm.substring(rm.length()-10, rm.length()-8),16);
			 money[1] =  (byte) Integer.parseInt(rm.substring(rm.length()-12, rm.length()-10),16);
			 money[0] = 0x00;
//		     money =   money <<8;
		     ByteBuffer buffer = ByteBuffer.wrap(money);
//				System.out.println(rm.substring(rm.length()-2, rm.length()));
//				System.out.println(rm.substring(rm.length()-4, rm.length()-2));
//				System.out.println(rm.substring(rm.length()-6, rm.length()-4));
//				System.out.println(rm.substring(rm.length()-8, rm.length()-6));
//				System.out.println(rm.substring(rm.length()-10, rm.length()-8));
//				System.out.println(rm.substring(rm.length()-12, rm.length()-10));
//	
			 
//			 System.out.println("DUIT :"+Arrays.toString(Bytearray_toStringarray(money)));
//			 log.info("DUIT :"+Arrays.toString(Bytearray_toStringarray(money)));
			 
		 }catch (Exception v)
		 {v.printStackTrace();}
		 return money;
	 }
	 
	 public static byte[] FareDouble_tobyte(double fare) {
		 byte[] money=new byte[8];
		 
		 double hh = 34234.19;
		  
		 try {
//			 fare = hh;
//			 String str_ratusribu = Integer.toHexString(datebyte[0]);
//			 String str_puluhribu = Integer.toHexString(datebyte[1]);
//			 String str_ribu = Integer.toHexString(datebyte[2]);
//			 String str_ratus = Integer.toHexString(datebyte[3]);
//			 String str_puluh = Integer.toHexString(datebyte[4]);
//			 String str_sen = Integer.toHexString(datebyte[5]);
//			 fare = fare *100;  	//[0, 0, 0, 7, 69, ffffff88, 0, 0]
			 // fare =fare //:[0, 0, 0, 0, 7, 69, ffffff88, 0]
//			 money = Double.parseDouble(str_ratusribu+str_puluhribu+str_ribu+str_ratus+str_puluh);
	         NumberFormat nfm = new DecimalFormat("000000000000.00");
	
 			 
			 String duit = String.valueOf(nfm.format(fare));
			 String [] tmp_duit = duit.split("\\.");
			 String rm = tmp_duit[0];
			 money[7] =  (byte) Integer.parseInt(tmp_duit[1],16);
			 money[6] =  (byte) Integer.parseInt(rm.substring(rm.length()-2, rm.length()),16);
			 money[5] =  (byte) Integer.parseInt(rm.substring(rm.length()-4, rm.length()-2),16);
			 money[4] =  (byte) Integer.parseInt(rm.substring(rm.length()-6, rm.length()-4),16);
			 money[3] =  (byte) Integer.parseInt(rm.substring(rm.length()-8, rm.length()-6),16);
			 money[2] =  (byte) Integer.parseInt(rm.substring(rm.length()-10, rm.length()-8),16);
			 money[1] =  (byte) Integer.parseInt(rm.substring(rm.length()-12, rm.length()-10),16);
			 money[0] = 0x00;
		
		
//				System.out.println(rm.substring(rm.length()-2, rm.length()));
//				System.out.println(rm.substring(rm.length()-4, rm.length()-2));
//				System.out.println(rm.substring(rm.length()-6, rm.length()-4));
//				System.out.println(rm.substring(rm.length()-8, rm.length()-6));
//				System.out.println(rm.substring(rm.length()-10, rm.length()-8));
//				System.out.println(rm.substring(rm.length()-12, rm.length()-10));
//	
				money = shiftLeft(money,8);
//			    System.out.println("duit :"+Arrays.toString(Bytearray_toStringarray(money)));
			 
		 }catch (Exception v)
		 {v.printStackTrace();}
		 return money;
	 }
	 
	 static byte[] shiftLeft(byte[] byteArray, int shiftBitCount) {
		    final int shiftMod = shiftBitCount % 8;
		    final byte carryMask = (byte) ((1 << shiftMod) - 1);
		    final int offsetBytes = (shiftBitCount / 8);

		    int sourceIndex;
		    for (int i = 0; i < byteArray.length; i++) {
		        sourceIndex = i + offsetBytes;
		        if (sourceIndex >= byteArray.length) {
		            byteArray[i] = 0;
		        } else {
		            byte src = byteArray[sourceIndex];
		            byte dst = (byte) (src << shiftMod);
		            if (sourceIndex + 1 < byteArray.length) {
		                dst |= byteArray[sourceIndex + 1] >>> (8 - shiftMod) & carryMask;
		            }
		            byteArray[i] = dst;
		        }
		    }
		    return byteArray;
		}
	 
	 public static Byte Int_inAsciito_AsciiByte(String ss) {
		return Byte.valueOf(ss,16);
	 }
	 
	 public static Byte Int_inAsciito_AsciiByte(int ss) {
		    String msg = String.valueOf(ss);
			return Byte.valueOf(msg,16);
		 }
		 
	 
	 public static Byte IntVal_to_AsciiByte(int ss) {
//		    String msg = String.valueOf(ss);
			return Byte.valueOf((byte) ss);
		 }
	 
	 public static int ChartoAscii(char character){
			return (int)character;
		}

		public static char IntValToAsciiChar(int val){
//			plno +=ASCIIToChar(Byte.toUnsignedInt(s[i]));
			return (char)val;
		}
	 public  static String logtxt(String str) 
	 {
		   Date dateNow = new Date();
		   
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
		    
		        String date_to_string = format_dt.format(dateNow);
	    		String data = str;
//	    		 System.out.println(str+"\n");
	 
	    			 	    		
	    		BufferedWriter out;
				try {
					out = new BufferedWriter(new FileWriter
					        ("log//"+date_to_string+".log",true));
					    out.write(tarikh()+"::"+data+"\n");
	    	            out.flush();
	    	            out.close();
	    	            
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	         
	    	            return "ok";
	     	//	BufferedWriter  out = new BufferedWriter(new FileWriter
	         //    (file,true));
	        //     out.write(data+"..system run..\n");
	         //    out.close();
	 
		      //  System.out.println("Done");
	 
	    	
	    } 
	 
	 public  static String logtxt_tx(String str) 
	 {
		   Date dateNow = new Date();
		   
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
		    
		        String date_to_string = format_dt.format(dateNow);
	    		String data = str;
//	    		 System.out.println(str+"\n");
	 
	    			 	    		
	    		BufferedWriter out;
				try {
					out = new BufferedWriter(new FileWriter
					        ("log//"+date_to_string+".txlog",true));
					    out.write(tarikh()+"::"+data+"\n");
	    	            out.flush();
	    	            out.close();
	    	            
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	         
	    	            return "ok";
	     	//	BufferedWriter  out = new BufferedWriter(new FileWriter
	         //    (file,true));
	        //     out.write(data+"..system run..\n");
	         //    out.close();
	 
		      //  System.out.println("Done");
	 
	    	
	    } 
	 
	 public static int ordinalIndexOf(String str, String substr, int n) {
		    int pos = -1;
		    do {
		        pos = str.indexOf(substr, pos + 1);
		    } while (n-- > 0 && pos != -1);
		    return pos;
		}
	 
	 public  static String logtxt_rx(String str) 
	 {
		   Date dateNow = new Date();
		   
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
		    
		        String date_to_string = format_dt.format(dateNow);
	    		String data = str;
//	    		 System.out.println(str+"\n");
	 
	    			 	    		
	    		BufferedWriter out;
				try {
					out = new BufferedWriter(new FileWriter
					        ("log//"+date_to_string+".rxlog",true));
					    out.write(tarikh()+"::"+data+"\n");
	    	            out.flush();
	    	            out.close();
	    	            
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	         
	    	            return "ok";
	     	//	BufferedWriter  out = new BufferedWriter(new FileWriter
	         //    (file,true));
	        //     out.write(data+"..system run..\n");
	         //    out.close();
	 
		      //  System.out.println("Done");
	 
	    	
	    } 
	 public  static String logtxtjson(String str) 
	 {
		   Date dateNow = new Date();
		   
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
		    
		        String date_to_string = format_dt.format(dateNow);
	    		String data = str;
//	    		 System.out.println(str+"\n");
	 
	    			 	    		
	    		BufferedWriter out;
				try {
					out = new BufferedWriter(new FileWriter
					        ("log//"+date_to_string+".json",true));
					    out.write(tarikh()+"::"+data+"\n");
	    	            out.flush();
	    	            out.close();
	    	            
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	         
	    	            return "ok";
	     	//	BufferedWriter  out = new BufferedWriter(new FileWriter
	         //    (file,true));
	        //     out.write(data+"..system run..\n");
	         //    out.close();
	 
		      //  System.out.println("Done");
	 
	    	
	    } 
	 
	 public static  String [] Bytearray_toStringarray(byte[] byt) {
//		 Long.decode("0x58").byteValue());
		 String[] bb = new String[byt.length];
		  try {
				 int i=0;
				 for(byte byts: byt) {
			
					 bb[i] = Integer.toHexString((int)byts);
					 i++;
				 }
		  }catch(Exception n) {
			  n.printStackTrace();
		  }
	
		return bb;
	 } 
	
	 public static  String [] Bytearray_toStringarray(int[] byt) {
//		 Long.decode("0x58").byteValue());
		 String[] bb = new String[byt.length];
		  try {
				 int i=0;
				 for(int byts: byt) {
			
					 bb[i] = Integer.toHexString((int)byts);
					 i++;
				 }
		  }catch(Exception n) {
			  n.printStackTrace();
		  }
	
		return bb;
	 } 

		
		public static byte[] byteSENDbuild(byte[] farebyt,byte byteseq,byte bytecmd) {
			byte [] bytes = new byte[5];
//			ArrayList<Byte> bytss = new ArrayList<Byte>();
			
			int byt_len = farebyt.length + 2;
			bytes[0]= 0x02; //head
			bytes[1]= 0x00; //len
			bytes[2]= (byte) byt_len; //len
			bytes[3]= byteseq; //seqnumber
			bytes[4]= bytecmd; //cmd
			
           
			byte [] byt_Head = byteconcate(bytes, farebyt);
//            bytss.addAll(Arrays.);
//			bytss.addAll(Arrays.asList(bytes));
//			bytss.addAll(Arrays.asList(bytdata));
			
			
//		    System.out.print("head : "+Arrays.toString(byt_Head)+" :"+byt_Head.length);
//			System.exit(0);
			byte chcsum =  getchecksum(byt_Head);
			
            byte [] byt_Tail ={chcsum , 0x03};
//    	    System.out.print("tail :"+Arrays.toString(byt_Tail));
            byte [] byt_All = byteconcate(byt_Head, byt_Tail);
            
            
			return byt_All;
		}
		
		public static byte[] byteSENDbuildSale(byte[] farebyt,byte byteseq,byte bytecmd) {
			byte [] bytes = new byte[5];
//			ArrayList<Byte> bytss = new ArrayList<Byte>();
			
			int byt_len = farebyt.length +2;
			bytes[0]= 0x02; //head
			bytes[1]= 0x00; //len
			bytes[2]= (byte) byt_len; //len
			bytes[3]= byteseq; //seqnumber
			bytes[4]= bytecmd; //cmd
			
//			bytes[5]= 0x00; //cmd
//			bytes[6]= 0x00; //cmd
//			bytes[7]= 0x00; //cmd
//			bytes[8]= 0x00; //cmd
//			bytes[9]= 0x00; //cmd
//			bytes[10]= 0x01; //cmd
			

			byte [] byt_Head = byteconcate(bytes, farebyt);
//            bytss.addAll(Arrays.);
//			bytss.addAll(Arrays.asList(bytes));
//			bytss.addAll(Arrays.asList(bytdata));
			
			
//		    System.out.print("head : "+Arrays.toString(byt_Head)+" :"+byt_Head.length);
//			System.exit(0);
			byte chcsum =  getchecksum(byt_Head);
			
            byte [] byt_Tail ={chcsum , 0x03};
//    	    System.out.print("tail :"+Arrays.toString(byt_Tail));
            byte [] byt_All = byteconcate(byt_Head, byt_Tail);
            
            
			return byt_All;
		}
		
		public static byte[] byteSENDbuildsale(byte byteseq,byte bytecmd) {
			byte [] bytes = new byte[5];
//			ArrayList<Byte> bytss = new ArrayList<Byte>();
			
			int byt_len =  2;
			bytes[0]= 0x02; //head
			bytes[1]= 0x00; //len
			bytes[2]= (byte) byt_len; //len
			bytes[3]= byteseq; //seqnumber
			bytes[4]= bytecmd; //cmd
//			byte [] byt_Head = byteconcate(bytes, bytdata);
//            bytss.addAll(Arrays.);
//			bytss.addAll(Arrays.asList(bytes));
//			bytss.addAll(Arrays.asList(bytdata));
			
			
//		    System.out.print("head : "+Arrays.toString(bytes)+" :"+bytes.length);
//			System.exit(0);
			byte chcsum =  getchecksum(bytes);
			
            byte [] byt_Tail ={chcsum , 0x03};
    	   
            byte [] byt_All = byteconcate(bytes, byt_Tail);
            
            
			return byt_All;
		}
		
		public static byte[] byteDisplay(String msg,String msg0) {
//			byte [] bytes = new byte[5];
//			ArrayList<Byte> bytss = new ArrayList<Byte>();
			byte bytd [] = msg.getBytes();
			byte bytd0 [] = msg0.getBytes();
			byte [] bytes = new byte[9];
//			int byt_len =  2;
			bytes[0]= 0x02; //head
			bytes[1]= 0x00; //len
			bytes[2]= (byte) (bytes.length+bytd.length+bytd0.length-1); //len
			bytes[3]= 0x36; //seqnumber
			bytes[4]= (byte) 0x9c; //cmd
			bytes[5]= (byte) 0x21;
			bytes[6]= (byte) 0x11;
			bytes[7]= (byte) 0x01;
			bytes[8]= (byte) 0x01;
			
			byte [] byt_All_0 = byteconcate(bytes, bytd);
			byte [] byte_null = {0x00};
			byte [] byt_All_1 = byteconcate(byt_All_0,  byte_null);
			byte [] byt_All_2 = byteconcate(byt_All_1,  bytd0);
			byte chcsum =  getchecksum(byt_All_2);
            String chksm = Integer.toHexString((int)chcsum);
//			System.out.println("check sum : "+chksm);
			byte [] byt_Tail ={chcsum ,0x00, 0x03};
			byte [] byt_All_3 = byteconcate(byt_All_2, byt_Tail);
			return byt_All_3;
		}
		
		public static byte[] byteSENDbuild(byte byteseq,byte bytecmd) {
			byte [] bytes = new byte[5];
//			ArrayList<Byte> bytss = new ArrayList<Byte>();
			
			int byt_len =  2;
			bytes[0]= 0x02; //head
			bytes[1]= 0x00; //len
			bytes[2]= (byte) byt_len; //len
			bytes[3]= byteseq; //seqnumber
			bytes[4]= bytecmd; //seqnumber
			

//			byte [] byt_Head = byteconcate(bytes, bytdata);
//            bytss.addAll(Arrays.);
//			bytss.addAll(Arrays.asList(bytes));
//			bytss.addAll(Arrays.asList(bytdata));
			
			
//		    System.out.print("head : "+Arrays.toString(bytes)+" :"+bytes.length);
//			System.exit(0);
			byte chcsum =  getchecksum(bytes);
			
            byte [] byt_Tail ={chcsum , 0x03};
    	   
            byte [] byt_All = byteconcate(bytes, byt_Tail);
            
            
			return byt_All;
		}
		
		   public static <T> Object[] concatenate(T[] a, T[] b) 
		    { 
		     
		        return Stream.of(a, b) 
		                     .flatMap(Stream::of) 
		                     .toArray(); 
		  
		 
		    } 
		   
		   public static byte[] byteconcate(byte [] st_byte,byte[] end_byt) {

			      byte[]ret_byte = new byte[st_byte.length+end_byt.length];
			      int count;
			      
//			      System.out.println("st_byte.length: "+ st_byte.length);
			      for(count = 0; count<st_byte.length; count++) { 
			    	  ret_byte[count] = st_byte[count];
//			          count++;
//			          System.out.println( ret_byte[count] );
			      } 
			      for(int j = 0;j<end_byt.length;j++) { 
			    	  ret_byte[count++] = end_byt[j];
//			    	  count =+1;
//			    	  System.out.println(ret_byte[count]);
//			    	  System.out.println( ret_byte[count] );
			      } 
//			      for(int i = 0;i<ret_byte.length;i++) System.out.print(ret_byte[i]+" ");
			      
			      return ret_byte;
			      
		   }
		   
		   static byte getchecksum(byte[] objects)
	       {
	           byte crc = (byte) 0 ;
//	           int checksum = 0;
	           for (int i = 0; i < objects.length; i++){
	               
	           crc = (byte) (crc ^ (byte)(objects[i]));
	          // crc[1] = (byte)(checksum jRASP_var.DBcreatelog("dblogall"); & 0xff);
	          
	           }
//	           System.out.println(" crc "+Integer.toHexString(crc)+" dataLen:"+objects.length);
	           return crc;
	       }
	 
	 public static String hextodec(String hexstr) {
			NumberFormat card_nf= new DecimalFormat("0000000000");	
	    	long shit = Long.parseLong(hexstr, 16); 
//	    	long shit = Long.parseLong("27FAA9CC", 16);
	    	String crd = card_nf.format(shit);
	    	System.out.println("==CARD ID: "+crd+" ===========\n");
	    	return crd;
	 }
	 public  static String logtxtentry(String str) throws IOException
	 {
		   Date dateNow = new Date();
		   
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
		    
		    String date_to_string = format_dt.format(dateNow);
	    		String data = str;
	 
	    			 	    		
	    		BufferedWriter  out = new BufferedWriter(new FileWriter
	    	            ("log//"+date_to_string+"_ga_entry.txt",true));
	    	            out.write(tarikh()+"::"+data+"\n");
	    	            out.flush();
	    	            out.close();
	    	            
	    	            return "ok";
	    	 
	    	
	    } 
	 

	 public static String tarikh(){
	    	
	   	 Date dateNow = new Date();
			    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
			    String date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
	   	
	   	return date_to_string;
	   	
	   }

	 public static Date tarikhdate(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
			    String date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return datetime;
	   	
	   }
	 
	 public static String string(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	     String date_to_string="kimak";
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
		        date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return date_to_string;
	   	
	   }
	 
	 @SuppressWarnings("finally")
	public static String TRX_DTyymmddHHMMss(String dt) {
		 SimpleDateFormat dfnew = new SimpleDateFormat("yyyyMMddHHmmss");
		 SimpleDateFormat dfold = new SimpleDateFormat("yyyyMMddHHmmss");
		 Date dateold;
		 String dtnew ="";
		try {
			dateold = dfold.parse(dt);
			dtnew = dfnew.format(dateold);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}finally {
			return dtnew;
		}
		
	 }
	 
	 public static String tarikhyyyyMMddHHmmss(String dt) {
		 SimpleDateFormat dfnew = new SimpleDateFormat("yyyMMddHHmmss");
		 SimpleDateFormat dfold = new SimpleDateFormat("yyyyMMddHHmmss");
		 Date dateold;
		 String dtnew ="";
		try {
			dateold = dfold.parse(dt);
			dtnew = dfnew.format(dateold);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}finally {
			return dtnew;
		}
		
	 }
	 
	 public static String TRX_IDold() {
		 String trxid="";
		 String trxdt = tarikhyyyyMMddHHmmss(pkByteData.EXIT_DATE+pkByteData.EXIT_TIME);
		 try {
			trxid = pkconfig.spid+pkconfig.plzno+pkconfig.lane+(String.valueOf(boj.sialw.pktmr_resetboj.BOJcount))+trxdt;
		 }catch(Exception v) {
			 v.printStackTrace();
		 }
		return trxid;
	 }
	 
	 public static String TRX_ID() {
		 String trxid="";
		 
		 if(pkconfig.readerpath.equals("exit")) {
			 String trxdt = pkByteData.EXIT_DATE+pkByteData.EXIT_TIME;
			 try {
				trxid = pkconfig.spid+pkconfig.plzno+pkconfig.lane+(String.valueOf(pkboj.BOJcountget()))+trxdt;
			 }catch(Exception v) {
				 v.printStackTrace();
			 }
		 }
		 
	   if(pkconfig.readerpath.equals("entry")) {
			 String trxdt = pkPROCrecv_ENT.EXIT_DATE+pkPROCrecv_ENT.EXIT_TIME;
			 try {
				trxid = pkconfig.spid+pkconfig.plzno+pkconfig.lane+(String.valueOf(pkboj.BOJcountget()))+trxdt;
			 }catch(Exception v) {
				 v.printStackTrace();
			 }
		 }
		 
		 
	
		return trxid;
	 }
	 
	 
	 public static String simtrxno_dd(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	     String date_to_string="kimak";
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMddHHmmssSSS");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
		        date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return date_to_string;
	   	
	   }
	 
	 public static String tarikhoperationaltime(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	     String date_to_string="kimak";
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("HHmmss");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
		        date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return date_to_string;
	   	
	   }
	 
	 public static String tarikhoperationaldate(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	     String date_to_string="kimak";
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
		        date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return date_to_string;
	   	
	   }
	 
	 public static Date tarikhtimeBOJ(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
			    String date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return datetime;
	   	
	   }
	 
	 public static String tarikhdateonlyBOJ(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	     String date_to_string = "00:00:00";
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
			    date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
			
	   	
	   	 return date_to_string;
	   	
	   }
	 
	 public static String tarikhtimeonlyBOJ(){
	    	
	   	 Date dateNow = new Date();
	     Date datetime=null;
	     String date_to_string = "00:00:00";
	   	 try {
	   	    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("HHmmss");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
			    date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
			    datetime = format_dt.parse(date_to_string);
	   		 
	   	 }catch (Exception n) {
	   		n.printStackTrace(); 
	   	 }
		
	   	 //--bojtime always set to zero---
	   	 date_to_string ="000000";
	   	 return date_to_string;
	   	
	   }
	     static  long checksum(byte[] bytes) {
	    	   byte sum = 0;
	    	   for (byte b : bytes) {
	    		   sum = (byte) (sum ^ b); 
	    	   }
	    	   return (byte) (sum % 256);
	    	}
	       
	       		 
	
		 	
			public static boolean isSocketAlive(String hostName, int port) {
				boolean isAlive = false;
		 

				SocketAddress socketAddress = new InetSocketAddress(hostName, port);
				Socket socket = new Socket();
		 
				
				int timeout = 1000;
		 

				try {
					socket.connect(socketAddress, timeout);
					socket.close();
					isAlive = true;
//					System.out.println(" -- live connection ["+hostName+"]["+port+"] -- ");
//					logtxt(" -- live connection ["+hostName+"]["+port+"] -- good! ");
		 
				} catch (SocketTimeoutException exception) {
//					System.out.println("SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
					logtxt("SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
				} catch (IOException exception) {
//					System.out.println("IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
					logtxt(	"IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
				}

				return isAlive;
			}
			  
}
