package pk.mainsys;


import java.util.Arrays;
import java.util.HashMap;

import com.pi4j.io.gpio.RaspiPin;
import com.rabbitmq.tools.json.JSONWriter;

import drac.bytenotail.pk_tmrByteNoTail;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.api.sync.RedisHashCommands;
import io.lettuce.core.api.sync.RedisStringCommands;
import jsonsvc.httpsvc;
import netty.recon_entry.InboundClientHandler_ENTRY;
import netty.recon_entry.mainClient_ENTRY;
import netty.recon_exit.mainClient_EXIT;
import nioSOCKreaderCoherent.nioCoher_ENT;
import nioSOCKreaderCoherent.nioCoher_EXIT;
import rdr.config.MAINXXXXXXX;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import  javax.json.JsonObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.slf4j.LoggerFactory;

import java.sql.*;

import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;
import redis.drac.pktask_redis;
import spring.controller.GreetingController;
import spring.fcukoff.greet_cardinfo;
import spring.fcukoff.greet_status;
import spring.main.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
@SpringBootApplication
@RestController
@Configuration

public class MainXXXXX extends RaspiPin {
	 static Thread thread_init_SUBrabbitMQ ;
	 static Thread thread_init_PUBrabbitMQ ;
	 
	public  static nioCoher_EXIT nio_mfgreader_EXIT;
	public  static nioCoher_ENT nio_mfgreader_ENT;
	 static String paymode = "OFFLINE";
	 public static String ostype ="amd64";
	 public static String vers = "#1.2.7acc#";
		private static Log log = LogFactory.getLog(MainXXXXX.class);
	 
	 public static  void start() {
		 try{
			 
			 //----------testing---
//			 JsonObject jsonobj = null;
//			 jsonobj = Json.createObjectBuilder().add("testing", Json.createObjectBuilder().add("data","01010101")).build();
//			 StringWriter strwr =  new StringWriter();
//			 JsonWriter jsonwr = Json.createWriter(strwr);
//			 jsonwr.writeObject(jsonobj);
//			 jsonwr.close();
//			 //--end test---
//	
//			
//			 
//			  new greet_cardinfo(strwr.getBuffer().toString());
//			  pkvar.setupLog4j("");
		      log.info(" ["+pkvar.israsp()+"] OStype :"+ostype);
			  new boj.sialw.pktmr_resetboj();
			//  new amqp_threadpool();
			  init_sqlite();
			  pkredis_var.set_cmd("simulatedate","0");
			  pkredis_var.set_cmd("simulate","0");
//			 org.apache.log4j.BasicConfigurator.configure();
//    		 pk_var.jedis_local_pool = new JedisPool(new JedisPoolConfig(), "localhost");
//    		 pk_var.jedis_local = pk_var.jedis_local_pool.getResource();	
//    		 jedis = pk_var.jedis_local_pool.getResource();	
			    
//    			log.info("start!");
				 if(pkvar.israsp().equals("arm")) {
    				 if(pkconfig.lop_en.equals("1")) {
//       	  	          System.out.println(" <<<<<<< start event loop sensor >>>>>>>>> \n");
       			        System.out.println(" <<<<<<<[ARM] init GPIO output >>>>>>>>> \n");
       			        pkvar.logtxt("<<<<<<<[ARM] init GPIO output >>>>>>>>>");
       			        pkrasp_io 	pkrasp_io = new pkrasp_io(pkvar.pinENT_alb,pkvar.pinENT_buzz,pkvar.pinENT_auth,pkvar.pinEXT_alb,pkvar.pinEXT_buzz,pkvar.pinEXT_auth); 
       	  	          new pktask_rasp_io();
       	  	          new pktaskbuzz();
       	  	        } 
    			 }else {
    					pkconfig.loop1=false; 
    			 }
    			   
    			//--USED THIS IN EXIT---------------------------------------------------------------------
			 if(pkvar.israsp().equals(ostype)) { //---------check if arm-----
					 
				 
    			if(paymode.equals("OFFLINE") && pkvar.readerpath.equals("exit") ) {
    				System.err.println(" {{{{{ START }}}}} ->EXIT");
    				new pktsk_Qrecv_EXT();
    				new pktask_timeout();
    	    		
    				
    				
    				//---------for nio ---
    				nio_mfgreader_EXIT = new nioCoher_EXIT();
    				nio_mfgreader_EXIT.init();
    				//----- pls see  pktask_rxtx_EXT
    				//---------test---
//    			    new mainClient_EXIT().run();  
    				new pktsk_Qsnd_EXT();
    				//--------------------------
//        			pkinit_rxtx.connect(pkvar.serialport);
        			if( pkconfig.connectTTY.equals("yes")) {
//        				new pktask_rxtx_EXT();	
        			}else {
        				log.info(" ---- Serial port not connected [exit]----");
        			}
        		
        		
        		
        			
        			pkredis_var.del_cmd(pkredis_var.lpopkeysendbyte);
        			new pktask_redis();
    			    new pktskPOLLtimeout_EXT();
    			}
    			
    			
    			//---USE THIS IN ENTRY-----------------------------------------------------------------
    			if(paymode.equals("NORMAL")  && pkvar.readerpath.equals("entry")) {
    		
    				System.err.println( " {{{{{ START }}}}} ENTRY ");
    				
    		
    				
//    				System.out.println((char) 27 + "[31m" + "ERROR MESSAGE IN RED");
    				new pktsk_Qrecv_ENT();
    				new pktask_timeout();
    	    		
    				
    				//---------for nio ---
    				nio_mfgreader_ENT = new nioCoher_ENT();
    				nio_mfgreader_ENT.init();
    				
    			
    				
    				//----- pls see  pktask_rxtx_ENT
    				//--for netty ---
//    			    new mainClient_ENTRY().run(); 
    				//-------------------
    				new pktsk_Qsnd_ENT();
    				//--------------------------
//        			pkinit_rxtx.connect(pkvar.serialport);
        			if( pkconfig.connectTTY.equals("yes")) {
//        			 new pktask_rxtx_ENT();//  change temp
        			}else {
        				log.info(" -- Serial port not connected [entry] --");
        			}
        		
        		
        			
        			pkredis_var.del_cmd(pkredis_var.lpopkeysendbyte);
        			new pktask_redis();
    			    new pktskPOLLtimeout_ENT();
    			}
    			 new pktskDATApush();
//    			 new pk_tmrByteNoTail();  //----------------------------------recover byte----------------------------------
		 } else {
			 log.info(" ----- Supose to run on arm platform !!\n");
//			 Application.SpringStart();
		 }
    	
	         
		 } catch (Exception c){  	
		        c.printStackTrace();
//			    jedis = new Jedis("localhost");
//		    	pk_var.jedis_local = new Jedis("localhost");		 
		 } 
	 }
	 
	 private static void setupLog4j(String appName) {

	        // InputStream inStreamLog4j = getClass().getResourceAsStream("/log4j.properties");
//			File Filenm = new File(codeSource.getLocation().toURI().getPath());
	        String propFileName = appName + "log4j.config";
//	        System.out.println("log :"+propFileName);
	        File f = new File( propFileName);
	        if (f.exists()) {

	            try {
	            	 InputStream inStreamLog4j = new FileInputStream(f);
//	                InputStream inStreamLog4j = new FileInputStream(f);
	                Properties propertiesLog4j = new Properties();

	                propertiesLog4j.load(inStreamLog4j);
	                PropertyConfigurator.configure(propertiesLog4j);
	            } catch (Exception e) {
	                e.printStackTrace();
	                BasicConfigurator.configure();
	            }
//	            log.info( "DETECT!!! log4j");
	        } else {
//	        	 log.info( "NOT found log4j setup");
	            BasicConfigurator.configure();
	        }

//	         logger.setLevel(Level.TRACE);
	        log.debug("log4j configured");

	    }
	 public static void init() {
		 try{
			
//			    BasicConfigurator.configure();
			      Logger root = Logger.getRootLogger();
			      root.setLevel(Level.INFO);
			      
			       setupLog4j("");
		           pkconfig.config_init();
		    
		    if(pkconfig.readerpath.equals("exit")) {
				 paymode="OFFLINE";
			 }
		    
		    if(pkconfig.readerpath.equals("entry")) {
		    	 paymode="NORMAL";
		    }
		    
			pkredis_var.redisINITCONN_cmd(pkredis_var.redisip);//--------------------redis sentinel init-----
//			pkredis_var.redisconn_sentinel_init(pkredis_var.redisip);//--------------------redis sentinel init-----
			if(pkvar.fareurl_enable.equals("1")) {
			    System.out.println("Start Fare URL json conversion to old skool socket");
			    httpsvc.HttpPost_PS =  httpsvc.buildatahttp(pkvar.fareurl);
			}
	    } catch (Exception c){  	
	        c.printStackTrace();
		 
	    } 
	 }
	 
	 public static void init_sqlite() {
	      pkSQLITE.SQLite_init();
	      pkSQLITE.SQLite_init_log();
	 }
	 
    public static void main(String[] args) {
    	try {

    	//--------------------------------------------------------------------------------------------
   //################################################ All task task after SUCCESS REDIS connection ############################################
        	init(); //init run after succes connect to redis(pkredis_var)--then run all task!!!!!
        	
        	if(pkvar.springboot_enable.equals("1")) {
        		HashMap<String, Object> props = new HashMap<>();
            	props.put("server.port", Integer.parseInt(pkconfig.jReadConfig("springbootport")));

            	new SpringApplicationBuilder()
            	    .sources(GreetingController.class)                
            	    .properties(props)
            	    .run(args);
        	}else {
        		System.out.println("====== API spring disable !!===========\n");	
        	}
        
//            SpringApplication.run(GreetingController.class, args);
          
//            greet_status.contentLoop="";
//    	System.out.println("Arrays test!! : "+Arrays.toString(byt)+" len:"+byt.length);
    	
    	//----testing-------------
    	
//    	pk_var.FareDouble_tobyte(123.33);
    	
//    	char yourInt = 1;
//    	int here = 53;// in hex 31// in ascii 1
//    	double init_cash = 19.34;
//    	double last_cash = 4.21;
//    	NumberFormat nfmt = new DecimalFormat("00.00");
    	
//    	double tot = init_cash - last_cash;
//    	System.out.println(" total : "+nfmt.format(tot));
//    	String str1 = String.valueOf(yourInt);
//        System.out.println("ASCIIToChar :"+pk_var.Int_inAsciito_AsciiByte(yourInt));
//        System.out.println("Int to ascii :"+pk_var.Int_inAsciito_AsciiByte(here));
//        System.out.println("Int to ascii :"+pk_var.IntValToAsciiChar(here));
    	
//   	 String str = "2018-09-18 23:08:09";
   	 
//   	 byte [] byte_dt = pk_var.DateString_toByte(str);
// 	 byte[] byt = pk_var.byteSENDbuild(byte_dt,(byte) pk_bytedata.byte_CMD_EN_RDR, (byte) pk_bytedata.byte_SEQ_EN_RDR);
//       System.out.println("bytSND_PROC_ENT_compiled : "+Arrays.toString(pk_var.Bytearray_toStringarray(pk_bytedata.bytSND_PROC_ENT_compiled())));
    	
//    	System.out.println("check convert string hex to long/byte:: "+ Long.parseLong("58", 16));
//       byte[] chcsm = {0x02, 0x00, 0x08, 0x10, 0x10, 0x18, 0x09, 0x12, 0x23, 0x08, 0x09};
//       System.out.println("checksum :"+pk_var.getchecksum( chcsm));
    	
//       System.out.println("ascii:"+String.valueOf(pkvar.IntValToAsciiChar(70)));
       //-----------------------------------------------------------------------------------------------------
     
//    	jedis.set("appver", vers);
//    	BasicConfigurator.configure();//initi log4j.........................
       	System.out.println("================[pp] Park ver: "+vers+"=====["+pkvar.tarikh()+"]======\n");	
    	
//   	
//    int r= -99;
//    
//       if(r<0) {
//    	  int  cr = 0xff & r;
//    	   System.out.print("heheheh "+Integer.toHexString(cr)); 
////    	   System.out.print("hahah :"+Integer.toHexString(r));
//       }else {
////    	   System.out.print("hahah :"+Integer.toHexString(r));
//       }
//    	
//    	System.out.println("================ SID:prod,user:pms,pwd:pms,ip:192.168.168.100 ===========\n");
//    	   Long longObject = new Long("1234567");
//    	    byte b = longObject.byteValue();
//    	    System.out.println("byte:"+b);
// 
//    	    short s = longObject.shortValue();
//    	    System.out.println("short:"+s);
//
//    	    int i = longObject.intValue();
//    	    System.out.println("int:"+i);
//
//    	    float f = longObject.floatValue();
//    	    System.out.println("float"+f);
//
//    	    double d = longObject.doubleValue();
//    	    System.out.println("double:"+d);
//
//    	    long l = longObject.longValue();
//    	    System.out.println("long:"+l);
//        	System.exit(0);
    	
    	
//    		byte byt[] = {0x02,0x00,0x02,0x58,0x00};
////        	System.out.println("check sum: "+Long.toHexString(pk_var.getchecksum(byt)));
//        	
//        	System.out.println("check convert string hex to long/byte:: "+ Long.parseLong("58", 16));
//        
//           	
//        	System.out.println("check decode byte: "+	Long.decode("0x58").byteValue());
//        	
//        	
//    		pkconfig.config_init();
//    		System.out.println("--> Start init config <-- ");
//    		pkinit_rxtx.connect(pkvar.serialport);
//    		new pktask_rxtx();
//    		System.out.println("--> Start init rxtx <-- ");
//    		pkredis_var.redisconn_sentinel_init();
//    		
//    		pkredis_var.del_cmd(pkredis_var.lpopkeysendbyte);
//    		new pktask_redis();
//    		System.out.println("--> init redis cluster <-- ");
//    		String str =pk_redis_var.lpop_cmd("sendbyte");
    		
//    		Long [] recv_b = pk_var.string_byte(str);
    		
//    		System.out.println("--> "+Arrays.toString(recv_b));
    		
    		
		} catch (Exception e) {
		
			e.printStackTrace();
		}
    }
}
