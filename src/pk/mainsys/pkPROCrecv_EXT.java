package pk.mainsys;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import boj.sialw.pkboj;
import datatrx.json.pkjson;
import farecsc.faredata;
import jsonsvc.httpsvc;
import jsonsvc.json;
import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;

public class pkPROCrecv_EXT {
//	static Jedis jedis;

	
	public static void RECV_RAWbytefilter (byte[] data) {
		try {
			  pkByteData.TIMOUT=0;
			  pkByteData.TIMOUT_whole=0;
			
//			  System.err.println("[RECV] EXIT  :"+Arrays.toString(pkvar.Bytearray_toStringarray(data))+" SEQ : "+pkvar.checkSEQstring(data[3])+", time recv :"+(System.currentTimeMillis() - pkinit_rxtx.ts));
//			  pkvar.logtxt("[RECV] EXIT [SEQ: "+pkvar.checkSEQstring(data[3])+"] :"+Arrays.toString(pkvar.Bytearray_toStringarray(data))+" SEQ : "+pkvar.checkSEQstring(data[3])+",time recv :"+(System.currentTimeMillis() - pkinit_rxtx.ts));
//			  System.out.println("FIRST raw byte is clean !,data[0] "+data[0] +" ,data[data.length-1: "+data[data.length-1]);
			
	

//				System.out.println(" ---  raw byte is clean !,data[0] "+data[0] +" ,data.lenght : "+data[data.length-1]+" ---proceed next");
			  pkByteData.is_okcard = pkByteData.bizrulcontext(data[4]);

	             if(pkvar.readerpath.equals("exit")) {/// ####################################--for EXIT LANE ############################
					if(!pkByteData.is_okcard.equals("okcard")) {
						if(pktsk_Qrecv_EXT.byte_RECV_SEQ_PROCESS_FLAG!=16) {

							pkByteData.VALIDCARD = 99;
							
							System.out.println( ">>>[exit] CHECK CARD, ["+pkByteData.is_okcard+"],cardtype: "+pkByteData.CARDTYPE+" >>> CANNOT process card ! <<< : "+String.format("%02X",data[4]));

													 							
						 }

						return;
					}else {
						if(pktsk_Qrecv_EXT.byte_RECV_SEQ_PROCESS_FLAG!=16) {
							if(pktsk_Qrecv_EXT.byte_RECV_SEQ_PROCESS_FLAG==3 && pkByteData.is_okcard.equals("okcard") ) {
								
								pkByteData.VALIDCARD = 1;
								pkByteData.CARDTYPE = pkByteData.processCARD(data[33],data);
								
						
								
								int entID = pkByteData.check_ENTRYID(data);// for pinalti
//								int entID=0;
							 					
								if(entID<1) {
							    	 pkByteData.NO_ENTRY_INFO = "true";
//							    	 byte [] bytedisplay = pkvar.byteDisplay("Please wait", "__");
//							         pktask_Q_EXT.fifobyt_TX.add(bytedisplay);
//							         Thread.sleep(50);
							    	System.out.println("###Entry Info  "+entID +" ### "+pkvar.tarikh());
						 							    	
							    }
								
							
								
								pkconfig.REQ_url="false";
								pkByteData.TIMOUT=0;
								
								System.out.println(" >> [exit] CHECK CARD :["+pkByteData.is_okcard+"] cardtype: "+pkByteData.CARDTYPE+"["+String.format("%02X",data[33])+"] >>> VALID ! <<< RESP BYTE:"+String.format("%02X",data[4]));
							
							}		
							 else if(pktsk_Qrecv_EXT.byte_RECV_SEQ_PROCESS_FLAG==4) {
								
								 pkByteData.is_okcard = pkByteData.bizrulcontext(data[4]);
								 
								 if(pkByteData.CARDTYPE.equals("EMV")) {
									 
									 pkByteData.detaisplitlEMV_proc_exit(data);
									 pkconfig.RESP_CODE = String.format("%02X",data[4]);
									 pkByteData.printinfoEMV_proc_exit();
								 }
								 
								 if(pkByteData.CARDTYPE.equals("TNG")) {
//									
									
					        		
									         pkByteData.detaisplitlTNG_proc_exit(data);
											 pkconfig.RESP_CODE = String.format("%02X",data[4]);
											 pkByteData.printinfoTNG_proc_exit();
					        		
					        	
					        		   

									
								 }
//								 sndByte_proceedexit();
								 if(pkByteData.CARDTYPE.equals("TNG")) {
									 //##settlement rest-----
									 pkByteData.rest_settle("ok", "00",pkvar.requestsettleurl);	 
								 }
								
								 System.err.println("[exit] ### END-TRAX CHECK ### "+pkByteData.is_okcard+", RESP BYTE:"+String.format("%02X",data[4]));
							}
													
						
						 } else {
							 
//							 System.out.println(" scatter byte receive !");r
							 
						 }
					
					
					}
			 }
				
			
		}catch(Exception b) {
			b.printStackTrace();
		}
		
	
		
	}
	
	
	

}
