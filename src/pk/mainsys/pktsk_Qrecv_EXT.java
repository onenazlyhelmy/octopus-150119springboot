package pk.mainsys;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Toolkit;















import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
 

 
public class pktsk_Qrecv_EXT {
	Toolkit toolkit;
	Timer timer;


	public static Queue<byte[]> fifobyt_RX = new LinkedList<byte[]>();
	public static int byte_RECV_SEQ_PROCESS_FLAG = 99;
//	public static int SEQ_INIT_ENT_FLAG = 1; 
//	public static int SEQ_PROC_ENT_FLAG = 2; 
//	public static int SEQ_INIT_EXT_FLAG = 3; 
//	public static int SEQ_PROC_EXT_FLAG = 4; 
//	public static int SEQ_INIT_SALE_FLAG = 5; 
//	public static int SEQ_PROC_SALE_FLAG = 6; 	

	
	//--coherent-----
//	private static  org.slf4j.Logger log = LoggerFactory.getLogger(pktask_RECVbyte.class.toString());
	int  timeout = Integer.parseInt( pkvar.timeoutread);
 
	public pktsk_Qrecv_EXT() throws Exception {

		System.out.println("--> start pk_task_byteRECV <-- ");
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 50); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {

		  int tot_byt =0;
		
		public void run()  {
			 ByteBuffer b ;
			 byte[] byte_RECV ;
//			 int dat ;
//			 byte [] byte_read = null;
			
			
		try {
			
			if(!fifobyt_RX.isEmpty()) {		
				
				 while (! fifobyt_RX.isEmpty()) {
			    
					 pkByteData.TIMOUT=0;
				 
					 byte_RECV = new byte [fifobyt_RX.peek().length];
					 byte_RECV = fifobyt_RX.poll();
					 System.err.println("[RECV]Q- EXIT  [SEQ: "+pkvar.checkSEQstring(byte_RECV[3])+"] :"+Arrays.toString(pkvar.Bytearray_toStringarray(byte_RECV))+" SEQ : "+pkvar.checkSEQstring(byte_RECV[3])+" #");
					 pkvar.logtxt("[RECV] EXIT [SEQ: "+pkvar.checkSEQstring(byte_RECV[3])+"] :"+Arrays.toString(pkvar.Bytearray_toStringarray(byte_RECV))+" SEQ : "+pkvar.checkSEQstring(byte_RECV[3])+",time recv :"+(System.currentTimeMillis() - pkinit_rxtx.ts));
					 //----------------------------------------------------------
					 pktsk_Qrecv_EXT.byte_RECV_SEQ_PROCESS_FLAG  =byte_RECV[3];
					 
					 
					 ///head == 2, tail == 3
					if(byte_RECV[0]==2 && byte_RECV[byte_RECV.length-1]==3 && byte_RECV.length > 6 ) {
						
					  pkByteData.is_okcard = pkByteData.bizrulcontext(byte_RECV[4]); 
					  
					  //--gpio--fast respon
					  if(pkByteData.is_okcard.equals("okcard")) {
						  pkByteData.VALIDCARD =1;
					  }
					     
				      if(pkByteData.byte_SEQ_PROCext_tng==byte_RECV_SEQ_PROCESS_FLAG && pkByteData.VALIDCARD ==1  && pkconfig.AUTHORIZE.equals("no") ) {
				        	
//				    	  pkByteData.NO_ENTRY_INFO="false";
				    	  if(pkByteData.CARDTYPE.equals("TNG")) {
	                   	      pkconfig.AUTHORIZE="yes";
	             			  System.err.println("###[TNG] ALB EXIT UP #### ");
//	                   	      pkrasp_io.gpio_trigAUTH_ALB(1);
	                   	 	  pkredis_var.set_cmd("forcealb","1");
				    	  }else {
	                   	      pkconfig.AUTHORIZE="yes";
	             			  System.err.println("###[EMV] ALB EXIT UP #### ");
//	                   	      pkrasp_io.gpio_trigAUTH_ALB(1);
	                   	 	  pkredis_var.set_cmd("forcealb","1");
				    	  }

				        }
				      
					  System.err.println("EXIT--byte_RECV_SEQ_PROCESS_FLAG: "+byte_RECV_SEQ_PROCESS_FLAG);
					  System.err.println("EXIT--byte_SEQ_PROCent_tng: "+pkByteData.byte_SEQ_PROCext_tng);
					  System.err.println("EXIT--SEQUENCE_PROC: "+pkByteData.SEQUENCE_PROC);
					  System.err.println("EXIT--VALIDCARD "+pkByteData.VALIDCARD+",pkconfig.AUTHORIZE:"+pkconfig.AUTHORIZE);
				      //----------------------- gpio--fast--
					
						 //hex 16 --- integer --enable reader process cmd--
						 if(pktsk_Qrecv_EXT.byte_RECV_SEQ_PROCESS_FLAG!=16 ) {
						
								 pkPROCrecv_EXT.RECV_RAWbytefilter(byte_RECV);
							 
					    
						 }
						 
						 
						 
			             
						          if(pkByteData.byte_SEQ_EN_RDR==byte_RECV_SEQ_PROCESS_FLAG) {
						        	  pkByteData.TIMOUT=0;
					                     	  if(pkvar.readerpath.equals("exit")) { 
//					                     		   System.out.println("-- RECV SUCCESS INIT EXIT >>> "+pkvar.tarikh());
					                     		     byte [] byts = pkByteData.bytSND_INIT_EXT_compiled();
							                      	 pktsk_Qsnd_EXT.fifobyt_TX.add(byts);
							                      	 System.out.println("-- SUCCESS EXIT ENABLE READER  >>> "+pkvar.tarikh());
							                      	 pkByteData.SEQUENCE_PROC=3;
//							                      	 new pktskPOLL();
					                     	  }
				                     }
						  
						          
						          if(pkvar.readerpath.equals("exit")) { // ########################## EXIT ########################
						                
						        	  String simulateloop1 = pkredis_var.get_cmd("simulateloop1");
										
										if((pkconfig.simulateloop1.equals("1") || 	pkconfig.loop1 ||  simulateloop1.equals("1")) && pkconfig.AUTHORIZE.equals("no")   ) {
											if(byte_RECV[4]==20) {
												//if receve byte 0x14 = cannot detect anycard
												  
													     byte [] byts = pkByteData.bytSND_INIT_EXT_compiled();
								                      	 pktsk_Qsnd_EXT.fifobyt_TX.add(byts);
												     
												
											}
									 	}
//						        	  	 System.out.println(" ### RECV_SEQ_PROCESS_FLAG >>> "+byte_RECV_SEQ_PROCESS_FLAG);
										
										 if(pkconfig.PENALTI_EN.equals("true")) {
						        			   System.out.println("-- Disable Penalti >>> "+pkvar.tarikh()); 
						        		   }
										  
										
										  
						        	   if(pkByteData.byte_SEQ_INIText_tng==byte_RECV_SEQ_PROCESS_FLAG && pkByteData.VALIDCARD ==1 &&   pkByteData.SEQUENCE_PROC==3  && pkByteData.NO_ENTRY_INFO.equals("false") && pkconfig.PENALTI_EN.equals("false")) {
						        		   System.out.println("-- RECV SUCCESS INIT EXIT >>> "+pkvar.tarikh());
						        		  
						        	
						        			   pktsk_Qsnd_EXT.fifobyt_TX.clear();
							        		   
							        		   pkByteData.NO_ENTRY_INFO="false";
							        		   
							        		   System.out.println(" ### RECV INIT EXIT SEQ FLAG >>> "+byte_RECV_SEQ_PROCESS_FLAG);
							        		   System.out.println("-- compile send proc_exit >>> "+pkvar.tarikh());
							        		  if( pkByteData.CARDTYPE.equals("TNG")) {
							        			   pkByteData.detaisplitlTNG_proc_exit(byte_RECV); 
							        		   }
							        	       
							        		   if(pkByteData.CARDTYPE.equals("EMV")) {
							        	    	
							        	       }
							        	       
						        			   byte [] byts = pkByteData.bytSND_PROC_EXT_compiled(pkByteData.AMNTCHARGEdbl);
						        			   
						        			   String byt =Arrays.toString(pkvar.Bytearray_toStringarray(byts));
						        			   System.out.println("Data byte : "+byt+",Amount : "+pkByteData.AMNTCHARGEdbl);
						        			   
											   pktsk_Qsnd_EXT.fifobyt_TX.add(byts);
							        		   		   
							        		   pkByteData.SEQUENCE_PROC=4;  
						        		 
						        		   
						        		
										  
										  
//						        		   System.exit(0);
		                                }
						        	   
						        	   ///for no entry data!!!...penalti
						        	   if(pkByteData.byte_SEQ_INIText_tng==byte_RECV_SEQ_PROCESS_FLAG && pkByteData.VALIDCARD ==1 &&   pkByteData.SEQUENCE_PROC==3 && pkByteData.NO_ENTRY_INFO.equals("true") && pkconfig.PENALTI_EN.equals("true")) {
						        		   System.out.println("-- RECV SUCCESS INIT EXIT >>> "+pkvar.tarikh());
						        		  
						        		  
						        		   System.out.println(" ### RECV INIT EXIT SEQ FLAG and No Entry Info >>> "+byte_RECV_SEQ_PROCESS_FLAG);
						        		   if(pkconfig.PENALTI_EN.equals("true")) {
						        			   pktsk_Qsnd_EXT.fifobyt_TX.clear();
		//					        		   System.out.println("-- compile send proc_exit >>> "+pkvar.tarikh());
							        		   System.out.println("[exit] ##### send Display  ##############   ");
							      		       byte [] bytedisplay = pkvar.byteDisplay("No entry Info", "xooox");
										       pktsk_Qsnd_EXT.fifobyt_TX.add(bytedisplay);
						        			   pkByteData.PENALTI_AMNT = pkByteData.calculateFARE( pkByteData.ENTRY_DATE+ pkByteData.ENTRY_TIME, pkByteData.EXIT_DATE+pkByteData.EXIT_TIME,"1");
												  
		//									     pkByteData.PENALTI_AMNT =  pkByteData.PENALTI_AMNT;
						        			     Thread.sleep(100);
						        	             System.out.println("[exit] ##### send Init Sale  ##############   ");
										    	 byte [] byts = pkByteData.bytSND_INIT_SALE_compiled(pkByteData.PENALTI_AMNT);
						                      	 pktsk_Qsnd_EXT.fifobyt_TX.add(byts);
						                      	 
						                         System.out.println("[exit] ##### penalti amount : "+pkByteData.PENALTI_AMNT+"  ##############   ");
		//				                         
						                    	 
		
		//				                         System.out.println("[exit] ##### send Display  ##############   ");
		
		//				                    	 Thread.sleep(100);
						                         
						                    	 System.out.println("[exit] ##### send Proceed Sale ##############   ");
						                         byts = pkByteData.bytSND_PROC_SALE_compiled();
												 pktsk_Qsnd_EXT.fifobyt_TX.add(byts);
							        		   		   
							        		     pkByteData.SEQUENCE_PROC=4;
										  
						        		    }
						        		   
						        		
//						        		   System.exit(0);
		                                }
						        	   
						        	   if(pkByteData.byte_SEQ_INIText_tng==byte_RECV_SEQ_PROCESS_FLAG && pkByteData.VALIDCARD ==1 && pkByteData.NO_ENTRY_INFO.equals("true") &&   pkByteData.SEQUENCE_PROC==3) {
										   pktsk_Qsnd_EXT.fifobyt_TX.clear();
										   System.out.println("[exit] ##### send Display  ##############   ");
						      		       byte [] bytedisplay = pkvar.byteDisplay("No entry Info", "ooo");
									       pktsk_Qsnd_EXT.fifobyt_TX.add(bytedisplay);
									       pkByteData.NO_ENTRY_INFO="false";
									  }
						        	   
		                                if(pkByteData.byte_SEQ_PROCext_tng==byte_RECV_SEQ_PROCESS_FLAG && pkByteData.VALIDCARD ==1  && pkconfig.AUTHORIZE.equals("no")  ) {
		                                	   pkByteData.NO_ENTRY_INFO="false";
//		                                	   pkconfig.AUTHORIZE="yes";
		                                	   System.out.println(" ### RECV_SEQ_PROC_FLAG >>> "+byte_RECV_SEQ_PROCESS_FLAG);
		                                	   System.out.println("-- RECV SUCCESS PROC EXIT >>> "+pkvar.tarikh());
		                                	   pktsk_Qsnd_EXT.fifobyt_TX.clear();
		                                	   fifobyt_RX.clear();
		                                	  
		                                	   System.err.println(" <<<<< &&&&&&&&& END TRX  OFFLINE &&&  >>>>>>> "+pkvar.tarikh());
		                                	   
		                                	   pkByteData.SEQUENCE_PROC=99;
		                                	   Thread.sleep(3000);
		                                	
		                                	  
		                                }

						          }
					 
					 }

//		             String tmp_byte_RX [] = pkvar.Bytearray_toStringarray(byte_RECV);
//		             pkvar.logtxt("PROCESS RECV Byte >>> "+Arrays.toString(tmp_byte_RX)+", RECV_SEQ_PROCESS_FLAG :"+byte_RECV_SEQ_PROCESS_FLAG);  
//		             System.out.println(" PROCESS RECV Byte >>> "+Arrays.toString(tmp_byte_RX)+", RECV_SEQ_PROCESS_FLAG recv :"+byte_RECV_SEQ_PROCESS_FLAG);  
		           

					          
							
					          //----
		             
		             }


			         }

			
				

			  } catch (Exception e) {
				 e.printStackTrace();
				 
			 }
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}