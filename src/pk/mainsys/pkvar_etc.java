package pk.mainsys;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.security.CodeSource;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.sql.*;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;


import java.util.zip.CRC32;
import java.util.zip.Checksum;



public class pkvar_etc {
	
	

	
	public static  String gpioAddr_loop1;
	public static  String gpioAddr_loop2; 
	public static  String gpioAddr_ent_alb;
	public static  String gpioAddr_ext_alb; 
	
	public static  String gpioAddr_ent_buzz; 
	public static  String gpioAddr_ext_buzz; 
	
	public static  String gpioAddr_ent_auth; 
	public static  String gpioAddr_ext_auth; 
	
	public static String card_ser;
	
	public static int ent_AUTH=0;
	public static int ext_AUTH=0;
	
	public static int ent_BUZZ=0;
	
//  	public static   int gpio_wie0=0;
//  	public static	int gpio_wie1=1;
//  	public static	int gpio_wie2=0;
//  	public static	int gpio_wie3=1;
  	
	public static   int gpio_loop1=0;
  	public static	int gpio_loop2=1;

  	//---log--interlock--
  	public static int log_en=0;
  	//--------------
 
	//PI--pin out-----------------------
	static Pin pinENT_buzz = null ;
	static Pin pinENT_alb  = null  ;
	static Pin pinENT_auth  = null  ;
    
	static Pin pinEXT_buzz = null ;
	static Pin pinEXT_alb  = null  ;
	static Pin pinEXT_auth  = null  ;
	
	//-pin IN
//	static Pin pinLOOP1 = null ;
//	static Pin pinLOOP2  = null  ;
	
	static PinState pinLOOP1_state;
	static PinState pinLOOP2_state;
	
	static boolean loop1,loop2;
	
	public static String serialport;
	
	//-----------------------------
	public static JedisPool jedis_local_pool;
	public static Jedis jedis_local;
  	public static String redisIPexit ="localhost";
  	public static String redisIPentry ="localhost";
	public static String pathType ="entry";
	public static String antipassbck = "false";
	public static String antipass_period = "2"; //in minute motherfucker..
	public static String[] antipass_whitelist_arr = new String[10]; //--exception for antipass
	public static String antipass_whitelist_redis = "0000|0000";
	public static String antipass_period_chck="false";
	//---------------
	public static String timeoutread;


	
	static public Pin getRaspPin(Integer getpin) {
		switch(getpin.intValue()){
		case 0:
			return RaspiPin.GPIO_00;
		case 1:
			return RaspiPin.GPIO_01;
		case 2:
			return RaspiPin.GPIO_02;
		case 3:
			return RaspiPin.GPIO_03;
		case 4:
			return RaspiPin.GPIO_04;
		case 5:
			return RaspiPin.GPIO_05;
		case 6:
			return RaspiPin.GPIO_06;
		case 7:
			return RaspiPin.GPIO_07;
		case 8:
			return RaspiPin.GPIO_08;
		case 9:
			return RaspiPin.GPIO_09;
		case 10:
			return RaspiPin.GPIO_10;
		case 11:
			return RaspiPin.GPIO_11;
		case 12:
			return RaspiPin.GPIO_12;
		case 13:
			return RaspiPin.GPIO_13;
		case 14:
			return RaspiPin.GPIO_14;
		case 15:
			return RaspiPin.GPIO_15;
		case 16:
			return RaspiPin.GPIO_16;
		case 17:
			return RaspiPin.GPIO_17;
		case 18:
			return RaspiPin.GPIO_18;
		case 19:
			return RaspiPin.GPIO_19;
		case 20:
			return RaspiPin.GPIO_20;
		case 21:
			return RaspiPin.GPIO_21;
		case 22:
			return RaspiPin.GPIO_22;
		case 23:
			return RaspiPin.GPIO_23;
		case 24:
			return RaspiPin.GPIO_24;
			
		case 25:
			return RaspiPin.GPIO_25;
		case 26:
			return RaspiPin.GPIO_26;
		case 27:
			return RaspiPin.GPIO_27;
		case 28:
			return RaspiPin.GPIO_28;
		case 29:
			return RaspiPin.GPIO_29;
		case 30:
			return RaspiPin.GPIO_30;
		default:
			throw new IllegalArgumentException("illigal argurment numbering :).");
		}
	}
	
   

	 
	 public static byte[] DateString_byte(String dt)  {
		 String str1 = "2018-09-12 23:08:09";
		 dt = str1;
		 byte [] byte_dt = new byte[6];
		 Integer [] int_dt = new Integer[6];
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 SimpleDateFormat dt_ss= new SimpleDateFormat("ss");
	 	 SimpleDateFormat dt_min= new SimpleDateFormat("mm");
	 	 SimpleDateFormat dt_HH= new SimpleDateFormat("HH");
	 	 SimpleDateFormat dt_yyyy= new SimpleDateFormat("yy");
	 	 SimpleDateFormat dt_MM= new SimpleDateFormat("MM");
	 	 SimpleDateFormat dt_dd= new SimpleDateFormat("dd");
	
		 try {
			 Date dates = sdf.parse(dt);
			 String str_ss = dt_ss.format(dates);
			 String str_min = dt_min.format(dates);
			 String str_HH = dt_HH.format(dates);
			 String str_yyyy = dt_yyyy.format(dates);
			 String str_MM = dt_MM.format(dates);
			 String str_dd = dt_dd.format(dates);
			 
			 //--these need to re-convert
			 int_dt[5] =  Integer.valueOf(str_ss);
			 int_dt[4] = Integer.valueOf(str_min);
			 int_dt[3] =Integer.valueOf(str_HH);
			 int_dt[2] = Integer.valueOf(str_dd);
			 int_dt[1] =Integer.valueOf(str_MM);
			 int_dt[0] =Integer.valueOf(str_yyyy);
			 
			 //--take 'string' value as exactly parse to 'Byte'..example in String= 18,Byte=18(but in Integer value is 24)
			 byte_dt[5] =  Byte.valueOf(str_ss,16);
			 byte_dt[4] = Byte.valueOf(str_min,16);
			 byte_dt[3] =Byte.valueOf(str_HH,16);
			 byte_dt[2] =Byte.valueOf(str_dd,16);
			 byte_dt[1] =Byte.valueOf(str_MM,16);
			 byte_dt[0] = Byte.valueOf(str_yyyy,16);
			
			 
		 	
//		 	    System.out.println("date :"+  str_dd);
		 }catch(Exception c) {
			 c.printStackTrace();
		 }
		
		 
	
		 return byte_dt;
		 
	 }
	 

	 public  static String logtxt(String str) 
	 {
		   Date dateNow = new Date();
		   
		    SimpleDateFormat format_dt= new SimpleDateFormat("yyyyMMdd");
		    
		        String date_to_string = format_dt.format(dateNow);
	    		String data = str;
//	    		 System.out.println(str+"\n");
	 
	    			 	    		
	    		BufferedWriter out;
				try {
					out = new BufferedWriter(new FileWriter
					        ("log//"+date_to_string+".log",true));
					    out.write(tarikh()+"::"+data+"\n");
	    	            out.flush();
	    	            out.close();
	    	            
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	         
	    	            return "ok";
	     	//	BufferedWriter  out = new BufferedWriter(new FileWriter
	         //    (file,true));
	        //     out.write(data+"..system run..\n");
	         //    out.close();
	 
		      //  System.out.println("Done");
	 
	    	
	    } 
	 
	 

	

		
	


	
	 

	 public static String tarikh(){
	    	
	   	 Date dateNow = new Date();
			    //SimpleDateFormat format_dt= new SimpleDateFormat("HH:mm:ss");
			    SimpleDateFormat format_dt= new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
			    
			  // SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("ddMMyyyy");
			    String date_to_string = format_dt.format(dateNow);
			 //  System.out.println("Today's date into ddMMyyyy format: " + date_to_string);
	   	
	   	return date_to_string;
	   	
	   }

	     	       static  long checksum(byte[] bytes) {
	    	   byte sum = 0;
	    	   for (byte b : bytes) {
	    		   sum = (byte) (sum ^ b); 
	    	   }
	    	   return (byte) (sum % 256);
	    	}
	       
	       
	       static int crc16cal(byte[] bytearr) {
	    	     int[] table = {
	    	    		 0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	    	    			0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	    	    			0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	    	    			0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	    	    			0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	    	    			0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	    	    			0x3653, 0x2772, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	    	    			0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,

	    	    			0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	    	    			0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	    	    			0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	    	    			0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	    	    			0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	    	    			0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	    	    			0x7e97, 0xe6b6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	    	    			0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,

	    	    			0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	    	    			0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	    	    			0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	    	    			0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	    	    			0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	    	    			0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	    	    			0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	    	    			0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,

	    	    			0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	    	    			0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	    	    			0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	    	    			0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	    	    			0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	    	    			0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	    	    			0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	    	    			0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
	    		        };


//	    		        byte[] bytes = args[0].getBytes();
	    	            
//	    	         	long wCrc = convertToLong(bytearr) ;
	    		        int crc = 0x0000;
	    		        for (byte b : bytearr) {
//	    		            crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
	    		        	crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
	    		        }
	    		        
	    		        System.out.println("crc: "+ (crc  ^ 0xFFFF));
	    		        long crc16 = crc ;
	    		        
	    		        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
	    		        buffer.putLong(crc16);
//	    		        return buffer.array();
	    		        System.out.println("crc: "+  Arrays.toString(buffer.array()));
//	    		        
                        return (int) crc  ^ 0xFFFF;
//	    		        StdOut.println("CRC16 = " + Integer.toHexString(crc));
	       }

	       public static long convertToLong(byte[] array) {
	    	    ByteBuffer buffer = ByteBuffer.wrap(array);
	    	    return buffer.getLong();

	    	  } 
	
	    
	    
	       
	   
	  	 public static void refresh_app()  {
//				try {
//					Process pr = Runtime.getRuntime().exec("/bin/sh  rfid.sh");
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					
//					e.printStackTrace();
//				}
			  
			//   final ArrayList<String> command = new ArrayList<String>();
			//   command.add("/bin/sh");
			//   command.add("-i");
			//   command.add("/root/jrfid/rfid.sh");
			//   final ProcessBuilder builder = new ProcessBuilder(command);
			//   builder.start();
				//ProcessBuilder pb = new ProcessBuilder("/bin/sh"," -i", "rfid.sh");
				//pb.start();
				System.exit(0);  //--DETECT system JVMShutdownHook-->
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
		 }
	  	 
		 

		 
		 public static void printanim() {
		     final String alphabet = "oO-";
		    	final int N = alphabet.length();

		   	Random r = new Random();
		   	
		   	int i = 1;
		   	do 
		   	{
		   		System.out.println("   __");
		   		System.out.println("  /__\\");
		   		System.out.println(" [[o>o]");
		   		System.out.print("  \\\\");
		   	   	System.out.print(alphabet.charAt(r.nextInt(N)));
		   	   	System.out.print("/" + "\n");
		       	System.out.println("  / /\\");
		       }
		       while (i > 0);
		       
		  	}

		 public static void printanim1() {
			  char[] animationChars = new char[] {'O', 'o', 'X', 'x'};
			  final String alphabet = "oO-";
		    	final int N = alphabet.length();

				        System.out.println("hi name is Clay..im handle this system...!");
			            System.out.println("   __");
				   		System.out.println("  /__\\");
				   		System.out.println(" [[o>o]");
				   		System.out.print("  \\\\");
				   	   	System.out.print("O");
//				   		System.out.print(animationChars[i % 4]);
				   	   	System.out.print("/" + "\n");
				       	System.out.println("  / /\\");
				        System.out.println("   __");
				    }
		 	
			public static boolean isSocketAlive(String hostName, int port) {
				boolean isAlive = false;
		 

				SocketAddress socketAddress = new InetSocketAddress(hostName, port);
				Socket socket = new Socket();
		 
				
				int timeout = 1000;
		 

				try {
					socket.connect(socketAddress, timeout);
					socket.close();
					isAlive = true;
//					System.out.println(" -- live connection ["+hostName+"]["+port+"] -- ");
//					logtxt(" -- live connection ["+hostName+"]["+port+"] -- good! ");
		 
				} catch (SocketTimeoutException exception) {
//					System.out.println("SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
					logtxt("SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
				} catch (IOException exception) {
//					System.out.println("IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
					logtxt(	"IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
				}

				return isAlive;
			}
			  
}
