package pk.mainsys;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Toolkit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
 

 
public class pktaskbuzz {
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;

	public static Logger log = LoggerFactory.getLogger(pktaskbuzz.class);
 
	public pktaskbuzz() throws Exception {
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
	
	       
	      log.info("start > pktaskbuzz <");
		  
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 100); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			
				
		if(pkvar.ent_BUZZ>0) {
			
			try {
				pkvar.ent_BUZZ=0;
				pkrasp_io.gpio_trigAUTH_buzz(1);
				Thread.sleep(10);
				pkrasp_io.gpio_trigAUTH_buzz(0);
				Thread.sleep(10);
				pkrasp_io.gpio_trigAUTH_buzz(1);
				Thread.sleep(50);
				pkrasp_io.gpio_trigAUTH_buzz(0);
				Thread.sleep(100);
				  pkrasp_io.gpio_trigAUTH_buzz(1);
				Thread.sleep(100);
				pkrasp_io.gpio_trigAUTH_buzz(0);
				  Thread.sleep(50);
				pkrasp_io.gpio_trigAUTH_buzz(1);
				  Thread.sleep(100);
				pkrasp_io.gpio_trigAUTH_buzz(0);
				
			} catch (Exception  e) {
				try {
					Thread.sleep(2000);
					new pktaskbuzz();
				} catch (Exception e1) {
					
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		
		}
			
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}