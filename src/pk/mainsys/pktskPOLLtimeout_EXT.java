package pk.mainsys;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import nioSOCKreaderCoherent.nioCoher_ENT;
import nioSOCKreaderCoherent.nioCoher_EXIT;
import redis.drac.pkredis_var;

import java.awt.Toolkit;


 
public class pktskPOLLtimeout_EXT {
	public static final TimerTask tmrReminder = null;
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
//	 static int SEQUENCE_PROC=99;
//	 static int TIMOUT=0;
	int timeout = 10;
	nioCoher_EXIT nio_mfgreader_EXITxx;
    int timeoutautorecon;
		private static Log log = LogFactory.getLog(pktskPOLLtimeout_EXT.class);
	public pktskPOLLtimeout_EXT() throws Exception {
		log.info("start --> "+pktskPOLLtimeout_EXT.class);
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
		timeout = Integer.valueOf(pkvar.timeout);
		timeoutautorecon = Integer.valueOf(pkvar.timeoutautorecon);
	       
//	       System.out.println(" -- WIE TASK TRXDATA -- " );
		nio_mfgreader_EXITxx = new nioCoher_EXIT();
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 1000); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			
			try{	
			

				String simulateloop1 = pkredis_var.get_cmd("simulateloop1");
		
//			   System.out.println("----- count time out ----: "+TIMOUT);
				if((pkconfig.simulateloop1.equals("1") || 	pkconfig.loop1 ||  simulateloop1.equals("1"))  && pkconfig.AUTHORIZE.equals("no")  ) {
					
					pkByteData.TIMOUT++;
					pkByteData.TIMOUT_whole++;
				   if(pkByteData.TIMOUT>timeout) {
					   System.out.println("-RESET [EXIT] #not receive any byte#: "+pkByteData.TIMOUT);
					   pkByteData.TIMOUT=0;
					   pkByteData.SEQUENCE_PROC=3;
					     
	
						     byte [] byts = pkByteData.bytSND_INIT_EXT_compiled();
	                      	 pktsk_Qsnd_EXT.fifobyt_TX.add(byts);
				

						
				   }
				   
				}else {
					pkByteData.TIMOUT=40;
				}
				
				if(pkByteData.TIMOUT_whole>timeoutautorecon) {
//					pkbytedata.TIMOUT=0;
//					pkbytedata.SEQUENCE_PROC=3;
//					pkbytedata. TIMOUT_whole=0;
//              	nio_mfgreader_EXIT nio_mfgreader_EXIT = new nio_mfgreader_EXIT();
//              	nio_mfgreader_EXIT.init();
				pkredis_var.set_cmd("readerbanktimeout", "true");
				pkvar.logtxt("readerbanktimeout-exit");
                System.err.println("----- TIMEOUT--WHOLE %%%%% [EXIT]: "+pkByteData.TIMOUT_whole);
				}

			
			}
			catch (Exception e) {

				e.printStackTrace();
//				pk_var.logtxt("\n--------------------------\n"+e.toString()+"\n------------------------------\n");
			}
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}