package pk.mainsys;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;

import com.google.gson.Gson;
import com.pi4j.io.gpio.Pin;

import redis.drac.pkredis_var;

public class pkconfig {
	public static String configss = null;
 // public static void main(String[] args) throws Exception {
	  static boolean loop1=false;
	  
	  static String connectTTY="no";
	  String dbip = "";
	   String dbport = "";
	   String svrport = "";
	   String searchstring = "";
	   static String jarDir_name ;
	   static   String pinout;
	   static	String gpio_ent_en = null,gpio_ext_en = null;
	   static	String lop_en="0" ;
	   static String simulateloop1="";
	   
	   public static String readerip="10.222.103.212";
	   public static int  readerport=5000;
	   
	  public  static String datebojtrigger="00:00:00";
	  public  static String datebojtime="000000";
	  public  static String datebojdate="20001212";
	  
	    public static Queue<String> data_topush = new LinkedList<String>();
	    public static Queue<String> data_topushdb = new LinkedList<String>();
	  
	  //---data
	  public  static String locationno="00:00:00";
	  public  static String terminalno="00:00:00";
	  public  static String jobno="00:00:00";
	  public  static  String comjobno="00:00:00";
	  public  static String jobtype="00:00:00";
	  public  static String readerpath="00:00:00";
	  public  static String opermode="00:00:00";
	  public  static String badgeno="00:00:00";
	  public  static String trxtype="00:00:00";
	  public  static String exitoperatorid="00:00:00";
	  public  static String exitlocation="00:00:00";
	  public  static String exitterminalno="00:00:00";
	  public  static String exittcclass="00:00:00";
	  public  static String exitdetectclass="00:00:00";
	  public  static String farelocation="00:00:00";
	  
	  public  static String paymenttype="00:00:00";
	  
	  public  static String spid="00:00:00";
	  public  static String plzno="00:00:00";
	  public  static String lane="00:00:00";
	  
	  public static String RESP_CODE="";
	  
	  //simulate data --> 
	  public static String SIM_DATEENTRY="";
	  public static String SIM_DATEEXIT="";
	  public static String SIM_TRXNO="";
	  public static String SIM_AMNT="";
	  public static String SIM_ENABLE="false";
	  public static String SIM_PH;
	  
	  //tax----
	  public static String  BANK_ORIFARE ="";
	  public static String  BANK_TAXFARE ="";
	  public static String  BANK_SURCHARGE ="";
	  public static String  BANK_TAXSURCHARGE ="";
	  
	  public static String AUTHORIZE="no";
	public static int bytenotail;
	public static ArrayList<String> bytno_tail;
	public static  NumberFormat nfn_card = new DecimalFormat("0000000000");
	public static String REQ_url="false";
	public static String PENALTI_EN="false";
	


    public static void config_init() {
    	
    	System.out.println("--> Start  config_init <-- ");
    	pkSQLITE wie_sqlite = new pkSQLITE();
    	
    	
    	pkvar.springboot_enable=jReadConfig("springboot_enable");
    	
    	pkvar.serialport=jReadConfig("serialport");
//    	System.out.println("serialport :"+pkvar.serialport);
    	pkredis_var.redisip=jReadConfig("redisip");
    	pkredis_var.lpopkeysendbyte=jReadConfig("lpopkeysendbyte");
    	pkvar.timeoutread=jReadConfig("timeoutread");
    	
    	pkvar.timeoutautorecon=jReadConfig("timeoutautorecon");
    	pkvar.timeout=jReadConfig("timeout");
    	
    	pkvar.sst=jReadConfig("sst");
    	pkvar.sst_enable=jReadConfig("sst_enable");
    	pkvar.surcharge_enable=jReadConfig("surcharge_enable");
    	pkvar.surcharge=jReadConfig("surcharge");
    	pkvar.graceperiod=jReadConfig("graceperiod");
    	
    	//---rate setting
    	pkvar.flateratestarthour=jReadConfig("flateratestarthour");
     	pkvar.flaterateendhour=jReadConfig("flaterateendhour");
    	pkvar.flathourvalue=jReadConfig("fareurl");
    	
    	//fare url setting
    	pkvar.fareurl_enable=jReadConfig("fareurl_enable");
    	pkvar.fareurl=jReadConfig("fareurl");
    	pkvar.verifycscurl=jReadConfig("verifycscurl");
    	
    	MainXXXXX.ostype = pkconfig.jReadConfig("ostype");
    	
    	pkvar.requestsettleurl = pkconfig.jReadConfig("requestsettleurl");
    	
    	PENALTI_EN=jReadConfig("penalti_enable");
    	
    	//----
//         pk_var. antipassbck = jReadConfig("antipassbck_chck");
//	     pk_var. redisIPexit =jReadConfig("redisIPexit");
//	     pk_var. redisIPentry = jReadConfig("redisIPentry");
//	     pk_var. pathType = jReadConfig("pathType");
//	     pk_var.antipass_whitelist_redis = jReadConfig("antipass_whitelist_redis"); // eg: 1232221|98293999
//	     pk_var.antipass_period = jReadConfig("antipass_period");
//	     pk_var.antipass_period_chck = jReadConfig("antipass_period_chck");
	     
	     //----
	 	
		 amqp_attr.rabbit_en =jReadConfig("rabbit_en");
		
		 amqp_attr.amqp_ip =jReadConfig("amqp_ip");	
		 amqp_attr.amqp_q =jReadConfig("amqp_q");
		 amqp_attr.amqp_q1 =jReadConfig("amqp_q1");
		 amqp_attr.amqp_port =Integer.valueOf(jReadConfig("amqp_port"));
		 amqp_attr.amqp_user =jReadConfig("amqp_user");
		 amqp_attr.amqp_pwd =jReadConfig("amqp_pwd");
		 amqp_attr.amqp_timeout =Integer.valueOf(jReadConfig("amqp_timeout"));
		 
		
		 gpio_ent_en = jReadConfig("gpio_ent_en");
		 gpio_ext_en = jReadConfig("gpio_ext_en");
		 pkvar.gpioAddr_loop1 = jReadConfig("loop1");
		 pkvar.gpioAddr_loop2 = jReadConfig("loop2");
		 pkvar.gpioAddr_ent_alb =   jReadConfig("ent_alb");
		 pkvar.gpioAddr_ext_alb =   jReadConfig("ext_alb");
		 pkvar.gpioAddr_ent_buzz =   jReadConfig("ent_buzz");
		 pkvar.gpioAddr_ext_buzz =   jReadConfig("ext_buzz");
		 pkvar.gpioAddr_ent_auth =   jReadConfig("ent_auth");
		 pkvar.gpioAddr_ext_auth =   jReadConfig("ext_auth");
		 
		 //--ouput--
		 
		  pkvar.pinENT_alb = (Pin) pkvar.getRaspPin(Integer.valueOf( pkvar.gpioAddr_ent_alb));
		  pkvar.pinEXT_alb = (Pin) pkvar.getRaspPin(Integer.valueOf( pkvar.gpioAddr_ext_alb));
		  pkvar.pinENT_buzz = (Pin) pkvar.getRaspPin(Integer.valueOf( pkvar.gpioAddr_ent_buzz));
		  pkvar.pinEXT_buzz = (Pin) pkvar.getRaspPin(Integer.valueOf( pkvar.gpioAddr_ext_buzz));
		  pkvar.pinENT_auth = (Pin) pkvar.getRaspPin(Integer.valueOf( pkvar.gpioAddr_ent_auth));
		  pkvar.pinEXT_auth = (Pin) pkvar.getRaspPin(Integer.valueOf( pkvar.gpioAddr_ext_auth));

		 
		 
		  lop_en = jReadConfig("lop_en");
		  pinout = jReadConfig("pinout");
		  pkvar.readerpath = jReadConfig("readerpath");
		  wie_sqlite.loc_type =Integer.parseInt(jReadConfig("loc_type"));
		  simulateloop1 = jReadConfig("simulateloop1");
		  
		  
		  if(simulateloop1.equals("1")) {
			  System.out.println(" ------ Simulate run on Loop 1 ON STATE ------ ");
		  }
		  
	     //----
		  
		  //--data reconcile ---
		  
			 pkconfig.datebojdate = pkvar.tarikhdateonlyBOJ();
			 pkconfig.datebojtime= pkvar.tarikhtimeonlyBOJ();
//			 pkconfig.datebojdate = pkvar.tarikhdateonlyBOJ();
		  
		  locationno= jReadConfig("locationid");
		  terminalno=jReadConfig("terminalno");
		  jobno=jReadConfig("jobno");
		comjobno=jReadConfig("comjobno");
		jobtype=jReadConfig("jobtype");
		readerpath=jReadConfig("readerpath");
		 opermode=jReadConfig("opermode");
	badgeno=jReadConfig("badgeno");
		trxtype=jReadConfig("trxtype");
exitoperatorid=jReadConfig("exitoperatorid");
	exitlocation=jReadConfig("exitlocation");
		exitterminalno=jReadConfig("exitterminalno");
		 exittcclass=jReadConfig("exittcclass");
	exitdetectclass=jReadConfig("exitdetectclass");
	 farelocation=jReadConfig("farelocation");
		  
	 spid=jReadConfig("spid");
	 plzno=jReadConfig("plzno");
	 lane=jReadConfig("lane");

	 
	 readerip= jReadConfig("readerip");
	 readerport=Integer.valueOf(jReadConfig("readerport"));
	 
//    System.out.println(" =============== redis-antipass setup =============\n");
//      System.out.println("antipass_whitelist_en :"+jedis.get("antipass_whitelist_en")+"\n");
// 	  System.out.println("antipass_period_chck :"+jedis.get("antipass_period_chck")+"\n");
// 	  System.out.println("allowall :"+jedis.get("allowall")+"\n");
// 	  System.out.println("antipass_period :"+jedis.get("antipass_period")+"\n");
// 	  System.out.println("antipassbck_chck :"+jedis.get("antipassbck_chck")+"\n");
// 	     System.out.println(" =============== end =============\n");
//	        System.out.println("READER LOCATION :"+wie_sqlite.wieloc+"\n");
//	        System.out.println("ENABLE GPIO :"+pk_config.gpio_ent_en+"\n");
//	        System.out.println("ENABLE GPIO :"+pk_config.gpio_ext_en+"\n");
//	        System.out.println("gpio_wie0 :"+wie_var.gpio_wie0+"\n");
//	        System.out.println("gpio_wie1 : "+wie_var.gpio_wie1+"\n");
//	        System.out.println("gpio_wie2 :"+wie_var.gpio_wie2+"\n");
//	        System.out.println("gpio_wie3 : "+wie_var.gpio_wie3+"\n");
//	        System.out.println("gpio_loop1: "+pk_var.gpioAddr_loop1+"\n");
//	        System.out.println("gpio_loop2 : "+pk_var.gpioAddr_loop2+"\n");
//	        System.out.println("gpio_Entry ALB : "+pk_var.gpioAddr_ent_alb+"\n");
//	        System.out.println("gpio_Exit ALB: "+pk_var.gpioAddr_ext_alb+"\n");
//	        System.out.println("gpio_Entry BUZZ : "+pk_var.gpioAddr_ent_buzz+"\n");
//	        System.out.println("gpio_Exit BUZZ: "+pk_var.gpioAddr_ext_buzz+"\n");
//	        System.out.println("gpio_Entry AUTH : "+pk_var.gpioAddr_ent_auth+"\n");
//	        System.out.println("gpio_Exit AUTH: "+pk_var.gpioAddr_ext_auth+"\n");
//	        System.out.println("LOOP DETECTION ENABLE? : "+pk_config.lop_en+"\n");
//	        System.out.println(" =============== AMQP setup =============\n");
	        System.out.println("Server AMQP : "+ amqp_attr.amqp_ip+"\n");
	        System.out.println("Server AMQP port : "+ amqp_attr.amqp_port+"\n");
	        System.out.println("Server AMQP Qname_log : "+ amqp_attr.amqp_q+"\n");
	        System.out.println("Server AMQP Qname consume : "+ amqp_attr.amqp_q1+"\n");
	   
    	
    }
  
	   public static String jReadConfig(String configs)  {
		   
		   //---app name--- as SETTING FILE
		      CodeSource codeSource = pkMainTEST.class.getProtectionDomain().getCodeSource();
		       File jarFile;
			try {
				
				jarFile = new File(codeSource.getLocation().toURI().getPath());
				jarDir_name = jarFile.getName();
//			      System.out.print("<< CONFIG NAME: >> :"+jarDir_name+"\n");
			     
			} catch (URISyntaxException e1) {
				
				e1.printStackTrace();
			}
		   
		       //---------------
		   
		    Properties prop = new Properties();

		    
//		    String dirs = System.getProperty("user.dir");
	   
	 	    String fileName=jarDir_name+".config";

//		        final String os = System.getProperty("os.name");
	//
//		        if (os.contains("Windows"))
//		        {
//		        	fileName = "config.properties";
//		        	System.out.println("Detect Window!");
//		        }
        try {
		    InputStream is = new FileInputStream(fileName);

		    prop.load(is);

		
	
		    configss = prop.getProperty(configs);
        }catch(Exception c) {
        	c.printStackTrace();
        }
        
		    return configss;
		  }
}