package pk.mainsys;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.LoggerFactory;

import java.awt.Toolkit;















import redis.clients.jedis.Jedis;
import redis.drac.pkredis_var;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import netty.recon_entry.InboundClientHandler_ENTRY;
import nioSOCKreaderCoherent.nioCoher_ENT;
 

 
public class pktsk_Qsnd_ENT {
	Toolkit toolkit;
	Timer timer;
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;
	public static Queue<byte[]> fifobyt_TX = new LinkedList<byte[]>();
	
	static int byteRECVNoTailNoHeader;
	public static byte [] byt_sndscancard={0x09,(byte) 0xb5,0x00,0x00,0x00,0x00,0x00,0x00,(byte) 0xbc};
	public static byte [] byt_sndbuzzOK={0x04,(byte) 0x11,0x4d,0x58};
	public static byte [] byt_sndbuzzNOK={0x04,(byte) 0x11,0x52,0x47};
	
	//--coherent-----
	public static byte [] byt_ENABLE={0x02,0x00,0x08,0x10,0x01,0x18,0x10,0x08,0x12,0x30,0x25,0x3a,0x03};
	int  timeout = Integer.parseInt( pkvar.timeoutread);
	  public static org.slf4j.Logger log = LoggerFactory.getLogger(pktsk_Qsnd_ENT.class);
	public pktsk_Qsnd_ENT() throws Exception {
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		 log.info("--> Start init pktask_rxtx_ENT <-- ");
	
		  byte[] byt_EN_RDR = pkByteData.bytSND_EN_compiled();
//			
			pktsk_Qsnd_ENT.fifobyt_TX.add(byt_EN_RDR);
		 	pkByteData.SEQUENCE_PROC=1;
//			pk_task_rxtx.fifobyt_TX.add(byt_EN_RDR);
	
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 50); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
		  int tot_byt =0;
		  Long ts;
			 byte[] byte_send ;
//			 int dat ;
			 byte [] byte_read;
		  
		public void run()  {
			 ByteBuffer b ;

			
			  
		try {
			 String simulateloop1 = pkredis_var.get_cmd("simulateloop1");
			 boolean loop1 = false;
		   try {
			    loop1 = pkvar.pinLOOP1_state.isLow();
		   } catch(Exception c) {
			    loop1 = false;
//			   c.printStackTrace();
		   }
			
			
			if(!fifobyt_TX.isEmpty()) {		
			
			 while (! fifobyt_TX.isEmpty()) {
		    
				 
			 
				 byte_send = new byte [fifobyt_TX.peek().length];
				 byte_send = fifobyt_TX.poll();
	
//	             pkinit_rxtx.write(byte_send);
//				 InboundClientHandler_ENTRY.send(InboundClientHandler_ENTRY.channel_, byte_send);
				 
				 // for netty---
//				 InboundClientHandler_ENTRY.send(InboundClientHandler_ENTRY.channel_, byte_send);
				 //-----------
				 //--for nio----
				 nioCoher_ENT.fifobyt_TX.add(byte_send);
				 //-------------
	             ts = System.currentTimeMillis();
	            
	             try {
					  Thread.sleep(50);
//					  byte_read = new byte [pk_init_rxtx.dat_in.available()]; 
//					  tot_byt = pk_init_rxtx.dat_in.available();
//					  pk_init_rxtx.dat_in.read(byte_read);
				 } catch ( Exception  e) {
				
					e.printStackTrace();
				}
	          
			  		
		         }
			 

			} 

			  } catch (Exception e) {
				 e.printStackTrace();
				 
			 }
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}