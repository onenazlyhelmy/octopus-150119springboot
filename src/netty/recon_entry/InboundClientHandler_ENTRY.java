package netty.recon_entry;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.EventLoop;
import io.netty.channel.SimpleChannelInboundHandler;
import pk.mainsys.pktsk_Qrecv_ENT;


public class InboundClientHandler_ENTRY extends SimpleChannelInboundHandler {  
	
	
	   private mainClient_ENTRY client;  
	   private static ByteBuf bytesnd;
	   public static Channel channel_;
	   public InboundClientHandler_ENTRY(mainClient_ENTRY client) {  
	     this.client = client;  
	   }  
	   @Override  
	   public void channelInactive(ChannelHandlerContext ctx) throws Exception {  
	     final EventLoop eventLoop = ctx.channel().eventLoop();  
	     eventLoop.schedule(new Runnable() {  
	       @Override  
	       public void run() {  
	         client.createBootstrap(new Bootstrap(), eventLoop);  
	       }  
	     }, 1L, TimeUnit.SECONDS);  
	     super.channelInactive(ctx);  
	   }
	   
	    @Override
	    public void channelActive(ChannelHandlerContext ctx) {
	    	channel_ = ctx.channel();

	     
//	        doConnect( inboundChannel);

	        
	    }
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {

//		   ByteBuf buf = (ByteBuf) msg;
		   
		           ByteBuf buf =  (ByteBuf) msg;
				   byte[] recv_bytes = new byte[buf.readableBytes()];
				   int readerIndex = buf.readerIndex();
				   buf.getBytes(readerIndex, recv_bytes);
		   
		   pktsk_Qrecv_ENT.fifobyt_RX.add(recv_bytes);
		
	   }  
	    @Override
	    public void channelReadComplete(ChannelHandlerContext ctx) {
	       ctx.flush();
	    }
	
	    @Override
	    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
	   
	        cause.printStackTrace();
	        ctx.close();
	    }
	    
	    public static void send( Channel ctx,byte[] msg)  {
	    	
//	    	byte [] babi = {(byte) 0xff,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x2a};
	    	bytesnd = Unpooled.wrappedBuffer(msg);
	    	
//	    	byte [] babi = {(byte) 0xff,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x13,0x00,0x24,0x2a};
//	    	firstMessage = Unpooled.wrappedBuffer(babi);
	    	
	  
			try {
			
				if( msg.length>0 ) {
				  
			      ctx.writeAndFlush(bytesnd);

				} else {
	//				log.error( "[Svr] Can't send message to inactive connection");
					throw new IOException( "[Svr] Can't send message to inactive connection");
					
				}
			} catch(Exception c) {
				c.printStackTrace();
			}
	    }
	    

	    
	 }  