package netty.recon_exit;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import pk.mainsys.pkconfig;

public class mainClient_EXIT  
{  
  private EventLoopGroup loop = new NioEventLoopGroup();  
  public static void main( String[] args )
  
  {  
    new mainClient_EXIT().run();  
    InboundClientHandler_EXIT.send(InboundClientHandler_EXIT.channel_, new byte[] {1,2});
  }  
  
  
  public Bootstrap createBootstrap(Bootstrap bootstrap, EventLoopGroup eventLoop) {  
    if (bootstrap != null) {  
      final InboundClientHandler_EXIT handler = new InboundClientHandler_EXIT(this);  
      bootstrap.group(eventLoop);  
      bootstrap.channel(NioSocketChannel.class);  
      bootstrap.option(ChannelOption.SO_KEEPALIVE, true);  
      bootstrap.handler(new ChannelInitializer<SocketChannel>() {  
        @Override  
        protected void initChannel(SocketChannel socketChannel) throws Exception {  
          socketChannel.pipeline().addLast(handler);  
        }  
      });  
      System.out.println("|| === netty.io connect  === :"+pkconfig.readerip+", port : "+pkconfig.readerport+", readerpath : "+pkconfig.readerpath);
      bootstrap.remoteAddress( pkconfig.readerip, pkconfig.readerport);
      bootstrap.connect().addListener(new ConnListener(this)); 
    }  
    return bootstrap;  
  }  
  public void run() {  
    createBootstrap(new Bootstrap(), loop);
  }  
} 
