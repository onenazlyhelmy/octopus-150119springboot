package netty.recon_exit;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoop;

public class ConnListener implements ChannelFutureListener {  
	   private mainClient_EXIT client;  
	   public ConnListener(mainClient_EXIT client) {  
	     this.client = client;  
	   }  
	   @Override  
	   public void operationComplete(ChannelFuture channelFuture) throws Exception {  
	     if (!channelFuture.isSuccess()) {  
	       System.out.println("||---Reconnect----->> EXIT --");  
	       final EventLoop loop = channelFuture.channel().eventLoop();  
	       loop.schedule(new Runnable() {  
	         @Override  
	         public void run() {  
	           client.createBootstrap(new Bootstrap(), loop);  
	         }  
	       }, 1L, TimeUnit.SECONDS);  
	     }  
	   }  
	 }  