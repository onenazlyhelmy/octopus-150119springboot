package farecsc;

public class faredata {
//	 res = "{\"PlazaNo\" : \""+tmpstr[0]+"\",\"AreaCode\" : \""+tmpstr[1]+"\",\"LaneNo\" : \""+tmpstr[1]
//	    		+"\",\"TransactionNo\" : \""+tmpstr[1]+"\",\"ParkingFare\" : \""+tmpstr[1]+"\",\"DurationDay\" : \""+tmpstr[1]+"\"}";
    
	String PlazaNo;
	String AreaCode;
	String LaneNo;
	String TransactionNo;
	String ParkingFare;
	String DurationDay;
	
	public faredata(String PlazaNo,String AreaCode,String LaneNo,String TransactionNo,String ParkingFare,String DurationDay) {
		this.PlazaNo = PlazaNo;
		this.AreaCode=AreaCode;
		this.LaneNo = LaneNo;
		this.TransactionNo = TransactionNo;
		this.ParkingFare = ParkingFare;
		this.DurationDay = DurationDay;
	
	}
	
	public String getPlazaNo() {
		return PlazaNo;
	}
	public void setPlazaNo(String plazaNo) {
		PlazaNo = plazaNo;
	}
	public String getAreaCode() {
		return AreaCode;
	}
	public void setAreaCode(String areaCode) {
		AreaCode = areaCode;
	}
	public String getLaneNo() {
		return LaneNo;
	}
	public void setLaneNo(String laneNo) {
		LaneNo = laneNo;
	}
	public String getTransactionNo() {
		return TransactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		TransactionNo = transactionNo;
	}
	public String getParkingFare() {
		return ParkingFare;
	}
	public void setParkingFare(String parkingFare) {
		ParkingFare = parkingFare;
	}
	public String getDurationDay() {
		return DurationDay;
	}
	public void setDurationDay(String durationDay) {
		DurationDay = durationDay;
	}
}
