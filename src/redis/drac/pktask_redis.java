package redis.drac;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Toolkit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import pk.mainsys.MainXXXXX;
import pk.mainsys.pkrasp_io;
import pk.mainsys.pktsk_Qsnd_EXT;
import pk.mainsys.pkvar;
 

 
public class pktask_redis {
	Toolkit toolkit;
	Timer timer;

	
//	public int rst_time;
//	public int appclrtmrlog;
 //	static Class raspiPin = RaspiPin.class;

	static byte byte_wr[] = {0x02,0x00,0x08,0x10,0x01,0x18,0x10,0x08,0x12,0x30,0x25,0x3a,0x03};
 
	public pktask_redis() throws Exception {
//		final jRFID_config jRFID_config = new jRFID_config();
		
//		rst_time = Integer.parseInt(jRFID_config.jReadConfig("app.sys.rst"));
//		appclrtmrlog = Integer.parseInt(jRFID_config.jReadConfig("app.clrtmr.log"));
		
	
	       
	      
		pkredis_var.set_cmd("forcealb","0");
		pkredis_var.set_cmd("simulateloop1","0");
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new tmrReminder(), 0, // initial delay
				1 * 500); // subsequent rate
	}
 
	class tmrReminder extends TimerTask {
	
        
 
		public void run()  {
			
							
			try {
			
				Long sendbyte_tot = pkredis_var.llen_cmd(pkredis_var.lpopkeysendbyte);
				if(sendbyte_tot>0) {
					String str =pkredis_var.lpop_cmd(pkredis_var.lpopkeysendbyte);
					byte[] b =  pkvar.String_byteArrayfull(str);
					
					pktsk_Qsnd_EXT.fifobyt_TX.add(b);
				}
				pkredis_var.set_cmd("statloop1", pkredis_var.statloop1);
				pkredis_var.set_cmd("statloop2", pkredis_var.statloop2);
				pkredis_var.set_cmd("statalb", pkredis_var.statalb);
				pkredis_var.set_cmd("hbeat",String.valueOf(pkredis_var.hbeat));
			String forcealb = 	pkredis_var.get_cmd("forcealb");
				if(forcealb.equals("1")) {
				   	pkredis_var.set_cmd("forcealb","0");
				   	
				   	if(MainXXXXX.ostype.equals("arm")) {
				   	   pkrasp_io.gpio_trigAUTH_ALB(1);
				   	}
					
					
				}
				if(pkredis_var.hbeat>10) {
					pkredis_var.hbeat=0;	
				}
				pkredis_var.hbeat++;
			} catch (Exception  e) {
			
				e.printStackTrace();
			}
		
		
			
		}
	}
// 
//	public static void main(String args[]) throws Exception {
//		new jRFID_tmr();
//		//System.out.format("Task scheduled..!%n \n");
//	}
}