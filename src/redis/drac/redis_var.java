package redis.drac;
import java.util.Arrays;
import java.util.Map;

import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.codec.Utf8StringCodec;
import io.lettuce.core.masterslave.MasterSlave;
import io.lettuce.core.masterslave.StatefulRedisMasterSlaveConnection;
import io.lettuce.core.pubsub.RedisPubSubAdapter;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.async.RedisPubSubAsyncCommands;
import io.lettuce.core.support.ConnectionPoolSupport;
import pk.mainsys.MainXXXXX;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class redis_var {
	
	public static RedisClient redcon;
	public static RedisClusterClient clusterClient ;
//	public static  RedisCommands<String, String> redcon_cmd ;
	public static  StatefulRedisClusterConnection<String, String> redcon_cmd; //--for cluster
	public static  RedisCommands<String, String> red_cmd ; //--not  cluster
	public static GenericObjectPool<StatefulRedisClusterConnection<String, String>> pool ;
	public static StatefulRedisPubSubConnection<String, String> pubConn;
	public static  StatefulRedisMasterSlaveConnection<String, String> connection = null;
	static String url = "redis://localhost:6379";
	
//	public static RedisPubSubAsyncCommands<String, String> async;

	public static Logger log = LoggerFactory.getLogger(redis_var.class);
	public static void rediscluster_init() {
//		redcon =  RedisClient.create("redis://10.222.50.127:7000");
//		StatefulRedisConnection<String, String> connection = redcon.connect();
//		redcon_cmd = connection.sync();  
//		return commands;
		
		 
		String url = "redis://localhost:6379";
		System.out.println("try connect redis : "+url);
		try {
			RedisURI node1 = RedisURI.create(url);
//			RedisURI node2 = RedisURI.create("redis://10.222.50.127:7000");
//			clusterClient = RedisClusterClient.create(Arrays.asList(node1,node2));
		clusterClient = RedisClusterClient.create(Arrays.asList(node1));

	    pool = ConnectionPoolSupport
		               .createGenericObjectPool(() -> clusterClient.connect(), new GenericObjectPoolConfig());
		
		redcon_cmd = pool.borrowObject();
	
		}catch (Exception x) {
//			x.printStackTrace();
		}
//		try ((redcon_cmd = pool.borrowObject() {

//		}
	}
	public static void redisconn_sentinel_init(String redisip) {
//		final Level VERBOSE = Level.toLevel("VERBOSE", 550);
		//--example
//		StatefulRedisMasterSlaveConnection<String, String> connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),
//		        RedisURI.create("redis-sentinel://localhost:26379,localhost:26380/0#mymaster"));
//		connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
		//--------------------------------
		System.out.println("--> init redisconn_sentinel_init cluster : ' "+redisip+"'<-- ");
		connection = null;
		try {
	    redcon = RedisClient.create();

		connection = MasterSlave.connect(redcon, new Utf8StringCodec(),
		        RedisURI.create(redisip));
		connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
		
		
		red_cmd = connection.sync();  
//		Map<String, String> value = commands.hgetall("mfgno_rfidno");
		
//		String val = red_commands.get("java-key-999");
		
//    	System.out.println("test lectuce :" +Arrays.asList(value));
//		System.out.println("get key  :" +val);


		} catch (Exception x) {
			x.printStackTrace();
			connection.close();
			redcon.shutdown();
		} finally {
		log.info(" -- Success Connected to Redis : "+redisip);
		
//			try {
//				 new pktskPOLL();//--startpoll
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}
		}

		
	}
	public static void redisINITCONN_cmd(String redisip){
		try {
//		String url = "redis://"+amqp_attr.redis_ip+":"+ amqp_attr.redis_port;
		log.info("**************** connect redis : "+redisip+" *****************");
		redcon =  RedisClient.create(redisip);
		StatefulRedisConnection<String, String> connection = redcon.connect();

		pubConn = redcon.connectPubSub();//--instance for pub
		red_cmd = connection.sync();  
	
		} catch(Exception ff){
			ff.printStackTrace();
		}
//		return commands;
	}
	
	public static void closecluster(){
		pool.close();
		clusterClient.shutdown();	
	}
	
//	public static void redisINITCONN_cmd(){
//		redcon =  RedisClient.create(url);
//		StatefulRedisConnection<String, String> connection = redcon.connect();
//		redcon_cmd = connection.sync();  
//		new redistmr();//-------------------------------------------------------------tmr
//		System.out.println("--- connect to : "+url);
////		return commands;
//	}
	
	
	
	// Subscribe channel 
	public static void redis_sub(String chQue) {
//		ISOconfig.chQue = ISOconfig.readconfig("chQue");
//		System.out.println("subscribe!");
		log.info("********* redis - subscribe - "+chQue+" **********");
		
		try {
		RedisPubSubAdapter<String, String> listener = new RedisPubSubAdapter<String, String>() {
			@Override
			public void message(String channel, String message) {
				System.out.println(String.format("RECV-SUB  <> Channel <> %s, Message: %s", channel, message));
				subcri_filter(channel,message);
			}
	};
		//	pubsubConn = redcon.connectPubSub();//--init pubsub--
			StatefulRedisPubSubConnection<String, String> subConn = redcon.connectPubSub();//--instance for sub
			subConn.addListener(listener);
			RedisPubSubAsyncCommands<String, String> async = subConn.async();
			async.subscribe(chQue);	
		}catch (Exception x) {
			x.printStackTrace();
		}
	}

	// PUBLISH channel message
	public static void publish(String ch,String msg) {
		try {
//			StatefulRedisPubSubConnection<String, String> pubsubConn = redcon.connectPubSub();
			pubConn.async().publish(ch, msg);
//			async.publish(ch, msg);
//			System.out.println("publish "+channel+" Hello"+idx);
//			idx++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void subcri_filter(String ch,String msg) {
		
		
	 try {
		//respon success process msg to clients
		 if(ch.equals("evt_client_reply")) {
			   String[] tmpmsg = msg.split(",");
			   String chnid = tmpmsg[0];
			   String hostip = tmpmsg[1];
			   String isodata = tmpmsg[2];
		
//			   ISOSVRhandler.send(chnid, hostip,isodata);
			}
		 
		 if(ch.equals("evt_client_senddata")) {
				
			}
	 }	catch(Exception c) {
		 c.printStackTrace();
	 }
		
		
	}

    public static void testing(String cmd){
//    	RedisClient redcon = RedisClient.create("redis://localhost");
//    	RedisClient client = RedisClient.create(RedisURI.create("localhost", 6379));
//    	StatefulRedisConnection<String, String> connection = redcon.connect();
//    	RedisCommands<String, String> commands = redcon_cmd.sync();  
    	String data = redcon_cmd.sync().hget("drac", cmd);
//		redcon_cmd.sync().blpop(10, "list");
//    	RedisStringCommands commands = connection.sync();
//    	String value = (String) commands.hgetall("mfgno_rfidno");
//    	Map<String, String> value = redcon_cmd.(cmd);
//    	System.out.println("test lectuce :" +Arrays.asList(value));
    	System.out.println("data : "+ data);
    	
//    	System.exit(0);
    }
    
    public static Map<String, String> reds_hgetall_cmd(String cmd){
		  
    	Map<String, String> value = red_cmd.hgetall(cmd);
    	return value;
  }

  public static String lpop_cmd(String cmd){
	  
    	String value = red_cmd.lpop(cmd);
    	return value;
  }
  
  public static String get_cmd(String cmd){
	  
    	String value = red_cmd.get(cmd);
    	return value;
//	  return "1";
  }
  
  public static String set_cmd(String key,String value){
	  
    	String val = red_cmd.set(key,value);
    	return val;
  }
  
  public static Long lpush_cmd(String key,String val){
	  
    	Long value = red_cmd.lpush(key,val);
    	return value;
  }
  
  public static Long llen_cmd(String cmd){
	  
    	Long value = red_cmd.llen(cmd);
    	return value;
  }
  
  public static Long del_cmd(String cmd){
	  
    	Long value = red_cmd.del(cmd);
    	return value;
//        return (long) 0;
  }
  
  public static Boolean hset_cmd(String hash,String key,String val){
	  
    	Boolean value = red_cmd.hset(hash, key, val);
    	return value;
  }
  
  public static String hget_cmd(String hash,String key){
	  
	    String value = red_cmd.hget(hash, key);
    	return value;
  }
  
  public static Long hdel_cmd(String hash,String key,String val){
	  
    	Long value = red_cmd.hdel(hash, key, val);
    	return value;
  }
    
	 
    //--cluster command
	  public static Map<String, String> redclus_hgetall_cmd(String cmd){
		  
	    	Map<String, String> value = redcon_cmd.sync().hgetall(cmd);
	    	return value;
	  }
	
	  public static String redclus_lpop_cmd(String cmd){
		  
	    	String value = redcon_cmd.sync().lpop(cmd);
	    	return value;
	  }
	  
	  public static Long redclus_lpush_cmd(String key,String val){
		  
	    	Long value = redcon_cmd.sync().lpush(key,val);
	    	return value;
	  }
	  
	  public static Long redclus_llen_cmd(String cmd){
		  
	    	Long value = redcon_cmd.sync().llen(cmd);
	    	return value;
	  }
	  
	  public static Boolean redclus_hset_cmd(String hash,String key,String val){
		  
	    	Boolean value = redcon_cmd.sync().hset(hash, key, val);
	    	return value;
	  }
	  
	  public static String redclus_hget_cmd(String hash,String key){
		  
		    String value = redcon_cmd.sync().hget(hash, key);
	    	return value;
	  }
	  
	  public static Long redclus_hdel_cmd(String hash,String key,String val){
		  
	    	Long value = redcon_cmd.sync().hdel(hash, key, val);
	    	return value;
	  }
}
