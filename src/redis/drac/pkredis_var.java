package redis.drac;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.lettuce.core.ReadFrom;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.codec.Utf8StringCodec;
import io.lettuce.core.masterslave.MasterSlave;
import io.lettuce.core.masterslave.StatefulRedisMasterSlaveConnection;
import io.lettuce.core.pubsub.RedisPubSubAdapter;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.async.RedisPubSubAsyncCommands;
import io.lettuce.core.support.ConnectionPoolSupport;
import pk.mainsys.MainXXXXX;


public class pkredis_var {
	public static RedisClient redcon;
	public static RedisClient redis_client;
//	public static RedisClusterClient clusterClient ;
//	public static  RedisCommands<String, String> redcon_cmd ;
//	public static StatefulRedisClusterConnection<String, String> redcon_cmd;
	public static GenericObjectPool<StatefulRedisClusterConnection<String, String>> pool ;
	public static  StatefulRedisMasterSlaveConnection<String, String> connection = null;
	static StatefulRedisConnection<String, String> connection_redis;
//	public static  RedisCommands<String, String> red_cmd ; //--not  cluster
	public static RedisCommands<String, String> red_cmd;
	public static String redisip="redis-sentinel://127.0.0.1:26379/0#redis-cluster";
	public static String lpopkeysendbyte="sendbyte";
	public static StatefulRedisPubSubConnection<String, String> pubConn;
	public static Logger log = LoggerFactory.getLogger(pkredis_var.class);
	public static int hbeat=0;
	public static String statloop1="0",statloop2="0",statalb="0",forcealb="0";
//	public static url_sentinel="redis-sentinel://10.222.104.113:16380,10.222.104.105:16381,10.222.104.105:16382/0#redis-cluster";

	
//	public static void redisconnect() {
//		// Syntax: redis://[password@]host[:port][/databaseNumber]
//        RedisClient redisClient = RedisClient.create();
//        List<RedisURI> nodes = Arrays.asList(RedisURI.create("redis-sentinel://10.222.104.113:16380#redis-cluster"),
//                RedisURI.create("redis-sentinel://10.222.104.105:16381#redis-cluster"),
//                RedisURI.create("redis-sentinel://10.222.104.105:16382#redis-cluster"));
//        StatefulRedisMasterSlaveConnection<String, String> connection = MasterSlave
//                .connect(redisClient, new Utf8StringCodec(), nodes);
//        connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
//        System.out.println("Connected to Redis");
//	}
	
	//--pubsub---------
//	RedisURI redisUri = RedisURI.create("redis-sentinel://localhost:26379,localhost:26380/0#mymaster");
//	RedisClient client = RedisClient.create(redisUri);
//
//	RedisPubSubConnection<String, String> connection = client.connectPubSub();
	public static void redisINITCONN_cmd(String redisip){
		try {
//		String url = "redis://"+amqp_attr.redis_ip+":"+ amqp_attr.redis_port;
		log.info("**************** connect redis : "+redisip+" *****************");
		redcon =  RedisClient.create(redisip);
		StatefulRedisConnection<String, String> connection = redcon.connect();

//		pubConn = redcon.connectPubSub();//--instance for pub
		red_cmd = connection.sync();  
//		connection_redis = redis_client.connect();
		log.info(" -- Success Connected to Redis non-sentinel : "+redisip);
		MainXXXXX.start();
		set_cmd("vers", MainXXXXX.vers);
		
		
		} catch(Exception ff){
			ff.printStackTrace();
		}
//		return commands;
	}
	//-----------------
	public static void redis_initconn(String redisip,int port){
		try {
//		String url = "redis://"+amqp_attr.redis_ip+":"+ amqp_attr.redis_port;
	
		String url = "redis://"+redisip+":"+ port;
		log.info("**************** connect redis : "+url+" *****************");
//		int port = Integer.valueOf(wie_config.redis_port);
		
		RedisURI uris = RedisURI
                .builder()
                .withHost(redisip)
                .withPort(port)
//                .withPassword(password)
                
//                .withClientName(clientName)
                .withTimeout(Duration.ofSeconds(30))
                .build();
		
		redis_client =  RedisClient.create(uris);
//		redis_client_pubsub =  RedisClient.create(uris);
//		StatefulRedisConnection<String, String>;;	redis_client =  RedisClient.create(uris);
//		redis_client_pubsubSOP =  RedisClient.create(uris);
		connection_redis = redis_client.connect();
		
	

//		pubConn = redis_client.connectPubSub();//--instance for pub
		
		red_cmd = connection_redis.sync();  //= connection.async();
//		red_cmd_async = connection_redis.async();  //= connection.async();
	
		} catch(Exception ff){
			ff.printStackTrace();
		}
//		return commands;
	}
	public static void redisconn_sentinel_init(String redisip) {
//		final Level VERBOSE = Level.toLevel("VERBOSE", 550);
		//--example
//		StatefulRedisMasterSlaveConnection<String, String> connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),
//		        RedisURI.create("redis-sentinel://localhost:26379,localhost:26380/0#mymaster"));
//		connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
		//--------------------------------
		System.out.println("--> init redisconn_sentinel_init cluster : ' "+redisip+"'<-- ");
		connection = null;
		try {
	    redcon = RedisClient.create();

		connection = MasterSlave.connect(redcon, new Utf8StringCodec(),
		        RedisURI.create(redisip));
		connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
		
		
		red_cmd = connection.sync();  
//		Map<String, String> value = commands.hgetall("mfgno_rfidno");
		
//		String val = red_commands.get("java-key-999");
		
//    	System.out.println("test lectuce :" +Arrays.asList(value));
//		System.out.println("get key  :" +val);


		} catch (Exception x) {
			x.printStackTrace();
			connection.close();
			redcon.shutdown();
		} finally {
		log.info(" -- Success Connected to Redis : "+redisip);
		MainXXXXX.start();
		set_cmd("vers", MainXXXXX.vers);
//			try {
//				 new pktskPOLL();//--startpoll
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}
		}

		
	}
	public static void redisconn_sentinel_init(String redisip,String typecon) {
//		final Level VERBOSE = Level.toLevel("VERBOSE", 550);
		//--example
//		StatefulRedisMasterSlaveConnection<String, String> connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),
//		        RedisURI.create("redis-sentinel://localhost:26379,localhost:26380/0#mymaster"));
//		connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
		//--------------------------------
		System.out.println("-- "+typecon+" -- init redisconn_sentinel_init cluster : ' "+redisip+"'<-- ");
		connection = null;
		try {
	    redcon = RedisClient.create();

		connection = MasterSlave.connect(redcon, new Utf8StringCodec(),
		        RedisURI.create(redisip));
		connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
		
		
		red_cmd = connection.sync();  
//		Map<String, String> value = commands.hgetall("mfgno_rfidno");
		
//		String val = red_commands.get("java-key-999");
		
//    	System.out.println("test lectuce :" +Arrays.asList(value));
//		System.out.println("get key  :" +val);


		} catch (Exception x) {
			x.printStackTrace();
			connection.close();
			redcon.shutdown();
		} finally {
		log.info(" -- "+typecon+" -- Success Connected to Redis : "+redisip);
	
//			try {
//				 new pktskPOLL();//--startpoll
//			} catch (Exception e) {
//				
//				e.printStackTrace();
//			}
		}

		
	}
	// Subscribe channel 
	public static void redis_sub(String chQue) {
//		ISOconfig.chQue = ISOconfig.readconfig("chQue");
//		System.out.println("subscribe!");
		log.info("********* redis - subscribe - "+chQue+" **********");
		
		try {
		RedisPubSubAdapter<String, String> listener = new RedisPubSubAdapter<String, String>() {
			@Override
			public void message(String channel, String message) {
				System.out.println(String.format("RECV-SUB  <> Channel <> %s, Message: %s", channel, message));
//				subcri_filter(channel,message);
			}
	};
		//	pubsubConn = redcon.connectPubSub();//--init pubsub--
			StatefulRedisPubSubConnection<String, String> subConn = redcon.connectPubSub();//--instance for sub
			subConn.addListener(listener);
			RedisPubSubAsyncCommands<String, String> async = subConn.async();
			async.subscribe(chQue);	
		}catch (Exception x) {
			x.printStackTrace();
		}
	}

	// PUBLISH channel message
	public static void publish(String ch,String msg) {
		try {
//			StatefulRedisPubSubConnection<String, String> pubsubConn = redcon.connectPubSub();
			pubConn.async().publish(ch, msg);
//			async.publish(ch, msg);
//			System.out.println("publish "+channel+" Hello"+idx);
//			idx++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	  public static Map<String, String> reds_hgetall_cmd(String cmd){
		  
	    	Map<String, String> value = red_cmd.hgetall(cmd);
	    	return value;
	  }
	
	  public static String lpop_cmd(String cmd){
		  
	    	String value = red_cmd.lpop(cmd);
	    	return value;
	  }
	  
	  public static String get_cmd(String cmd){
		  
	    	String value = red_cmd.get(cmd);
	    	return value;
//		  return "1";
	  }
	  
	  public static String set_cmd(String key,String value){
		  
	    	String val = red_cmd.set(key,value);
	    	return val;
	  }
	  
	  public static Long lpush_cmd(String key,String val){
		  
	    	Long value = red_cmd.lpush(key,val);
	    	return value;
	  }
	  
	  public static Long llen_cmd(String cmd){
		  
	    	Long value = red_cmd.llen(cmd);
	    	return value;
	  }
	  
	  public static Long del_cmd(String cmd){
		  
	    	Long value = red_cmd.del(cmd);
	    	return value;
//            return (long) 0;
	  }
	  
	  public static Boolean hset_cmd(String hash,String key,String val){
		  
	    	Boolean value = red_cmd.hset(hash, key, val);
	    	return value;
	  }
	  
	  public static String hget_cmd(String hash,String key){
		  
		    String value = red_cmd.hget(hash, key);
	    	return value;
	  }
	  
	  public static Long hdel_cmd(String hash,String key,String val){
		  
	    	Long value = red_cmd.hdel(hash, key, val);
	    	return value;
	  }
	public static void hshget() {
		String val = red_cmd.get("java-key-999");
		System.out.println("get key  :" +val);
	}
	
//	public static void redisconn_commands() {
////		redcon =  RedisClient.create("redis://10.222.50.127:7000");
////		StatefulRedisConnection<String, String> connection = redcon.connect();
////		redcon_cmd = connection.sync();  
////		return commands;
//		try {
//			RedisURI node1 = RedisURI.create("redis://10.222.50.241:7001");
//			RedisURI node2 = RedisURI.create("redis://10.222.50.127:7000");
//	
//		clusterClient = RedisClusterClient.create(Arrays.asList(node1, node2));
//
//	    pool = ConnectionPoolSupport
//		               .createGenericObjectPool(() -> clusterClient.connect(), new GenericObjectPoolConfig());
//		
//		redcon_cmd = pool.borrowObject();
//	
//		}catch (Exception x) {
////			x.printStackTrace();
//		}
////		try ((redcon_cmd = pool.borrowObject() {
//
////		}
//	}
	
//	public static void closecluster(){
//		pool.close();
//		clusterClient.shutdown();	
//	}
	
//    public static void testing(){
//    	RedisClient client = RedisClient.create("redis://10.222.50.127:7000");
////    	RedisClient client = RedisClient.create(RedisURI.create("localhost", 6379));
//    	StatefulRedisConnection<String, String> connection = client.connect();
//    	RedisCommands<String, String> commands = connection.sync();  
////    	RedisStringCommands commands = connection.sync();
////    	String value = (String) commands.hgetall("mfgno_rfidno");
//    	Map<String, String> value = commands.hgetall("mfgno_rfidno");
//    	System.out.println("test lectuce :" +Arrays.asList(value));
//    	
//    	System.exit(0);
//    }
    



}
