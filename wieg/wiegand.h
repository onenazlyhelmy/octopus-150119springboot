

extern void data0Pulse(void);
extern void data1Pulse(void);
extern int wiegandInit(int d0pin, int d1pin);
extern void wiegandReset();
extern int wiegandGetPendingBitCount();
extern int wiegandReadData(void* data, int dataMaxLen);
extern void printCharAsBinary(unsigned char ch);
extern int wieInit();

